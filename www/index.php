<?php 
require('../subdomains/b2b/wp-load.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Nordic Spirits - B2B</title>
	<link rel='stylesheet' id='buttons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/buttons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='dashicons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='login-css'  href='http://b2b.nordicspirits.dk/wp-admin/css/login.min.css?ver=4.4.2' type='text/css' media='all' />
  <style type="text/css">
  </style>
	</head>
<body class="wp-core-ui locale-da-dk">
<?php 
  $recent = new WP_Query("page_id=356"); 
  while ($recent->have_posts()) {
    $recent->the_post();
    the_content(); 
  }
  
  
 // Footer
 echo '
    <div style="text-align: center;">
    ';

  $recent = new WP_Query("page_id=518"); 
  while ($recent->have_posts()) {
    $recent->the_post();
    the_content(); 
  }
  
  echo '  </div>    
  ';
?>
</body>
</html>

