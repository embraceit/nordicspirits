<?php 
require('../../subdomains/b2b/wp-load.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Nordic Spirits &rsaquo; Oversigt</title>
  <?php wp_head(); ?>
  <style type="text/css">
    body {
      padding: 2em;
    }
  </style>
	</head>
<body class="wp-core-ui locale-da-dk">
<?php 
  $recent = new WP_Query("page_id=2165"); 
  while ($recent->have_posts()) {
    $recent->the_post();
    echo '<h1>';
    the_title();
    echo '</h1>'; 
    echo
    the_content(); 
  }  
?>
<?php wp_footer(); ?>
</body>
</html>

