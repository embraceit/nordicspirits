<?php
require('simplexlsx.class.php');

$pcode_to_city = array();
$pcode_to_city_arr = array_map('str_getcsv', file('2017-danske-postnumre-byer.csv'));
for ($i = 1; $i < count($pcode_to_city_arr); ++$i) {
  $pcode_to_city[$pcode_to_city_arr[$i][0]] = $pcode_to_city_arr[$i][1];
}
//print_r($pcode_to_city);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Nordic Spirits &rsaquo; Den Sorte Boks</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
      font-family: Georgia, 'Helvetica Neue', Times, 'Times New Roman', serif;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<h1>Forhandlere af Den Sorte Boks 2017</h1>
<img src="2017-densorteboks.png" width="750" />
<?php
if ( $xlsx = SimpleXLSX::parse('2017-densorteboks-forhandlerliste.xlsx') ) {
  $rows = $xlsx->rows();
  
  $phys_shops = array();
  $web_shops = array();
  
  for ($i = 1; $i < count($rows); ++$i) {
    $r = $rows[$i];
    
	  if ((stripos($r[3], 'web') !== false)) {
	    $web_shops[] = $r;
	    continue;
	  }
	  
	  $key = 'Særpostnummer';
	  $pcode = $r[2];
	   
	  if ($pcode >= 1000 && $pcode <= 4999) {
	    $key = 'Sjælland og øerne øst for Storebælt';
	  } else if ($pcode >= 5000 && $pcode <= 5999) {
	    $key = 'Fyn og øerne';
	  } else if ($pcode >= 6000 && $pcode <= 9999) {
	    $key = 'Jylland';
	  }  
	  
	  $phys_shops[$key][] = $r;
	}
  
  function cmp($a, $b) {
    if ($a[2] == $b[2]) {
      return 0;
    }
    
    return ($a[2] < $b[2]) ? -1 : 1;
  }
  
  foreach ($phys_shops as &$a) {
    usort($a, "cmp");  
  }
  usort($a, "cmp");
  ksort($phys_shops);
  
  //print_r($phys_shops);
  
  //////////////////////////////////////////////////
  // DISPLAY
  //////////////////////////////////////////////////

  echo "<h2>Fysiske butikker</h2>";
  
  echo '<table>';
		
	foreach($phys_shops as $regname => $rows) {
		//echo '<h3>' . $regname . '</h3>';
		echo '<tr><th colspan="3" style="background-color: #666666; color: #eeeeee;">' . $regname . '</th></tr>';
		echo '<tr><th>Navn</th><th>Postnr. og by</th><th></th></tr>';
		
	  foreach($rows as $r) {	 
	    $www = $r[4];	    
	    if (substr($www, 0, 4) != 'http') {
	      $www = 'http://' . $www;
	    }
	       
		  echo '<tr><td><a href="' . $www  . '">' . $r[1] . '</a></td><td>' . $r[2] . ' ' . $pcode_to_city[$r[2]] . '</td><td><a href="https://maps.google.com/?q=' . urlencode($r[1] . ', ' . $r[2] . ' ' . $pcode_to_city[$r[2]]) . '">Kort</a></td></tr>';
	  }
	}	  
  echo '</table>';
  
  echo "<h2>Web-butikker</h2>";
  echo '<table>';
	foreach($web_shops as $r ) {
	  if ((stripos($r[3], 'web') === false)) {
	    continue;
	  }
		echo '<tr><td><a href="http://' . $r[4] . '">' . $r[1] . '</a></td></tr>';
	}
	echo '</table>';  
	
} else {
	echo SimpleXLSX::parse_error();
}
?>
</body>
</html>

