<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nordicspirits_dk_db');

/** MySQL database username */
define('DB_USER', 'nordicspir');

/** MySQL database password */
define('DB_PASSWORD', '_AL3mSBx');

/** MySQL hostname */
define('DB_HOST', 'mysql12.123hotel.dk');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hIT|4S^x7K1j$]r<~DyVeA~_Y3J@zF|;U)<Jt&o)c v+Cu|P[EmfOmpEx2{dPj+F');
define('SECURE_AUTH_KEY',  'R@c5M/#e-Y(E&Gy,b/VWTXbQ-p%Ou4:@Ku(Od&fI+8f)$<}p&VSe#tABA>v-bwwx');
define('LOGGED_IN_KEY',    'Mc,1>w]_u@wun3WT:P4iQ!<K?*yVvG_Yb,Z/OQixse3^:!QPPn+&4<W7wDwx6D%c');
define('NONCE_KEY',        'xnLVvHx,Re$Q%+8yD0pOT2aA9a*,<@_yl-Ym=>3uww?d#YJAgQ@rga*TgHA)4|ac');
define('AUTH_SALT',        '#o_hY_kYXJF{EMF*x{c|(-8;lWBPt9e-=FkbVs*@BT C#kuSL2-[!ue=Cqv&C{9`');
define('SECURE_AUTH_SALT', ',oRUxM(6O6;dVX2!.ul;n7EzO<06lQ3j-Xu0s >9,|o%q*7/;RNo6k9`!?2N_0U}');
define('LOGGED_IN_SALT',   '[R}9D)$-1-Ubf|S&%+KD@lAa+s-ZMNNC[]J@|p~%wkY0ah53Q*3)/Mh[2m,m1j|H');
define('NONCE_SALT',       '+h7e1*<J1p6NTNY(-5;arXFkS[Y+m>;Dd-im)SFhjfC^7p*Stfv6MMT;+52<Nm$5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'b2b_wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define('WP_DEBUG', false);
//define( 'WP_DEBUG', true );

//https://aristath.github.io/blog/wp-hide-php-errors
// Production
ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
//define('WP_DEBUG', true);
//define('WP_DEBUG_LOG', true);
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);


// DEBUG -- DO NOT ENABLE BEFORE COORDINATING WITH Morten Krogh Lorentzen <mail@mortenkrogh.net>
/*
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
//define('WP_DEBUG_DISPLAY', true);
define('WP_DEBUG_DISPLAY', false);
@ini_set( 'display_errors', 0 );
define( 'SCRIPT_DEBUG', true );
ini_set('log_errors','On');
ini_set('display_errors','On');
ini_set('error_reporting', E_ALL );
*/

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
