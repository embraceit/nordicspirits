<?php
session_start(); // to keep tokenlogin happy

if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../wp-load.php');
}

?>
<!DOCTYPE html>
<!--[if IE 8]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" lang="da-DK">
<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Ny forhandler</title>
	<link rel='stylesheet' id='buttons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/buttons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='dashicons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='login-css'  href='http://b2b.nordicspirits.dk/wp-admin/css/login.min.css?ver=4.4.2' type='text/css' media='all' />
  <style type="text/css">
		h1 a { 
		  background-image: none !important; 
		}
		#login h1, #login h2 {
		  margin-bottom: 1em;
		}
		#login {
		  width: 500px;
		  padding-top: 2em;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
  <script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
	<div id="login">

<h1>Nordic Spirits</h1>
<h1>Ansøg om oprettelse som forhandler</h1>

<form name="loginform" id="loginform" action="./" method="post">


<?php
/*
* username = email = billing_email
* [password]
* user_url
* display_name
* billing_company
* billing_first_name = first_name
* billing_last_name = last_name
* billing_phone
* billing_cvr
* billing_address_1
* billing_address_2
* billing_postcode
* billing_city
* billing_country
*/

$DISPLAY_FIELDS = TRUE;

//print_r($POST);

if (isset($_POST) && 
  isset($_POST['user_company']) &&
  isset($_POST['user_cvr']) &&
  isset($_POST['user_adr1']) &&
  isset($_POST['user_adr2']) &&
  isset($_POST['user_postcode']) &&
  isset($_POST['user_city']) &&
  isset($_POST['user_firstname']) &&
  isset($_POST['user_lastname']) &&
  isset($_POST['user_email']) &&
  isset($_POST['billing_invoiceemail']) &&
  isset($_POST['user_phone'])) {
  
  $errors = array();
  
  // captcha  
  if (!isset($_POST['g-recaptcha-response'])) {
    $errors[] = 'Venligst klik "Jeg er ikke en robot" og løs udfordringen';
  } else {
    $secret = "6LcKlxwTAAAAAIIp4h3mvTjTOYa7ML2KyOTjrII9";
    $response = $_POST['g-recaptcha-response'];
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response);
    $responseData = json_decode($verifyResponse);
    
    if (!($responseData->success === TRUE)) {
      $errors[] = '"Jeg er ikke en robot"-udfordringen er ikke løst; prøv venligst igen';
    }
  }
  
  // 1st level
  if (strlen($_POST['user_company']) < 2) {
    $errors[] = 'Firma ikke udfyldt';
  }
  
  if (strlen($_POST['user_cvr']) < 8) {
    $errors[] = 'CVR ikke korrekt udfyldt';
  }
  
  if (strlen($_POST['user_adr1']) < 2) {
    $errors[] = 'Adresse 1 ikke udfyldt';
  }
  
  // user_adr2 no requirements
  
  if (strlen($_POST['user_postcode']) < 3) {
    $errors[] = 'Postnr. ikke korrekt udfyldt';
  }
  
  if (strlen($_POST['user_city']) < 2) {
    $errors[] = 'By ikke udfyldt';
  }
  
  
  
  if (strlen($_POST['user_firstname']) < 2) {
    $errors[] = 'Fornavn ikke udfyldt';
  }
  
  if (strlen($_POST['user_lastname']) < 2) {
    $errors[] = 'Efternavn ikke udfyldt';
  }
  
  if (strlen($_POST['user_email']) < 2) {
    $errors[] = 'Kontakt e-mail ikke udfyldt';
  }
  
  if (strlen($_POST['billing_invoiceemail']) < 2) {
    $errors[] = 'Faktura e-mail ikke udfyldt';
  }
  
  if (strlen($_POST['user_phone']) < 2) {
    $errors[] = 'Mobilnummer ikke udfyldt';
  }
  
  // 2nd level
  require('../wp-load.php');
  
  $email = $_POST['user_email'];
  $email_invoice = $_POST['billing_invoiceemail'];
  
  if (!validate_username($email) || !is_email($email)) {
    $errors[] = 'Kontakt e-mail er ikke en gyldig e-mail-adresse';
  }
  
  if (username_exists($email) || email_exists($email)) {
    $errors[] = 'Kontakt e-mail findes allerede';
  }
  
  if (!is_email($email_invoice)) {
    $errors[] = 'Faktura e-mail er ikke en gyldig e-mail-adresse';
  }

  // Handle
  
  //die('Tester - oppe om lidt igen');
    
  if (count($errors) > 0) {
    echo "<h2>Fejl</h2>\n";
    echo "<p>\n";
    echo "<ul style=\"padding-left: 2em;\">\n";
    foreach ($errors as $e) {
      echo "<li>" . $e . "</li>\n";
    }    
    echo "</ul>"; 
    echo "</p>\n";
    echo "<div style=\"margin-bottom: 2em;\"></div>\n";
  } else {
    // CREATE!
    
    $password = wp_generate_password(8, true);
    
    $user_id = wp_create_user($email, $password, $email);
    
    wp_update_user(array('ID' => $user_id, 
      'nickname' => $email,
      'display_name' => $_POST['user_company'],
      'first_name' => $_POST['user_firstname'],
      'last_name' => $_POST['user_lastname']
      ));
          
    update_user_meta($user_id, 'billing_email', $email);
    update_user_meta($user_id, 'billing_company', $_POST['user_company']);
    update_user_meta($user_id, 'billing_first_name', $_POST['user_firstname']);    
    update_user_meta($user_id, 'billing_last_name', $_POST['user_lastname']);
    update_user_meta($user_id, 'billing_phone', $_POST['user_phone']);
    update_user_meta($user_id, 'billing_cvr', $_POST['user_cvr']);    
    update_user_meta($user_id, 'billing_invoiceemail', $email_invoice);
    update_user_meta($user_id, 'billing_address_1', $_POST['user_adr1']);
    update_user_meta($user_id, 'billing_address_2', $_POST['user_adr2']);
    update_user_meta($user_id, 'billing_postcode', $_POST['user_postcode']);
    update_user_meta($user_id, 'billing_city', $_POST['user_city']);
    update_user_meta($user_id, 'billing_country', 'DK');
    
    $user = new WP_User($user_id);
    $user->set_role('subscriber');
    
    $mailadmin_to = 'post@nordicspirits.dk';
    //$mailadmin_to = 'mikl@mikl.dk';
    $mailadmin_subject = '[NordicSpirits] Ny forhandler: Ansøgning';
    $mailadmin_message = 
"
Forhandler:

Firma: " . $_POST['user_company'] . "
CVR: " . $_POST['user_cvr'] . "
Adresse 1: " . $_POST['user_adr1'] . "
Adresse 2: " . $_POST['user_adr2'] . "
Postnr.: " . $_POST['user_postcode'] . "
By: " . $_POST['user_city'] . "
Faktura e-mail: " . $email_invoice . "

Kontaktperson:

Fornavn: " . $_POST['user_firstname'] . "
Efternavn: " . $_POST['user_lastname'] . "
Kontakt e-mail: " . $email . "
Faktura e-mail: " . $email_invoice . "
Adgangskode: " . $password . "
Mobilnummer: " . $_POST['user_phone'] . "

Gå til http://b2b.nordicspirits.dk/wp-admin/users.php?role=subscriber for at se afventende ansøgninger. For at godkende dem, skal forhandleres flyttes til customer-rollen. I velkomstmailen skal token samt password oplyses. Den nye forhandler kender altså hverken token eller sin adgangskode, så disse skal oplyses i velkomstmail (såfremt ansøgningen godkendes).
";
    
    wp_mail($mailadmin_to, $mailadmin_subject, $mailadmin_message);    
    
    $DISPLAY_FIELDS = FALSE;
    echo 'Ansøgning afsendt. Du modtager en mail fra os, når vi har set din ansøgning igennem.';
  }
}

function req($key, $default = '') {
  return isset($_POST[$key]) ? $_POST[$key] : $default;
}

//print_r($_POST);
?>

  <?php
    if ($DISPLAY_FIELDS) {    
  ?>
  
  <h2>Forhandler</h2>
	<p>
		<label for="user_company">Firma<br />
		<input type="text" name="user_company" id="user_company" class="input" value="<?php echo htmlentities(req('user_company')) ?>" size="20" /></label>
	</p>
	
  <p>
		<label for="user_cvr">CVR<br />
		<input type="text" name="user_cvr" id="user_cvr" class="input" value="<?php echo htmlentities(req('user_cvr')) ?>" size="20" /></label>
	</p>

	<p>
		<label for="user_adr1">Adresse 1<br />
		<input type="text" name="user_adr1" id="user_adr1" class="input" value="<?php echo htmlentities(req('user_adr1')) ?>" size="20" /></label>
	</p>
	
	<p>
		<label for="user_adr2">Adresse 2<br />
		<input type="text" name="user_adr2" id="user_adr2" class="input" value="<?php echo htmlentities(req('user_adr2')) ?>" size="20" /></label>
	</p>
	
	<p>
		<label for="user_postcode">Postnr.<br />
		<input type="text" name="user_postcode" id="user_postcode" class="input" value="<?php echo htmlentities(req('user_postcode')) ?>" size="20" /></label>
	</p>
	
	<p>
		<label for="user_city">By<br />
		<input type="text" name="user_city" id="user_city" class="input" value="<?php echo htmlentities(req('user_city')) ?>" size="20" /></label>
	</p>
	
		
	<h2>Kontaktperson</h2>
	<p>
		<label for="user_firstname">Fornavn<br />
		<input type="text" name="user_firstname" id="user_firstname" class="input" value="<?php echo htmlentities(req('user_firstname')) ?>" size="20" /></label>
	</p>
	
	<p>
		<label for="user_lastname">Efternavn<br />
		<input type="text" name="user_lastname" id="user_lastname" class="input" value="<?php echo htmlentities(req('user_lastname')) ?>" size="20" /></label>
	</p>

	<p>
		<label for="user_email">Kontakt e-mail<br />
		<input type="text" name="user_email" id="user_email" class="input" value="<?php echo htmlentities(req('user_email')) ?>" size="20" /></label>
	</p>

	<p>
		<label for="billing_invoiceemail">Faktura e-mail<br />
		<input type="text" name="billing_invoiceemail" id="billing_invoiceemail" class="input" value="<?php echo htmlentities(req('billing_invoiceemail')) ?>" size="20" /></label>
	</p>
		
	<p>
		<label for="user_phone">Mobilnummer (til sms)<br />
		<input type="text" name="user_phone" id="user_phone" class="input" value="<?php echo htmlentities(req('user_phone')) ?>" size="20" /></label>
	</p>
	

	<div class="g-recaptcha" data-sitekey="6LcKlxwTAAAAADNEE-7qMW4RpQL2Vs0QVhkZgvib"></div>


	<p class="submit">
		<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Ansøg" />
	</p>
	
  <?php
    } // if ($DISPLAY_FIELDS)
  ?>
  
</form>

  
	</div>

	
	<?php
	/*
    <div style="text-align: center; padding-top: 50px;">
      <table style="margin-left: auto; margin-right: auto;" border="0">
        <tr>
          <td style="padding: 10px; text-align: right; width: 150px">
            <img style="width: 100%" src="../wp-content/themes/storefront-child/images/herbie-gin-logo-trans.png" />
          </td>
          <td style="padding: 10px; text-align: center; width: 100px">
            <img style="width: 100%" src="../wp-content/themes/storefront-child/images/logo.png" />
          </td>
          <td style="padding: 10px; text-align: left; width: 150px">
            <img style="width: 100%" src="../wp-content/themes/storefront-child/images/nordisk.png" />
          </td>
          <td style="padding: 10px; text-align: left; width: 150px">
            <img style="width: 100%" src="../wp-content/themes/storefront-child/images/clumsebear-logo.png" />
          </td>
        </tr>
      </table>
    </div>
    */
    
  echo '
    <div style="text-align: center; padding-top: 50px;">
    ';

  $recent = new WP_Query("page_id=518"); 
  while ($recent->have_posts()) {
    $recent->the_post();
    the_content(); 
  }
  
  echo '  </div>';
  ?>
    
    <div style="text-align: center; padding-top: 25px;">
      Har du spørgsmål til vores produkter eller andet, så kontakt os i dag:
      <br />
      Telefon 2615 1514 / post@nordicspirits.dk
    </div>
  	<div class="clear"></div>
</body>
</html>

