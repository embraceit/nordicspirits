<?php 

function auto_login() {
  wp_clear_auth_cookie();

  $user_id = 2; // nordicspiritsadmin
  $user = get_user_by('id', $user_id);
  $user_login = $user->data->user_login;
  
  //login
  wp_set_current_user($user_id, $user_login);
  wp_set_auth_cookie($user_id);
}

require('../wp-load.php');

auto_login();

$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

$name = $dt->format('Y-m-d--His');
$subfolder = $dt->format('Y/m/');

$folder = '/var/www/www.nordicspirits.dk/asdvF3wesdvvsdv-history/';
$path = $folder . $subfolder;
if (!file_exists($path)) {
  mkdir($path, 0755, true);
}

ob_start();
require('asdvF3wesdvvsdv.php');
$content = ob_get_contents();
ob_end_clean();
  
file_put_contents($path . 'asdvF3wesdvvsdv-' . $name . '.html', $content);

// LOGOUT!
//wp_clear_auth_cookie(); 
//wp_logout(); 
?>

