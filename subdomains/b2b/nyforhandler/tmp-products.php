<?php 
require('../wp-load.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<?php
$products = get_posts( array(
    'numberposts' => -1,
    'post_type'   => 'product'
));


echo "
<table border=\"1\">
  <tr>
    <th>ID</th>
    <th>Title</th>
    <th>vtprd_rule_category</th>
    <th>prod_attrib_category</th>
  </tr>    
";

foreach ($products as $product) {
  //print_r($product);
  
  $vtprd_rule_category = wp_get_post_terms($product->ID, 'vtprd_rule_category');
  $prod_attrib_category = get_the_terms($product->ID, 'pa_package-discount');
  
  if ($vtprd_rule_category === FALSE || count($vtprd_rule_category) == 0) {
    $vtprd_rule_category = '[none]';
  } else if (count($vtprd_rule_category) == 1) {
    $vtprd_rule_category = $vtprd_rule_category[0]->slug;
  } else { //
    echo 'ERROR';
    print_r($vtprd_rule_category);
    die;
  }
  
  if ($prod_attrib_category === FALSE || count($prod_attrib_category) == 0) {
    $prod_attrib_category = '[none]';
  } else if (count($vtprd_rule_category) == 1) {
    $prod_attrib_category = $prod_attrib_category[0]->slug;
  } else { //
    echo 'ERROR';
    print_r($prod_attrib_category);
    die;
  }
  
  
  
  
  echo "
    <tr>
      <td>" . $product->ID . "</td>
      <td>" . $product->post_title . "</th>
      <td>" . $vtprd_rule_category . "</th>
      <td>" . $prod_attrib_category . "</th>
    </tr>    
  ";
}

echo "
</table>\n
";
?>
</body>
</html>

