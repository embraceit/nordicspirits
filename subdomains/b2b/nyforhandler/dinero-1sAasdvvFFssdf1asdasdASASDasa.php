<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
//unset($_SESSION['token']);
//print_r($_SESSION);


function get_access_token($client_id_secret, $apikey)
{
  if (isset($_SESSION['token'][$client_id_secret][$apikey])) {
    //echo 'B*H';
    return $_SESSION['token'][$client_id_secret][$apikey];
  }
  
  $url = 'https://authz.dinero.dk/dineroapi/oauth/token';
  
  $curl = curl_init();

  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_USERPWD, $client_id_secret);
  
  $headers = array('Content-Type: application/x-www-form-urlencoded');
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

  $data = array('grant_type' => 'password',
                'scope' => 'read',
                'username' => $apikey,
                'password' => $apikey);
  
  curl_setopt($curl, CURLOPT_POST, count($data));
  curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

  curl_setopt($curl, CURLOPT_URL, $url);

  //return data instead of output
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  
  $result = curl_exec($curl);
  
  curl_close($curl);
  
  $access_token = json_decode($result, $assoc = true);
 
  $_SESSION['token'][$client_id_secret][$apikey] = $access_token;

  return $access_token;
}


function get_endpoint($url, $access_token)
{
  $curl = curl_init();

  $headers = array('Content-Type: application/json',
                   'Authorization: Bearer ' . $access_token);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

  curl_setopt($curl, CURLOPT_URL, 'https://api.dinero.dk/' . $url);
  
  //return data instead of output
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  
  $result = curl_exec($curl);
  
  curl_close($curl);
  
  $result = json_decode($result, $assoc = true);
  
  return $result;
}

/*
function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}
*/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		#login h1, #login h2 {
		  margin-bottom: 1em;
		}
		#login {
		  width: 500px;
		  padding-top: 2em;
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 10px;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">

<?php

$client_id_secret = 'Nordic Spirits IVS' . ':' . 'TojI4fWZePZhMi6zg7PfaKHys9gmTv0YRetYdIQaK4';
$apikey = '9361e8bbcd144a208ed715d2415489c6';
//$apikey = '3f7870c2a68841969ee815c85073d1e9';


$token = get_access_token($client_id_secret, $apikey);
$access_token = $token['access_token'];

/*
$orgs = get_endpoint('v1/organizations?fields=Name,Id', $access_token);
//print_r($orgs);
$org_id = $orgs[0]['Id'];
print_r($org_id);
*/

//die('asds');

$org_id = '117742';

function get_endpoint_all_pages($url, $access_token) {
  $res = array();
  
  $PAGESIZE = 100;
  $page = 0;
  
  do {
    $info = get_endpoint($url . '&page=' . $page . '&pageSize=' . $PAGESIZE, $access_token);
    $info = $info['Collection'];
    array_push($res, $info);
    echo count($info);  
    ++$page;
    echo $page;
    die;
  } while (count($info) == $PAGESIZE);
  
  return($res);
}


$contacts = get_endpoint('v1/' . $org_id . '/contacts?fields=VatNumber,Name,ContactGuid,Street,Zipcode,City,CountryKey&deletedOnly=false&page=0&pageSize=10', $access_token);
print_r($contacts);

$contacts = get_endpoint_all_pages('v1/' . $org_id . '/contacts?fields=VatNumber,Name,ContactGuid,Street,Zipcode,City,CountryKey&deletedOnly=false', $access_token);
print_r($contacts);

?>

</body>
</html>

