<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

function show_money($x) {
  return number_format($x, 0, ',', '.');
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>
	<!--
	<link rel='stylesheet' id='buttons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/buttons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='dashicons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='login-css'  href='http://b2b.nordicspirits.dk/wp-admin/css/login.min.css?ver=4.4.2' type='text/css' media='all' />
  -->
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		#login h1, #login h2 {
		  margin-bottom: 1em;
		}
		#login {
		  width: 500px;
		  padding-top: 2em;
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 10px;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<h1>Gennemførte ordrer (godkendt/afsluttede)</h1>
<?php
$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

echo '<p>Genereret ' . $dt->format('d-m-Y H:i:s') . '.</p>';
//print_r(wc_get_order_statuses());
?>
<?php
  function get_producent($product_id) {
    
    $_pf = new WC_Product_Factory();
    $product = $_pf->get_product($product_id);
    
    if (!is_a($product, 'WC_Product_Simple')) {
      return '';
    }

    return $product->get_attribute('pa_producent');    
  }
  
  //echo '<pre>'; print_r(get_producent(286)); echo '</pre>';  
  
  /**************************************/
  //echo '<!-- ' . implode(wc_get_order_statuses(), ', ') . ' -->';

  $orders = get_posts( array(
      'numberposts' => -1,
      'post_type'   => wc_get_order_types(),
      'post_status' => 'wc-completed'
      //'post_status' => wc_get_order_statuses()//array('wc-completed', 'wc-processing')
  ));
  
  //echo '<h2>Antal = ' . count($orders) . '</h2>';
  
  //echo '<pre>'; print_r($orders); echo '</pre>'; 
  
  // discount:
  //require('../wp-content/themes/storefront-child/inc-woocommerce-cart.php');
  //sale_custom_price($cart_object)
  
  $products_producent = array();
  $overview = array();
  $discounts_overview = array();
  
  foreach ($orders as $post) {
    $order = new WC_Order($post->ID);
    $order_month = substr($order->order_date, 0, 7); // order_date vs modified_date
    //print_r($order);
    $items = $order->get_items();
    
    $company = get_post_meta($order->post->ID, '_billing_company', true) . ' (' . get_post_meta($order->post->ID, '_billing_city', true) . ')';
    
    //echo "id = " . $post->ID . " (" . $order_month . ")\n";
    //print_r($items);
    foreach ($items as $item) {
      //print_r($item->get_post_meta());

      $product_id = $item['product_id'];
      $product_name = $item['name'];
      $product_qty = $item['qty'];      
      $product_total = $item['line_subtotal'];
      
      $product_producent = (isset($products_producent[$product_id]) ? $products_producent[$product_id] : get_producent($product_id));
      if (!isset($products_producent[$product_id])) {
        $products_producent[$product_id] = $product_producent;
      }

      // Because discount has changed from being put on each item to now being added as a order discount...
      $product_price = $product_total / $product_qty;
      
      $discount = calc_discount($product_id, $product_qty);
      
      $discount_txt = '';
      $discount_n_total = 0;
      
      if (!is_null($discount) && $discount['n'] > 0) {
        $discount_n = $discount['n'];
        $free_pcs_deal = $discount['free_pcs_deal'];
        $discount_n_total = $discount_n * $free_pcs_deal;
        $discount_price = $discount_n_total * $product_price;
        
        //print_r($discount['discount_type']);
        //$discount_txt = ' (discounted ' . $discount_n . ' x ' . $free_pcs_deal . ' stk. = ' . $discount_price . ') => total = ' . ($product_total - $discount_price);
        $discounts_overview[$discount['discount_type']['label']][$product_name][] = 'Rabat på ' . $discount_n . ' x ' . $free_pcs_deal . ' stk.: ' . $discount_price . '. Produkttotal = ' . ($product_total - $discount_price) . ' (på ordre ' . $order->post->ID . ' hvor der var ' . $product_qty . ' stk. til produkttotal ' . $product_total . ' uden rabat)';
      }
      
      $array_key = $product_name . ' (id ' . $product_id . ')';
      
      if (strlen(trim($product_producent)) > 0) {
        $overview[$order_month][$product_producent]['products'][$array_key][] = array(
          'free' => $discount_n_total, 
          'paid' => array('n' => $product_qty - $discount_n_total, 
                          'product_price' => $product_price),
          'order_id' => $post->ID
        );
        
        $overview[$order_month][$product_producent]['customer'][] = $company;
      }
      
      //echo $product_qty . " stk. " . $product_name . " (by " . $product_producent . ") á " . $product_price . " = ". $product_total . "" . $discount_txt . "\n";
    }  
  }
  
  print_r($overview);
  //print_r($discounts_overview);
  
  /*
   * Products
   */
  
  foreach ($overview as $month => $producenter) {
    echo "<h2>" . $month . "</h2>\n";
    
    foreach ($producenter as $producent => $products) {
      /*
      if (strlen(trim($producent)) == 0) {
        continue;
      }
      */
      
      echo "<h3>" . $producent . "</h3>\n";
      
      echo "<table>\n";
      
      echo "<tr>\n";
      echo "  <th style=\"width: 400px; text-align: left;\">Produkt</th>\n";
      echo "  <th style=\"width: 100px; text-align: right;\">Gratis</th>\n";
      echo "  <th style=\"width: 200px; text-align: right;\">Betalt</th>\n";
      echo "  <th style=\"width: 100px; text-align: right;\">Total</th>\n";
      echo "</tr>\n";
        
      
      $total_producent = 0;
      
      foreach ($products['products'] as $product_name => $product_details) {
        echo "<tr>\n";
        echo "  <th>" . $product_name . "</th>\n";
        //echo "<h4>" . $product_name . "</h4>\n";
        
        $prod_free_n = 0;
        $prod_paid = array();
        $total = 0;
        
        foreach ($product_details as $entry) {
          $prod_free_n += $entry['free'];
          $prod_paid[$entry['paid']['product_price']] += $entry['paid']['n'];
          $total += $entry['paid']['product_price'] * $entry['paid']['n'];
        }
        
        $total_producent += $total;
        
        $paid = array();        
        foreach ($prod_paid as $p => $n) {
          $paid[] = $n . ' stk. á ' . show_money($p) . ' DKK';
        }

        echo "  <td>" . $prod_free_n . " stk.</td>\n";
        echo "  <td>" . implode($paid, '<br />') . "</td>\n";
        echo "  <td>" . show_money($total) . " DKK</td>\n";
        
        //print_r($prod_paid);
        
        echo "</tr>\n";
      }
      
      echo "<tr>\n";
      echo "  <th style=\"text-align: left;\">Total</th>\n";
      echo "  <th></th>\n";
      echo "  <th></th>\n";
      echo "  <th style=\"text-align: right;\">" . show_money($total_producent) . " DKK</th>\n";
      echo "</tr>\n";
      
      echo "</table>\n";
      
      echo "<h4 style=\"color: #666666;\">Månedens kunder</h4>\n";
      echo "<ul style=\"color: #666666;\">\n";
      $products['customer'] = array_unique($products['customer']);
      foreach ($products['customer'] as $company) {
        echo "<li>" . $company . "</li>\n";
      }
      echo "</ul>\n";
    }
  }
  
  /*
   * Disounts
   */
  echo "<h2>Anvendte rabatter</h2>\n";
  foreach ($discounts_overview as $discount_name => $discounts) {
    echo "<h4>" . $discount_name . "</h4>\n";
    echo "<ul>\n";
    
    foreach ($discounts as $discount_prod => $discounts_txt) {
      echo "    <li>" . $discount_prod . "</li>\n";
      echo "<ul>\n";
      foreach ($discounts_txt as $discount_txt) {
        echo "    <li>" . $discount_txt . "</li>\n";
      }
      echo "</ul>\n";
    }
    
    echo "</ul>\n";
  }
  
  /*
   * Products with no producent
   */
  $_pf = new WC_Product_Factory();
  $prods_no_producent = array();

  foreach ($products_producent as $key => $value) {
    if (strlen(trim($value)) == 0) {
      $product = $_pf->get_product($key);
      
      if (!is_a($product, 'WC_Product_Simple')) {
        $prods_no_producent[] = $key;
      } else {
        $prods_no_producent[] = $product;
      }
    }
  }
  
  echo "<h2>Produkter uden producent</h2>\n"; 
  
  if (count($prods_no_producent) > 0) {
    echo "<ul style=\"padding-left: 2em\">\n";

    foreach ($prods_no_producent as $product) {            
      if (!is_a($product, 'WC_Product_Simple')) {
        echo "<li>[Slettet produkt med id = " . $product . "]</li>\n";
        continue;
      }
      
      echo "<li>" . $product->post->post_title . " (id = " . $product->post->ID . ")</li>\n";
    }
    
    echo "</ul>\n";
  } else {
    echo '<p>Ingen fundet.</p>';
  }
?>
</body>
</html>

