<?php 

function auto_login() {
  wp_clear_auth_cookie();

  $user_id = 2; // nordicspiritsadmin
  $user = get_user_by('id', $user_id);
  $user_login = $user->data->user_login;
  
  //login
  wp_set_current_user($user_id, $user_login);
  wp_set_auth_cookie($user_id);
}

require('../wp-load.php');

auto_login();

$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

$name = $dt->format('Y-m-d--His');
$subfolder = $dt->format('Y/m/');

$folder = '/var/www/www.nordicspirits.dk/asdvxcXXcvxcvdf-stock-history/';
$path = $folder . $subfolder;
if (!file_exists($path)) {
  mkdir($path, 0755, true);
}


/////////////////////////////////////////////////////////////////////////

  $products = get_posts( array(
      'numberposts' => -1,
      'post_type'   => 'product',
      'post_status' => 'any,draft,publish'
  ));
  
  //print_r($products);
  
  $prods = array();
  
  foreach($products as $p) {
    $product = wc_get_product($p->ID);
    
    $sku = $product->get_sku();
    $stock_quantity = $product->get_stock_quantity();
    
    /*
    if ($sku == 'herbie-gin-original') {
      print_r($product);
      die;
    }
    */
    
    $prods[] = array('ID' => $p->ID,
                     'post_title' => $p->post_title,
                     'guid' => $p->guid,
                     'SKU' => $sku,
                     'stock_quantity' => $stock_quantity);
  }
  
  //print_r($prods);
  $json = json_encode($prods);
  //echo $json;
  
/////////////////////////////////////////////////////////////////////////
  
file_put_contents($path . 'asdvxcXXcvxcvdf-' . $name . '.json', $json);

// LOGOUT!
//wp_clear_auth_cookie(); 
//wp_logout(); 
?>

