<?php 
require('include.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>
 
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}

		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #666666;      
    }
    
    td, th {
      padding: 0.1em;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
		
		.invoicepaid {
		  
		}
		
		.notpaid {
		  color: red;
		}
		
		.creditnote {
		  color: blue;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">

<?php
$data = json_decode(file_get_contents('data.json'), $assoc = true);
//print_r($data);
//print_r($data['ProductNames']);

$start_date = $data['FetchOptions']['start_date'];
$end_date = $data['FetchOptions']['end_date'];

function test_name_uniqueness($array_key) {
  global $data;
  
  foreach ($data[$array_key] as $number => $names) {
    if (count($names) != 1) {
      // FIXME
      //print_r($names);
      //trigger_error('More than one name for an account', E_USER_NOTICE);
    }
  }
}
test_name_uniqueness('AccountNames');
test_name_uniqueness('ProductNames');
test_name_uniqueness('ContactNames');


//print_r($data);
//die;

// Only DKK!
foreach ($data['AccountTransactions'] as $account_number => $sales) {
  if ($account_number == 'A') {
    continue;
  }
  
  foreach ($sales as $month => $types) {
    foreach ($types['ProductsSold'] as $type_key => $type_items) {
      foreach ($type_items as $item_guid => $orders) {        
        foreach ($orders['Orders'] as $subkeyguid => $order_lines) {
          foreach ($order_lines as $order_line) {
            
            if ($order_line['Currency'] != "DKK") {
              print_r($order_line);
              die('Forventede kun DKK valuta');            
            }
          }
        }
      }
    }
  }
} 
         
//print_r($data);
//die;


$labels = array('ByProductThenContact' => 'Per produkt',
                'ByContactThenProduct' => 'Per distributør');

$name_table = array('ByProductThenContact' => 'ProductNames',
                    'ByContactThenProduct' => 'ContactNames');
$subname_table = array('ByProductThenContact' => 'ContactNames',
                       'ByContactThenProduct' => 'ProductNames');

// http://stackoverflow.com/a/2021729/7720448
function to_filename($str) {
  $str = preg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $str);
  $str = preg_replace("([\.]{2,})", '', $str);
  return $str;
}

function format_money($x) {
  if (strlen($x) == 0) {
    return '';
  }
  
  if ($x === 'NA') {
    return $x;
  }

  return number_format($x, $decimals = 2, $dec_point = ",", $thousands_sep = ".");
}

function format_number_csv($x) {
  if (strlen($x) == 0) {
    return '';
  }

  if ($x === 'NA') {
    return $x;
  }
  
  return number_format($x, $decimals = 2, $dec_point = ",", $thousands_sep = "");
}

function cmp_rawdata($a, $b) {
  $a_d = strtotime($a['date']);
  $b_d = strtotime($b['date']);
  return ($a_d < $b_d);
}
?>

<?php
/**************************
 **************************/
?>


<?php

echo '<h1>' . $start_date . ' -> ' . $end_date . '</h1>';

ksort($data['AccountTransactions'], SORT_REGULAR);

foreach ($data['AccountTransactions'] as $account_number => &$sales) { 
  krsort($sales, SORT_REGULAR);
}
?>

<?php
function get_kvartal_from_date($str) {
  $month = (int)substr($str, 5, 2);
  $kvartal = ceil($month / 3);
  return $kvartal;
}

function get_kvartal_from_month($str) {
  return get_kvartal_from_date($str . '-01');
  return 'NA';
}

$stock_info_raw = json_decode(file_get_contents('../../../../../asdvxcXXcvxcvdf-stock-history/2017/06/asdvxcXXcvxcvdf-2017-06-29--211507.json'), $assoc = true);
$stock_info = array();
foreach ($stock_info_raw as $product) {
  $product['seen'] = 0;
  $product['seen_account'] = array();
  
  if (strlen(trim($product['stock_quantity'])) == 0) {
    $product['stock_quantity'] = NULL;
  }
  
  $stock_info[ $product['post_title'] ] = $product;
}

$dinero_woo_stock_missing = array();
$dinero_woo_stock_auto_found = array();

// Dinero key => WooC key
$dinero_woo_stock = array(
'rum-4-liquorice - Skotlander Rum IV (lakrids)' => 'Skotlander RUM IV (lakrids)', 
  'gaveaeske - Gaveæske (3x10 cl)' => 'Skotlander Rum Gaveæske (3x10 cl)', 
  'cask - Skotlander Cask Rum' => 'Skotlander Cask Rum', 
  
  'Skotlander Cask Rum (colli pris)' => 'Skotlander Cask Rum', 
  'Skotlander Cask Rum (key-account-pris)' => 'Skotlander Cask Rum', 
  'Skotlander White Rum (key-account-pris)' => 'Skotlander White Rum', 
  'Skotlander White Rum (colli pris)' => 'Skotlander White Rum', 
  'den-roegede-bjorn - Den Røgede Bjørn Vodka' => 'Den Rene Bjørn Vodka', 
  
  'white - Skotlander White Rum' => 'Skotlander White Rum', 
  'skot001 - 2 x Skotlander Rum Glas' => '2 x Skotlander Rum Glas', 
  'bland-selv-rom - Bland-selv-rom (80 miniature-flasker)' => 'Bland-selv-rom (80 miniature-flasker)', 
  'Skotlander Rum III (havtorn)' => 'Skotlander RUM III (havtorn)', 
  'bland-selv-rom-refill - Bland-selv-rom (REFILL)' => 'Bland-selv-rom (REFILL)', 
  'Romtønde + 4 liter rom ' => 'Romtønde + 4 liter rom', 
  'skotlander-agricole - Skotlander Agricole Rum' => 'Skotlander Agricole Rum', 
  'den-klodsede-bjorn - Den Klodsede Bjørn Vodka' => 'Den Klodsede Bjørn Vodka', 
  'lillebjorn - Den Lille Bjørn' => 'Den Lille Bjørn', 
  'den-rene-bjorn - Den Rene Bjørn Vodka' => 'Den Rene Bjørn Vodka', 
  'Rugbjorn50 - Rugbjørnen' => 'Rugbjørnen', 
  'Den Klodsede Bjørn Vodka (key-account-pris)' => 'Den Klodsede Bjørn Vodka', 
  'Den Rene Bjørn Vodka (key-account-pris)' => 'Den Rene Bjørn Vodka', 
  'Den Lille Bjørn (key-account)' => 'Den Lille Bjørn', 
  'Den Klodsede Bjørn Vodka (colli-pris)' => 'Den Klodsede Bjørn Vodka', 
  'enghaven-valnod - Enghaven Valnød Snaps' => 'Enghaven Valnød Snaps', 
  'enghaven-dild - Enghaven Dild Snaps' => 'Enghaven Dild Snaps', 
  'enghaven-skovmaerke-snaps - Enghaven Skovmærke Snaps' => 'Enghaven Skovmærke Snaps', 
  'enghaven-klitrose - Enghaven Klitrose Snaps ' => 'Enghaven Klitrose Snaps', 
  'copperpot-gin - Copperpot Gin' => 'Copperpot Gin', 
  'trolden-lakridslikoer-50cl - The New Black (lakridslikør) 50 cl' => 'The New Black (lakridslikør) 50 cl', 
  'Copperpot Gin (colli-pris)' => 'Copperpot Gin (5 cl)', 
  'Copperpot Gin (key-account-pris)' => 'Copperpot Gin (5 cl)', 
  'The New Black (lakridslikør) 50 cl (colli)' => 'The New Black (lakridslikør) 50 cl', 
  'nordisk-gin - Nordisk Gin' => 'Nordisk Gin', 
  'hedesnaps - Hedesnaps' => 'Hedesnaps (2 cl)', 
  'nordisk-sneppesnaps - Sneppesnaps' => 'Sneppesnaps', 
  'nordisk-havesnaps - Nordisk Havesnaps' => 'Enghaven Klitrose Snaps', 
  'nordisk-rode-willy - Røde Willy' => 'Røde Willy', 
  'Nordisk Gin (key-account-pris)' => 'Nordisk Gin (5 cl)', 
  'mjod-no-1 - MJÖD no.1' => 'MJÖD no.1', 
  'MJÖD no.1 (key-account-pris)' => 'MJÖD no. 1 -2 cl', 
  'Phantom Spirits Rum - Mikkeller X (key-account-pris)' => 'Phantom Spirits Rum - Mikkeller X', 
  'For meget udbetalt betalt for q1' => 'Brown bag (til rom)', 
  'Køb af produkter' => 'Røgbjørnen', 
  'herbie-gin-original - Herbie Gin Original' => 'Herbie Gin Original', 
  'herbie-gin-colli6 - Herbie Gin Original (colli 6 stk)' => 'Herbie Gin Original (colli 6 stk)', 
  'Herbie Gin Original (UB)' => 'Herbie Gin Original', 
  'Herbie Gin Original (key-account-pris)' => 'Herbie Gin Original (colli 6 stk)', 
  'mondino - Mondino Bio Amaro' => 'Mondino drinks brochure', 
  'Heiko gin - Heiko Gin' => 'Heiko Gin (Rhinlandet)', 
  'siegfried - Siegfried Rheinland Dry Gin' => 'Siegfried Gin (Rhinlandet)', 
  'Heiko gin-kolli - Heiko Gin (Rhinlandet) - Kolli' => 'Heiko Gin (Rhinlandet) -Kolli', 
  'Heiko Gin (colli-pris)' => 'Heiko Gin (Rhinlandet)', 
  'Mondino Bio Amaro' => 'Mondino Bio Amaro -10 cl', 
  'Heiko Gin (key-account-pris)' => 'Heiko Gin (Rhinlandet)', 
  'Siegfried Gin (key-account)' => 'Siegfried Gin (Rhinlandet)', 
  'Æblerov (Benene på Nakken 2015) 75cl  (colli)' => 'Æblerov (Benene på Nakken 2015) 75cl', 
  'aeblerov-benene-2015-375cl - Æblerov (Benene på Nakken 2015) 37,5 cl' => 'Æblerov (Benene på Nakken 2015) 37,5 cl', 
  'aeblerov-frankofil-75 - Æblerov (Frankofil) 75 cl' => 'Æblerov (Frankofil) 75 cl', 
  'amanero - Amanero Bitterlikør' => 'Amanero Bitterlikør (Bayern)', 
  'Mondino Bio Amaro (key-account-pris)' => 'Mondino Bio Amaro (Tyrol)', 
  'kalevala-gin - Kalevala Gin' => 'Kalevala Gin (Finland)', 

  'Kalevala Gin (key-account)' => 'Kalevala Gin (Finland)', 
  'Kever Genever Gaveæske (Holland) (colli)' => 'Kever Genever Gaveæske (Holland)', 
  'kever-genever - Kever Genever (Holland)' => 'Kever Genever (Holland)', 

  'the-new-black-lakridslikoer-5-cl - The New Black (lakridslikør) 5 cl' => 'The New Black (lakridslikør) 5 cl',
  );

foreach ($data['AccountTransactions'] as $account_number => $sales) {
  if ($account_number == 'A') {
    continue;
  }

  $account_name = array_keys($data['AccountNames'][$account_number])[0];
  
  $csv_prefix = $account_number . ' - ' . to_filename($account_name);
  $csv_overview = '';
  $csv_details = '';
  
  echo "<h2>Konto " . $account_number . " (" . $account_name . ")</h2>\n";
  
  $rawdata = array();
  $summary = array();  
  
  foreach ($sales as $month => $types) {
    $rawdata[$month] = array();
    
    if (count($types) == 0 || !isset($types['ProductsSold']) || count($types['ProductsSold']) == 0) {
      continue;
    }
    
    $type_key = 'ByProductThenContact';
    $type_items = $types['ProductsSold'][$type_key];
    
    foreach ($type_items as $item_guid => $orders) {        
      foreach ($orders['Orders'] as $subkeyguid => $order_lines) {
        foreach ($order_lines as $order_line) {
          $sgn = ($order_line['SalesType'] == 'CreditNote') ? -1 : 1;
          $cust = array_keys($data[$subname_table[$type_key]][$subkeyguid])[0];
          $prod = array_keys($data[$name_table[$type_key]][$item_guid])[0];
          
          if ($order_line['Currency'] != "DKK") {
              print_r($order_line);
              die('Forventede kun DKK valuta');
          }
          
          //print_r($order_line);
          
          $total_cost = 0;
          $quantity_cost = 0;
          $quantity_free = 0;          
          
          if (abs($order_line['TotalAmount']) < 0.01) { // Dinero is rounded to two decimals!
            $quantity_free = $sgn * $order_line['Quantity'];
          } else {
            $total_cost = $sgn * $order_line['TotalAmount'];
            $quantity_cost = $sgn * $order_line['Quantity'];          
          }
          
          $stock_ultimo = NULL;
          $product_stock_found = FALSE;
          
          if (isset($stock_info[$prod])) {
            $stock_ultimo = $stock_info[$prod]['stock_quantity'];
            $product_stock_found = TRUE;
            $stock_info[$prod]['seen'] += 1;
            $stock_info[$prod]['seen_account'][$account_number][$month][] = $order_line;
          } else if (isset($dinero_woo_stock[$prod])) {
            $woo_prod = $dinero_woo_stock[$prod];
            $product_stock_found = TRUE;
            
            $stock_ultimo = $stock_info[$woo_prod]['stock_quantity'];
            
            $stock_info[$woo_prod]['seen'] += 1;
            $stock_info[$woo_prod]['seen_account'][$account_number][$month][] = $order_line;          
          } else {            
            $save_key = NULL;
            $save_dist = NULL;
            
            foreach ($stock_info as $key => $value) {
              //if ($key == 'Herbie Virgin (alkoholfri gin)') {
              //  continue;
              //}
              
              
              //$dist = levenshtein($prod, $key, 0, 1, 0);
              $dist = levenshtein($prod, $key);
              
              if (is_null($save_dist) || $save_dist > $dist) {
                $save_dist = $dist;
                $save_key = $key;
              }        
            }
            
            $dinero_woo_stock_missing[$prod] = TRUE;
            /*
            if ($save_dist < 30) {            
              $dinero_woo_stock_auto_found[$prod][] = $save_key;
            } else {
              $dinero_woo_stock_missing[$prod] = TRUE;
            }
            */
          }
          
          
          $rawdata[$month][] = array(
                         'date' => $order_line['InvoiceDate'], 
                         'prod' => $prod,
                         'cust' => $cust, 
                         'addr' => $order_line['OrderAddress'],
                         'quantity_cost' => $quantity_cost,
                         'quantity_free' => $quantity_free,
                         'total_cost' => $total_cost,
                         'stock_ultimo' => $stock_ultimo,
                         'stock_found' => $product_stock_found);
          
          if (!isset($summary[$prod])) {
            $summary[$prod] = array('quantity_cost' => 0, 
                                    'quantity_free' => 0, 
                                    'total_cost' => 0,
                                    'stock_ultimo' => NULL,
                                    'stock_found' => FALSE);
          }
          
          $summary[$prod]['quantity_cost'] += $quantity_cost;
          $summary[$prod]['quantity_free'] += $quantity_free;
          $summary[$prod]['total_cost'] += $total_cost;

          if (!is_null($stock_ultimo)) {
            if (is_null($summary[$prod]['stock_ultimo'])) {
              $summary[$prod]['stock_ultimo'] = $stock_ultimo;
            } else {
              $summary[$prod]['stock_ultimo'] += $stock_ultimo;
            }
          }  
          
          if ($product_stock_found) {
            $summary[$prod]['stock_found'] = TRUE;
          }
          
          /*
          if ($prod == 'bland-selv-rom - Bland-selv-rom (80 miniature-flasker)') {
            echo $stock_ultimo;
            echo (is_null($stock_ultimo) ? 'isNULL' : 'OK');
            print_r($summary[$prod]);
            echo "\n<br/>";
            die;
          }
          */
        }
      }
    }    
  }
  
  //echo "<pre>" . print_r($rawdata, true) . "</pre>";

  //echo "<pre>" . $html_summary . "</pre>";


  $html_summary_head = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Oversigt</title>
 
  <style type="text/css">
    body {
      padding: 2em;
    }
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #666666;      
    }
    
    td, th {
      padding: 0.1em;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}		
  </style>
	</head>
<body>
  <h1>' . $start_date . ' -> ' . $end_date . '</h1>';
   $html_summary_tail = '</body>
</html>';
  
  $html_summary = '
  <h2>' . $account_name . '</h2>
  <h3>Oversigt</h3>
  <table>
    <thead>
      <tr style="background-color: #cccccc">
        <th style="text-align: left">Produkt</th>
        <th>Antal betalte</th>
        <th>Totalpris for betalte excl. moms</th>
        <th>Gns. stykpris for betalte</th>
        <th>Antal til 0 DKK</th>
        <th>Lager ultimo</th>
      </tr>
    </thead>
    <tbody>
  ';
  
  $csv_details = "Dato;År;Kvartal;Produkt;Kunde;Adresse;Postnr. GÆT;Antal betalte;Totalpris for betalte excl. moms;Gns. stykpris for betalte;Antal til 0 DKK\n";
  $csv_overview = "Produkt;Antal betalte;Totalpris for betalte excl. moms;Gns. stykpris for betalte;Antal til 0 DKK;Lager ultimo\n";
  
  $total_total_cost = 0;
  $total_quantity_cost = 0;
  $total_quantity_free = 0;
  $total_stock_ultimo = 0;
  
  ksort($summary, SORT_REGULAR);
  
  //print_r($stock_info);
  //print_r($summary);
  
  foreach ($summary as $prod => $s) {
    $total_total_cost += $s['total_cost'];
    $total_quantity_cost += $s['quantity_cost'];
    $total_quantity_free += $s['quantity_free'];

    $gnspris = ($s['quantity_cost'] == 0) ? 'NA' : (format_money($s['total_cost'] / $s['quantity_cost']) . ' DKK');
    $gnspris_CSV = ($s['quantity_cost'] == 0) ? 'NA' : (format_number_csv($s['total_cost'] / $s['quantity_cost']));
    
    $kvartal = get_kvartal_from_month($month);
    
    if (!is_null($s['stock_ultimo'])) {
      $total_stock_ultimo += $s['stock_ultimo'];
    }    
    
    $html_summary .= '
      <tr>
        <td style="text-align: left">' . $prod . '</td>
        <td>' . $s['quantity_cost'] . '</td>
        <td>' . format_money($s['total_cost']) . ' DKK</td>
        <td>' . $gnspris . '</td>
        <td>' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . '</td>
        <td' . (!$s['stock_found'] ? ' style="background-color: red"' : '') . '>' . $s['stock_ultimo'] . '</td>
      </tr>
    ';
    
    $csv_overview .= $prod . ";" . $s['quantity_cost'] . ";" . format_number_csv($s['total_cost']) . ";" . $gnspris_CSV . ";" . $s['quantity_free'] . ";" . $s['stock_ultimo'] . "\n";
  }
  
  $gnspris = ($total_quantity_cost == 0) ? 'NA' : (format_money($total_total_cost / $total_quantity_cost) . ' DKK');
  
  $html_summary .= '
    <tr style="background-color: #eeeeee;  font-weight: bold;">
      <td></td>
      <td>' . $total_quantity_cost . '</td>
      <td>' . format_money($total_total_cost) . ' DKK</td>
      <td>' . $gnspris . '</td>
      <td>' . $total_quantity_free . '</td>
      <td>' . $total_stock_ultimo . '</td>
    </tr>  
    </tbody>
  </table>
  ';
  //echo "<pre>" . $html_summary . "</pre>";
  
  echo $html_summary;
  
  echo '  
  <h3>Datagrundlag</h3>
  <table>
    <thead>
      <tr style="background-color: #cccccc">
        <th>Dato</th>
        <th>Kvartal</th>
        <th style="text-align: left">Produkt</th>
        <th style="text-align: left">Kunde</th>
        <th style="text-align: left">Adresse</th>
        <th style="text-align: left">Postnr. GÆT</th>
        <th>Antal betalte</th>
        <th>Totalpris for betalte excl. moms</th>
        <th>Gns. stykpris for betalte</th>
        <th>Antal til 0 DKK</th>        
      </tr>
    </thead>
    <tbody>
  ';
  
  
  $total_total_cost = 0;
  $total_quantity_cost = 0;
  $total_quantity_free = 0;
  
  foreach ($rawdata as $month => $sales) {
    usort($sales, "cmp_rawdata");
    
    echo '
      <tr style="background-color: #eeeeee">
        <td style="font-weight: bold;">' . $month . '</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    ';
    
    foreach ($sales as $s) {
      $total_total_cost += $s['total_cost'];
      $total_quantity_cost += $s['quantity_cost'];
      $total_quantity_free += $s['quantity_free'];
      $addr = str_replace("\n", ', ', str_replace("\r", '', $s['addr']));
      $postnrguess = preg_replace('/^.*([0-9]{4}).*$/', '${1}', $addr);
      
      $gnspris = ($s['quantity_cost'] == 0) ? 'NA' : (format_money($s['total_cost'] / $s['quantity_cost']) . ' DKK');
      $gnspris_CSV = ($s['quantity_cost'] == 0) ? 'NA' : (format_number_csv($s['total_cost'] / $s['quantity_cost']));
      
      $kvartal = get_kvartal_from_date($s['date']);
      
      echo '
        <tr>
          <td>' . $s['date'] . '</td>
          <td>' . $kvartal . '</td>
          <td style="text-align: left">' . $s['prod'] . '</td>
          <td style="text-align: left">' . $s['cust'] . '</td>
          <td style="text-align: left">' . $addr . '</td>
          <td style="text-align: left">' . $postnrguess . '</td>
          <td>' . $s['quantity_cost'] . '</td>
          <td>' . format_money($s['total_cost']) . ' DKK</td>
          <td>' . $gnspris . '</td>
          <td>' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . '</td>
        </tr>
      ';
      
      $csv_details .= $month . ';' . substr($month, 0, 4) . ';' . $kvartal . ';' . $s['prod'] . ';' . $s['cust'] . ';' . $addr . ';' . $postnrguess . ';' . $s['quantity_cost'] . ';' . format_number_csv($s['total_cost']) . ';' . $gnspris_CSV . ';' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . "\n";    
    }
  }
  
  $gnspris = ($total_quantity_cost == 0) ? 'NA' : (format_money($total_total_cost / $total_quantity_cost) . ' DKK');
  
  echo '
    <tr style="background-color: #eeeeee;  font-weight: bold;">
      <td style="font-weight: bold;">Total</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>' . $total_quantity_cost . '</td>
      <td>' . format_money($total_total_cost) . ' DKK</td>
      <td>' . $gnspris . '</td>
      <td>' . $total_quantity_free . '</td>
    </tr>
    </tbody>
  </table>
  ';
  
  //echo "<pre>" . $csv_details . "</pre>";
  
  //print_r($rawdata);
  
  $html_summary = $html_summary_head . $html_summary . $html_summary_tail;
  
  $html_summary = iconv("UTF-8", "ISO-8859-1", $html_summary);
  $csv_details = iconv("UTF-8", "ISO-8859-1", $csv_details);
  $csv_overview = iconv("UTF-8", "ISO-8859-1", $csv_overview);
  
  file_put_contents('export/export - ' . $csv_prefix . ' - summary.html', $html_summary);
  file_put_contents('export/export - ' . $csv_prefix . ' - datagrundlag.csv', $csv_details);
  file_put_contents('export/export - ' . $csv_prefix . ' - summary.csv', $csv_overview);

  
  //print_r($sales);
  //die;
}


print_r($stock_ultimo);

////////////////////////////////////////////////////////////////////////////////

echo "<h1>Lager</h1>\n";

///////////////////////////////////////////////////////////////////////
echo "<h2>Produkter fra Dinero der ikke kunne findes i WooC</h2>\n";
///////////////////////////////////////////////////////////////////////

//print_r($dinero_woo_stock_missing);
echo "<ul>\n";
foreach ($dinero_woo_stock_missing as $key => $value) {
  echo "  <li>" . $key . "</li>\n";
}
echo "</ul>\n";


///////////////////////////////////////////////////////////////////////
echo "<h2>Produkter set på Woo-lager men ikke set solgt i Dinero i perioden</h2><p>Til at matche ovenstående</p>\n";
///////////////////////////////////////////////////////////////////////
$stock_info_seen_not_1 = array();
foreach ($stock_info as $product) {
  if ($product['seen'] != 0) {
    continue;
  }
  
  $stock_info_seen_not_1[] = $product;
}

if (count($stock_info_seen_not_1) == 0) {  
  echo "Ingen fundet.";
} else {
  echo "<ul>\n";
  foreach ($stock_info_seen_not_1 as $product) {
    echo "<li>" . $product['post_title'] . "</li>\n";
  }
  echo "</ul>\n";
}


if (count($dinero_woo_stock_auto_found) > 0) {
  ///////////////////////////////////////////////////////////////////////
  echo "<h2>Produkter fundet vha. fuzzy-matching</h2>\n";
  ///////////////////////////////////////////////////////////////////////
  echo "<pre>\$dinero_woo_stock = array(\n";

  foreach ($dinero_woo_stock_auto_found as $key => $value) {
    if (is_array($value) && count($value) > 0) {
      echo "'" . $key . "' => '" . $value[0] . "', \n  ";
    }  
  }

  echo ");</pre>\n";
}
?>
</body>
</html>

