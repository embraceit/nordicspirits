<?php 
require('include.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		#login h1, #login h2 {
		  margin-bottom: 1em;
		}
		#login {
		  width: 500px;
		  padding-top: 2em;
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 10px;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">

<?php
$client_id_secret = 'Nordic Spirits IVS' . ':' . 'TojI4fWZePZhMi6zg7PfaKHys9gmTv0YRetYdIQaK4';
$apikey = '9361e8bbcd144a208ed715d2415489c6';

$token = get_access_token($client_id_secret, $apikey);
$access_token = $token['access_token'];

$org_id = '117742';

// Sales (BETA)
$start_date = '2017-04-01';
$end_date = '2017-06-30';


$sales_cache_filename = 'cache-dinero-invoices/sales-' . $org_id . '-' . $start_date . '-' . $end_date . '__upd-' . date("YmdH") . '0000.json';
$sales = NULL;  

if (file_exists($sales_cache_filename)) {
  //echo "CACHE!\n";
  $sales_json = file_get_contents($sales_cache_filename);
  $sales = json_decode($sales_json, true);
} else {
  //echo "API!\n";
  $sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=' . $start_date . '&endDate=' . $end_date . '&fields=Number,Guid,UpdatedAt,ContactName,Date,Description,Currency,TotalExclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
  $sales_json = json_encode($sales);
  file_put_contents($sales_cache_filename, $sales_json);
}



//$sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=2017-04-01&endDate=2017-12-31&fields=Number,Guid,ContactName,Date,Description,Currency,TotalInclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
for ($i = 0; $i < count($sales); ++$i) {
  $cache_filename_invoice = 'cache-dinero-invoices/invoices-' . $org_id . '-' . sprintf("%05d", $sales[$i]['Number']) . '-' . $sales[$i]['Guid'] . '-' . str_replace(':', '', $sales[$i]['UpdatedAt']) . '.json';
  $cache_filename_creditnote = 'cache-dinero-invoices/creditnote-' . $org_id . '-' . sprintf("%05d", $sales[$i]['Number']) . '-' . $sales[$i]['Guid'] . '-' . str_replace(':', '', $sales[$i]['UpdatedAt']) . '.json';
  
  $invoice = NULL;  
  
  if (file_exists($cache_filename_invoice)) {
    //echo "CACHE:\n<br />";
    $invoice_json = file_get_contents($cache_filename_invoice);
    $invoice = json_decode($invoice_json, true);
    $sales[$i]['Invoice'] = $invoice;
  } else if (file_exists($cache_filename_creditnote)) {
    //echo "CACHE:\n<br />";
    $invoice_json = file_get_contents($cache_filename_creditnote);
    $invoice = json_decode($invoice_json, true);
    $sales[$i]['CreditNote'] = $invoice;
  }
  
  // Not found in invoice nor credit note cache
  if (is_null($invoice)) {
    // Many more invoices than credit notes, not too expensive to ask and get a miss
    $invoice = get_endpoint('v1/' . $org_id . '/invoices/' . $sales[$i]['Guid'], $access_token);
    
    // Only cache useful results
    if (!isset($invoice['validationErrors'])) {
      $sales[$i]['Invoice'] = $invoice;
      $invoice_json = json_encode($invoice);
      file_put_contents($cache_filename_invoice, $invoice_json);
    } else {
      $invoice = get_endpoint('v1/' . $org_id . '/sales/creditnotes/' . $sales[$i]['Guid'], $access_token);
      $sales[$i]['CreditNote'] = $invoice;
      $invoice_json = json_encode($invoice);
      file_put_contents($cache_filename_creditnote, $invoice_json);
    }
  }  
}

//print_r($sales);
//die;

// Extract data
$data = array('FetchOptions' => array('start_date' => $start_date, 'end_date' => $end_date));

for ($i = 0; $i < count($sales); ++$i) {
  $sales_type = 'Invoice';
  
  if (!isset($sales[$i]['Invoice'])) {
    $sales_type = 'CreditNote';
  }
  
  $invoice = $sales[$i][$sales_type];
  $invoice_month = substr($invoice['Date'], 0, 7);

  for ($j = 0; $j < count($sales[$i][$sales_type]['ProductLines']); ++$j) {  
    $line = $sales[$i][$sales_type]['ProductLines'][$j];    
    
    if (strlen(trim($line['ProductGuid'])) == 0) {
      $line['ProductGuid'] = $line['Description'];
    }
    
    //$data['AccountTransactions'][$line['AccountNumber']]['AccountName'][$line['AccountName']] += 1;
    $data['AccountNames'][$line['AccountNumber']][$line['AccountName']] += 1;
    $data['ProductNames'][$line['ProductGuid']][$line['Description']] += 1;
    $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']][$line['ProductGuid']] += 1;
    $data['ContactNames'][$invoice['ContactGuid']][$invoice['ContactName']] += 1;

    
    $sales_info = array(
      'Number' => $invoice['Number'],
      
      'SalesType' => $sales_type,
      
      'CreditNoteFor' => $invoice['CreditNoteFor'],
      
      'ContactGuid' => $invoice['ContactGuid'],
      'ContactName' => $invoice['ContactName'],
      'OrderAddress' => $invoice['Address'],
      
      'ProductGuid' => $line['ProductGuid'],
      'ProductName' => $line['Description'],
      
      'Currency' => $invoice['Currency'],
      'ExternalReference' => $invoice['ExternalReference'],
      'Description' => $invoice['Description'],
      
      'InvoiceDate' => $invoice['Date'],
      'InvoicePaymentStatus' => $invoice['PaymentStatus'],

      'InvoiceTotalExclVat' => $invoice['TotalExclVat'],
      'InvoiceTotalVatableAmount' => $invoice['TotalVatableAmount'],
      'InvoiceTotalInclVat' => $invoice['TotalInclVat'],
      'InvoiceTotalTotalNonVatableAmount' => $invoice['TotalNonVatableAmount'],      
      'InvoiceTotalVat' => $invoice['TotalVat'],

      'BaseAmountValue' => $line['BaseAmountValue'],
      'BaseAmountValueInclVat' => $line['BaseAmountValueInclVat'],
      'Quantity' => $line['Quantity'],
      'TotalAmount' => $line['TotalAmount'],
      'TotalAmountInclVat' => $line['TotalAmountInclVat'],      
    );
    

    $data['AccountTransactions'][$line['AccountNumber']][$invoice_month]['ProductsSold']['ByProductThenContact'][$line['ProductGuid']]['Orders'][$invoice['ContactGuid']][] = $sales_info;
    $data['AccountTransactions'][$line['AccountNumber']][$invoice_month]['ProductsSold']['ByContactThenProduct'][$invoice['ContactGuid']]['Orders'][$line['ProductGuid']][] = $sales_info;
  }
}

print_r($data);

file_put_contents('data.json', json_encode($data));
?>

</body>
</html>

