<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!file_exists('export')) {
  mkdir('export', 0777, true);
}

if (!file_exists('cache-dinero-invoices')) {
  mkdir('cache-dinero-invoices', 0777, true);
}

//////////////////////////////////////////////////////////////////////


function get_access_token($client_id_secret, $apikey) {
  /*
  if (isset($_SESSION['token'][$client_id_secret][$apikey])) {
    //echo 'B*H';
    return $_SESSION['token'][$client_id_secret][$apikey];
  }
  */
  
  $url = 'https://authz.dinero.dk/dineroapi/oauth/token';
  
  $curl = curl_init();

  curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($curl, CURLOPT_USERPWD, $client_id_secret);
  
  $headers = array('Content-Type: application/x-www-form-urlencoded');
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

  $data = array('grant_type' => 'password',
                'scope' => 'read',
                'username' => $apikey,
                'password' => $apikey);
  
  curl_setopt($curl, CURLOPT_POST, count($data));
  curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

  curl_setopt($curl, CURLOPT_URL, $url);

  //return data instead of output
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  
  $result = curl_exec($curl);
  
  curl_close($curl);
  
  $access_token = json_decode($result, $assoc = true);
 
  //$_SESSION['token'][$client_id_secret][$apikey] = $access_token;

  return $access_token;
}


function get_endpoint($url, $access_token)
{
  $curl = curl_init();

  $headers = array('Content-Type: application/json',
                   'Accept: application/json',
                   'Authorization: Bearer ' . $access_token);
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

  curl_setopt($curl, CURLOPT_URL, 'https://api.dinero.dk/' . $url);
  //echo 'https://api.dinero.dk/' . $url;
  
  //return data instead of output
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  
  $result = curl_exec($curl);
  
  curl_close($curl);
  
  $result = json_decode($result, $assoc = true);
  
  return $result;
}

function get_endpoint_all_pages($url, $access_token) {
  $res = array();
  
  $PAGESIZE = 1000;
  $page = 0;
  
  do {
    $info = get_endpoint($url . '&page=' . $page . '&pageSize=' . $PAGESIZE, $access_token);
    //print_r($info);
    //die;
    $info = $info['Collection'];
    $res = array_merge($res, $info);
    ++$page;
    
    if ($page > 100) {
      die('More than 100 pages visited...');
    }
  } while (count($info) == $PAGESIZE);
  
  return($res);
}

