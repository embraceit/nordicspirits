<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Palleforsendelser</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body>

<?php
$POST_ID_LOWER_BOUND = 1850;
?>

<h1><code>billing_invoiceemail</code> vs <code>billing_email</code> for ordrer med id >= <?php echo $POST_ID_LOWER_BOUND; ?></h1>


<p>
Rækker highlighted:
Ordrestatus = 'wc-completed' og enten (tom &quot;Faktura e-mail&quot;) eller (&quot;Faktura e-mail&quot; forskellig fra &quot;Kontakt e-mail&quot;).
</p>
<?php

/*
SELECT      post_id,
            MAX(CASE WHEN meta_key = '_billing_email' THEN meta_value ELSE NULL END) _billing_email,
            MAX(CASE WHEN meta_key = '_billing_invoiceemail' THEN meta_value ELSE NULL END) _billing_invoiceemail
FROM        b2b_wp_postmeta
GROUP BY    post_id
ORDER BY    post_id;


SELECT t1.*, t2.post_status FROM (
  SELECT      post_id,
              MAX(CASE WHEN meta_key = '_billing_email' THEN meta_value ELSE NULL END) _billing_email,
              MAX(CASE WHEN meta_key = '_billing_invoiceemail' THEN meta_value ELSE NULL END) _billing_invoiceemail
  FROM        b2b_wp_postmeta
  GROUP BY    post_id
  ORDER BY    post_id
) t1
LEFT JOIN b2b_wp_posts AS t2 ON t1.post_id = t2.ID
ORDER BY    post_id
*/
$querystr = "
SELECT t1.*, t2.post_status FROM (
  SELECT      post_id,
              MAX(CASE WHEN meta_key = '_billing_email' THEN meta_value ELSE NULL END) _billing_email,
              MAX(CASE WHEN meta_key = '_billing_invoiceemail' THEN meta_value ELSE NULL END) _billing_invoiceemail
  FROM        $wpdb->postmeta
  WHERE post_id >= " . $POST_ID_LOWER_BOUND . "
  GROUP BY    post_id
) t1
INNER JOIN $wpdb->posts AS t2 ON t1.post_id = t2.ID
ORDER BY    post_id DESC
 ";
//LIMIT 10;

 $orders = $wpdb->get_results($querystr, ARRAY_A);
 
 //print_r($orders);
 
  echo "
    <table>
      <tr>
        <th>Ordrenr.</th>
        <th>Ordrestatus</th>
        <th>Kontakt e-mail</th>
        <th>Faktura e-mail</th>
      </tr>\n";
  
  foreach ($orders as $o) {
    $style = '';
    
    if ($o['post_status'] == 'wc-completed' && ((strlen($o['_billing_invoiceemail']) == 0) || ($o['_billing_invoiceemail'] != $o['_billing_email']))) {
      $style = ' style="background-color: red;"';
    }
    
    echo "
        <tr" . $style . ">
          <td>" . $o['post_id'] . "</td>
          <td>" . $o['post_status'] . "</td>
          <td>" . $o['_billing_email'] . "</td>
          <td>" . $o['_billing_invoiceemail'] . "</td>
        </tr>\n";  
  }
  
 
  echo "
    </table>
    \n";
?>

</body>
</html>

