<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}


/*
FIXME: KOLLI
*/

$month = (new DateTime())->format('Y-m');

if (isset($_GET['month'])) {
  $month = preg_replace('/[^0-9-]/', '', $_GET['month']);
}

$month_input = $month . '-01';
$d_input = new DateTime($month_input);
$DATE_FROM = $d_input->format('Y-m-01');
$DATE_TO = $d_input->format('Y-m-t');


////////////////////////////////////////////////////////////


$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

$FILENAME = 'report-' . $month . '---' . $dt->format('Y_m_d_His') . '.csv';
$filename = "./reports/" . $FILENAME;

////////////////////////////////////////////////////////////

require('../include-dinero.php');
$client_id_secret = 'Nordic Spirits IVS' . ':' . 'TojI4fWZePZhMi6zg7PfaKHys9gmTv0YRetYdIQaK4';
$apikey = '9361e8bbcd144a208ed715d2415489c6';
$token = get_access_token($client_id_secret, $apikey);
$access_token = $token['access_token'];
$org_id = '117742';



// cache each hour
$sales_cache_filename = 'cache-dinero-invoices/sales-' . $org_id . '-' . $DATE_FROM . '-' . $DATE_TO . date("H") . '00.json';
$sales = NULL;  
if (file_exists($sales_cache_filename)) {
  //echo "CACHE!\n";
  $sales_json = file_get_contents($sales_cache_filename);
  $sales = json_decode($sales_json, true);
} else {
  //echo "API!\n";
  $sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=' . $DATE_FROM . '&endDate=' . $DATE_TO . '&fields=Number,Guid,UpdatedAt,ContactName,Date,Description,Currency,TotalExclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
  $sales_json = json_encode($sales);
  file_put_contents($sales_cache_filename, $sales_json);
}

$data = array(
  'FetchOptions' => array('start_date' => $DATE_FROM, 'end_date' => $DATE_TO),
  'AccountNames' => array(),
  'ProductNames' => array(),
  'ProductGuids' => array(),
  'ContactNames' => array()
  );

foreach ($sales as $post) {
  $parsed = (int)preg_replace('/ordre[^0-9]*([0-9]+)/i', '${1}', $post['Description']);
  
  if ($parsed == 0) {
    continue;
  }
  
  $cache_filename = 'cache-dinero-invoices/invoices-' . $org_id . '-' . sprintf("%05d", $post['Number']) . '-' . $post['Guid'] . '-' . str_replace(':', '', $post['UpdatedAt']) . '.json';    
  $invoice = NULL;  
  if (file_exists($cache_filename)) {
    //echo "CACHE:\n<br />";
    $invoice_json = file_get_contents($cache_filename);
    $invoice = json_decode($invoice_json, true);
  } else {
    $endpoint_url = 'v1/' . $org_id . '/invoices/' . $post['Guid'];
    //echo "API: " . $endpoint_url . "\n<br />";
    $invoice = get_endpoint($endpoint_url, $access_token);
    
    // Only cache useful results
    if (isset($invoice['ProductLines'])) {
      $invoice_json = json_encode($invoice);
      file_put_contents($cache_filename, $invoice_json);
    }
  }
  
  if (isset($invoice['validationErrors'])) {
    // No need to include these as they are credit notes
    continue;
  }

  else if (!isset($invoice['ProductLines'])) {
    echo "\nInvoice ProductLines not set:\n";
    print_r($invoice);
    die;
  }

  $invoice_month = substr($invoice['Date'], 0, 7);

  for ($j = 0; $j < count($invoice['ProductLines']); ++$j) {  
    $line = $invoice['ProductLines'][$j];    
    
    if (strlen(trim($line['ProductGuid'])) == 0) {
      $line['ProductGuid'] = $line['Description'];
    }
    
    //$data['AccountTransactions'][$line['AccountNumber']]['AccountName'][$line['AccountName']] += 1;

    if (!isset($data['AccountNames'][$line['AccountNumber']])) {
      //$data['AccountNames'] = array($line['AccountNumber'] => array());
      $data['AccountNames'][$line['AccountNumber']] = array($line['AccountName'] => 0);
    }     
    if (!isset($data['ProductNames'][$line['ProductGuid']])) {
      //$data['ProductNames'] = array($line['ProductGuid'] => array());
      $data['ProductNames'][$line['ProductGuid']] = array($line['Description'] => 0);
    }     
    if (!isset($data['ProductGuids'][$line['Description']])) {
      //$data['ProductGuids'] = array($line['Description'] => array());
      $data['ProductGuids'][$line['Description']] = array($line['BaseAmountValue'] => array());
      $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']] = array($line['ProductGuid'] => 0);
    } else if (!isset($data['ProductGuids'][$line['Description']][$line['BaseAmountValue']])) {
      //$data['ProductGuids'][$line['Description']] = array($line['BaseAmountValue'] => array());
      $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']] = array($line['ProductGuid'] => 0);
    }    
    if (!isset($data['ContactNames'][$invoice['ContactGuid']])) {
      //$data['ContactNames'] = array($invoice['ContactGuid'] => array());
      $data['ContactNames'][$invoice['ContactGuid']] = array($invoice['ContactName'] => 0);
    }
    

    $data['AccountNames'][$line['AccountNumber']][$line['AccountName']] += 1;
    $data['ProductNames'][$line['ProductGuid']][$line['Description']] += 1;
    $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']][$line['ProductGuid']] += 1;
    $data['ContactNames'][$invoice['ContactGuid']][$invoice['ContactName']] += 1;

    
    $sales_info = array(
      'Number' => $invoice['Number'],
      
      'ContactGuid' => $invoice['ContactGuid'],
      'ContactName' => $invoice['ContactName'],
      'OrderAddress' => $invoice['Address'],
      
      'ProductGuid' => $line['ProductGuid'],
      'ProductName' => $line['Description'],
      
      'Currency' => $invoice['Currency'],
      'ExternalReference' => $invoice['ExternalReference'],
      'Description' => $invoice['Description'],
      
      'InvoiceDate' => $invoice['Date'],
      'InvoicePaymentStatus' => $invoice['PaymentStatus'],

      'InvoiceTotalExclVat' => $invoice['TotalExclVat'],
      'InvoiceTotalVatableAmount' => $invoice['TotalVatableAmount'],
      'InvoiceTotalInclVat' => $invoice['TotalInclVat'],
      'InvoiceTotalTotalNonVatableAmount' => $invoice['TotalNonVatableAmount'],      
      'InvoiceTotalVat' => $invoice['TotalVat'],

      'BaseAmountValue' => $line['BaseAmountValue'],
      'BaseAmountValueInclVat' => $line['BaseAmountValueInclVat'],
      'Quantity' => $line['Quantity'],
      'TotalAmount' => $line['TotalAmount'],
      'TotalAmountInclVat' => $line['TotalAmountInclVat'],      
    );
    

    $data['AccountTransactions'][$line['AccountNumber']][$invoice_month]['ProductsSold']['ByProductThenContact'][$line['ProductGuid']]['Orders'][$invoice['ContactGuid']][] = $sales_info;
    $data['AccountTransactions'][$line['AccountNumber']][$invoice_month]['ProductsSold']['ByContactThenProduct'][$invoice['ContactGuid']]['Orders'][$line['ProductGuid']][] = $sales_info;
  }
}

//print_r($data);
//die;

// Only DKK!
foreach ($data['AccountTransactions'] as $account_number => $sales) {
  if ($account_number == 'A') {
    continue;
  }
  
  foreach ($sales as $month => $types) {
    foreach ($types['ProductsSold'] as $type_key => $type_items) {
      foreach ($type_items as $item_guid => $orders) {        
        foreach ($orders['Orders'] as $subkeyguid => $order_lines) {
          foreach ($order_lines as $order_line) {
            
            if ($order_line['Currency'] != "DKK") {
              print_r($order_line);
              die('Forventede kun DKK valuta');            
            }
          }
        }
      }
    }
  }
} 



// http://stackoverflow.com/a/2021729/7720448
function to_filename($str) {
  $str = preg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $str);
  $str = preg_replace("([\.]{2,})", '', $str);
  return $str;
}

function format_money($x) {
  if (strlen($x) == 0) {
    return '';
  }
  
  if ($x === 'NA') {
    return $x;
  }

  return number_format($x, $decimals = 2, $dec_point = ",", $thousands_sep = ".");
}

function format_number_csv($x) {
  if (strlen($x) == 0) {
    return '';
  }

  if ($x === 'NA') {
    return $x;
  }
  
  return number_format($x, $decimals = 2, $dec_point = ",", $thousands_sep = "");
}

function cmp_rawdata($a, $b) {
  $a_d = strtotime($a['date']);
  $b_d = strtotime($b['date']);
  return ($a_d < $b_d);
}



ksort($data['AccountTransactions'], SORT_REGULAR);

foreach ($data['AccountTransactions'] as $account_number => &$sales) { 
  krsort($sales, SORT_REGULAR);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
print_r($data['AccountNames']);
print_r($data['ProductNames']);
print_r($data['ProductGuids']);
print_r($data['ContactNames']);
die;
*/

$csv_details = "Ordredato;Måned;DineroKontoNr;DineroKontoNavn;Produkt;Kunde;Adresse;Postnr. GÆT;Antal;Totalpris excl. moms;Gns. stykpris;Antal til 0 DKK\n";

foreach ($data['AccountTransactions'] as $account_number => $sales) {
  if ($account_number == 'A') {
    continue;
  }

  $account_name = array_keys($data['AccountNames'][$account_number])[0];
  
  $csv_prefix = $account_number . ' - ' . to_filename($account_name);

  $rawdata = array(); 
  
  foreach ($sales as $month => $types) {
    $rawdata[$month] = array();
    
    if (count($types) == 0 || !isset($types['ProductsSold']) || count($types['ProductsSold']) == 0) {
      continue;
    }
    
    foreach ($types['ProductsSold']['ByProductThenContact'] as $item_guid => $orders) {        
      foreach ($orders['Orders'] as $subkeyguid => $order_lines) {
        foreach ($order_lines as $order_line) {
        
          /*
          print_r($data['ProductNames']);
          var_dump($item_guid);
          var_dump($subkeyguid);
          die;
          */
          //print_r($data['ProductNames']);
          //die;
          //$sgn = ($order_line['SalesType'] == 'CreditNote') ? -1 : 1;
          $sgn = 1;
          $cust = array_keys($data['ProductNames'][$item_guid])[0];
          $prod = array_keys($data['ContactNames'][$subkeyguid])[0];
          
          if ($order_line['Currency'] != "DKK") {
              print_r($order_line);
              die('Forventede kun DKK valuta');
          }
          
          //print_r($order_line);
          
          $total_cost = 0;
          $quantity_cost = 0;
          $quantity_free = 0;          
          
          if (abs($order_line['TotalAmount']) < 0.01) { // Dinero is rounded to two decimals!
            $quantity_free = $sgn * $order_line['Quantity'];
          } else {
            $total_cost = $sgn * $order_line['TotalAmount'];
            $quantity_cost = $sgn * $order_line['Quantity'];          
          }
                    
          $rawdata[$month][] = array(
                         'date' => $order_line['InvoiceDate'], 
                         'prod' => $prod,
                         'cust' => $cust, 
                         'addr' => $order_line['OrderAddress'],
                         'quantity_cost' => $quantity_cost,
                         'quantity_free' => $quantity_free,
                         'total_cost' => $total_cost);
        }
      }
    }    
  }

  $total_total_cost = 0;
  $total_quantity_cost = 0;
  $total_quantity_free = 0;
  
  foreach ($rawdata as $month => $sales) {
    usort($sales, "cmp_rawdata");
    
    foreach ($sales as $s) {
      $total_total_cost += $s['total_cost'];
      $total_quantity_cost += $s['quantity_cost'];
      $total_quantity_free += $s['quantity_free'];
      $addr = str_replace("\n", ', ', str_replace("\r", '', $s['addr']));
      $postnrguess = preg_replace('/^.*([0-9]{4}).*$/', '${1}', $addr);
      
      $gnspris_CSV = ($s['quantity_cost'] == 0) ? 'NA' : (format_number_csv($s['total_cost'] / $s['quantity_cost']));

      $csv_details .= $s['date'] . ';' . $month . ';' . $account_number . ';' . $account_name . ';' . $s['prod'] . ';' . $s['cust'] . ';' . $addr . ';' . $postnrguess . ';' . $s['quantity_cost'] . ';' . format_number_csv($s['total_cost']) . ';' . $gnspris_CSV . ';' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . "\n";    
    }
  }  
}

//$csv_details = iconv("UTF-8", "ISO-8859-1", $csv_details);  
$csv_details = utf8_decode($csv_details);
file_put_contents($filename, $csv_details);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////


$quoted = sprintf('"%s"', addcslashes(basename($FILENAME), '"\\'));
$size   = filesize($filename);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $quoted); 
header('Content-Transfer-Encoding: binary');
header('Connection: Keep-Alive');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . $size);

readfile($filename);
