<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

error_reporting(E_ALL);


$reason_map = array(
  '0' => 'Binding changed',
  '1' => 'Physical destocking for transfer to new SKU',
  '2' => 'Saleable destocking for transfer to new SKU',
  '3' => 'Physical stock entry for transfer to new SKU',
  '4' => 'Saleable stock entry for transfer to new SKU',
  '5' => 'Stock entry because of error',
  '6' => 'Destocking because of error',
  '7' => 'Goods reception',
  '8' => 'Damaged in stock',
  '9' => 'Damaged return from customer',
  '10' => 'Damaged on arrival',
  '11' => 'Shipped/Dispatched',
  '12' => 'Customer returns',
  '13' => 'Stock entry on client request',
  '14' => 'Destocking on client request',
  '15' => 'Stock entry because of goods found',
  '16' => 'Destocking because of goods lost'
);


require 'out/InventoryService_V1_1.php';

$authid_prod = 'A9C311DC-F6D2-4C54-95C0-F47E2C908D0F';
$date_from = '2017-01-01';
$stock_pars = new Get_StockTransactions($authid_prod, $date_from);

$sas = array();

try {    
  $client = new InventoryService_V1_1($options = array('url' => 'http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc', 'trace' => 1));
  $stock_trans = $client->Get_StockTransactions($stock_pars);
  
  foreach ($stock_trans->Get_StockTransactionsResult->StockTransaction as $o) {
    $sas[] = (array)$o;
  }

} catch(Exception $fault){
  die("Error");
  /*
  var_dump($client->__getLastRequestHeaders());
  var_dump($client->__getLastRequest());
  var_dump($client->__getLastResponseHeaders());
  var_dump($client->__getLastResponse());
  echo $fault->getMessage();
  print_r($fault->detail);
  */
  die;
}

$n = 0;

$out = "Date;Time;EAN;ChangeInStock;CurrentlyInStock;Reason1Txt;Reason2Txt;Reason1Code;Reason2Code;DateTimeOriginal\n";

    
foreach ($sas as $a) {
  if ($n >= 100) {
    break;
  }
  
  $mth = substr($a['Date'], 0, 7);
  
  if (strpos($a['Date'], '.') !== false) {        
    $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s.u", $a['Date']);
  } else {
    $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s", $a['Date']);
  }
  
  $a['new_Date'] = $a['Dato']->format('Y-m-d');
  $a['new_Time'] = $a['Dato']->format('H:i');
  
  $reason1 = 'UNKNOWN';  
  if (isset($reason_map[$a['StockTransactionDetailReasonID']])) {
    $reason1 = $reason_map[$a['StockTransactionDetailReasonID']];
  }
  
  $reason2 = 'UNKNOWN';  
  if (isset($a['StockTransactionReasonID'])) {
    if ($a['StockTransactionReasonID'] === 2) {
      $reason2 = "Binding_changed";
    } else if ($a['StockTransactionReasonID'] === 3) {
      $reason2 = "Inventory_changed";
    }
  }

  
  $out .= $a['new_Date'] . ";" . 
    $a['new_Time'] . ";" . 
    $a["EAN"] . ";" . 
    $a["ChangeInStock"] . ";" . 
    $a["InStock"] . ";" . 
    $reason1 . ";" .
    $reason2 . ";" .
    $a["StockTransactionDetailReasonID"] . ";" . 
    $a["StockTransactionReasonID"] . ";" . 
    $a["Date"] . "\n";
    
  //print_r($a);  
  
  $n++;
}

//echo $out;

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename=DEL.csv');
header('Pragma: no-cache');
echo $out;

