<?php

class Get_OrderChangesResponse
{

    /**
     * @var OrderChange[] $Get_OrderChangesResult
     * @access public
     */
    public $Get_OrderChangesResult = null;

    /**
     * @param OrderChange[] $Get_OrderChangesResult
     * @access public
     */
    public function __construct($Get_OrderChangesResult)
    {
      $this->Get_OrderChangesResult = $Get_OrderChangesResult;
    }

}
