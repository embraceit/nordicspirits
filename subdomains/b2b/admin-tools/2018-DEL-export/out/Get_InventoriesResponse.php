<?php

class Get_InventoriesResponse
{

    /**
     * @var Inventory[] $Get_InventoriesResult
     * @access public
     */
    public $Get_InventoriesResult = null;

    /**
     * @param Inventory[] $Get_InventoriesResult
     * @access public
     */
    public function __construct($Get_InventoriesResult)
    {
      $this->Get_InventoriesResult = $Get_InventoriesResult;
    }

}
