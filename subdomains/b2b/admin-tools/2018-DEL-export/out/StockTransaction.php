<?php

class StockTransaction
{

    /**
     * @var int $ChangeInStock
     * @access public
     */
    public $ChangeInStock = null;

    /**
     * @var dateTime $Date
     * @access public
     */
    public $Date = null;

    /**
     * @var string $EAN
     * @access public
     */
    public $EAN = null;

    /**
     * @var int $InStock
     * @access public
     */
    public $InStock = null;

    /**
     * @var int $StockTransactionDetailReasonID
     * @access public
     */
    public $StockTransactionDetailReasonID = null;

    /**
     * @var int $StockTransactionReasonID
     * @access public
     */
    public $StockTransactionReasonID = null;

    /**
     * @param int $ChangeInStock
     * @param dateTime $Date
     * @param int $InStock
     * @param int $StockTransactionDetailReasonID
     * @param int $StockTransactionReasonID
     * @access public
     */
    public function __construct($ChangeInStock, $Date, $InStock, $StockTransactionDetailReasonID, $StockTransactionReasonID)
    {
      $this->ChangeInStock = $ChangeInStock;
      $this->Date = $Date;
      $this->InStock = $InStock;
      $this->StockTransactionDetailReasonID = $StockTransactionDetailReasonID;
      $this->StockTransactionReasonID = $StockTransactionReasonID;
    }

}
