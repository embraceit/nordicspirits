<?php

include_once('Inventory.php');
include_once('CustomExpMsg.php');
include_once('OrderChange.php');
include_once('Product.php');
include_once('StockTransaction.php');
include_once('Stock.php');
include_once('Get_Inventories.php');
include_once('Get_InventoriesResponse.php');
include_once('Get_OrderChanges.php');
include_once('Get_OrderChangesResponse.php');
include_once('Send_Products.php');
include_once('Send_ProductsResponse.php');
include_once('Get_StockTransactions.php');
include_once('Get_StockTransactionsResponse.php');
include_once('Get_Stock.php');
include_once('Get_StockResponse.php');

class InventoryService_V1_1 extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Inventory' => '\Inventory',
      'CustomExpMsg' => '\CustomExpMsg',
      'OrderChange' => '\OrderChange',
      'Product' => '\Product',
      'StockTransaction' => '\StockTransaction',
      'Stock' => '\Stock',
      'Get_Inventories' => '\Get_Inventories',
      'Get_InventoriesResponse' => '\Get_InventoriesResponse',
      'Get_OrderChanges' => '\Get_OrderChanges',
      'Get_OrderChangesResponse' => '\Get_OrderChangesResponse',
      'Send_Products' => '\Send_Products',
      'Send_ProductsResponse' => '\Send_ProductsResponse',
      'Get_StockTransactions' => '\Get_StockTransactions',
      'Get_StockTransactionsResponse' => '\Get_StockTransactionsResponse',
      'Get_Stock' => '\Get_Stock',
      'Get_StockResponse' => '\Get_StockResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param Get_Inventories $parameters
     * @access public
     * @return Get_InventoriesResponse
     */
    public function Get_Inventories(Get_Inventories $parameters)
    {
      return $this->__soapCall('Get_Inventories', array($parameters));
    }

    /**
     * @param Get_OrderChanges $parameters
     * @access public
     * @return Get_OrderChangesResponse
     */
    public function Get_OrderChanges(Get_OrderChanges $parameters)
    {
      return $this->__soapCall('Get_OrderChanges', array($parameters));
    }

    /**
     * @param Send_Products $parameters
     * @access public
     * @return Send_ProductsResponse
     */
    public function Send_Products(Send_Products $parameters)
    {
      return $this->__soapCall('Send_Products', array($parameters));
    }

    /**
     * @param Get_StockTransactions $parameters
     * @access public
     * @return Get_StockTransactionsResponse
     */
    public function Get_StockTransactions(Get_StockTransactions $parameters)
    {
      return $this->__soapCall('Get_StockTransactions', array($parameters));
    }

    /**
     * @param Get_Stock $parameters
     * @access public
     * @return Get_StockResponse
     */
    public function Get_Stock(Get_Stock $parameters)
    {
      return $this->__soapCall('Get_Stock', array($parameters));
    }

}
