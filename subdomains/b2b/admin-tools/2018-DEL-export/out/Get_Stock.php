<?php

class Get_Stock
{

    /**
     * @var guid $AuthenticationID
     * @access public
     */
    public $AuthenticationID = null;

    /**
     * @var string[] $EANs
     * @access public
     */
    public $EANs = null;

    /**
     * @param guid $AuthenticationID
     * @param string[] $EANs
     * @access public
     */
    public function __construct($AuthenticationID, $EANs)
    {
      $this->AuthenticationID = $AuthenticationID;
      $this->EANs = $EANs;
    }

}
