<?php

class Get_StockTransactions
{

    /**
     * @var guid $AuthenticationID
     * @access public
     */
    public $AuthenticationID = null;

    /**
     * @var dateTime $AfterDate
     * @access public
     */
    public $AfterDate = null;

    /**
     * @param guid $AuthenticationID
     * @param dateTime $AfterDate
     * @access public
     */
    public function __construct($AuthenticationID, $AfterDate)
    {
      $this->AuthenticationID = $AuthenticationID;
      $this->AfterDate = $AfterDate;
    }

}
