<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

$month = (new DateTime())->format('Y-m');

if (isset($_GET['month'])) {
  $month = preg_replace('/[^0-9-]/', '', $_GET['month']);
}

$month_input = $month . '-01';
$d_input = new DateTime($month_input);
$DATE_FROM = $d_input->format('Y-m-01');
$DATE_TO = $d_input->format('Y-m-t');

////////////////////////////////////////////////////////////

$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

$FILENAME = 'report-' . $month . '-' . $dt->format('Y_m_d_His') . '.csv';

$filename = "./reports/" . $FILENAME;


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

/*
var_dump($DATE_FROM);
var_dump($DATE_TO);
die;
*/

$orders = get_posts( array(
    'numberposts' => -1,
    'post_type'   => wc_get_order_types(),
    'post_status' => 'wc-completed',      
    'orderby'     => 'date',
    'order'       => 'DESC',
    'date_query'  => array(
        'column'  => 'post_date',
        'before'  => $DATE_TO,
        'after'   => $DATE_FROM
    )
));

$CONTENT = 'OrdreId;KundeId;KundeNavn;Adresse1;Postnr;By;CVR;ProduktId;ProduktNavn;ProduktVariantId;SKU;ProduktKolli;ProduktStk;Producent;';

$data = array();

foreach ($orders as $post) {
  $order_id = $post->ID;
  $order = new WC_Order($order_id);
  
  $customer_user_id = get_post_meta($order->post->ID, '_customer_user', true );
  $customer_user = get_user_by('id', $customer_user_id);
  $customer_user_roles = implode(';', $customer_user->roles);
  $customer_company = get_post_meta($order->post->ID, '_billing_company', true);
  $customer_city = get_post_meta($order->post->ID, '_billing_city', true);
  $customer_postcode = get_post_meta($order->post->ID, '_billing_postcode', true);
  $customer_address_1 = get_post_meta($order->post->ID, '_billing_address_1', true);
  $customer_cvr = get_post_meta($order->post->ID, '_billing_cvr', true);
  
  $items = $order->get_items();
  
  $items_simple = array();
  
  foreach ($items as $item) {
    $product_id = $item['product_id'];
    $product = new WC_Product($product_id);
    $product_name = $item['name'];
    $product_qty = $item['qty'];
    $variation_id = $item['variation_id'];
    
    if ($variation_id != 0) {
      $product = new WC_Product_Variation($variation_id);
    }
    
    //$qty_mult = 1;
    $is_colli = false;
    
    if (stripos($product_name, 'kolli') !== false) {
      //$qty_mult = 6;
      $is_colli = true;
    }
    
    //print_r($item);
    
    if ($product_qty > 0) {
      $items_simple[] = array(
        'product_id' => $product_id,
        'subtotal' => $item['subtotal'],
        'subtotal_tax' => $item['subtotal_tax'],
        'total' => $item['total'],
        'total_tax' => $item['total_tax'],
        'sku' => $product->get_sku(),
        'variation_id' => $variation_id,
        'name' => $product_name,
        'cats' => strip_tags($product->get_categories(';')),
        'producent' => $product->get_attribute('producent'),
        'is_colli' => $is_colli,
        'qty' => $product_qty);
    }
  }
  
  $data[$order_id] = array(
    'order_date' => substr($order->order_date, 0, 10),
    'shipping_method' => $shipping_method, 
    'order_id' => $order_id,

    'subtotal' => $order->subtotal,
    'subtotal_tax' => $order->subtotal_tax,
    'total' => $order->total,
    'total_tax' => $order->total_tax,
    'discount_total' => $order->discount_total,
    'discount_total_tax' => $order->discount_total_tax,

    'customer_user_id' => $customer_user_id,
    //'customer_user' => $customer_user,
    'customer_user_roles' => $customer_user_roles,
    'customer_company' => $customer_company,
    'customer_city' => $customer_city,
    'customer_postcode' => $customer_postcode,
    'customer_address_1' => $customer_address_1,
    'customer_cvr' => $customer_cvr,
    'items_simple' => $items_simple);

}

//file_put_contents('ns-b2b-dk.json', json_encode($data));

die;

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

$quoted = sprintf('"%s"', addcslashes(basename($FILENAME), '"\\'));
$size   = filesize($filename);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $quoted); 
header('Content-Transfer-Encoding: binary');
header('Connection: Keep-Alive');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . $size);

readfile($filename);
