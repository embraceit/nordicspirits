<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

/*
if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}
*/

$PCS_IN_COLLI = 6;
$DATE_FROM = date('Y-m-01', strtotime("-6 months"));
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Palleforsendelser</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<h1>Gennemførte ordrer med &quot;palle&quot; i fragtmådebeskrivelse eller i kundebemærkning fra <?php echo $DATE_FROM; ?> til dags dato</h1>
<?php

if ($PCS_IN_COLLI != 6) {
  die('$PCS_IN_COLLI != 6');
}

$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

echo '<p>Genereret ' . $dt->format('d-m-Y H:i:s') . '.</p>';
echo '<p>Det antages, at et kolli består af ' . $PCS_IN_COLLI . ' enkeltstk. og at alle produkter afsendes i kolli á 6 så snart det giver mening (fx 9 stk. konverteres til 1 kolli + 3 enkeltstk.).</p>';


?>
<?php
  $orders = get_posts( array(
      'numberposts' => -1,
      'post_type'   => wc_get_order_types(),
      'post_status' => 'wc-completed',      
      'orderby'     => 'date',
      'order'       => 'DESC',
      'date_query'  => array(
          'column'  => 'post_date',
          'after'   => $DATE_FROM
      )
  ));
  
  function get_shipping_methods($order) {
    $shipping = $order->get_shipping_methods();
    $shipping_methods = array();    
    foreach ($shipping as $s) {
      $shipping_methods[] = $s['method_title'];
    }
    
    $shipping_methods = array_unique($shipping_methods);
    
    if (count($shipping_methods) != 1) {
      die('Order had more than 1 shipping method.');
    }
    
    return($shipping_methods[0]);
  }
  
  $overview_paller = array();
  $shipping_methods = array();
  
  foreach ($orders as $post) {
    $order_id = $post->ID;
    $order = new WC_Order($order_id);
    $shipping_method = get_shipping_methods($order);
    $order_note = $order->get_customer_note();
    
    $shipping_methods[$shipping_method] = true;
    
    //if ($shipping_method != 'På palle') {
    if (stripos($shipping_method, 'palle') === false && stripos($order_note, 'palle') === false) {
      continue;
    }
    
    $order_month = substr($order->order_date, 0, 7); // order_date vs modified_date
    $items = $order->get_items();
    
    $company = get_post_meta($order->post->ID, '_billing_company', true) . ' (' . get_post_meta($order->post->ID, '_billing_city', true) . ')';

    $items_simple = array();
    
    foreach ($items as $item) {
      $product_id = $item['product_id'];
      $product_name = $item['name'];
      $product_qty = $item['qty'];
      $variation_id = $item['variation_id'];
      
      //$qty_mult = 1;
      $is_colli = false;
      
      if (stripos($product_name, 'kolli') !== false) {
        //$qty_mult = 6;
        $is_colli = true;
      } else if ($product_qty > $PCS_IN_COLLI) {
        // Add an extra colli item
        $qty_colli = floor($product_qty / $PCS_IN_COLLI);
        
        // $product_qty > $PCS_IN_COLLI so $qty_colli >= 1
        
        $items_simple[] = array(
          'product_id' => $product_id,
          'variation_id' => $variation_id,
          'name' => $product_name,
          'is_colli' => true,
          'qty' => $qty_colli);
          
        $product_qty = $product_qty - ($qty_colli * $PCS_IN_COLLI);
      }

      $items_simple[] = array(
        'product_id' => $product_id,
        'variation_id' => $variation_id,
        'name' => $product_name,
        'is_colli' => $is_colli,
        'qty' => $product_qty);
    }

    
    $overview_paller[$order_month][$order_id] = array(
      'order_date' => substr($order->order_date, 0, 10),
      'shipping_method' => $shipping_method, 
      'order_id' => $order_id,
      'company' => $company,
      'items_simple' => $items_simple);

  }
  
  //print_r($shipping_methods);
  //die;
  //print_r($overview);

  echo "
    <table>
      <tr>
        <th>Kunde</th>
        <th>Ordredato</th>
        <th>Ordrenr.</th>
        <th>Totalstk.</th>
        <th>Heraf enkelt</th>
        <th>Heraf kolli</th>
      </tr>\n";
      
  foreach ($overview_paller as $month => $sales) {
    echo "
        <tr>
          <th style=\"background-color: #efefef;\"></th>
          <th colspan=\"5\" style=\"background-color: #efefef; text-align: center;\">" . $month . "</th>
        </tr>\n";

    foreach ($sales as $order_id => $info) {
      $tot = 0;
      $pcs = 0;
      $pcs_details = array();
      $kolli = 0;
      $kolli_details = array();
      
      foreach ($info['items_simple'] as $item) {
        if ($item['is_colli'] === true) {
          $kolli += $item['qty'];
          $kolli_details[] = $item['qty'] . ' x ' . $item['name'];

          $tot += $item['qty'] * $PCS_IN_COLLI;
        } else {
          $pcs += $item['qty'];
          $pcs_details[] = $item['qty'] . ' x ' . $item['name'];
          
          $tot += $item['qty'];
        }
      }
      
      $pcs_details_str = implode(', ', $pcs_details);
      $kolli_details_str = implode(', ', $kolli_details);

      echo "
          <tr>
            <td>" . $info['company'] . "</td>
            <td>" . $info['order_date'] . "</td>
            <td>" . $order_id . "</td>
            <td>" . $tot . "</td>
            <td>" . $pcs . "</td>
            <td>" . $kolli . "</td>
          </tr>\n";
      /*
      echo "  <li><strong>" . $order_id . "</strong> @ " . $info['order_date'] . ": " . $info['company'] . ": <strong>" . $tot . " stk. i alt</strong> fordelt på <strong>" . $pcs . " enkeltstk.</strong> og <strong>" . $kolli . " kolli</strong> [" . $info['shipping_method'] . "]</li>\n";      
      echo "  <ul>\n";
      echo ($pcs == 0) ? '' : "    <li>Enkeltstk.: " . $pcs_details_str . "</li>\n";
      echo ($kolli == 0) ? '' : "    <li>Kolli: " . $kolli_details_str . "</li>\n";
      echo "  </ul>\n";
      */
    }
    
    /*
    echo "<ul>\n";
    foreach ($sales as $order_id => $info) {
      $tot = 0;
      $pcs = 0;
      $pcs_details = array();
      $kolli = 0;
      $kolli_details = array();
      
      foreach ($info['items_simple'] as $item) {
        if ($item['is_colli'] === true) {
          $kolli += $item['qty'];
          $kolli_details[] = $item['qty'] . ' x ' . $item['name'];

          $tot += $item['qty'] * $PCS_IN_COLLI;
        } else {
          $pcs += $item['qty'];
          $pcs_details[] = $item['qty'] . ' x ' . $item['name'];
          
          $tot += $item['qty'];
        }
      }
      
      $pcs_details_str = implode(', ', $pcs_details);
      $kolli_details_str = implode(', ', $kolli_details);
      
      echo "  <li><strong>" . $order_id . "</strong> @ " . $info['order_date'] . ": " . $info['company'] . ": <strong>" . $tot . " stk. i alt</strong> fordelt på <strong>" . $pcs . " enkeltstk.</strong> og <strong>" . $kolli . " kolli</strong> [" . $info['shipping_method'] . "]</li>\n";      
      echo "  <ul>\n";
      echo ($pcs == 0) ? '' : "    <li>Enkeltstk.: " . $pcs_details_str . "</li>\n";
      echo ($kolli == 0) ? '' : "    <li>Kolli: " . $kolli_details_str . "</li>\n";
      echo "  </ul>\n";
      
    }
    echo "</ul>\n";
    */
  }
  
  echo "</table>\n";
?>

</body>
</html>

