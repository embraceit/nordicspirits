<?php
///////////////////////////////////
// E-CONOMIC
///////////////////////////////////
// https://github.com/e-conomic/eco-api-ex/blob/master/examples/REST/PHP%20cURL/restcurl.php
/*
X-AgreementGrantToken: T2Lq7uYKYnJy9MP3WUaWZsRgz4KHehzJNxg1eaRZfxo1
*/
function get_endpoint($service_url) {
  $curl = curl_init($service_url);
  
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      	'X-AppSecretToken:XA034wvkfupBERIEWHE6VYlEAETFqyA2R6EOEUp0h801',
      	'X-AgreementGrantToken:T2Lq7uYKYnJy9MP3WUaWZsRgz4KHehzJNxg1eaRZfxo1',
      	'Content-Type:application/json'
      ));
  $curl_response = curl_exec($curl);
  if ($curl_response === false) {
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('Error occured during curl exec. Additional info: ' . var_export($info));
  }
  curl_close($curl);
  $decoded = json_decode($curl_response);
  if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
      die('Error occured: ' . $decoded->response->errormessage);
  }
  
  /*
  if ($decoded->pagination->results > 1000) {
    die('More than 1000 results; only 1000 fetched');
  }
  */
  
  return $decoded;
}

function get_endpoint_all_pages($url) {
  $res = array();
  
  $PAGESIZE = 1000;
  $page = 0;
  
  $append_char = "?";
  
  if (strpos($url, '?') !== false) {
    $append_char = "&";
  }
  
  do {
    $url_new = $url . $append_char . 'skippages=' . $page . '&pagesize=' . $PAGESIZE;
    $info = get_endpoint($url_new);      
    $info = $info->collection;
    //print_r($info);
    $res = array_merge($res, $info);
    ++$page;
    
    if ($page > 10) {
      die('More than 10 pages visited...');
    }
  } while (count($info) == $PAGESIZE);    
  
  return($res);
}

