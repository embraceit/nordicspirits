<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

function show_money($x) {
  return number_format($x, 0, ',', '.');
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>
	<!--
	<link rel='stylesheet' id='buttons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/buttons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='dashicons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='login-css'  href='http://b2b.nordicspirits.dk/wp-admin/css/login.min.css?ver=4.4.2' type='text/css' media='all' />
  -->

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		#login h1, #login h2 {
		  margin-bottom: 1em;
		}
		#login {
		  width: 500px;
		  padding-top: 2em;
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 10px;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<?php
  ///////////////////////////////////
  // WOOCOMMERCE
  ///////////////////////////////////
  $MIN_WOOC_ORDER_ID = 2922;
  
  $orders = get_posts( array(
      'numberposts' => -1,
      'post_type'   => wc_get_order_types(),
      'post_status' => 'wc-completed',
      'date_query'    => array(
        'column'  => 'post_date',
        'after'   => '2018-03-01'
      )
      //'post_status' => wc_get_order_statuses()//array('wc-completed', 'wc-processing')
  ));

  
  // Mail AS 2018-04-12
  $exclude_ids = array(2952, 2950, 2949, 2945, 2940, 2939, 2936, 2934, 2933, 2932, 2929, 2926, 2924, 2923,
      // Mail AS 2018-04-17
      2922, 2993
  );
  
  $woo_order_ids = array();
  
  foreach ($orders as $post) {
    $woo_id = (int)($post->ID);
    
    if (!($woo_id >= $MIN_WOOC_ORDER_ID)) {
      continue;
    }

    if (in_array($woo_id, $exclude_ids)) {
      continue;
    }
        
    $woo_order_ids[] = $woo_id;
  }
    
  ///////////////////////////////////
  // DINERO
  ///////////////////////////////////
  /*
  require('../include-dinero.php');
    
  $client_id_secret = 'Nordic Spirits IVS' . ':' . 'TojI4fWZePZhMi6zg7PfaKHys9gmTv0YRetYdIQaK4';
  $apikey = '9361e8bbcd144a208ed715d2415489c6';

  $token = get_access_token($client_id_secret, $apikey);
  $access_token = $token['access_token'];

  $org_id = '117742';

  $start_date = '2016-01-01';
  $end_date = date("Y-m-d");//'2017-03-31';
  
  //$sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=' . $start_date . '&endDate=' . $end_date . '&fields=Number,Guid,ContactName,Date,Description,Currency,TotalExclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
  $sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=' . $start_date . '&endDate=' . $end_date . '&fields=Number,Guid,ContactName,Date,Description,Currency,TotalExclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
  //print_r($sales);
  
  $dinero_info = array();
  $dinero_order_ids = array();
  $missing = array();
  
  foreach ($sales as $post) {
    //$parsed = (int)preg_replace('/^[^0-9]*([0-9]+).*$/i', '${1}', $post['Description']);
    $parsed = (int)preg_replace('/ordre[^0-9]*([0-9]+)/i', '${1}', $post['Description']);
    
    $dinero_info[] = array('dinero' => $post['Number'], 
                           'woo' => $post['Description'], 
                           'woo_parsed' => $parsed);
    
    $dinero_order_ids[] = $parsed;

    if (!in_array($parsed, $woo_order_ids)) {
      $missing[] = $dinero_info;
    }
  }
  */
  ///////////////////////////////////
  
  ///////////////////////////////////
  // E-CONOMIC
  ///////////////////////////////////
  // https://github.com/e-conomic/eco-api-ex/blob/master/examples/REST/PHP%20cURL/restcurl.php
  /*
  X-AgreementGrantToken: T2Lq7uYKYnJy9MP3WUaWZsRgz4KHehzJNxg1eaRZfxo1
  */
  function get_endpoint($service_url) {
	  $curl = curl_init($service_url);
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	      	'X-AppSecretToken:XA034wvkfupBERIEWHE6VYlEAETFqyA2R6EOEUp0h801',
	      	'X-AgreementGrantToken:T2Lq7uYKYnJy9MP3WUaWZsRgz4KHehzJNxg1eaRZfxo1',
	      	'Content-Type:application/json'
	      ));
	  $curl_response = curl_exec($curl);
	  if ($curl_response === false) {
	      $info = curl_getinfo($curl);
	      curl_close($curl);
	      die('Error occured during curl exec. Additional info: ' . var_export($info));
	  }
	  curl_close($curl);
	  $decoded = json_decode($curl_response);
	  if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
	      die('Error occured: ' . $decoded->response->errormessage);
	  }
	  
	  return $decoded;
  }
  
  $inv_booked = get_endpoint('https://restapi.e-conomic.com/invoices/booked');
  $inv_draft = get_endpoint('https://restapi.e-conomic.com/invoices/drafts');
  $invoices = array_merge($inv_booked->collection, $inv_draft->collection);
	
  //$economic_info = array();
  $economic_order_ids = array();
  
  foreach ($invoices as $post) {
    //$parsed = (int)preg_replace('/^[^0-9]*([0-9]+).*$/i', '${1}', $post['Description']);
    //$parsed = (int)preg_replace('/ordre[^0-9]*([0-9]+)/i', '${1}', $post['Description']);
    
    /*
    $economic_info[] = array('economic' => $post['Number'], 
                           'woo' => $post['Description'], 
                           'woo_parsed' => $parsed);
    */
    
    $woo_id = $post->references->other;
    
    if (substr($woo_id, 0, 2) != 'NS') {
      continue;
      //die($woo_id . ' not a valid WooC reference');
    }
    
    $woo_id = (int)substr($woo_id, 2);    
    $economic_order_ids[] = $woo_id;
  }
  //print_r($economic_order_ids);
	//print_r($decoded);
	//die;
  
?>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<h1>Kun gennemførte WooC ordrer (godkendt/afsluttede, order id >= <?php echo $MIN_WOOC_ORDER_ID; ?>) samt E-conomic-fakturaer i kladde eller bogført</h1>
<?php
?>
<?php
  
  //print_r($dinero_info);
  //die;
  
  echo '<h2>I WooC, men ikke i E-conomic</h2>';
  
  $shown = 0;
  
  echo "<ul>\n";
  foreach ($woo_order_ids as $woo_id) {
    if (in_array($woo_id, $economic_order_ids)) {
      continue;
    }
    
    $order = new WC_Order($woo_id);
    //print_r($order);
    
    $order_total = $order->get_total();
    if (abs($order_total) < 1) {
      continue;
    }
    
    $order_note = $order->customer_note;
    
    $pos = strpos(strtolower($order_note), 'intern flytning');    
    if ($pos !== FALSE) {
      continue;
    }
    
    echo "<li><a href=\"http://b2b.nordicspirits.dk/wp-admin/post.php?post=" . $woo_id . "&action=edit\">WooC order id <strong>" . $woo_id . "</strong> " . $order->billing_company . " [" . $order->order_date . "] til " . wc_price($order_total) . " (note: &quot;<i>" . $order_note . "</i>&quot;)</a></li>\n";
    //print_r($order);
    ++$shown;
  }
  echo "</ul>\n";

  if ($shown == 0) {
    echo '<p>Ingen ordrer.</p>';
  }
  
  
  ///////////////////////////////////
  /*
  echo '<h2>I Dinero, men ikke i WooC</h2>';
  
  echo "<ul>\n";
  foreach ($dinero_info as $info) {
    if (in_array($info['woo_parsed'], $woo_order_ids)) {
      continue;
    }
    
    echo "<li>Dinero bilag nr. " . $info['dinero'] . "</li>\n";
  }
  echo "</ul>\n";
  
  //echo '<h1>I Dinero, men ikke WooC</h1>';
  */
?>

</body>
</html>

