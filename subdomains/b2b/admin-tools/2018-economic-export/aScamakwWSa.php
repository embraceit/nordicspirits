<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

function show_money($x) {
  return number_format($x, 0, ',', '.');
}

require('include.php');

//http://b2b.nordicspirits.dk/admin-tools/2018-economic-export/aScamakwWSa.php
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Oversigt</title>
	<!--
	<link rel='stylesheet' id='buttons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/buttons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='dashicons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='login-css'  href='http://b2b.nordicspirits.dk/wp-admin/css/login.min.css?ver=4.4.2' type='text/css' media='all' />
  -->

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		#login h1, #login h2 {
		  margin-bottom: 1em;
		}
		#login {
		  width: 500px;
		  padding-top: 2em;
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 10px;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: left;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<h1>E-conomic</h1>
<?php
  $invoices = get_endpoint_all_pages('https://restapi.e-conomic.com/invoices/booked');
  print_r($invoices);
  $invoice = get_endpoint_all_pages('https://restapi.e-conomic.com/invoices/booked/1');
  print_r($invoice);
  die;
  //////////////////////////
  $product_groups = get_endpoint_all_pages('https://restapi.e-conomic.com/product-groups');
  
  $accounts = array();

  foreach ($product_groups as $pg) {
    $producent = $pg->name;
    $type_key = 'salg';
    
    if (strpos($producent, '(kreditnota)') !== false) {
      $type_key = 'kreditnota';
      $producent = str_replace(' (kreditnota)', '', $producent);
    }
    
    $accounts[$producent][$type_key] = $pg;
  }  
  
  //print_r($product_groups);
  print_r($accounts);
  
  $producenter = array();
  $non_producenter = array();
  
  foreach ($accounts as $p) {
    if (isset($p['salg']) && isset($p['kreditnota'])) {
      $sales_accounts = get_endpoint_all_pages('https://restapi.e-conomic.com/product-groups/' . $p['salg']->productGroupNumber . '/sales-accounts');
      $p['salesaccounts'] = $sales_accounts;
      $producenter[] = $p;
    } else {
      $non_producenter[] = $p;
    }
  }  
  
  print_r($producenter);
  
  echo "<h2>Producenter</h2>\n"; 
  echo "<table>
    <tr>
      <th>Salgskonto</th>
      <th>Kreditnotakonto</th>
    </tr>
    "; 
  foreach ($producenter as $p) {
    echo "
      <tr>
        <td>" . $p['salg']->name . " (" . $p['salg']->productGroupNumber . ")</td>
        <td>" . $p['kreditnota']->name . " (" . $p['kreditnota']->productGroupNumber . ")</td>
      </tr>
      "; 
  }  
  echo "</table>\n";
  
  echo "<h2>Andre konti</h2>\n";
  foreach ($non_producenter as $p) {
    $key = array_keys($p)[0];
    echo $p[$key]->name . "<br />\n";
  }  
  
?>
</body>
</html>

