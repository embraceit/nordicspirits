<?php 
require('include-dinero-fetch.php');

/*
if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}
*/

function get_producent_md5($producent) {
  $producent_md5 = md5('sXasdVcxcsdvaA1021' . $producent);
  return $producent_md5;
}

//http://umap.openstreetmap.fr/da/map/anonymous-edit/156314:nWkWyUCfawBRkoK2MDMS496y894

$files_output = array();
$prefix_server = '/var/www/www.nordicspirits.dk/subdomains/b2b';
$prefix_webpath = '/wp-content/uploads/maps/generated';

foreach ($sales_info as $producent => $landsdelssalg) {
  $producent_md5 = get_producent_md5($producent);
  $producent_filename_prefix = preg_replace("([^A-Za-z0-9._-])", '', $producent) . '_' . $producent_md5;
  $output_map_html = '';
  $output_list_html = '';
  $output_markers_csv = '"Navn";"Landsdel";"Postnummer";"By";"Adresse";"Latitude";"Longitude"' . "\n";
  $output_markers_json = 'eqfeed_callback({"type": "FeatureCollection",
    "features": [' . "\n";
  
  $markers = array();
  
  foreach ($landsdelssalg as $landsdel => $kundetypesalg) {
    foreach ($kundetypesalg as $kundetype => $kundesalg) {
      foreach ($kundesalg as $salg) {
        $kunde = $salg['info']; 
        //print_r($kunde);
        //die;
        
        //$output = json_decode($geocode_json);
        $latitude = 57.0058086;
        $longitude = 10.4473925;
        $descr = 'NotFound';
        
        if ($kunde['cust_postcode'] != 'Ukendt') {      
          // https://stackoverflow.com/questions/3807963/how-to-get-longitude-and-latitude-of-any-address        
          $possible_addresses = array(
            str_replace(' ', '+', $kunde['cust_address'] . ', Danmark'),
            str_replace(' ', '+', $kunde['cust_postcode'] . ' ' . $kunde['cust_city'] . ', Danmark'),
            str_replace(' ', '+', $kunde['cust_address'] . ''),
          );       
          
          $cache_adr_filename = 'cache-dinero-invoices/adr-' . $kunde['cust_postcode'] . '-' . $possible_addresses[0] . '.json';
          $output = NULL;  
          
          if (file_exists($cache_adr_filename)) {
            //echo "CACHE\n<br />";
            $geocode_json = file_get_contents($cache_adr_filename);
            $output = json_decode($geocode_json);
          } else {
            
            // to make it easy; we don't believe in huge improvements in geocode; or else this foreach could be around the entire caching mechanism
            foreach ($possible_addresses as $prep_addr) {
              //echo "API\n<br />";
              // key: NS Map
              $geocode_json = file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyCu8734SVrIb5-_aQFJwH8hxPUulJ4C72o&address='.$prep_addr.'&sensor=false');
              //$geocode_json = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prep_addr . '&sensor=false');
              $output = json_decode($geocode_json);
              
              //echo "==============\n" . $prep_addr . ":\n";
              //print_r($output);
              //echo "\n\n";
              
              if (count($output->results) == 0) {
                $output = NULL;
              } else {
                file_put_contents($cache_adr_filename, $geocode_json);
                break;
              }
            }
          }        
          
          // First result taken... Probably okay
          if (!is_null($output) && count($output->results) >= 1) {
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;          
            $descr = 'Found';
          }
        } else {
          //print_r($kunde);
          //die;                
        }
          
        $markers[$descr][] = array('lat' => $latitude, 'lon' => $longitude, 'found_status' => $descr, 'kunde' => $kunde, 'salg' => $salg);
        //print_r($markers);
        //die;
      }
    }
  }
  
  
  // leaflet
  $output_map_html .= "
  <div id=\"mapid_" . $producent_md5 . "\"></div>
  <script>
	  var tilelayer_" . $producent_md5 . " = new L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', 
	  {
	    minZoom: 4, 
	    maxZoom: 13,
	    attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
  });

    var mymap_" . $producent_md5 . " = L.map('mapid_" . $producent_md5 . "');
    mymap_" . $producent_md5 . ".setView(new L.LatLng(56.25, 10.5), 7);
	  mymap_" . $producent_md5 . ".addLayer(tilelayer_" . $producent_md5 . ");	  
	";
	
	$marker_i = 0;
	$markers_N = count($markers['Found']);
	foreach ($markers['Found'] as $m) {
	  $marker_name = "marker_" . $producent_md5 . "_" . ++$marker_i;
	  
	  $output_map_html .= "
	    var " . $marker_name . " = L.marker([" . $m['lat'] . ", " . $m['lon'] . "]).addTo(mymap_" . $producent_md5 . ");
	    " . $marker_name . ".bindPopup(\"" . $m['kunde']['cust_name'] . "<br />" . $m['kunde']['cust_address'] . "\");
	    ";
	    
	  $output_markers_csv .= '"' . $m['kunde']['cust_name'] . '";"' . 
	    $m['kunde']['cust_landsdel'] . '";"' . 
	    $m['kunde']['cust_postcode'] . '";"' . 
	    $m['kunde']['cust_city'] . '";"' . 
	    $m['kunde']['cust_address'] . '";"' . 
	    $m['lat'] . '";"' .
	    $m['lon'] . '"' . "\n";
	    
     $output_markers_json .= '
     {
      "type": "Feature",
      "properties": {
        "name": "' . $m['kunde']['cust_name'] . '",
        "region": "' . $m['kunde']['cust_landsdel'] . '",
        "postcode": "' . $m['kunde']['cust_postcode'] . '",
        "city": "' . $m['kunde']['cust_city'] . '",
        "address": "' . $m['kunde']['cust_address'] . '",
        "lat": ' . $m['lat'] . ',
        "lng": ' . $m['lon'] . '
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          ' . $m['lon'] . ',
          ' . $m['lat'] . '
        ]
      }
     }';
     
     if ($marker_i < $markers_N) {
      $output_markers_json .= ', ';
     }
     
     $output_markers_json .= "\n";
	}
	$output_map_html .= "
  </script>\n";

  $output_html_header = '<!DOCTYPE html>
  <html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
  <head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	  <title>Forhandleroversigt</title>
    <style type="text/css">
      body {
        padding: 2em;
      }
    
      #mapid_' . $producent_md5 . ' { height: 600px; width: 600px; }
    </style>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"
      integrity="sha512-wcw6ts8Anuw10Mzh9Ytw4pylW8+NAD4ch3lqm9lzAsTxg0GFeJgoAtxuCLREZSC5lUXdVyo/7yfsqFjQ4S+aKw=="
      crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"
      integrity="sha512-mNqn2Wg7tSToJhvHcqfzLMU6J4mkOImSPTxVZAdo+lcPlk+GhZmYgACEe0x35K7YzW1zJ7XyJV/TT1MrdXvMcA=="
      crossorigin=""></script>
    <meta name="robots" content="noindex,nofollow" />
  </head>
  <body>
  ';

  $output_map_html_out = $output_html_header . $output_map_html . "\n</body>\n</html>";
  
  $output_markers_json .= '
  ]
}
)
  ';
  
  
  $file_markers_csv = $prefix_webpath . '/' . $producent_filename_prefix . '_markers.csv';
  $file_markers_json = $prefix_webpath . '/' . $producent_filename_prefix . '_markers.json';
  $file_map_html = $prefix_webpath . '/' . $producent_filename_prefix . '_map.html';
  
  $output_markers_json = "\xEF\xBB\xBF" . $output_markers_json; // UTF-8 BOM
  
  file_put_contents($prefix_server . $file_markers_csv, $output_markers_csv);
  file_put_contents($prefix_server . $file_markers_json, $output_markers_json);
  file_put_contents($prefix_server . $file_map_html, $output_map_html_out);  
  
  $files_output[] = array('producent' => $producent, 'markers_csv' => $file_markers_csv, 'markers_json' => $file_markers_json, 'map_html' => $file_map_html);  
}

$output_header = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Forhandleroversigt</title>
  <style type="text/css">
    body {
      padding: 2em;
    }
  </style>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"
    integrity="sha512-wcw6ts8Anuw10Mzh9Ytw4pylW8+NAD4ch3lqm9lzAsTxg0GFeJgoAtxuCLREZSC5lUXdVyo/7yfsqFjQ4S+aKw=="
    crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"
    integrity="sha512-mNqn2Wg7tSToJhvHcqfzLMU6J4mkOImSPTxVZAdo+lcPlk+GhZmYgACEe0x35K7YzW1zJ7XyJV/TT1MrdXvMcA=="
    crossorigin=""></script>
  <meta name="robots" content="noindex,nofollow" />
</head>
<body>
';

$out_index = '';
foreach ($files_output as $o) {
  $out_index .= '<li>' . $o['producent'] . ': <a href="' . $o['map_html'] . '">Map HTML</a> | <a href="' . $o['markers_csv'] . '">Markers CSV</a> | <a href="' . $o['markers_json'] . '">Markers JSON</a></li>' . "\n";
}
$out_index = $output_header . $out_index . "\n<p>Genereret " . date('r') . "</p></body>\n</html>";

file_put_contents($prefix_server . '/' . $prefix_webpath . '/index.html', $out_index);

