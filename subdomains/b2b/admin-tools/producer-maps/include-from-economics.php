<?php
///////////////////////////////////
// E-CONOMIC
///////////////////////////////////
// https://github.com/e-conomic/eco-api-ex/blob/master/examples/REST/PHP%20cURL/restcurl.php
/*
X-AgreementGrantToken: T2Lq7uYKYnJy9MP3WUaWZsRgz4KHehzJNxg1eaRZfxo1
*/
function get_endpoint($service_url) {
  $curl = curl_init($service_url);
  
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      	'X-AppSecretToken:XA034wvkfupBERIEWHE6VYlEAETFqyA2R6EOEUp0h801',
      	'X-AgreementGrantToken:T2Lq7uYKYnJy9MP3WUaWZsRgz4KHehzJNxg1eaRZfxo1',
      	'Content-Type:application/json'
      ));
  $curl_response = curl_exec($curl);
  if ($curl_response === false) {
      $info = curl_getinfo($curl);
      curl_close($curl);
      die('Error occured during curl exec. Additional info: ' . var_export($info));
  }
  curl_close($curl);
  $decoded = json_decode($curl_response);
  if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
      die('Error occured: ' . $decoded->response->errormessage);
  }
  
  /*
  if ($decoded->pagination->results > 1000) {
    die('More than 1000 results; only 1000 fetched');
  }
  */
  
  return $decoded;
}

function get_endpoint_all_pages($url) {
  $res = array();
  
  $PAGESIZE = 1000;
  $page = 0;
  
  $append_char = "?";
  
  if (strpos($url, '?') !== false) {
    $append_char = "&";
  }
  
  do {
    $url_new = $url . $append_char . 'skippages=' . $page . '&pagesize=' . $PAGESIZE;
    $info = get_endpoint($url_new);      
    $info = $info->collection;
    //print_r($info);
    $res = array_merge($res, $info);
    ++$page;
    
    if ($page > 10) {
      die('More than 10 pages visited...');
    }
  } while (count($info) == $PAGESIZE);    
  
  return($res);
}


if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

/*
$output_markers_csv = '"Navn";"Landsdel";"Postnummer";"By";"Adresse";"Latitude";"Longitude"' . "\n";
  
  foreach ($landsdelssalg as $landsdel => $kundetypesalg) {
    foreach ($kundetypesalg as $kundetype => $kundesalg) {
      foreach ($kundesalg as $salg) {
        $kunde = $salg['info']; 
	    
	  $output_markers_csv .= '"' . $m['kunde']['cust_name'] . '";"' . 
	    $m['kunde']['cust_landsdel'] . '";"' . 
	    $m['kunde']['cust_postcode'] . '";"' . 
	    $m['kunde']['cust_city'] . '";"' . 
	    $m['kunde']['cust_address'] . '";"' . 
	    $m['lat'] . '";"' .
	    $m['lon'] . '"' . "\n";	    
	    
$sales_info_dinero = array (
  'Herbie Gin' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => ...
          'invoices_dates' => ...,
        ),       
*/
//////////////////////////

$sales_info_economic = array();

$product_groups = get_endpoint_all_pages('https://restapi.e-conomic.com/product-groups');
$accounts = array();
foreach ($product_groups as $pg) {
  $producent = $pg->name;
  $type_key = 'salg';
  
  if (strpos($producent, '(kreditnota)') !== false) {
    $type_key = 'kreditnota';
    $producent = str_replace(' (kreditnota)', '', $producent);
  }
  
  $accounts[$producent][$type_key] = $pg;
}  

//print_r($product_groups);
//print_r($accounts);

$producenter = array();
$non_producenter = array();

foreach ($accounts as $p) {
  if (isset($p['salg']) && isset($p['kreditnota'])) {
    //$sales_accounts = get_endpoint_all_pages('https://restapi.e-conomic.com/product-groups/' . $p['salg']->productGroupNumber . '/sales-accounts');
    //$p['salesaccounts'] = $sales_accounts;
    
    $producent_name = $p['salg']->name;
    
    if (!isset($sales_info_economic[$producent_name])) {
      $sales_info_economic[$producent_name] = array();
    }
    
    
    $salg_products = get_endpoint_all_pages('https://restapi.e-conomic.com/product-groups/' . $p['salg']->productGroupNumber . '/products');
    //$salg_products_invoices = array();
    $cust = array();
        
    foreach ($salg_products as $salg_product) {
      $sales_info = get_endpoint_all_pages($salg_product->invoices->booked);
      
      foreach ($sales_info as $inv) {
        //$cust[] = $inv->recipient;
        
        // FIXME: Skip if older than 12 months?

        if (!isset($inv->references->other)) {
          continue;          
        }        
        
        $wooc_order = NULL;

        $parsed = str_replace('NS', '', $inv->references->other);
        
        try {
          $wooc_order = new WC_Order($parsed);
        } catch (Exception $e) {    
          // do something?
        }
         
        if (is_null($wooc_order)) {
          continue;
        }
        
        $wooc_order_customer = NULL;
        
        if (!is_a($wooc_order->post, 'WP_Post')) {
          continue;
        }
      
        $wooc_order_customer_id = get_post_meta($wooc_order->id, '_customer_user', true);
        
        $wooc_order_customer = NULL;
        try {
          $wooc_order_customer = new WC_Customer($wooc_order_customer_id);
        } catch (Exception $e) {    
          // do something?
        }
        
        $wooc_order_customer_name = get_post_meta($wooc_order->id, '_shipping_company', true);
        $wooc_order_customer_postcode = get_post_meta($wooc_order->id, '_shipping_postcode', true);
        $wooc_order_customer_city = get_post_meta($wooc_order->id, '_shipping_city', true);
        $wooc_order_customer_address = get_post_meta($wooc_order->id, '_shipping_address_1', true);

        $user_info = get_userdata($wooc_order_customer_id);  
        $user_roles = (array)$user_info->roles;
        $user_onlineforretning = in_array('onlineforretning', $user_roles) ? 'Yes' : 'No';
        
        $user_shop_types = array();
        
        foreach ($user_roles as $r) {
          if (strpos($r, 'forhandler_') !== false || 
              strpos($r, 'barer_')      !== false || 
              strpos($r, 'restaurant_') !== false) {
            //$user_shop_types[] = $r;
            $user_shop_types[] = $wp_roles->roles[$r]['name']; // global $wp_roles
          }
        }
        
        $user_shop_type = 'Ukendt';
        
        if (count($user_shop_types) > 1) {
          print_r($user_shop_types);
          print_r($wooc_order_customer_name);
          print_r($wooc_order_customer_id);
          die('Customer had too many/few roles'); 
        } else if (count($user_shop_types) == 1) {
          $user_shop_type = $user_shop_types[0];
        }
        
        ///////////////////////////////////////////
        // OVERRIDES
        ///////////////////////////////////////////    
        $maps_displayname = NULL;
         
        $user_meta = $wooc_order_customer->meta_data;      
        foreach ($user_meta as $m) {
          if (!isset($results_maps_fields[$m->key])) {
            continue;
          }
          
          if ($results_maps_fields[$m->key] == 'maps_displayname') {
            $maps_displayname = $m->value;
          }
        }

        if (!is_null($maps_displayname) && strlen(trim($maps_displayname)) > 0) {
          $wooc_order_customer_name = $maps_displayname;
        }

        $wooc_order_customer = array('user_id' => $wooc_order_customer_id,
                                     'user_name' => $wooc_order_customer_name,
                                     'user_postcode' => $wooc_order_customer_postcode,
                                     'user_city' => $wooc_order_customer_city,
                                     'user_address' => $wooc_order_customer_address . ', ' . $wooc_order_customer_postcode . ' ' . $wooc_order_customer_city,
                                     'user_info' => $user_info,
                                     'user_roles' => $user_roles,
                                     'user_onlineforretning' => $user_onlineforretning,
                                     'user_shop_type' => $user_shop_type);          

        $cust_name = 'Ukendt';
        $cust_postcode = 'Ukendt';
        $cust_city = 'Ukendt';
        $cust_address = 'Ukendt';
        $cust_landsdel = 'Ukendt';
        $cust_shortname = 'Ukendt';
        $cust_shop_type = 'Ukendt';
        
        if (!is_null($wooc_order_customer)) {
          $cust_name = $wooc_order_customer['user_name'];
          $cust_postcode = $wooc_order_customer['user_postcode'];
          $cust_city = $wooc_order_customer['user_city'];
          $cust_address = $wooc_order_customer['user_address'];
          //$cust_shortname = $cust_name . ' (' . $cust_postcode . ')';
          $cust_shortname = $cust_name . ' (' . $cust_address . ')';
          $cust_shop_type = $wooc_order_customer['user_shop_type'];
          
          //https://da.wikipedia.org/wiki/Postnumre_i_Danmark
          if ($wooc_order_customer['user_onlineforretning'] == 'Yes') {
            $cust_landsdel = 'Onlinebutik';
          } else if ($cust_postcode <= 999) {
            $cust_landsdel = 'Særpostnummer';
          } else if ($cust_postcode >= 1000 && $cust_postcode <= 4999) {
            $cust_landsdel = 'Sjælland m.fl.';
          } else if ($cust_postcode >= 5000 && $cust_postcode <= 5999) {
            $cust_landsdel = 'Fyn m.fl.';
          } else if ($cust_postcode >= 6000 && $cust_postcode <= 9999) {
            $cust_landsdel = 'Jylland';
          }
        }

        // Skip some dealers...    
        if (strpos($cust_name, 'Tada IVS') !== false) {      
          //echo $cust_name . "<br />\n";
          continue;
        }
        
        
        if (!isset($sales_info_economic[$producent_name][$cust_landsdel][$cust_shop_type][$cust_shortname])) {
          $sales_info_economic[$producent_name][$cust_landsdel][$cust_shop_type][$cust_shortname] = array(
          'info' => array(
            'cust_name' => $cust_name, 
            'cust_landsdel' => $cust_landsdel, 
            'cust_postcode' => $cust_postcode,
            'cust_city' => $cust_city,  
            'cust_address' => $cust_address));
        }
      }
    }
    
    //$p['salg_products_invoices'] = $salg_products_invoices;    
    $p['salg_customers'] = $cust;
    $producenter[] = $p;
    
    //break;
  } else {
    $non_producenter[] = $p;
  }
}  

//print_r($sales_info_economic);
//print_r($producenter);

/*
echo "<h2>Producenter</h2>\n"; 
echo "<table>
  <tr>
    <th>Salgskonto</th>
    <th>Kreditnotakonto</th>
  </tr>
  "; 
foreach ($producenter as $p) {
  echo "
    <tr>
      <td>" . $p['salg']->name . " (" . $p['salg']->productGroupNumber . ")</td>
      <td>" . $p['kreditnota']->name . " (" . $p['kreditnota']->productGroupNumber . ")</td>
    </tr>
    "; 
}  
echo "</table>\n";

echo "<h2>Andre konti</h2>\n";
foreach ($non_producenter as $p) {
  $key = array_keys($p)[0];
  echo $p[$key]->name . "<br />\n";
}  


*/

