<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

$orderdt = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " - 1 year"));
//$orderdt = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " - 1 month"));
//$orderdt = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " - 2 months"));

//global $wp_roles;

///////////////////////////////////
// EXTRA FIELDS (Registration Attributes)
///////////////////////////////////
$results_maps_fields_raw = $wpdb->get_results('SELECT * FROM `b2b_wp_fmera_fields`', ARRAY_A);
$results_maps_fields = array();
foreach ($results_maps_fields_raw as $f) {
  $results_maps_fields[$f['field_name']] = $f['field_label'];
}
//print_r($results_maps_fields);
//die;

///////////////////////////////////
// DINERO
///////////////////////////////////
require('../include-dinero.php');
  
$client_id_secret = 'Nordic Spirits IVS' . ':' . 'TojI4fWZePZhMi6zg7PfaKHys9gmTv0YRetYdIQaK4';
$apikey = '9361e8bbcd144a208ed715d2415489c6';

$token = get_access_token($client_id_secret, $apikey);
$access_token = $token['access_token'];

$org_id = '117742';


$start_date = $orderdt;
$end_date = date("Y-m-d");//'2017-03-31';

// cache each hour
$sales_cache_filename = 'cache-dinero-invoices/sales-' . $org_id . '-' . $start_date . '-' . $end_date . date("H") . '00.json';
$sales = NULL;  
if (file_exists($sales_cache_filename)) {
  //echo "CACHE!\n";
  $sales_json = file_get_contents($sales_cache_filename);
  $sales = json_decode($sales_json, true);
} else {
  //echo "API!\n";
  $sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=' . $start_date . '&endDate=' . $end_date . '&fields=Number,Guid,UpdatedAt,ContactName,Date,Description,Currency,TotalExclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
  $sales_json = json_encode($sales);
  file_put_contents($sales_cache_filename, $sales_json);
}

$dinero_info = array();
$sales_info = array();

foreach ($sales as $post) {
  //sleep(100);
  
  $parsed = (int)preg_replace('/ordre[^0-9]*([0-9]+)/i', '${1}', $post['Description']);
  
  if ($parsed == 0) {
    continue;
  }
  
  $cache_filename = 'cache-dinero-invoices/invoices-' . $org_id . '-' . sprintf("%05d", $post['Number']) . '-' . $post['Guid'] . '-' . str_replace(':', '', $post['UpdatedAt']) . '.json';    
  $invoice = NULL;  
  if (file_exists($cache_filename)) {
    //echo "CACHE:\n<br />";
    $invoice_json = file_get_contents($cache_filename);
    $invoice = json_decode($invoice_json, true);
  } else {
    $endpoint_url = 'v1/' . $org_id . '/invoices/' . $post['Guid'];
    //echo "API: " . $endpoint_url . "\n<br />";
    $invoice = get_endpoint($endpoint_url, $access_token);
    
    // Only cache useful results
    if (isset($invoice['ProductLines'])) {
      $invoice_json = json_encode($invoice);
      file_put_contents($cache_filename, $invoice_json);
    }
  }
  //echo "\n<br />";
  //print_r($post);
  //echo "\n\n\n<br /><br /><br />";
   
  
  if (isset($invoice['validationErrors'])) {
    // No need to include these as they are credit notes
    continue;
  }

  else if (!isset($invoice['ProductLines'])) {
    echo "\nInvoice ProductLines not set:\n";
    print_r($invoice);
    die;
  }
  
  $wooc_order = NULL;

  try {
    $wooc_order = new WC_Order($parsed);
  } catch (Exception $e) {    
    // do something?
  }
   
  if (is_null($wooc_order)) {
    continue;
  }
  
  //echo $parsed . "<br/>\n";  
  $wooc_order_customer = NULL;
  
  if (!is_a($wooc_order->post, 'WP_Post')) {
    $wooc_order = NULL;
  } else {
    $wooc_order_customer_id = get_post_meta($wooc_order->id, '_customer_user', true);
    
    $wooc_order_customer = NULL;
    try {
      $wooc_order_customer = new WC_Customer($wooc_order_customer_id);
    } catch (Exception $e) {    
      // do something?
    }
    
    /*
    $wooc_order_customer_name = get_post_meta($wooc_order->id, '_billing_company', true);
    $wooc_order_customer_postcode = get_post_meta($wooc_order->id, '_billing_postcode', true);
    $wooc_order_customer_city = get_post_meta($wooc_order->id, '_billing_city', true);
    $wooc_order_customer_address = get_post_meta($wooc_order->id, '_billing_address_1', true);
    */
    $wooc_order_customer_name = get_post_meta($wooc_order->id, '_shipping_company', true);
    $wooc_order_customer_postcode = get_post_meta($wooc_order->id, '_shipping_postcode', true);
    $wooc_order_customer_city = get_post_meta($wooc_order->id, '_shipping_city', true);
    $wooc_order_customer_address = get_post_meta($wooc_order->id, '_shipping_address_1', true);

    $user_info = get_userdata($wooc_order_customer_id);  
    $user_roles = (array)$user_info->roles;
    $user_onlineforretning = in_array('onlineforretning', $user_roles) ? 'Yes' : 'No';
    
    $user_shop_types = array();
    
    foreach ($user_roles as $r) {
      if (strpos($r, 'forhandler_') !== false || 
          strpos($r, 'barer_')      !== false || 
          strpos($r, 'restaurant_') !== false) {
        //$user_shop_types[] = $r;
        $user_shop_types[] = $wp_roles->roles[$r]['name']; // global $wp_roles
      }
    }
    
    $user_shop_type = 'Ukendt';
    
    if (count($user_shop_types) > 1) {
      print_r($user_shop_types);
      print_r($wooc_order_customer_name);
      print_r($wooc_order_customer_id);
      die('Customer had too many/few roles'); 
    } else if (count($user_shop_types) == 1) {
      $user_shop_type = $user_shop_types[0];
    }
    
    
    ///////////////////////////////////////////
    // OVERRIDES
    ///////////////////////////////////////////    
    $maps_displayname = NULL;
     
    $user_meta = $wooc_order_customer->meta_data;      
    foreach ($user_meta as $m) {
      if (!isset($results_maps_fields[$m->key])) {
        continue;
      }
      
      if ($results_maps_fields[$m->key] == 'maps_displayname') {
        $maps_displayname = $m->value;
      }
    }

    if (!is_null($maps_displayname) && strlen(trim($maps_displayname)) > 0) {
      $wooc_order_customer_name = $maps_displayname;
    }
    
    ///////////////////////////////////////////
    // SAVE
    ///////////////////////////////////////////    
    $wooc_order_customer = array('user_id' => $wooc_order_customer_id,
                                 'user_name' => $wooc_order_customer_name,
                                 'user_postcode' => $wooc_order_customer_postcode,
                                 'user_city' => $wooc_order_customer_city,
                                 'user_address' => $wooc_order_customer_address . ', ' . $wooc_order_customer_postcode . ' ' . $wooc_order_customer_city,
                                 'user_info' => $user_info,
                                 'user_roles' => $user_roles,
                                 'user_onlineforretning' => $user_onlineforretning,
                                 'user_shop_type' => $user_shop_type);
  }
  
  $d_info = array('dinero' => $post, 
                  'dinero_invoice' => $invoice,
                  'wooc_orderid_parsed' => $parsed,
                  'wooc_order' => $wooc_order,
                  'wooc_order_customer' => $wooc_order_customer);

  
  for ($j = 0; $j < count($invoice['ProductLines']); ++$j) {  
    $line = $invoice['ProductLines'][$j];    
    
    $cust_name = 'Ukendt';
    $cust_postcode = 'Ukendt';
    $cust_city = 'Ukendt';
    $cust_address = 'Ukendt';
    $cust_landsdel = 'Ukendt';
    $cust_shortname = 'Ukendt';
    $cust_shop_type = 'Ukendt';

    
    if (!is_null($wooc_order_customer)) {
      $cust_name = $wooc_order_customer['user_name'];
      $cust_postcode = $wooc_order_customer['user_postcode'];
      $cust_city = $wooc_order_customer['user_city'];
      $cust_address = $wooc_order_customer['user_address'];
      //$cust_shortname = $cust_name . ' (' . $cust_postcode . ')';
      $cust_shortname = $cust_name . ' (' . $cust_address . ')';
      $cust_shop_type = $wooc_order_customer['user_shop_type'];
      
      //https://da.wikipedia.org/wiki/Postnumre_i_Danmark
      if ($wooc_order_customer['user_onlineforretning'] == 'Yes') {
        $cust_landsdel = 'Onlinebutik';
      } else if ($cust_postcode <= 999) {
        $cust_landsdel = 'Særpostnummer';
      } else if ($cust_postcode >= 1000 && $cust_postcode <= 4999) {
        $cust_landsdel = 'Sjælland m.fl.';
      } else if ($cust_postcode >= 5000 && $cust_postcode <= 5999) {
        $cust_landsdel = 'Fyn m.fl.';
      } else if ($cust_postcode >= 6000 && $cust_postcode <= 9999) {
        $cust_landsdel = 'Jylland';
      }
    }

    // Skip some dealers...    
    if (strpos($cust_name, 'Tada IVS') !== false) {      
      //echo $cust_name . "<br />\n";
      continue;
    }
    
    $account_name = $line['AccountName'];
    
    if (strlen(trim($account_name)) == 0) {
      $account_name = '[Ukendt producent]';
    }
    
    if (!isset($sales_info[$account_name][$cust_landsdel][$cust_shop_type][$cust_shortname])) {
      $sales_info[$account_name][$cust_landsdel][$cust_shop_type][$cust_shortname] = array(
      'info' => array(
        'cust_name' => $cust_name, 
        'cust_landsdel' => $cust_landsdel, 
        'cust_postcode' => $cust_postcode,
        'cust_city' => $cust_city,  
        'cust_address' => $cust_address),
      'invoices' => array(),
      'invoices_dates' => array());
    }
    
    //print_r($invoice);
    //die;
    
    $sales_info[$account_name][$cust_landsdel][$cust_shop_type][$cust_shortname]['invoices'][] = $invoice['Number'];
    $sales_info[$account_name][$cust_landsdel][$cust_shop_type][$cust_shortname]['invoices_dates'][] = $invoice['Date'];
  }  
  
  $dinero_info[] = $d_info;
}
?>
