<?php 
$sales_info_dinero = array (
  'Herbie Gin' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1474,
            1 => 1474,
            2 => 1474,
            3 => 1474,
            4 => 1266,
            5 => 1266,
            6 => 1266,
            7 => 1266,
            8 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-31',
            1 => '2018-03-31',
            2 => '2018-03-31',
            3 => '2018-03-31',
            4 => '2017-12-31',
            5 => '2017-12-31',
            6 => '2017-12-31',
            7 => '2017-12-31',
            8 => '2017-12-31',
          ),
        ),
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1424,
            1 => 1424,
            2 => 1290,
            3 => 1290,
            4 => 1007,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
            2 => '2018-01-19',
            3 => '2018-01-19',
            4 => '2017-10-31',
          ),
        ),
        'skjold burne (Østergade 1, 1100 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'skjold burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1100',
            'cust_city' => 'København',
            'cust_address' => 'Østergade 1, 1100 København',
          ),
          'invoices' => 
          array (
            0 => 1417,
            1 => 1417,
            2 => 1417,
            3 => 1417,
            4 => 1011,
            5 => 859,
            6 => 859,
            7 => 801,
            8 => 801,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-13',
            1 => '2018-03-13',
            2 => '2018-03-13',
            3 => '2018-03-13',
            4 => '2017-10-31',
            5 => '2017-09-19',
            6 => '2017-09-19',
            7 => '2017-08-03',
            8 => '2017-08-03',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1387,
            1 => 1375,
            2 => 1375,
            3 => 1355,
            4 => 1355,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-27',
            1 => '2018-02-22',
            2 => '2018-02-22',
            3 => '2018-02-15',
            4 => '2018-02-15',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1346,
            1 => 1346,
            2 => 1227,
            3 => 1219,
            4 => 1219,
            5 => 1122,
            6 => 1122,
            7 => 1041,
            8 => 1041,
            9 => 1041,
            10 => 929,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-30',
            1 => '2018-01-30',
            2 => '2017-12-12',
            3 => '2017-12-19',
            4 => '2017-12-19',
            5 => '2017-11-23',
            6 => '2017-11-23',
            7 => '2017-11-07',
            8 => '2017-11-07',
            9 => '2017-11-07',
            10 => '2017-10-18',
          ),
        ),
        'Holte Vinlager Roskilde (Ringstedgade 37, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager Roskilde',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ringstedgade 37, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1335,
            1 => 1335,
            2 => 802,
            3 => 802,
            4 => 638,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
            2 => '2017-08-03',
            3 => '2017-08-03',
            4 => '2017-06-02',
          ),
        ),
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1301,
            1 => 688,
            2 => 688,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-06-27',
            2 => '2017-06-27',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1238,
            1 => 1171,
            2 => 1171,
            3 => 1114,
            4 => 1052,
            5 => 1052,
            6 => 865,
            7 => 865,
            8 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-12',
            2 => '2017-12-12',
            3 => '2017-11-23',
            4 => '2017-11-09',
            5 => '2017-11-09',
            6 => '2017-09-23',
            7 => '2017-09-23',
            8 => '2017-09-23',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1230,
            1 => 1061,
            2 => 900,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-13',
            2 => '2017-10-10',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 1820 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1820',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 1820 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 1216,
            1 => 1163,
            2 => 950,
            3 => 950,
            4 => 950,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-07',
            2 => '2017-10-26',
            3 => '2017-10-26',
            4 => '2017-10-26',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1201,
            1 => 657,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-06-07',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 1102,
            1 => 1102,
            2 => 736,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
            2 => '2017-07-08',
          ),
        ),
        'Holte Vinlager, Hørsholm (Usserød kongevej 16, 2970 hørsholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Hørsholm',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2970',
            'cust_city' => 'hørsholm',
            'cust_address' => 'Usserød kongevej 16, 2970 hørsholm',
          ),
          'invoices' => 
          array (
            0 => 1068,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-15',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 903,
            2 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-09-30',
            2 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10 kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10 kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 836,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-24',
          ),
        ),
        'Holte Vinlager, Gentofte (Smakkegårdsvej 137, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Gentofte',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Smakkegårdsvej 137, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 822,
            1 => 822,
            2 => 667,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
            1 => '2017-08-20',
            2 => '2017-06-08',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, st. tv., 2400 Kbh NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'Kbh NV',
            'cust_address' => 'Frederikssundsvej 33, st. tv., 2400 Kbh NV',
          ),
          'invoices' => 
          array (
            0 => 798,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-02',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 6 kld th, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 6 kld th, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 778,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
        'Den Sidste Dråbe (Torvehallerne Hal 2, 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2, 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 777,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 2000 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 2000 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 671,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 659,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
        'Holte Vinlager, Hørsholm (usserød kongevej 16, 2970 hørsholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Hørsholm',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2970',
            'cust_city' => 'hørsholm',
            'cust_address' => 'usserød kongevej 16, 2970 hørsholm',
          ),
          'invoices' => 
          array (
            0 => 653,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
          ),
        ),
        'Den Sidste Dråbe (Hal 2, Frederiksborggade 21,, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Frederiksborggade 21,, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 648,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-23',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Café Svanen (Labæk 35, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Café Svanen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Labæk 35, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1465,
            1 => 1465,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2018-03-19',
          ),
        ),
        'R60 (Mosedalsvej 17, 3740 Svaneke)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'R60',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3740',
            'cust_city' => 'Svaneke',
            'cust_address' => 'Mosedalsvej 17, 3740 Svaneke',
          ),
          'invoices' => 
          array (
            0 => 1428,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
        'Klosterkælderen (Store Gråbrødrestræde 23, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Klosterkælderen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Store Gråbrødrestræde 23, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1004,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'The Bird & The Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & The Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 820,
            1 => 820,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
            1 => '2017-08-20',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1460,
            1 => 1252,
            2 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-12-21',
            2 => '2017-12-21',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1440,
            1 => 1340,
            2 => 1249,
            3 => 1242,
            4 => 1091,
            5 => 832,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-02-11',
            2 => '2017-12-26',
            3 => '2017-12-12',
            4 => '2017-11-17',
            5 => '2017-08-23',
          ),
        ),
        'MWine -Meny Kalundborg (Nørre Allé 133, 4400 Kalundborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MWine -Meny Kalundborg',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4400',
            'cust_city' => 'Kalundborg',
            'cust_address' => 'Nørre Allé 133, 4400 Kalundborg',
          ),
          'invoices' => 
          array (
            0 => 1432,
            1 => 1432,
            2 => 1432,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2018-03-21',
          ),
        ),
        'Den Franske Vinhandel (Skomagergade 3, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Franske Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Skomagergade 3, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1419,
            1 => 789,
            2 => 770,
            3 => 770,
            4 => 662,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2017-08-02',
            2 => '2017-07-24',
            3 => '2017-07-24',
            4 => '2017-06-06',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1416,
            1 => 1087,
            2 => 1087,
            3 => 1018,
            4 => 1018,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2017-11-20',
            2 => '2017-11-20',
            3 => '2017-11-01',
            4 => '2017-11-01',
          ),
        ),
        'Meny Hellerup (Strandvejen 64A, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Hellerup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Strandvejen 64A, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1407,
            1 => 1407,
            2 => 1206,
            3 => 1206,
            4 => 768,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-03-08',
            2 => '2017-12-20',
            3 => '2017-12-20',
            4 => '2017-07-25',
          ),
        ),
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 1397,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1388,
            1 => 1356,
            2 => 1272,
            3 => 1235,
            4 => 1235,
            5 => 1145,
            6 => 1074,
            7 => 949,
            8 => 949,
            9 => 837,
            10 => 761,
            11 => 761,
            12 => 759,
            13 => 759,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-03',
            1 => '2018-02-22',
            2 => '2018-01-13',
            3 => '2017-12-20',
            4 => '2017-12-20',
            5 => '2017-12-05',
            6 => '2017-11-15',
            7 => '2017-10-16',
            8 => '2017-10-16',
            9 => '2017-08-31',
            10 => '2017-07-23',
            11 => '2017-07-23',
            12 => '2017-07-21',
            13 => '2017-07-21',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1379,
            1 => 1244,
            2 => 1243,
            3 => 1138,
            4 => 942,
            5 => 942,
            6 => 871,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2017-12-20',
            2 => '2017-12-07',
            3 => '2017-11-22',
            4 => '2017-10-19',
            5 => '2017-10-19',
            6 => '2017-09-12',
          ),
        ),
        'Vinspecialisten Lyngby (Klampenborgvej 232, 2800 Lyngby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lyngby',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2800',
            'cust_city' => 'Lyngby',
            'cust_address' => 'Klampenborgvej 232, 2800 Lyngby',
          ),
          'invoices' => 
          array (
            0 => 1374,
            1 => 1374,
            2 => 864,
            3 => 864,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2017-09-23',
            3 => '2017-09-23',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1367,
            1 => 1267,
            2 => 1267,
            3 => 963,
            4 => 854,
            5 => 845,
            6 => 757,
            7 => 731,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2018-01-08',
            2 => '2018-01-08',
            3 => '2017-10-26',
            4 => '2017-09-08',
            5 => '2017-09-04',
            6 => '2017-07-20',
            7 => '2017-07-13',
          ),
        ),
        'Havannahuset (Algade 50, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Havannahuset',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Algade 50, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1327,
            1 => 1327,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
          ),
        ),
        'Skjold Burne Vanløse (jernbane alle 45, 2720 vanløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vanløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2720',
            'cust_city' => 'vanløse',
            'cust_address' => 'jernbane alle 45, 2720 vanløse',
          ),
          'invoices' => 
          array (
            0 => 1324,
            1 => 1209,
            2 => 1209,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-31',
            1 => '2017-12-20',
            2 => '2017-12-20',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1302,
            1 => 917,
            2 => 860,
            3 => 860,
            4 => 831,
            5 => 831,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-10-16',
            2 => '2017-09-19',
            3 => '2017-09-19',
            4 => '2017-08-31',
            5 => '2017-08-31',
          ),
        ),
        'Skjold Burne (Frederiksværksgade 95, 3400 hillerød)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3400',
            'cust_city' => 'hillerød',
            'cust_address' => 'Frederiksværksgade 95, 3400 hillerød',
          ),
          'invoices' => 
          array (
            0 => 1265,
            1 => 821,
            2 => 821,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-02',
            1 => '2017-08-20',
            2 => '2017-08-20',
          ),
        ),
        'Inco Glostrup (Ejby Industrivej 11, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco Glostrup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Ejby Industrivej 11, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1262,
            1 => 1262,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-04',
            1 => '2018-01-04',
          ),
        ),
        'Borgstrøm Vinhandel (Centerholmen 8A, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Borgstrøm Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Centerholmen 8A, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1237,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Østerbro Vin & Spiritus (Østerbrogade 156, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Østerbro Vin & Spiritus',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 156, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1229,
            1 => 1228,
            2 => 1228,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-12',
            2 => '2017-12-12',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1177,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Eriksen Vinhandel (Nygade 4, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Eriksen Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Nygade 4, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1173,
            1 => 1173,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-12-07',
          ),
        ),
        'VINSLOTTET I GREVE A/S (Håndværkerbyen 42, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VINSLOTTET I GREVE A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Håndværkerbyen 42, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1158,
            1 => 1158,
            2 => 1131,
            3 => 927,
            4 => 927,
            5 => 834,
            6 => 834,
            7 => 643,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-12-07',
            2 => '2017-11-29',
            3 => '2017-10-17',
            4 => '2017-10-17',
            5 => '2017-08-31',
            6 => '2017-08-31',
            7 => '2017-06-06',
          ),
        ),
        'Prøvestenens Vinhandel ApS (Prøvestensvej 14, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Prøvestenens Vinhandel ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Prøvestensvej 14, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 1112,
            1 => 1112,
            2 => 842,
            3 => 842,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
            2 => '2017-08-31',
            3 => '2017-08-31',
          ),
        ),
        'Bagsværd Vinhandel (Bagsværd Hovedgade 125, 2880 Bagsværd)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bagsværd Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2880',
            'cust_city' => 'Bagsværd',
            'cust_address' => 'Bagsværd Hovedgade 125, 2880 Bagsværd',
          ),
          'invoices' => 
          array (
            0 => 1054,
            1 => 1054,
            2 => 773,
            3 => 773,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-07',
            1 => '2017-11-07',
            2 => '2017-07-26',
            3 => '2017-07-26',
          ),
        ),
        'VIN121 (Peter Bangs Vej 121, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VIN121',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Peter Bangs Vej 121, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1050,
            1 => 1050,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-11-08',
          ),
        ),
        'Brdr. D (Rolighedsvej 9, 1958 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr. D',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1958',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Rolighedsvej 9, 1958 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 986,
            1 => 986,
            2 => 940,
            3 => 940,
            4 => 793,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-25',
            2 => '2017-10-26',
            3 => '2017-10-26',
            4 => '2017-08-03',
          ),
        ),
        'Skjold Burne - Stenløse (Egedal Centret 15, 3660 Stenløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne - Stenløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3660',
            'cust_city' => 'Stenløse',
            'cust_address' => 'Egedal Centret 15, 3660 Stenløse',
          ),
          'invoices' => 
          array (
            0 => 983,
            1 => 983,
            2 => 983,
            3 => 983,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
            1 => '2017-10-27',
            2 => '2017-10-27',
            3 => '2017-10-27',
          ),
        ),
        'Backe Vin (Bjergegade 1, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Backe Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Bjergegade 1, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 951,
            1 => 951,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-26',
          ),
        ),
        'Brdr D Østerbro (Østerbrogade 152, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Østerbro',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 152, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 947,
            1 => 947,
            2 => 947,
            3 => 792,
            4 => 792,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-26',
            2 => '2017-10-26',
            3 => '2017-08-03',
            4 => '2017-08-03',
          ),
        ),
        '2010 Vin & Velsmag (C. E. Christiansens Vej 1, 4930 Maribo)' => 
        array (
          'info' => 
          array (
            'cust_name' => '2010 Vin & Velsmag',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4930',
            'cust_city' => 'Maribo',
            'cust_address' => 'C. E. Christiansens Vej 1, 4930 Maribo',
          ),
          'invoices' => 
          array (
            0 => 892,
            1 => 892,
            2 => 892,
            3 => 756,
            4 => 756,
            5 => 672,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-04',
            1 => '2017-10-04',
            2 => '2017-10-04',
            3 => '2017-07-20',
            4 => '2017-07-20',
            5 => '2017-06-13',
          ),
        ),
        'BTB Vine (Højbakken 11, 4690 Haslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'BTB Vine',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Haslev',
            'cust_address' => 'Højbakken 11, 4690 Haslev',
          ),
          'invoices' => 
          array (
            0 => 883,
            1 => 883,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-29',
            1 => '2017-09-29',
          ),
        ),
        '1 Godt Sted (Buddingevej 310, 2860 Søborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => '1 Godt Sted',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2860',
            'cust_city' => 'Søborg',
            'cust_address' => 'Buddingevej 310, 2860 Søborg',
          ),
          'invoices' => 
          array (
            0 => 855,
            1 => 641,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
            1 => '2017-05-31',
          ),
        ),
        'Toft Vin (Islands Brygge 25, 2300 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toft Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København',
            'cust_address' => 'Islands Brygge 25, 2300 København',
          ),
          'invoices' => 
          array (
            0 => 850,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
          ),
        ),
        'Zaizon (Falkoner Alle 33, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Zaizon',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Alle 33, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 848,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'Skælskør Vinhandel (Algade 6-8, 4230 Skælskør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skælskør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4230',
            'cust_city' => 'Skælskør',
            'cust_address' => 'Algade 6-8, 4230 Skælskør',
          ),
          'invoices' => 
          array (
            0 => 805,
            1 => 805,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-10',
            1 => '2017-08-10',
          ),
        ),
        'Næstved vinhandel (Sct. Jørgens Park 70, 4700 Næstved)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Næstved vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4700',
            'cust_city' => 'Næstved',
            'cust_address' => 'Sct. Jørgens Park 70, 4700 Næstved',
          ),
          'invoices' => 
          array (
            0 => 788,
            1 => 788,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-02',
            1 => '2017-08-02',
          ),
        ),
        'Toft Bryggen (Islands Brygge 25, 2300 Kbh S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toft Bryggen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'Kbh S',
            'cust_address' => 'Islands Brygge 25, 2300 Kbh S',
          ),
          'invoices' => 
          array (
            0 => 764,
            1 => 764,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-20',
            1 => '2017-07-20',
          ),
        ),
        'Skaaning Vin - Rødøvre Centrum (Rødovre Centrum 80, 2610 Rødovre)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skaaning Vin - Rødøvre Centrum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2610',
            'cust_city' => 'Rødovre',
            'cust_address' => 'Rødovre Centrum 80, 2610 Rødovre',
          ),
          'invoices' => 
          array (
            0 => 677,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
        'Skjold Burne Vinhandel v/Anders Petersen (Algade 83, 4760 Vordingborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel v/Anders Petersen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4760',
            'cust_city' => 'Vordingborg',
            'cust_address' => 'Algade 83, 4760 Vordingborg',
          ),
          'invoices' => 
          array (
            0 => 668,
            1 => 668,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-08',
            1 => '2017-06-08',
          ),
        ),
        'Inco (Ingerslevsgade 42, 1711 København V.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V.',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V.',
          ),
          'invoices' => 
          array (
            0 => 656,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
          ),
        ),
        'Skjold Burne Ringsted (Nørregade 45, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Ringsted',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nørregade 45, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 637,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Stork Concepts (Frederiksborggade 1b, 4. sal, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Stork Concepts',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 1b, 4. sal, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1457,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Nobis Hotel Copenhagen (Niels Brocks Gade 1, 1574 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nobis Hotel Copenhagen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1574',
            'cust_city' => 'København V',
            'cust_address' => 'Niels Brocks Gade 1, 1574 København V',
          ),
          'invoices' => 
          array (
            0 => 1107,
            1 => 1107,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmand (Sankt Annæ Plads 22, 1250 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmand',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'København K',
            'cust_address' => 'Sankt Annæ Plads 22, 1250 København K',
          ),
          'invoices' => 
          array (
            0 => 1429,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Hav Kildegårdsvej (Kildegårdsvej 15, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hav Kildegårdsvej',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Kildegårdsvej 15, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1427,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Mikkeller Meatpacking Aps (Flæsketorvet 51-55, 1711 KBH V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mikkeller Meatpacking Aps',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'KBH V',
            'cust_address' => 'Flæsketorvet 51-55, 1711 KBH V',
          ),
          'invoices' => 
          array (
            0 => 1344,
            1 => 1344,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
          ),
        ),
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1338,
            1 => 1185,
            2 => 748,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-03',
            1 => '2017-12-13',
            2 => '2017-07-14',
          ),
        ),
        'GinGin I/S (Ternevej 30, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GinGin I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ternevej 30, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1320,
            1 => 1319,
            2 => 1037,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-27',
            1 => '2018-01-30',
            2 => '2017-11-04',
          ),
        ),
        'Smagning.com II (Bækkeskovvej 81, 2700 Brønshøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagning.com II',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2700',
            'cust_city' => 'Brønshøj',
            'cust_address' => 'Bækkeskovvej 81, 2700 Brønshøj',
          ),
          'invoices' => 
          array (
            0 => 1095,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
          ),
        ),
        'GoGift A/S (Marienbergvej 132, 4760 Vordingborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GoGift A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4760',
            'cust_city' => 'Vordingborg',
            'cust_address' => 'Marienbergvej 132, 4760 Vordingborg',
          ),
          'invoices' => 
          array (
            0 => 1089,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
          ),
        ),
        'Lidt-af-hvert I/S (Lindegårdsager 14, 2640 Hedehusene)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lidt-af-hvert I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2640',
            'cust_city' => 'Hedehusene',
            'cust_address' => 'Lindegårdsager 14, 2640 Hedehusene',
          ),
          'invoices' => 
          array (
            0 => 1083,
            1 => 1083,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
            1 => '2017-11-14',
          ),
        ),
        'Byens Specialiteter & Gaver (Tinggade 12, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Byens Specialiteter & Gaver',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Tinggade 12, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 744,
            1 => 744,
            2 => 738,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-18',
            1 => '2017-07-18',
            2 => '2017-07-13',
          ),
        ),
        'Smagsløget (Storgade 15b, 4180 Sorø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagsløget',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4180',
            'cust_city' => 'Sorø',
            'cust_address' => 'Storgade 15b, 4180 Sorø',
          ),
          'invoices' => 
          array (
            0 => 675,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-14',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Hotel Saxkjøbing (Torvet 9, 4990 Sakskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Saxkjøbing',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4990',
            'cust_city' => 'Sakskøbing',
            'cust_address' => 'Torvet 9, 4990 Sakskøbing',
          ),
          'invoices' => 
          array (
            0 => 758,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Vesløs Kro (Vesløs Stationsvej 25, 7742 Vesløs)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vesløs Kro',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7742',
            'cust_city' => 'Vesløs',
            'cust_address' => 'Vesløs Stationsvej 25, 7742 Vesløs',
          ),
          'invoices' => 
          array (
            0 => 1463,
            1 => 1463,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
          ),
        ),
        'Restaurant Villa Vest (Strandvejen 138, 9800 Lønstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Villa Vest',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Lønstrup',
            'cust_address' => 'Strandvejen 138, 9800 Lønstrup',
          ),
          'invoices' => 
          array (
            0 => 1425,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1350,
            1 => 1350,
            2 => 934,
            3 => 934,
            4 => 835,
            5 => 835,
            6 => 666,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2018-02-22',
            2 => '2017-10-25',
            3 => '2017-10-25',
            4 => '2017-08-31',
            5 => '2017-08-31',
            6 => '2017-06-09',
          ),
        ),
        'Hjerting Badehotel (Strandpromenaden 1, 6710 Esbjerg V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjerting Badehotel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6710',
            'cust_city' => 'Esbjerg V',
            'cust_address' => 'Strandpromenaden 1, 6710 Esbjerg V',
          ),
          'invoices' => 
          array (
            0 => 1164,
            1 => 1006,
            2 => 1006,
            3 => 1006,
            4 => 720,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
            1 => '2017-10-24',
            2 => '2017-10-24',
            3 => '2017-10-24',
            4 => '2017-07-05',
          ),
        ),
        'Mash Aarhus (Banegaardspladsen 12, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mash Aarhus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Banegaardspladsen 12, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1045,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
        'Brasserie Nieuwpoort (Marktplein 19, 8620 Nieuwpoort)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brasserie Nieuwpoort',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8620',
            'cust_city' => 'Nieuwpoort',
            'cust_address' => 'Marktplein 19, 8620 Nieuwpoort',
          ),
          'invoices' => 
          array (
            0 => 948,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Aabybro Mejeri (Brogårdsvej 148, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aabybro Mejeri',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Brogårdsvej 148, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1455,
            1 => 1101,
            2 => 894,
            3 => 790,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2017-11-23',
            2 => '2017-10-05',
            3 => '2017-08-03',
          ),
        ),
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1441,
            1 => 1258,
            2 => 1110,
            3 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-12-28',
            2 => '2017-11-23',
            3 => '2017-11-06',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1438,
            1 => 1117,
            2 => 918,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2017-11-27',
            2 => '2017-10-12',
          ),
        ),
        'Rene\'s Vin Og Grønttorv (Østergade 2 B, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rene\'s Vin Og Grønttorv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Østergade 2 B, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1368,
            1 => 1368,
            2 => 1162,
            3 => 1125,
            4 => 1125,
            5 => 1039,
            6 => 1039,
            7 => 893,
            8 => 893,
            9 => 893,
            10 => 846,
            11 => 846,
            12 => 732,
            13 => 732,
            14 => 732,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2017-12-08',
            3 => '2017-11-28',
            4 => '2017-11-28',
            5 => '2017-11-04',
            6 => '2017-11-04',
            7 => '2017-10-04',
            8 => '2017-10-04',
            9 => '2017-10-04',
            10 => '2017-09-04',
            11 => '2017-09-04',
            12 => '2017-07-13',
            13 => '2017-07-13',
            14 => '2017-07-13',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1366,
            1 => 1366,
            2 => 1250,
            3 => 1234,
            4 => 1154,
            5 => 1108,
            6 => 995,
            7 => 928,
            8 => 844,
            9 => 844,
            10 => 844,
            11 => 827,
            12 => 751,
            13 => 708,
            14 => 683,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
            2 => '2017-12-22',
            3 => '2017-12-07',
            4 => '2017-12-04',
            5 => '2017-11-21',
            6 => '2017-10-29',
            7 => '2017-10-16',
            8 => '2017-09-04',
            9 => '2017-09-04',
            10 => '2017-09-04',
            11 => '2017-08-20',
            12 => '2017-07-18',
            13 => '2017-07-05',
            14 => '2017-06-20',
          ),
        ),
        'Herremagasinet (Jyllandsgade 22, 9520 Skørping)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Herremagasinet',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9520',
            'cust_city' => 'Skørping',
            'cust_address' => 'Jyllandsgade 22, 9520 Skørping',
          ),
          'invoices' => 
          array (
            0 => 1313,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1240,
            1 => 863,
            2 => 630,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-09-21',
            2 => '2017-05-23',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1232,
            1 => 954,
            2 => 954,
            3 => 954,
            4 => 829,
            5 => 819,
            6 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-10-02',
            2 => '2017-10-02',
            3 => '2017-10-02',
            4 => '2017-08-23',
            5 => '2017-08-09',
            6 => '2017-08-09',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1217,
            1 => 1003,
            2 => 1003,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-10-31',
            2 => '2017-10-31',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1210,
            1 => 1210,
            2 => 902,
            3 => 875,
            4 => 817,
            5 => 755,
            6 => 658,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-10-11',
            3 => '2017-09-25',
            4 => '2017-08-20',
            5 => '2017-07-19',
            6 => '2017-06-08',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1165,
            1 => 1009,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-10-31',
          ),
        ),
        'Vinspecialisten Randers (Dytmærsken 3, 8900 Randers C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Randers',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8900',
            'cust_city' => 'Randers C',
            'cust_address' => 'Dytmærsken 3, 8900 Randers C',
          ),
          'invoices' => 
          array (
            0 => 1137,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-29',
          ),
        ),
        'Alexanders Blomster (østergade 1, 9560 Hadsund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alexanders Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9560',
            'cust_city' => 'Hadsund',
            'cust_address' => 'østergade 1, 9560 Hadsund',
          ),
          'invoices' => 
          array (
            0 => 1044,
            1 => 1044,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-11-08',
          ),
        ),
        'Hr Nielsens Specialiteter (jernbanegade 12 st. tv, 9460 Brovst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Nielsens Specialiteter',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9460',
            'cust_city' => 'Brovst',
            'cust_address' => 'jernbanegade 12 st. tv, 9460 Brovst',
          ),
          'invoices' => 
          array (
            0 => 923,
            1 => 840,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-13',
            1 => '2017-08-26',
          ),
        ),
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 737,
            1 => 737,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-07',
            1 => '2017-07-07',
          ),
        ),
        'Albert Rex (Nørregade 22, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Albert Rex',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nørregade 22, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 673,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
        'Got2LoveIt (Gunderupvej 96, 9640 Farsø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Got2LoveIt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9640',
            'cust_city' => 'Farsø',
            'cust_address' => 'Gunderupvej 96, 9640 Farsø',
          ),
          'invoices' => 
          array (
            0 => 632,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1449,
            1 => 1437,
            2 => 1437,
            3 => 1437,
            4 => 1199,
            5 => 1075,
            6 => 779,
            7 => 779,
            8 => 779,
            9 => 742,
            10 => 661,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-27',
            1 => '2018-03-21',
            2 => '2018-03-21',
            3 => '2018-03-21',
            4 => '2017-12-20',
            5 => '2017-11-15',
            6 => '2017-07-29',
            7 => '2017-07-29',
            8 => '2017-07-29',
            9 => '2017-07-14',
            10 => '2017-06-08',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1442,
            1 => 1442,
            2 => 1442,
            3 => 1434,
            4 => 1198,
            5 => 1076,
            6 => 1076,
            7 => 1056,
            8 => 1056,
            9 => 913,
            10 => 913,
            11 => 823,
            12 => 823,
            13 => 703,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2018-03-21',
            3 => '2018-03-21',
            4 => '2017-12-20',
            5 => '2017-11-15',
            6 => '2017-11-15',
            7 => '2017-11-10',
            8 => '2017-11-10',
            9 => '2017-10-12',
            10 => '2017-10-12',
            11 => '2017-08-20',
            12 => '2017-08-20',
            13 => '2017-06-30',
          ),
        ),
        'Nordisk Brænderi (Hjortdalvej 227, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nordisk Brænderi',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 227, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1394,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1345,
            1 => 1318,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-01-31',
          ),
        ),
        'Nicolas Vahé ApS (Industrivej 29, 7430 Ikast)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nicolas Vahé ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7430',
            'cust_city' => 'Ikast',
            'cust_address' => 'Industrivej 29, 7430 Ikast',
          ),
          'invoices' => 
          array (
            0 => 1204,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Skotlander ApS (Hjulmagervej 19, 9490 Pandrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9490',
            'cust_city' => 'Pandrup',
            'cust_address' => 'Hjulmagervej 19, 9490 Pandrup',
          ),
          'invoices' => 
          array (
            0 => 1200,
            1 => 1113,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-23',
          ),
        ),
        'Drinkx (Fiskerihavnsgade 23, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinkx',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Fiskerihavnsgade 23, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1055,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
        'Pakkecenter Nord (Aalborgvej 14, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pakkecenter Nord',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Aalborgvej 14, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 920,
            1 => 852,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-15',
            1 => '2017-09-06',
          ),
        ),
        'Nordisk Brænderi (Hjortdalvej227, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nordisk Brænderi',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej227, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 826,
            1 => 787,
            2 => 787,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-21',
            1 => '2017-08-02',
            2 => '2017-08-02',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1439,
            1 => 1439,
            2 => 1303,
            3 => 1303,
            4 => 933,
            5 => 745,
            6 => 745,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2018-03-19',
            2 => '2018-01-11',
            3 => '2018-01-11',
            4 => '2017-10-24',
            5 => '2017-07-17',
            6 => '2017-07-17',
          ),
        ),
        'Sigurd Muller Vinhandel A/S (Otto Mønstedsvej 2, 9200 Aalborg SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sigurd Muller Vinhandel A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg SV',
            'cust_address' => 'Otto Mønstedsvej 2, 9200 Aalborg SV',
          ),
          'invoices' => 
          array (
            0 => 1436,
            1 => 1096,
            2 => 813,
            3 => 813,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-11-22',
            2 => '2017-08-20',
            3 => '2017-08-20',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1430,
            1 => 1254,
            2 => 1021,
            3 => 1021,
            4 => 833,
            5 => 833,
            6 => 791,
            7 => 753,
            8 => 753,
            9 => 746,
            10 => 746,
            11 => 676,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2017-12-27',
            2 => '2017-11-02',
            3 => '2017-11-02',
            4 => '2017-08-23',
            5 => '2017-08-23',
            6 => '2017-08-02',
            7 => '2017-07-16',
            8 => '2017-07-16',
            9 => '2017-07-16',
            10 => '2017-07-16',
            11 => '2017-06-14',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 1413,
            1 => 1413,
            2 => 1132,
            3 => 901,
            4 => 901,
            5 => 824,
            6 => 762,
            7 => 696,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
            1 => '2018-03-09',
            2 => '2017-11-29',
            3 => '2017-10-09',
            4 => '2017-10-09',
            5 => '2017-08-21',
            6 => '2017-07-24',
            7 => '2017-06-30',
          ),
        ),
        'Huset Appel (Feldsingvej 3, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Huset Appel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Feldsingvej 3, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 1398,
            1 => 1398,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
          ),
        ),
        'Vinspecialisten Varde (Ndr.Boulevard 90, 6800 Varde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Varde',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6800',
            'cust_city' => 'Varde',
            'cust_address' => 'Ndr.Boulevard 90, 6800 Varde',
          ),
          'invoices' => 
          array (
            0 => 1396,
            1 => 733,
            2 => 733,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2017-07-10',
            2 => '2017-07-10',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1384,
            1 => 1373,
            2 => 1360,
            3 => 1294,
            4 => 1218,
            5 => 1157,
            6 => 1077,
            7 => 1014,
            8 => 956,
            9 => 889,
            10 => 889,
            11 => 879,
            12 => 879,
            13 => 750,
            14 => 681,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-22',
            3 => '2018-01-19',
            4 => '2017-12-20',
            5 => '2017-12-07',
            6 => '2017-11-16',
            7 => '2017-10-31',
            8 => '2017-10-25',
            9 => '2017-09-29',
            10 => '2017-09-29',
            11 => '2017-09-26',
            12 => '2017-09-26',
            13 => '2017-07-18',
            14 => '2017-06-21',
          ),
        ),
        'Vinoble Aarhus (Guldsmedgade 20, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Aarhus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Guldsmedgade 20, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1371,
            1 => 1371,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1370,
            1 => 1247,
            2 => 1247,
            3 => 1241,
            4 => 1241,
            5 => 1221,
            6 => 1221,
            7 => 1127,
            8 => 1042,
            9 => 1042,
            10 => 804,
            11 => 804,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2017-12-21',
            2 => '2017-12-21',
            3 => '2017-12-19',
            4 => '2017-12-19',
            5 => '2017-12-18',
            6 => '2017-12-18',
            7 => '2017-11-29',
            8 => '2017-11-06',
            9 => '2017-11-06',
            10 => '2017-08-02',
            11 => '2017-08-02',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1364,
            1 => 1364,
            2 => 1354,
            3 => 1215,
            4 => 1169,
            5 => 1169,
            6 => 1049,
            7 => 1049,
            8 => 1049,
            9 => 914,
            10 => 838,
            11 => 774,
            12 => 774,
            13 => 739,
            14 => 722,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-02-12',
            2 => '2018-02-22',
            3 => '2017-12-20',
            4 => '2017-12-11',
            5 => '2017-12-11',
            6 => '2017-11-08',
            7 => '2017-11-08',
            8 => '2017-11-08',
            9 => '2017-10-06',
            10 => '2017-08-25',
            11 => '2017-07-26',
            12 => '2017-07-26',
            13 => '2017-07-06',
            14 => '2017-07-05',
          ),
        ),
        'Smagførst (Jægergårdsgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Jægergårdsgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1352,
            1 => 1211,
            2 => 1211,
            3 => 1211,
            4 => 977,
            5 => 977,
            6 => 977,
            7 => 862,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-20',
            2 => '2017-12-20',
            3 => '2017-12-20',
            4 => '2017-10-27',
            5 => '2017-10-27',
            6 => '2017-10-27',
            7 => '2017-09-23',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1342,
            1 => 1300,
            2 => 1104,
            3 => 1104,
            4 => 960,
            5 => 960,
            6 => 960,
            7 => 828,
            8 => 828,
            9 => 749,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-01-19',
            2 => '2017-11-23',
            3 => '2017-11-23',
            4 => '2017-10-26',
            5 => '2017-10-26',
            6 => '2017-10-26',
            7 => '2017-08-22',
            8 => '2017-08-22',
            9 => '2017-07-18',
          ),
        ),
        'Vin&Vin (Torvet 8, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin&Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Torvet 8, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 1323,
            1 => 633,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
            1 => '2017-05-24',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 1315,
            1 => 1212,
            2 => 1167,
            3 => 766,
            4 => 766,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
            1 => '2017-12-20',
            2 => '2017-12-11',
            3 => '2017-07-24',
            4 => '2017-07-24',
          ),
        ),
        'Bisgaard Vinhandel (Jernbanegade 4, 9530 Støvring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bisgaard Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9530',
            'cust_city' => 'Støvring',
            'cust_address' => 'Jernbanegade 4, 9530 Støvring',
          ),
          'invoices' => 
          array (
            0 => 1311,
            1 => 943,
            2 => 943,
            3 => 922,
            4 => 922,
            5 => 922,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
            1 => '2017-10-26',
            2 => '2017-10-26',
            3 => '2017-10-17',
            4 => '2017-10-17',
            5 => '2017-10-17',
          ),
        ),
        'Vinspecialisten Frederikshavn (Jernbanegade 2, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Frederikshavn',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Jernbanegade 2, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1307,
            1 => 1307,
            2 => 981,
            3 => 740,
            4 => 740,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
            1 => '2018-01-08',
            2 => '2017-10-27',
            3 => '2017-07-05',
            4 => '2017-07-05',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1296,
            1 => 763,
            2 => 700,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-07-24',
            2 => '2017-06-30',
          ),
        ),
        'Kolding Vinhandel (Låsbybanke 12, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kolding Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Låsbybanke 12, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 1289,
            1 => 1103,
            2 => 1002,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-17',
            1 => '2017-11-23',
            2 => '2017-10-31',
          ),
        ),
        'Eriksens Vinhandel (Jyllandsgade 29, 7000 Fredericia)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Eriksens Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7000',
            'cust_city' => 'Fredericia',
            'cust_address' => 'Jyllandsgade 29, 7000 Fredericia',
          ),
          'invoices' => 
          array (
            0 => 1246,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Vinshoppen (Hadsundvej 12, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinshoppen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Hadsundvej 12, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1225,
            1 => 938,
            2 => 938,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-17',
            1 => '2017-10-23',
            2 => '2017-10-23',
          ),
        ),
        'Uhrskov Vinhandel (Bredgade 56, 7400 Herning)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Uhrskov Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7400',
            'cust_city' => 'Herning',
            'cust_address' => 'Bredgade 56, 7400 Herning',
          ),
          'invoices' => 
          array (
            0 => 1208,
            1 => 1208,
            2 => 1106,
            3 => 1106,
            4 => 1106,
            5 => 812,
            6 => 812,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-11-23',
            3 => '2017-11-23',
            4 => '2017-11-23',
            5 => '2017-08-20',
            6 => '2017-08-20',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1141,
            1 => 1022,
            2 => 1022,
            3 => 946,
            4 => 946,
            5 => 895,
            6 => 895,
            7 => 895,
            8 => 741,
            9 => 663,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-01',
            1 => '2017-11-02',
            2 => '2017-11-02',
            3 => '2017-10-19',
            4 => '2017-10-19',
            5 => '2017-10-08',
            6 => '2017-10-08',
            7 => '2017-10-08',
            8 => '2017-07-05',
            9 => '2017-06-07',
          ),
        ),
        'Diagonalen Vin Og Blomster (Rådhusbakken 29, 7323 Give)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Diagonalen Vin Og Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7323',
            'cust_city' => 'Give',
            'cust_address' => 'Rådhusbakken 29, 7323 Give',
          ),
          'invoices' => 
          array (
            0 => 1120,
            1 => 1120,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-24',
            1 => '2017-11-24',
          ),
        ),
        'Lahvino Italiensk Vinimport (Farvervej 41, 8800 Viborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lahvino Italiensk Vinimport',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8800',
            'cust_city' => 'Viborg',
            'cust_address' => 'Farvervej 41, 8800 Viborg',
          ),
          'invoices' => 
          array (
            0 => 1078,
            1 => 1078,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
            1 => '2017-11-14',
          ),
        ),
        'Peter Vin / Øl & Vin ApS (Skrågade 33, 9400 Nørresundby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Peter Vin / Øl & Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9400',
            'cust_city' => 'Nørresundby',
            'cust_address' => 'Skrågade 33, 9400 Nørresundby',
          ),
          'invoices' => 
          array (
            0 => 1048,
            1 => 1048,
            2 => 660,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-11-08',
            2 => '2017-06-05',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 1030,
            1 => 1030,
            2 => 1030,
            3 => 880,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
            1 => '2017-11-03',
            2 => '2017-11-03',
            3 => '2017-09-27',
          ),
        ),
        'Mundskænken ApS Broen Shopping (Mundskænken Broen Shopping, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mundskænken ApS Broen Shopping',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Mundskænken Broen Shopping, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1013,
            1 => 1013,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
            1 => '2017-10-17',
          ),
        ),
        'Vinspecialisten Lemvig (Vestergade 11, 7620 Lemvig)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lemvig',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7620',
            'cust_city' => 'Lemvig',
            'cust_address' => 'Vestergade 11, 7620 Lemvig',
          ),
          'invoices' => 
          array (
            0 => 987,
            1 => 987,
            2 => 987,
            3 => 985,
            4 => 985,
            5 => 985,
            6 => 984,
            7 => 984,
            8 => 984,
            9 => 772,
            10 => 772,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
            1 => '2017-10-27',
            2 => '2017-10-27',
            3 => '2017-07-26',
            4 => '2017-07-26',
            5 => '2017-07-26',
            6 => '2017-07-26',
            7 => '2017-07-26',
            8 => '2017-07-26',
            9 => '2017-07-26',
            10 => '2017-07-26',
          ),
        ),
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 939,
            1 => 795,
            2 => 639,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-08-03',
            2 => '2017-05-25',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 897,
            1 => 897,
            2 => 849,
            3 => 849,
            4 => 784,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-10-09',
            2 => '2017-09-04',
            3 => '2017-09-04',
            4 => '2017-07-29',
          ),
        ),
        'Smagførst (Otte Ruds Gade 106 st. 5, 8200 Aarhus N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus N',
            'cust_address' => 'Otte Ruds Gade 106 st. 5, 8200 Aarhus N',
          ),
          'invoices' => 
          array (
            0 => 896,
            1 => 887,
            2 => 887,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-09-30',
            2 => '2017-09-30',
          ),
        ),
        'Vinspecialisten Haderslev ApS (Storegade 30, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Haderslev ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Storegade 30, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 891,
            1 => 891,
            2 => 806,
            3 => 806,
            4 => 647,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-03',
            1 => '2017-10-03',
            2 => '2017-08-10',
            3 => '2017-08-10',
            4 => '2017-05-27',
          ),
        ),
        'Hjørring Vin & Tobak, Skjold Burne Vinhandel (Østergade 25, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjørring Vin & Tobak, Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Østergade 25, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 843,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'Vinspecialisten Silkeborg (Vestergade 23, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Silkeborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Vestergade 23, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 839,
            1 => 839,
            2 => 679,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
            1 => '2017-08-31',
            2 => '2017-06-13',
          ),
        ),
        'VINOBLE & Vinspecialisten Skanderborg (Adelgade 75, 8660 Skanderborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VINOBLE & Vinspecialisten Skanderborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8660',
            'cust_city' => 'Skanderborg',
            'cust_address' => 'Adelgade 75, 8660 Skanderborg',
          ),
          'invoices' => 
          array (
            0 => 743,
            1 => 734,
            2 => 734,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-14',
            1 => '2017-07-10',
            2 => '2017-07-10',
          ),
        ),
        'Vinspecialisten Brønderslev (Algade 60, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Brønderslev',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Algade 60, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 697,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'Restaurant Tabu II ApS (Vesterå 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Tabu II ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1392,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-03',
          ),
        ),
        'CANblau Aalborg (Ved stranden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'CANblau Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Ved stranden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 786,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-01',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1349,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-19',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1306,
            1 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
            1 => '2017-12-05',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hotel Skjern (Bredgade 58, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Skjern',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Bredgade 58, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 937,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
          ),
        ),
        'Mungo Park Kolding (Fredericiagade 1, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mungo Park Kolding',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Fredericiagade 1, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 936,
            1 => 936,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-25',
          ),
        ),
        'Anholt Gin (Østervej 11, 8592 Anholt)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Anholt Gin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8592',
            'cust_city' => 'Anholt',
            'cust_address' => 'Østervej 11, 8592 Anholt',
          ),
          'invoices' => 
          array (
            0 => 698,
            1 => 698,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
            1 => '2017-06-30',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Alkoholfributik (Bakkedraget 1, 4793 Bogø By)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alkoholfributik',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4793',
            'cust_city' => 'Bogø By',
            'cust_address' => 'Bakkedraget 1, 4793 Bogø By',
          ),
          'invoices' => 
          array (
            0 => 1447,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1365,
            1 => 1365,
            2 => 1214,
            3 => 1017,
            4 => 1017,
            5 => 1017,
            6 => 1017,
            7 => 796,
            8 => 796,
            9 => 729,
            10 => 729,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
            2 => '2017-12-20',
            3 => '2017-11-01',
            4 => '2017-11-01',
            5 => '2017-11-01',
            6 => '2017-11-01',
            7 => '2017-08-02',
            8 => '2017-08-02',
            9 => '2017-07-11',
            10 => '2017-07-11',
          ),
        ),
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1359,
            1 => 1359,
            2 => 1257,
            3 => 1005,
            4 => 1005,
            5 => 1005,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
            1 => '2018-02-15',
            2 => '2017-12-27',
            3 => '2017-10-31',
            4 => '2017-10-31',
            5 => '2017-10-31',
          ),
        ),
        'MIKKELLER APS / SCANGLOBAL (HELGESHØJ ALLE 12, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MIKKELLER APS / SCANGLOBAL',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'HELGESHØJ ALLE 12, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1298,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-03',
          ),
        ),
        'Drinky (Solbjergvej 14, 8260 Viby J)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinky',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8260',
            'cust_city' => 'Viby J',
            'cust_address' => 'Solbjergvej 14, 8260 Viby J',
          ),
          'invoices' => 
          array (
            0 => 1223,
            1 => 735,
            2 => 735,
            3 => 646,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-18',
            1 => '2017-07-09',
            2 => '2017-07-09',
            3 => '2017-05-30',
          ),
        ),
        'Alkoholfributik (Ringager 19, 2605 Brøndby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alkoholfributik',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2605',
            'cust_city' => 'Brøndby',
            'cust_address' => 'Ringager 19, 2605 Brøndby',
          ),
          'invoices' => 
          array (
            0 => 1205,
            1 => 953,
            2 => 797,
            3 => 727,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-10-26',
            2 => '2017-08-02',
            3 => '2017-07-13',
          ),
        ),
        'Drinky (Haslegårdsvej 20, 8210 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinky',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8210',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Haslegårdsvej 20, 8210 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1088,
            1 => 1088,
            2 => 769,
            3 => 769,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
            1 => '2017-11-20',
            2 => '2017-07-24',
            3 => '2017-07-24',
          ),
        ),
        'Uponor (Kornmarksvej 21, 2605 Brøndby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Uponor',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2605',
            'cust_city' => 'Brøndby',
            'cust_address' => 'Kornmarksvej 21, 2605 Brøndby',
          ),
          'invoices' => 
          array (
            0 => 1062,
            1 => 1062,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
            1 => '2017-11-14',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Rombo (Falkoner Allé 46, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rombo',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Allé 46, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1414,
            1 => 1353,
            2 => 1341,
            3 => 1220,
            4 => 941,
            5 => 941,
            6 => 888,
            7 => 888,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-02-22',
            2 => '2018-02-01',
            3 => '2017-12-18',
            4 => '2017-10-19',
            5 => '2017-10-19',
            6 => '2017-09-28',
            7 => '2017-09-28',
          ),
        ),
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1304,
            1 => 890,
            2 => 890,
            3 => 890,
            4 => 815,
            5 => 690,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-10',
            1 => '2017-10-02',
            2 => '2017-10-02',
            3 => '2017-10-02',
            4 => '2017-08-15',
            5 => '2017-06-26',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Rahbek&Læssøe Aps (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1418,
            1 => 1193,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2017-12-13',
          ),
        ),
        'Holte Vinlager, Odense (Dalumvej 59B, 5250 Odense SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Odense',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5250',
            'cust_city' => 'Odense SV',
            'cust_address' => 'Dalumvej 59B, 5250 Odense SV',
          ),
          'invoices' => 
          array (
            0 => 1233,
            1 => 1233,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
            1 => '2017-12-08',
          ),
        ),
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 1118,
            1 => 1118,
            2 => 1027,
            3 => 1027,
            4 => 1027,
            5 => 1027,
            6 => 1025,
            7 => 1025,
            8 => 1025,
            9 => 1025,
            10 => 649,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-26',
            1 => '2017-11-26',
            2 => '2017-11-03',
            3 => '2017-11-03',
            4 => '2017-11-03',
            5 => '2017-11-03',
            6 => '2017-11-03',
            7 => '2017-11-03',
            8 => '2017-11-03',
            9 => '2017-11-03',
            10 => '2017-05-22',
          ),
        ),
        'Rahbek&Læssøe Aps (Rahbeksvej 9, 5230 Odense M)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5230',
            'cust_city' => 'Odense M',
            'cust_address' => 'Rahbeksvej 9, 5230 Odense M',
          ),
          'invoices' => 
          array (
            0 => 945,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-19',
          ),
        ),
        'Rahbek&Læssøe AoS (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe AoS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 847,
            1 => 847,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-04',
            1 => '2017-09-04',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinens Verden, Slotsgade (Slotsgade 21 C-D, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinens Verden, Slotsgade',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Slotsgade 21 C-D, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1412,
            1 => 1412,
            2 => 969,
            3 => 765,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-05',
            1 => '2018-03-05',
            2 => '2017-10-24',
            3 => '2017-07-22',
          ),
        ),
        'Vinspecialisten Middelfart ApS (Østergade 7, 5500 Middelfart)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Middelfart ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5500',
            'cust_city' => 'Middelfart',
            'cust_address' => 'Østergade 7, 5500 Middelfart',
          ),
          'invoices' => 
          array (
            0 => 955,
            1 => 955,
            2 => 955,
            3 => 811,
            4 => 811,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-26',
            2 => '2017-10-26',
            3 => '2017-08-18',
            4 => '2017-08-18',
          ),
        ),
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 830,
            1 => 794,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-23',
            1 => '2017-08-02',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmandsgaard (Torvet 5, 5970 Ærøskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmandsgaard',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5970',
            'cust_city' => 'Ærøskøbing',
            'cust_address' => 'Torvet 5, 5970 Ærøskøbing',
          ),
          'invoices' => 
          array (
            0 => 952,
            1 => 952,
            2 => 952,
            3 => 807,
            4 => 728,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-10-09',
            2 => '2017-10-09',
            3 => '2017-08-10',
            4 => '2017-07-13',
          ),
        ),
      ),
    ),
  ),
  'Anholt Gin' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1474,
            1 => 1474,
            2 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-31',
            1 => '2018-03-31',
            2 => '2017-12-31',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1274,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-13',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1238,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 1397,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'Vinspecialisten Lyngby (Klampenborgvej 232, 2800 Lyngby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lyngby',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2800',
            'cust_city' => 'Lyngby',
            'cust_address' => 'Klampenborgvej 232, 2800 Lyngby',
          ),
          'invoices' => 
          array (
            0 => 1374,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1367,
            1 => 1256,
            2 => 1207,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-29',
            2 => '2017-12-20',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1340,
            1 => 1242,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-12-12',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1339,
            1 => 1177,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-12-13',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1321,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-24',
          ),
        ),
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
          ),
        ),
        'Borgstrøm Vinhandel (Centerholmen 8A, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Borgstrøm Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Centerholmen 8A, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1237,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1197,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Skælskør Vinhandel (Algade 6-8, 4230 Skælskør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skælskør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4230',
            'cust_city' => 'Skælskør',
            'cust_address' => 'Algade 6-8, 4230 Skælskør',
          ),
          'invoices' => 
          array (
            0 => 1176,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Helsingør Vinhandel (Bramstræde 2C, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Helsingør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Bramstræde 2C, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 1174,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Eriksen Vinhandel (Nygade 4, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Eriksen Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Nygade 4, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1173,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1338,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-03',
          ),
        ),
        'GinGin I/S (Ternevej 30, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GinGin I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ternevej 30, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1320,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-27',
          ),
        ),
        'Butik Ø (Nordre Frihavnsgade 26, st, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Ø',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Nordre Frihavnsgade 26, st, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1178,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1190,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-14',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Rahbek&Læssøe Aps (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1418,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
        'Holte Vinlager, Odense (Dalumvej 59B, 5250 Odense SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Odense',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5250',
            'cust_city' => 'Odense SV',
            'cust_address' => 'Dalumvej 59B, 5250 Odense SV',
          ),
          'invoices' => 
          array (
            0 => 1233,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1179,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Restaurant: Michelinrestaurant' => 
      array (
        'Restaurant Tabu II ApS (Vesterå 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Tabu II ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1392,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-03',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1378,
            1 => 1217,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-20',
          ),
        ),
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1258,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-28',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1240,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1231,
            1 => 1175,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-12-12',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vin&Vin (Torvet 8, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin&Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Torvet 8, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 1323,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1303,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-11',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1300,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1247,
            1 => 1221,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
            1 => '2017-12-18',
          ),
        ),
        'Vinshoppen (Hadsundvej 12, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinshoppen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Hadsundvej 12, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1226,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Peter Vin / Øl & Vin ApS (Skrågade 33, 9400 Nørresundby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Peter Vin / Øl & Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9400',
            'cust_city' => 'Nørresundby',
            'cust_address' => 'Skrågade 33, 9400 Nørresundby',
          ),
          'invoices' => 
          array (
            0 => 1224,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-18',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 1212,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1194,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1181,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1306,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Skotlander ApS (Damvej 54, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Damvej 54, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1299,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-15',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Aagaard Kro (Bramdrupvej 136, 6040 Egtved)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aagaard Kro',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6040',
            'cust_city' => 'Egtved',
            'cust_address' => 'Bramdrupvej 136, 6040 Egtved',
          ),
          'invoices' => 
          array (
            0 => 1182,
            1 => 1182,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-12-13',
          ),
        ),
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1180,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1385,
            1 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
            1 => '2018-01-03',
          ),
        ),
        'Drinky (Solbjergvej 14, 8260 Viby J)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinky',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8260',
            'cust_city' => 'Viby J',
            'cust_address' => 'Solbjergvej 14, 8260 Viby J',
          ),
          'invoices' => 
          array (
            0 => 1223,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-18',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1214,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Rombo (Falkoner Allé 46, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rombo',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Allé 46, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1341,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-01',
          ),
        ),
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1304,
            1 => 1222,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-10',
            1 => '2017-12-18',
          ),
        ),
      ),
    ),
  ),
  'Trolden Distillery' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1474,
            1 => 1474,
            2 => 1266,
            3 => 1266,
            4 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-31',
            1 => '2018-03-31',
            2 => '2017-12-31',
            3 => '2017-12-31',
            4 => '2017-12-31',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1387,
            1 => 1375,
            2 => 1375,
            3 => 1355,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-27',
            1 => '2018-02-22',
            2 => '2018-02-22',
            3 => '2018-02-15',
          ),
        ),
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1377,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1325,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1230,
            1 => 1230,
            2 => 1061,
            3 => 900,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-11-13',
            3 => '2017-10-10',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1227,
            1 => 1219,
            2 => 1192,
            3 => 929,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-12-19',
            2 => '2017-12-14',
            3 => '2017-10-18',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 1820 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1820',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 1820 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 1216,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1114,
            1 => 1052,
            2 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-09',
            2 => '2017-09-23',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 702,
            2 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-06-30',
            2 => '2017-06-30',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 866,
            1 => 866,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
            1 => '2017-09-23',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10 kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10 kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 836,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-24',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 736,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 6 kld th, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 6 kld th, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 706,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe (Hal 2, Frederiksborggade 21,, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Frederiksborggade 21,, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 705,
            1 => 705,
            2 => 648,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-01',
            1 => '2017-07-01',
            2 => '2017-05-23',
          ),
        ),
        'Holte Vinlager, Gentofte (Smakkegårdsvej 137, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Gentofte',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Smakkegårdsvej 137, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 667,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-08',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 659,
            1 => 634,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
            1 => '2017-05-24',
          ),
        ),
        'Att: Gin, Rom & Cocktail Festivalen i Pressen (Rådhuspladsen 37, 1785 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Att: Gin, Rom & Cocktail Festivalen i Pressen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1785',
            'cust_city' => 'København V',
            'cust_address' => 'Rådhuspladsen 37, 1785 København V',
          ),
          'invoices' => 
          array (
            0 => 655,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, Hal 2 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, Hal 2 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 644,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1453,
            1 => 1235,
            2 => 1074,
            3 => 761,
            4 => 759,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2017-12-20',
            2 => '2017-11-15',
            3 => '2017-07-23',
            4 => '2017-07-21',
          ),
        ),
        'Brdr D Østerbro (Østerbrogade 152, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Østerbro',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 152, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1402,
            1 => 947,
            2 => 792,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-07',
            1 => '2017-10-26',
            2 => '2017-08-03',
          ),
        ),
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 1397,
            1 => 904,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2017-10-11',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1380,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1339,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Skjold Burne (Frederiksværksgade 95, 3400 hillerød)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3400',
            'cust_city' => 'hillerød',
            'cust_address' => 'Frederiksværksgade 95, 3400 hillerød',
          ),
          'invoices' => 
          array (
            0 => 1265,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-02',
          ),
        ),
        'Inco Glostrup (Ejby Industrivej 11, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco Glostrup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Ejby Industrivej 11, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1262,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-04',
          ),
        ),
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
          ),
        ),
        'Bagsværd Vinhandel (Bagsværd Hovedgade 125, 2880 Bagsværd)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bagsværd Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2880',
            'cust_city' => 'Bagsværd',
            'cust_address' => 'Bagsværd Hovedgade 125, 2880 Bagsværd',
          ),
          'invoices' => 
          array (
            0 => 1054,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-07',
          ),
        ),
        'Brdr. D (Rolighedsvej 9, 1958 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr. D',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1958',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Rolighedsvej 9, 1958 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 986,
            1 => 986,
            2 => 940,
            3 => 940,
            4 => 793,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-25',
            2 => '2017-10-26',
            3 => '2017-10-26',
            4 => '2017-08-03',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 831,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'Næstved vinhandel (Sct. Jørgens Park 70, 4700 Næstved)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Næstved vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4700',
            'cust_city' => 'Næstved',
            'cust_address' => 'Sct. Jørgens Park 70, 4700 Næstved',
          ),
          'invoices' => 
          array (
            0 => 788,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-02',
          ),
        ),
        'Inco (Ingerslevsgade 42, 1711 København V.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V.',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V.',
          ),
          'invoices' => 
          array (
            0 => 656,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1421,
            1 => 1322,
            2 => 1271,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2018-01-31',
            2 => '2018-01-12',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'PostNord A/S, Screeningecenter Øst. ATT Restaurant Tårnet, Folketinget Christiansborg (Oliefabriksvej 51, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PostNord A/S, Screeningecenter Øst. ATT Restaurant Tårnet, Folketinget Christiansborg',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Oliefabriksvej 51, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1152,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-06',
          ),
        ),
        'Restaurant Skade (Rosengaarden 12, 1174 Copenhagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Skade',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1174',
            'cust_city' => 'Copenhagen',
            'cust_address' => 'Rosengaarden 12, 1174 Copenhagen',
          ),
          'invoices' => 
          array (
            0 => 640,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-30',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Smagning.com II (Bækkeskovvej 81, 2700 Brønshøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagning.com II',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2700',
            'cust_city' => 'Brønshøj',
            'cust_address' => 'Bækkeskovvej 81, 2700 Brønshøj',
          ),
          'invoices' => 
          array (
            0 => 1095,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
          ),
        ),
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 748,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-14',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1466,
            1 => 1466,
            2 => 1466,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2018-03-28',
          ),
        ),
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-03',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1214,
            1 => 796,
            2 => 796,
            3 => 729,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-08-02',
            2 => '2017-08-02',
            3 => '2017-07-11',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1304,
            1 => 815,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-10',
            1 => '2017-08-15',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Vesløs Kro (Vesløs Stationsvej 25, 7742 Vesløs)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vesløs Kro',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7742',
            'cust_city' => 'Vesløs',
            'cust_address' => 'Vesløs Stationsvej 25, 7742 Vesløs',
          ),
          'invoices' => 
          array (
            0 => 1463,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1295,
            1 => 1115,
            2 => 934,
            3 => 861,
            4 => 835,
            5 => 666,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-11-23',
            2 => '2017-10-25',
            3 => '2017-09-19',
            4 => '2017-08-31',
            5 => '2017-06-09',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinoble Aarhus (Guldsmedgade 20, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Aarhus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Guldsmedgade 20, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1371,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1294,
            1 => 1294,
            2 => 1077,
            3 => 1014,
            4 => 809,
            5 => 681,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
            2 => '2017-11-16',
            3 => '2017-10-31',
            4 => '2017-08-10',
            5 => '2017-06-21',
          ),
        ),
        'Skjold Burne/Lysholdt (Fiskebrogade 8, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne/Lysholdt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Fiskebrogade 8, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1172,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1128,
            1 => 753,
            2 => 746,
            3 => 676,
            4 => 676,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-27',
            1 => '2017-07-16',
            2 => '2017-07-16',
            3 => '2017-06-14',
            4 => '2017-06-14',
          ),
        ),
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 939,
            1 => 939,
            2 => 795,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-25',
            2 => '2017-08-03',
          ),
        ),
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 933,
            1 => 933,
            2 => 745,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-24',
            1 => '2017-10-24',
            2 => '2017-07-17',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 895,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-08',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 880,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-27',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 784,
            1 => 784,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-29',
            1 => '2017-07-29',
          ),
        ),
        'Vinspecialisten Lemvig (Vestergade 11, 7620 Lemvig)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lemvig',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7620',
            'cust_city' => 'Lemvig',
            'cust_address' => 'Vestergade 11, 7620 Lemvig',
          ),
          'invoices' => 
          array (
            0 => 772,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 766,
            1 => 766,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
            1 => '2017-07-24',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 700,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
        'Lahvino Italiensk Vinimport (Farvervej 41, 8800 Viborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lahvino Italiensk Vinimport',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8800',
            'cust_city' => 'Viborg',
            'cust_address' => 'Farvervej 41, 8800 Viborg',
          ),
          'invoices' => 
          array (
            0 => 674,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1345,
            1 => 1318,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-01-31',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1198,
            1 => 1056,
            2 => 913,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-10',
            2 => '2017-10-12',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1258,
            1 => 1110,
            2 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-28',
            1 => '2017-11-23',
            2 => '2017-11-06',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1251,
            1 => 1184,
            2 => 1028,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
            1 => '2017-12-11',
            2 => '2017-11-01',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1217,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1165,
            1 => 687,
            2 => 687,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-06-22',
            2 => '2017-06-22',
          ),
        ),
        'Aabybro Mejeri (Brogårdsvej 148, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aabybro Mejeri',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Brogårdsvej 148, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1101,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-09',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 755,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-19',
          ),
        ),
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 737,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-07',
          ),
        ),
        'Albert Rex (Nørregade 22, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Albert Rex',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nørregade 22, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 673,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
        'Got2LoveIt (Gunderupvej 96, 9640 Farsø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Got2LoveIt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9640',
            'cust_city' => 'Farsø',
            'cust_address' => 'Gunderupvej 96, 9640 Farsø',
          ),
          'invoices' => 
          array (
            0 => 632,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 867,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-15',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Rahbek&Læssøe Aps (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1418,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
        'Holte Vinlager, Odense (Dalumvej 59B, 5250 Odense SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Odense',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5250',
            'cust_city' => 'Odense SV',
            'cust_address' => 'Dalumvej 59B, 5250 Odense SV',
          ),
          'invoices' => 
          array (
            0 => 1233,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
          ),
        ),
        'Boxit (Thorslundsvej 3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Boxit',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Thorslundsvej 3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 707,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-03',
          ),
        ),
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 649,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-22',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1179,
            1 => 830,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-08-23',
          ),
        ),
        'Vinspecialisten Middelfart ApS (Østergade 7, 5500 Middelfart)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Middelfart ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5500',
            'cust_city' => 'Middelfart',
            'cust_address' => 'Østergade 7, 5500 Middelfart',
          ),
          'invoices' => 
          array (
            0 => 811,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-18',
          ),
        ),
      ),
    ),
  ),
  'Queens of Denmark' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1474,
            1 => 1474,
            2 => 1474,
            3 => 1474,
            4 => 1474,
            5 => 1266,
            6 => 1266,
            7 => 1266,
            8 => 1266,
            9 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-31',
            1 => '2018-03-31',
            2 => '2018-03-31',
            3 => '2018-03-31',
            4 => '2018-03-31',
            5 => '2017-12-31',
            6 => '2017-12-31',
            7 => '2017-12-31',
            8 => '2017-12-31',
            9 => '2017-12-31',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 10 kld tv, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 10 kld tv, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 1426,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        ' (Krogsbøllevej 13, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Krogsbøllevej 13, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1411,
            1 => 1411,
            2 => 1411,
            3 => 1411,
            4 => 1411,
            5 => 1411,
            6 => 1411,
            7 => 1411,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-03-08',
            2 => '2018-03-08',
            3 => '2018-03-08',
            4 => '2018-03-08',
            5 => '2018-03-08',
            6 => '2018-03-08',
            7 => '2018-03-08',
          ),
        ),
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1377,
            1 => 1377,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1375,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1041,
            1 => 1041,
            2 => 1041,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-07',
            1 => '2017-11-07',
            2 => '2017-11-07',
          ),
        ),
        'Mondo Kaos (Birkegade 1, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mondo Kaos',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Birkegade 1, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1029,
            1 => 1029,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
            1 => '2017-11-03',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Café Svanen (Labæk 35, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Café Svanen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Labæk 35, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1465,
            1 => 1465,
            2 => 1465,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2018-03-19',
            2 => '2018-03-19',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Stork Concepts (Frederiksborggade 1b, 4. sal, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Stork Concepts',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 1b, 4. sal, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1457,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Hotel Cecil (Niels Hemmingsens Gade 11, 1153 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Cecil',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1153',
            'cust_city' => 'København K',
            'cust_address' => 'Niels Hemmingsens Gade 11, 1153 København K',
          ),
          'invoices' => 
          array (
            0 => 1369,
            1 => 1369,
            2 => 1361,
            3 => 1361,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-22',
            3 => '2018-02-22',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'My Coffee Shop (Hundige Strandvej 212c, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'My Coffee Shop',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Hundige Strandvej 212c, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1386,
            1 => 1386,
            2 => 1386,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
            1 => '2018-02-20',
            2 => '2018-02-20',
          ),
        ),
        'Spiritium Cocktails IVS (c/o Eye-Grain ApS, 2730 Herlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Spiritium Cocktails IVS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2730',
            'cust_city' => 'Herlev',
            'cust_address' => 'c/o Eye-Grain ApS, 2730 Herlev',
          ),
          'invoices' => 
          array (
            0 => 1382,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'My 50\'s closet (Enghaven 11, 3630 Jægerspris)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'My 50\'s closet',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3630',
            'cust_city' => 'Jægerspris',
            'cust_address' => 'Enghaven 11, 3630 Jægerspris',
          ),
          'invoices' => 
          array (
            0 => 1191,
            1 => 1191,
            2 => 1191,
            3 => 1191,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-14',
            1 => '2017-12-14',
            2 => '2017-12-14',
            3 => '2017-12-14',
          ),
        ),
        'GinGin I/S (Ternevej 30, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GinGin I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ternevej 30, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1037,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-04',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1367,
            1 => 1367,
            2 => 1267,
            3 => 1267,
            4 => 1267,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2018-02-22',
            2 => '2018-01-08',
            3 => '2018-01-08',
            4 => '2018-01-08',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'PostNord A/S, Screeningecenter Øst. ATT Restaurant Tårnet, Folketinget Christiansborg (Oliefabriksvej 51, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PostNord A/S, Screeningecenter Øst. ATT Restaurant Tårnet, Folketinget Christiansborg',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Oliefabriksvej 51, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1152,
            1 => 1152,
            2 => 1152,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-06',
            1 => '2017-12-06',
            2 => '2017-12-06',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Fjelsted Skov Kro (Store Landevej 92, 5592 Ejby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fjelsted Skov Kro',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5592',
            'cust_city' => 'Ejby',
            'cust_address' => 'Store Landevej 92, 5592 Ejby',
          ),
          'invoices' => 
          array (
            0 => 1467,
            1 => 1467,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1094,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1466,
            1 => 1466,
            2 => 1466,
            3 => 1466,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2018-03-28',
            3 => '2018-03-28',
          ),
        ),
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1385,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
          ),
        ),
        'missmia.dk (Nørre Søgade 7, 2. th, 1370 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'missmia.dk',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '1370',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Søgade 7, 2. th, 1370 København K',
          ),
          'invoices' => 
          array (
            0 => 1064,
            1 => 1040,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-15',
            1 => '2017-10-24',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Kornblomsten (Vestergade 5, 7850 Stoholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kornblomsten',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7850',
            'cust_city' => 'Stoholm',
            'cust_address' => 'Vestergade 5, 7850 Stoholm',
          ),
          'invoices' => 
          array (
            0 => 1464,
            1 => 1464,
            2 => 1464,
            3 => 1464,
            4 => 1464,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2018-03-28',
            3 => '2018-03-28',
            4 => '2018-03-28',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1378,
            1 => 1378,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2018-02-22',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1366,
            1 => 1234,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-12-07',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1170,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
          ),
        ),
        'Rene\'s Vin Og Grønttorv (Østergade 2 B, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rene\'s Vin Og Grønttorv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Østergade 2 B, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1039,
            1 => 1039,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-04',
            1 => '2017-11-04',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Vesløs Kro (Vesløs Stationsvej 25, 7742 Vesløs)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vesløs Kro',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7742',
            'cust_city' => 'Vesløs',
            'cust_address' => 'Vesløs Stationsvej 25, 7742 Vesløs',
          ),
          'invoices' => 
          array (
            0 => 1463,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Restaurant Villa Vest (Strandvejen 138, 9800 Lønstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Villa Vest',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Lønstrup',
            'cust_address' => 'Strandvejen 138, 9800 Lønstrup',
          ),
          'invoices' => 
          array (
            0 => 1425,
            1 => 1425,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1435,
            1 => 1435,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-20',
            1 => '2018-03-20',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1296,
            1 => 1296,
            2 => 1296,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
            2 => '2018-01-19',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1294,
            1 => 1294,
            2 => 1294,
            3 => 1294,
            4 => 1294,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
            2 => '2018-01-19',
            3 => '2018-01-19',
            4 => '2018-01-19',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1254,
            1 => 1128,
            2 => 1128,
            3 => 1128,
            4 => 1128,
            5 => 1128,
            6 => 1128,
            7 => 1128,
            8 => 1128,
            9 => 1021,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-27',
            1 => '2017-11-27',
            2 => '2017-11-27',
            3 => '2017-11-27',
            4 => '2017-11-27',
            5 => '2017-11-27',
            6 => '2017-11-27',
            7 => '2017-11-27',
            8 => '2017-11-27',
            9 => '2017-11-02',
          ),
        ),
        'Skjold Burne/Lysholdt (Fiskebrogade 8, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne/Lysholdt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Fiskebrogade 8, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1186,
            1 => 1172,
            2 => 1172,
            3 => 1172,
            4 => 1079,
            5 => 1079,
            6 => 1079,
            7 => 1079,
            8 => 1079,
            9 => 1079,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-12-12',
            2 => '2017-12-12',
            3 => '2017-12-12',
            4 => '2017-11-17',
            5 => '2017-11-17',
            6 => '2017-11-17',
            7 => '2017-11-17',
            8 => '2017-11-17',
            9 => '2017-11-17',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1049,
            1 => 1049,
            2 => 1049,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-11-08',
            2 => '2017-11-08',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 1030,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1199,
            1 => 1020,
            2 => 1020,
            3 => 1020,
            4 => 1020,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-01',
            2 => '2017-11-01',
            3 => '2017-11-01',
            4 => '2017-11-01',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1056,
            1 => 913,
            2 => 913,
            3 => 913,
            4 => 913,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-10',
            1 => '2017-10-12',
            2 => '2017-10-12',
            3 => '2017-10-12',
            4 => '2017-10-12',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Teaterhotellet.dk (Søndergade 22, 8700 Horsens)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Teaterhotellet.dk',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8700',
            'cust_city' => 'Horsens',
            'cust_address' => 'Søndergade 22, 8700 Horsens',
          ),
          'invoices' => 
          array (
            0 => 1082,
            1 => 1082,
            2 => 1082,
            3 => 1082,
            4 => 1082,
            5 => 1082,
            6 => 1082,
            7 => 1082,
            8 => 1082,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
            1 => '2017-11-17',
            2 => '2017-11-17',
            3 => '2017-11-17',
            4 => '2017-11-17',
            5 => '2017-11-17',
            6 => '2017-11-17',
            7 => '2017-11-17',
            8 => '2017-11-17',
          ),
        ),
      ),
    ),
    'Ukendt' => 
    array (
      'Ukendt' => 
      array (
        'Statista (Johannes-Brahms-Platz 1, 20355 Hamborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Statista',
            'cust_landsdel' => 'Ukendt',
            'cust_postcode' => '20355',
            'cust_city' => 'Hamborg',
            'cust_address' => 'Johannes-Brahms-Platz 1, 20355 Hamborg',
          ),
          'invoices' => 
          array (
            0 => 1462,
            1 => 1462,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-20',
            1 => '2018-03-20',
          ),
        ),
      ),
    ),
  ),
  'Ginbogen' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1474,
            1 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-31',
            1 => '2017-12-31',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1201,
            1 => 1124,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-28',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 1102,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
          ),
        ),
        'Borgstrøm Vinhandel (Centerholmen 8A, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Borgstrøm Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Centerholmen 8A, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1237,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1197,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Eriksen Vinhandel (Nygade 4, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Eriksen Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Nygade 4, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1173,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1166,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
          ),
        ),
        'Prøvestenens Vinhandel ApS (Prøvestensvej 14, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Prøvestenens Vinhandel ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Prøvestensvej 14, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 1112,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1111,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1091,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1190,
            1 => 1147,
            2 => 1099,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-14',
            1 => '2017-12-04',
            2 => '2017-11-23',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1156,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-01',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1439,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 1413,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
          ),
        ),
        'Vinspecialisten Frederikshavn (Jernbanegade 2, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Frederikshavn',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Jernbanegade 2, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1307,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1194,
            1 => 1128,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-11-27',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1181,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1141,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-01',
          ),
        ),
        'Diagonalen Vin Og Blomster (Rådhusbakken 29, 7323 Give)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Diagonalen Vin Og Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7323',
            'cust_city' => 'Give',
            'cust_address' => 'Rådhusbakken 29, 7323 Give',
          ),
          'invoices' => 
          array (
            0 => 1120,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-24',
          ),
        ),
        'Sigurd Muller Vinhandel A/S (Otto Mønstedsvej 2, 9200 Aalborg SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sigurd Muller Vinhandel A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg SV',
            'cust_address' => 'Otto Mønstedsvej 2, 9200 Aalborg SV',
          ),
          'invoices' => 
          array (
            0 => 1096,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-22',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1092,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1438,
            1 => 1184,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2017-12-11',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1165,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1144,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
          ),
        ),
        'Vinspecialisten Randers (Dytmærsken 3, 8900 Randers C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Randers',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8900',
            'cust_city' => 'Randers C',
            'cust_address' => 'Dytmærsken 3, 8900 Randers C',
          ),
          'invoices' => 
          array (
            0 => 1136,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
          ),
        ),
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1134,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-29',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1109,
            1 => 1109,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1115,
            1 => 1115,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1365,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1257,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-27',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1351,
            1 => 1222,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-19',
            1 => '2017-12-18',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 1118,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-26',
          ),
        ),
        'Rahbek&Læssøe Aps (Rahbeksvej 9, 5230 Odense M)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5230',
            'cust_city' => 'Odense M',
            'cust_address' => 'Rahbeksvej 9, 5230 Odense M',
          ),
          'invoices' => 
          array (
            0 => 1090,
            1 => 1090,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
            1 => '2017-11-20',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinspecialisten Middelfart ApS (Østergade 7, 5500 Middelfart)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Middelfart ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5500',
            'cust_city' => 'Middelfart',
            'cust_address' => 'Østergade 7, 5500 Middelfart',
          ),
          'invoices' => 
          array (
            0 => 1093,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
    ),
  ),
  'Oh Deer Tonic' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1474,
            1 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-31',
            1 => '2017-12-31',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 1820 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1820',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 1820 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 1216,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1171,
            1 => 1114,
            2 => 1052,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-11-23',
            2 => '2017-11-09',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 1102,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Café Svanen (Labæk 35, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Café Svanen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Labæk 35, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1465,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
          ),
        ),
        'The Bird & Kissmeyer (Vesterbrogade 3, 1603 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & Kissmeyer',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1603',
            'cust_city' => 'København K',
            'cust_address' => 'Vesterbrogade 3, 1603 København K',
          ),
          'invoices' => 
          array (
            0 => 1409,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'Papa Bird (Kødboderne 7, 1717 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Papa Bird',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1717',
            'cust_city' => 'København V',
            'cust_address' => 'Kødboderne 7, 1717 København V',
          ),
          'invoices' => 
          array (
            0 => 1405,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'The Bird and the Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird and the Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 1404,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-07',
          ),
        ),
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1147,
            1 => 1099,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-04',
            1 => '2017-11-23',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1452,
            1 => 1111,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2017-11-23',
          ),
        ),
        'Meny Hellerup (Strandvejen 64A, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Hellerup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Strandvejen 64A, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1407,
            1 => 1336,
            2 => 1148,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-02-11',
            2 => '2017-12-06',
          ),
        ),
        'Vinspecialisten Lyngby (Klampenborgvej 232, 2800 Lyngby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lyngby',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2800',
            'cust_city' => 'Lyngby',
            'cust_address' => 'Klampenborgvej 232, 2800 Lyngby',
          ),
          'invoices' => 
          array (
            0 => 1374,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1340,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1339,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Skjold Burne Vanløse (jernbane alle 45, 2720 vanløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vanløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2720',
            'cust_city' => 'vanløse',
            'cust_address' => 'jernbane alle 45, 2720 vanløse',
          ),
          'invoices' => 
          array (
            0 => 1324,
            1 => 1209,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-31',
            1 => '2017-12-20',
          ),
        ),
        'Østerbro Vin & Spiritus (Østerbrogade 156, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Østerbro Vin & Spiritus',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 156, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1228,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1166,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
          ),
        ),
        ' (Dyrehovedgårds Alle 18, 4220 Korsør)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4220',
            'cust_city' => 'Korsør',
            'cust_address' => 'Dyrehovedgårds Alle 18, 4220 Korsør',
          ),
          'invoices' => 
          array (
            0 => 1098,
            1 => 1080,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-02',
            1 => '2017-11-02',
          ),
        ),
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1074,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-15',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1063,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
          ),
        ),
        'Zaizon (Falkoner Alle 33, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Zaizon',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Alle 33, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1060,
            1 => 1047,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-13',
            1 => '2017-11-08',
          ),
        ),
        'Bagsværd Vinhandel (Bagsværd Hovedgade 125, 2880 Bagsværd)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bagsværd Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2880',
            'cust_city' => 'Bagsværd',
            'cust_address' => 'Bagsværd Hovedgade 125, 2880 Bagsværd',
          ),
          'invoices' => 
          array (
            0 => 1054,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-07',
          ),
        ),
        'Vinoteket ApS (Gentoftegade 54, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoteket ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gentoftegade 54, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1053,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
        'VIN121 (Peter Bangs Vej 121, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VIN121',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Peter Bangs Vej 121, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1050,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmand (Sankt Annæ Plads 22, 1250 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmand',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'København K',
            'cust_address' => 'Sankt Annæ Plads 22, 1250 København K',
          ),
          'invoices' => 
          array (
            0 => 1429,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Hav Kildegårdsvej (Kildegårdsvej 15, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hav Kildegårdsvej',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Kildegårdsvej 15, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1427,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Lidt-af-hvert I/S (Lindegårdsager 14, 2640 Hedehusene)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lidt-af-hvert I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2640',
            'cust_city' => 'Hedehusene',
            'cust_address' => 'Lindegårdsager 14, 2640 Hedehusene',
          ),
          'invoices' => 
          array (
            0 => 1083,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
          ),
        ),
        'Att: Aliva Foods (Nrdr. Frihavnsgade 68, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Att: Aliva Foods',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Nrdr. Frihavnsgade 68, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1023,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Wineboutique (Faurskovvej 21, 8370 Hadsten)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Wineboutique',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8370',
            'cust_city' => 'Hadsten',
            'cust_address' => 'Faurskovvej 21, 8370 Hadsten',
          ),
          'invoices' => 
          array (
            0 => 1468,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1430,
            1 => 1129,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2017-11-29',
          ),
        ),
        'Huset Appel (Feldsingvej 3, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Huset Appel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Feldsingvej 3, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 1398,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1370,
            1 => 1247,
            2 => 1221,
            3 => 1127,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2017-12-21',
            2 => '2017-12-18',
            3 => '2017-11-29',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1364,
            1 => 1354,
            2 => 1169,
            3 => 1049,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-02-22',
            2 => '2017-12-11',
            3 => '2017-11-08',
          ),
        ),
        '1010 Vin (Fiskergangen 12b, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => '1010 Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Fiskergangen 12b, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1362,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-13',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 1315,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Kolding Vinhandel (Låsbybanke 12, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kolding Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Låsbybanke 12, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 1289,
            1 => 1103,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-17',
            1 => '2017-11-23',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1141,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-01',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1100,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Sigurd Muller Vinhandel A/S (Otto Mønstedsvej 2, 9200 Aalborg SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sigurd Muller Vinhandel A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg SV',
            'cust_address' => 'Otto Mønstedsvej 2, 9200 Aalborg SV',
          ),
          'invoices' => 
          array (
            0 => 1096,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-22',
          ),
        ),
        'Skjold Burne/Lysholdt (Fiskebrogade 8, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne/Lysholdt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Fiskebrogade 8, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1079,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1077,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-16',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Rene\'s Vin Og Grønttorv (Østergade 2 B, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rene\'s Vin Og Grønttorv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Østergade 2 B, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1368,
            1 => 1162,
            2 => 1039,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2017-12-08',
            2 => '2017-11-04',
          ),
        ),
        'Herremagasinet (Jyllandsgade 22, 9520 Skørping)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Herremagasinet',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9520',
            'cust_city' => 'Skørping',
            'cust_address' => 'Jyllandsgade 22, 9520 Skørping',
          ),
          'invoices' => 
          array (
            0 => 1313,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1154,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-04',
          ),
        ),
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1134,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-29',
          ),
        ),
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1110,
            1 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-06',
          ),
        ),
        'Aabybro Mejeri (Brogårdsvej 148, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aabybro Mejeri',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Brogårdsvej 148, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1101,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Alexanders Blomster (østergade 1, 9560 Hadsund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alexanders Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9560',
            'cust_city' => 'Hadsund',
            'cust_address' => 'østergade 1, 9560 Hadsund',
          ),
          'invoices' => 
          array (
            0 => 1044,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1032,
            1 => 1024,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
            1 => '2017-11-03',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1350,
            1 => 1180,
            2 => 1115,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-13',
            2 => '2017-11-23',
          ),
        ),
        'Domestic Restaurant ApS (Mejlgade 35B, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Domestic Restaurant ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Mejlgade 35B, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1269,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-12',
          ),
        ),
        'Hjerting Badehotel (Strandpromenaden 1, 6710 Esbjerg V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjerting Badehotel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6710',
            'cust_city' => 'Esbjerg V',
            'cust_address' => 'Strandpromenaden 1, 6710 Esbjerg V',
          ),
          'invoices' => 
          array (
            0 => 1164,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hornsleth Bar Aalborg (Jomfru Ane Gade 11, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hornsleth Bar Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Jomfru Ane Gade 11, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1314,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        ' (Thorsvej 6, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Thorsvej 6, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1202,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1199,
            1 => 1075,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-15',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1198,
            1 => 1076,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-15',
          ),
        ),
        'Pakkecenter Nord (Aalborgvej 14, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pakkecenter Nord',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Aalborgvej 14, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 1183,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
          ),
        ),
        'Skotlander ApS (Hjulmagervej 19, 9490 Pandrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9490',
            'cust_city' => 'Pandrup',
            'cust_address' => 'Hjulmagervej 19, 9490 Pandrup',
          ),
          'invoices' => 
          array (
            0 => 1113,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1057,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-13',
          ),
        ),
        'Drinkx (Fiskerihavnsgade 23, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinkx',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Fiskerihavnsgade 23, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1055,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Alkoholfributik (Bakkedraget 1, 4793 Bogø By)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alkoholfributik',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4793',
            'cust_city' => 'Bogø By',
            'cust_address' => 'Bakkedraget 1, 4793 Bogø By',
          ),
          'invoices' => 
          array (
            0 => 1447,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Alkoholfributik (Ringager 19, 2605 Brøndby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alkoholfributik',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2605',
            'cust_city' => 'Brøndby',
            'cust_address' => 'Ringager 19, 2605 Brøndby',
          ),
          'invoices' => 
          array (
            0 => 1205,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1168,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
          ),
        ),
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1046,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1094,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
    ),
  ),
  'Snaps Bornholm' => 
  array (
    'Jylland' => 
    array (
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1470,
            1 => 1469,
            2 => 1451,
            3 => 1451,
            4 => 1437,
            5 => 1437,
            6 => 1437,
            7 => 1437,
            8 => 1437,
            9 => 1437,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-26',
            2 => '2018-03-28',
            3 => '2018-03-28',
            4 => '2018-03-21',
            5 => '2018-03-21',
            6 => '2018-03-21',
            7 => '2018-03-21',
            8 => '2018-03-21',
            9 => '2018-03-21',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1442,
            1 => 1442,
            2 => 1442,
            3 => 1442,
            4 => 1442,
            5 => 1442,
            6 => 1442,
            7 => 1442,
            8 => 1442,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2018-03-21',
            3 => '2018-03-21',
            4 => '2018-03-21',
            5 => '2018-03-21',
            6 => '2018-03-21',
            7 => '2018-03-21',
            8 => '2018-03-21',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1444,
            1 => 1420,
            2 => 1420,
            3 => 1420,
            4 => 1420,
            5 => 1420,
            6 => 1420,
            7 => 1420,
            8 => 1420,
            9 => 1420,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-22',
            1 => '2018-03-14',
            2 => '2018-03-14',
            3 => '2018-03-14',
            4 => '2018-03-14',
            5 => '2018-03-14',
            6 => '2018-03-14',
            7 => '2018-03-14',
            8 => '2018-03-14',
            9 => '2018-03-14',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1435,
            1 => 1435,
            2 => 1435,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-20',
            1 => '2018-03-20',
            2 => '2018-03-20',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1461,
            1 => 1460,
            2 => 1460,
            3 => 1460,
            4 => 1460,
            5 => 1460,
            6 => 1460,
            7 => 1460,
            8 => 1460,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-21',
            2 => '2018-03-21',
            3 => '2018-03-21',
            4 => '2018-03-21',
            5 => '2018-03-21',
            6 => '2018-03-21',
            7 => '2018-03-21',
            8 => '2018-03-21',
          ),
        ),
        'Den Franske Vinhandel (Skomagergade 3, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Franske Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Skomagergade 3, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1419,
            1 => 1419,
            2 => 1419,
            3 => 1419,
            4 => 1419,
            5 => 1419,
            6 => 1419,
            7 => 1419,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-03-08',
            2 => '2018-03-08',
            3 => '2018-03-08',
            4 => '2018-03-08',
            5 => '2018-03-08',
            6 => '2018-03-08',
            7 => '2018-03-08',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1416,
            1 => 1416,
            2 => 1416,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2018-03-14',
            2 => '2018-03-14',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Stork Concepts (Frederiksborggade 1b, 4. sal, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Stork Concepts',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 1b, 4. sal, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1457,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1421,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
      ),
    ),
  ),
  'Skotlander Rum' => 
  array (
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Wineboutique (Faurskovvej 21, 8370 Hadsten)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Wineboutique',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8370',
            'cust_city' => 'Hadsten',
            'cust_address' => 'Faurskovvej 21, 8370 Hadsten',
          ),
          'invoices' => 
          array (
            0 => 1468,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1444,
            1 => 1360,
            2 => 1294,
            3 => 1157,
            4 => 1157,
            5 => 809,
            6 => 771,
            7 => 771,
            8 => 726,
            9 => 724,
            10 => 709,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-22',
            1 => '2018-02-22',
            2 => '2018-01-19',
            3 => '2017-12-07',
            4 => '2017-12-07',
            5 => '2017-08-10',
            6 => '2017-07-26',
            7 => '2017-07-26',
            8 => '2017-07-12',
            9 => '2017-07-12',
            10 => '2017-07-04',
          ),
        ),
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1439,
            1 => 1439,
            2 => 1439,
            3 => 1439,
            4 => 1303,
            5 => 933,
            6 => 745,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2018-03-19',
            2 => '2018-03-19',
            3 => '2018-03-19',
            4 => '2018-01-11',
            5 => '2017-10-24',
            6 => '2017-07-17',
          ),
        ),
        'Sigurd Muller Vinhandel A/S (Otto Mønstedsvej 2, 9200 Aalborg SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sigurd Muller Vinhandel A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg SV',
            'cust_address' => 'Otto Mønstedsvej 2, 9200 Aalborg SV',
          ),
          'invoices' => 
          array (
            0 => 1436,
            1 => 1096,
            2 => 813,
            3 => 813,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-11-22',
            2 => '2017-08-20',
            3 => '2017-08-20',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1430,
            1 => 1254,
            2 => 1021,
            3 => 833,
            4 => 833,
            5 => 676,
            6 => 676,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2017-12-27',
            2 => '2017-11-02',
            3 => '2017-08-23',
            4 => '2017-08-23',
            5 => '2017-06-14',
            6 => '2017-06-14',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 1413,
            1 => 1413,
            2 => 1132,
            3 => 696,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
            1 => '2018-03-09',
            2 => '2017-11-29',
            3 => '2017-06-30',
          ),
        ),
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1400,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-05',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1399,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'Huset Appel (Feldsingvej 3, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Huset Appel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Feldsingvej 3, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 1398,
            1 => 1398,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
          ),
        ),
        'Vinspecialisten Varde (Ndr.Boulevard 90, 6800 Varde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Varde',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6800',
            'cust_city' => 'Varde',
            'cust_address' => 'Ndr.Boulevard 90, 6800 Varde',
          ),
          'invoices' => 
          array (
            0 => 1396,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1342,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 1315,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1296,
            1 => 700,
            2 => 700,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-06-30',
            2 => '2017-06-30',
          ),
        ),
        'Peter Vin / Øl & Vin ApS (Skrågade 33, 9400 Nørresundby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Peter Vin / Øl & Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9400',
            'cust_city' => 'Nørresundby',
            'cust_address' => 'Skrågade 33, 9400 Nørresundby',
          ),
          'invoices' => 
          array (
            0 => 1224,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-18',
          ),
        ),
        'Mundskænken ApS Broen Shopping (Mundskænken Broen Shopping, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mundskænken ApS Broen Shopping',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Mundskænken Broen Shopping, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1013,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'Kolding Vinhandel (Låsbybanke 12, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kolding Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Låsbybanke 12, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 1002,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Vinspecialisten Lemvig (Vestergade 11, 7620 Lemvig)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lemvig',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7620',
            'cust_city' => 'Lemvig',
            'cust_address' => 'Vestergade 11, 7620 Lemvig',
          ),
          'invoices' => 
          array (
            0 => 987,
            1 => 985,
            2 => 984,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
            1 => '2017-07-26',
            2 => '2017-07-26',
          ),
        ),
        'Vinspecialisten Frederikshavn (Jernbanegade 2, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Frederikshavn',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Jernbanegade 2, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 981,
            1 => 981,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
            1 => '2017-10-27',
          ),
        ),
        'Vinshoppen (Hadsundvej 12, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinshoppen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Hadsundvej 12, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 938,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-23',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 880,
            1 => 880,
            2 => 880,
            3 => 880,
            4 => 880,
            5 => 880,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-27',
            1 => '2017-09-27',
            2 => '2017-09-27',
            3 => '2017-09-27',
            4 => '2017-09-27',
            5 => '2017-09-27',
          ),
        ),
        'Smagførst (Jægergårdsgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Jægergårdsgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 862,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 722,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-05',
          ),
        ),
        'Vinspecialisten Herning ApS (Merkurvej 84, 7400 Herning)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Herning ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7400',
            'cust_city' => 'Herning',
            'cust_address' => 'Merkurvej 84, 7400 Herning',
          ),
          'invoices' => 
          array (
            0 => 686,
            1 => 686,
            2 => 686,
            3 => 686,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-24',
            1 => '2017-06-24',
            2 => '2017-06-24',
            3 => '2017-06-24',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 670,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
        'Vinspecialisten Haderslev ApS (Storegade 30, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Haderslev ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Storegade 30, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 647,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-27',
          ),
        ),
        'Vin&Vin (Torvet 8, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin&Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Torvet 8, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 633,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Aabybro Mejeri (Brogårdsvej 148, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aabybro Mejeri',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Brogårdsvej 148, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1455,
            1 => 1455,
            2 => 1019,
            3 => 1019,
            4 => 1019,
            5 => 856,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2017-11-02',
            3 => '2017-11-02',
            4 => '2017-11-02',
            5 => '2017-09-08',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1408,
            1 => 1217,
            2 => 1144,
            3 => 1003,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2017-12-20',
            2 => '2017-12-05',
            3 => '2017-10-31',
          ),
        ),
        'Blach&Co (Algade 7, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Blach&Co',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Algade 7, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 1393,
            1 => 1393,
            2 => 1393,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
            2 => '2018-03-01',
          ),
        ),
        'Herremagasinet (Jyllandsgade 22, 9520 Skørping)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Herremagasinet',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9520',
            'cust_city' => 'Skørping',
            'cust_address' => 'Jyllandsgade 22, 9520 Skørping',
          ),
          'invoices' => 
          array (
            0 => 1313,
            1 => 1313,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
            1 => '2018-01-23',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1250,
            1 => 1234,
            2 => 1154,
            3 => 1154,
            4 => 1108,
            5 => 1084,
            6 => 928,
            7 => 928,
            8 => 872,
            9 => 827,
            10 => 751,
            11 => 708,
            12 => 683,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-22',
            1 => '2017-12-07',
            2 => '2017-12-04',
            3 => '2017-12-04',
            4 => '2017-11-21',
            5 => '2017-11-15',
            6 => '2017-10-16',
            7 => '2017-10-16',
            8 => '2017-09-09',
            9 => '2017-08-20',
            10 => '2017-07-18',
            11 => '2017-07-05',
            12 => '2017-06-20',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1210,
            1 => 1210,
            2 => 755,
            3 => 755,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-07-19',
            3 => '2017-07-19',
          ),
        ),
        'Rene\'s Vin Og Grønttorv (Østergade 2 B, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rene\'s Vin Og Grønttorv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Østergade 2 B, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1162,
            1 => 1039,
            2 => 846,
            3 => 732,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
            1 => '2017-11-04',
            2 => '2017-09-04',
            3 => '2017-07-13',
          ),
        ),
        'Arla Unika (Klostergade 20, 8000 Århus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Arla Unika',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus',
            'cust_address' => 'Klostergade 20, 8000 Århus',
          ),
          'invoices' => 
          array (
            0 => 1150,
            1 => 1150,
            2 => 1150,
            3 => 1051,
            4 => 1051,
            5 => 1051,
            6 => 1051,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
            1 => '2017-12-05',
            2 => '2017-12-05',
            3 => '2017-11-09',
            4 => '2017-11-09',
            5 => '2017-11-09',
            6 => '2017-11-09',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1117,
            1 => 1028,
            2 => 918,
            3 => 699,
            4 => 699,
            5 => 699,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-27',
            1 => '2017-11-01',
            2 => '2017-10-12',
            3 => '2017-06-30',
            4 => '2017-06-30',
            5 => '2017-06-30',
          ),
        ),
        'Alexanders Blomster (østergade 1, 9560 Hadsund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alexanders Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9560',
            'cust_city' => 'Hadsund',
            'cust_address' => 'østergade 1, 9560 Hadsund',
          ),
          'invoices' => 
          array (
            0 => 1044,
            1 => 1044,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-11-08',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1033,
            1 => 1009,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-04',
            1 => '2017-10-31',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1032,
            1 => 1024,
            2 => 1015,
            3 => 916,
            4 => 916,
            5 => 819,
            6 => 819,
            7 => 819,
            8 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
            1 => '2017-11-03',
            2 => '2017-10-31',
            3 => '2017-10-13',
            4 => '2017-10-13',
            5 => '2017-08-09',
            6 => '2017-08-09',
            7 => '2017-08-09',
            8 => '2017-08-09',
          ),
        ),
        'Mokkahouse (Lystrupvej 62, 8240 Risskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mokkahouse',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8240',
            'cust_city' => 'Risskov',
            'cust_address' => 'Lystrupvej 62, 8240 Risskov',
          ),
          'invoices' => 
          array (
            0 => 932,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-18',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 863,
            1 => 630,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-21',
            1 => '2017-05-23',
          ),
        ),
        'Hr Nielsens Specialiteter (jernbanegade 12 st. tv, 9460 Brovst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Nielsens Specialiteter',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9460',
            'cust_city' => 'Brovst',
            'cust_address' => 'jernbanegade 12 st. tv, 9460 Brovst',
          ),
          'invoices' => 
          array (
            0 => 840,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-26',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1451,
            1 => 1451,
            2 => 1126,
            3 => 1126,
            4 => 1126,
            5 => 723,
            6 => 723,
            7 => 661,
            8 => 661,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2017-11-29',
            3 => '2017-11-29',
            4 => '2017-11-29',
            5 => '2017-07-11',
            6 => '2017-07-11',
            7 => '2017-06-08',
            8 => '2017-06-08',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1434,
            1 => 1198,
            2 => 1056,
            3 => 1056,
            4 => 1056,
            5 => 1056,
            6 => 913,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-12-20',
            2 => '2017-11-10',
            3 => '2017-11-10',
            4 => '2017-11-10',
            5 => '2017-11-10',
            6 => '2017-10-12',
          ),
        ),
        'Nordisk Brænderi (Hjortdalvej 227, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nordisk Brænderi',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 227, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1395,
            1 => 1395,
            2 => 1312,
            3 => 1293,
            4 => 1293,
            5 => 1151,
            6 => 1151,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-04',
            1 => '2018-03-04',
            2 => '2018-01-22',
            3 => '2018-01-19',
            4 => '2018-01-19',
            5 => '2017-12-05',
            6 => '2017-12-05',
          ),
        ),
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1345,
            1 => 1318,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-01-31',
          ),
        ),
        'Skotlander ApS (Hjulmagervej 19, 9490 Pandrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9490',
            'cust_city' => 'Pandrup',
            'cust_address' => 'Hjulmagervej 19, 9490 Pandrup',
          ),
          'invoices' => 
          array (
            0 => 1200,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        ' (Antoniusvej 2, 9000 9000)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => '9000',
            'cust_address' => 'Antoniusvej 2, 9000 9000',
          ),
          'invoices' => 
          array (
            0 => 978,
            1 => 691,
            2 => 691,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-06-22',
            2 => '2017-06-22',
          ),
        ),
        'Nordisk Brænderi (Hjortdalvej227, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nordisk Brænderi',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej227, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 968,
            1 => 968,
            2 => 787,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-25',
            2 => '2017-08-02',
          ),
        ),
        'Pakkecenter Nord (Aalborgvej 14, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pakkecenter Nord',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Aalborgvej 14, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 852,
            1 => 852,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-06',
            1 => '2017-09-06',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1435,
            1 => 1435,
            2 => 1435,
            3 => 1349,
            4 => 1349,
            5 => 1349,
            6 => 1155,
            7 => 1155,
            8 => 1155,
            9 => 899,
            10 => 899,
            11 => 899,
            12 => 867,
            13 => 867,
            14 => 867,
            15 => 867,
            16 => 867,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-20',
            1 => '2018-03-20',
            2 => '2018-03-20',
            3 => '2018-02-19',
            4 => '2018-02-19',
            5 => '2018-02-19',
            6 => '2017-12-04',
            7 => '2017-12-04',
            8 => '2017-12-04',
            9 => '2017-10-09',
            10 => '2017-10-09',
            11 => '2017-10-09',
            12 => '2017-09-15',
            13 => '2017-09-15',
            14 => '2017-09-15',
            15 => '2017-09-15',
            16 => '2017-09-15',
          ),
        ),
        'Restaurant Sømærket (Jeckelsvej 6, 9900 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Sømærket',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Skagen',
            'cust_address' => 'Jeckelsvej 6, 9900 Skagen',
          ),
          'invoices' => 
          array (
            0 => 694,
            1 => 694,
            2 => 694,
            3 => 694,
            4 => 694,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-28',
            1 => '2017-06-28',
            2 => '2017-06-28',
            3 => '2017-06-28',
            4 => '2017-06-28',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Villa Vest (Strandvejen 138, 9800 Lønstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Villa Vest',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Lønstrup',
            'cust_address' => 'Strandvejen 138, 9800 Lønstrup',
          ),
          'invoices' => 
          array (
            0 => 1425,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1391,
            1 => 1391,
            2 => 1391,
            3 => 1391,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-03',
            1 => '2018-03-03',
            2 => '2018-03-03',
            3 => '2018-03-03',
          ),
        ),
        'Hjerting Badehotel (Strandpromenaden 1, 6710 Esbjerg V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjerting Badehotel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6710',
            'cust_city' => 'Esbjerg V',
            'cust_address' => 'Strandpromenaden 1, 6710 Esbjerg V',
          ),
          'invoices' => 
          array (
            0 => 1006,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-24',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'Restaurant Tabu II ApS (Vesterå 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Tabu II ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1392,
            1 => 1105,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-03',
            1 => '2017-11-23',
          ),
        ),
        'Textur ApS (Vesterbro 65 st th, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Textur ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterbro 65 st th, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1343,
            1 => 1343,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
          ),
        ),
        'CANblau Aalborg (Ved stranden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'CANblau Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Ved stranden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 935,
            1 => 935,
            2 => 886,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-25',
            2 => '2017-10-01',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hantwerk (Fiskerivej 2D, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hantwerk',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Fiskerivej 2D, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1347,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1460,
            1 => 1460,
            2 => 1460,
            3 => 1460,
            4 => 1460,
            5 => 1305,
            6 => 1305,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2018-03-21',
            3 => '2018-03-21',
            4 => '2018-03-21',
            5 => '2018-01-09',
            6 => '2018-01-09',
          ),
        ),
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 1397,
            1 => 1397,
            2 => 1397,
            3 => 904,
            4 => 904,
            5 => 904,
            6 => 904,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
            2 => '2018-03-01',
            3 => '2017-10-11',
            4 => '2017-10-11',
            5 => '2017-10-11',
            6 => '2017-10-11',
          ),
        ),
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1356,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1321,
            1 => 1243,
            2 => 1138,
            3 => 1063,
            4 => 871,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-24',
            1 => '2017-12-07',
            2 => '2017-11-22',
            3 => '2017-11-14',
            4 => '2017-09-12',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1270,
            1 => 1270,
            2 => 1270,
            3 => 1270,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-12',
            1 => '2018-01-12',
            2 => '2018-01-12',
            3 => '2018-01-12',
          ),
        ),
        'Villa Patina (Algade 44, st, 4220 Korsør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Villa Patina',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4220',
            'cust_city' => 'Korsør',
            'cust_address' => 'Algade 44, st, 4220 Korsør',
          ),
          'invoices' => 
          array (
            0 => 1255,
            1 => 1255,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-28',
            1 => '2017-12-28',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1249,
            1 => 1249,
            2 => 832,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-26',
            1 => '2017-12-26',
            2 => '2017-08-23',
          ),
        ),
        'Skjold Burne Vanløse (jernbane alle 45, 2720 vanløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vanløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2720',
            'cust_city' => 'vanløse',
            'cust_address' => 'jernbane alle 45, 2720 vanløse',
          ),
          'invoices' => 
          array (
            0 => 1209,
            1 => 1209,
            2 => 1209,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-12-20',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1197,
            1 => 1087,
            2 => 1087,
            3 => 1018,
            4 => 1018,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-11-20',
            2 => '2017-11-20',
            3 => '2017-11-01',
            4 => '2017-11-01',
          ),
        ),
        ' (Dyrehovedgårds Alle 18, 4220 Korsør)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4220',
            'cust_city' => 'Korsør',
            'cust_address' => 'Dyrehovedgårds Alle 18, 4220 Korsør',
          ),
          'invoices' => 
          array (
            0 => 1098,
            1 => 1098,
            2 => 1098,
            3 => 1080,
            4 => 1080,
            5 => 1080,
            6 => 701,
            7 => 701,
            8 => 701,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-02',
            1 => '2017-11-02',
            2 => '2017-11-02',
            3 => '2017-11-02',
            4 => '2017-11-02',
            5 => '2017-11-02',
            6 => '2017-06-30',
            7 => '2017-06-30',
            8 => '2017-06-30',
          ),
        ),
        'Zaizon (Falkoner Alle 33, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Zaizon',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Alle 33, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1060,
            1 => 1047,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-13',
            1 => '2017-11-08',
          ),
        ),
        'Skjold Burne Ringsted (Nørregade 45, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Ringsted',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nørregade 45, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1008,
            1 => 851,
            2 => 642,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
            1 => '2017-09-01',
            2 => '2017-06-01',
          ),
        ),
        'BTB Vine (Højbakken 11, 4690 Haslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'BTB Vine',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Haslev',
            'cust_address' => 'Højbakken 11, 4690 Haslev',
          ),
          'invoices' => 
          array (
            0 => 883,
            1 => 883,
            2 => 883,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-29',
            1 => '2017-09-29',
            2 => '2017-09-29',
          ),
        ),
        'Vinspecialisten Lyngby (Klampenborgvej 232, 2800 Lyngby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lyngby',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2800',
            'cust_city' => 'Lyngby',
            'cust_address' => 'Klampenborgvej 232, 2800 Lyngby',
          ),
          'invoices' => 
          array (
            0 => 864,
            1 => 864,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
            1 => '2017-09-23',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 860,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
          ),
        ),
        'Skjold Burne (Adelgade 70, 4720 Præstø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4720',
            'cust_city' => 'Præstø',
            'cust_address' => 'Adelgade 70, 4720 Præstø',
          ),
          'invoices' => 
          array (
            0 => 719,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-05',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 685,
            1 => 682,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-23',
            1 => '2017-06-22',
          ),
        ),
        'Den Franske Vinhandel (Skomagergade 3, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Franske Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Skomagergade 3, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 662,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Stork Concepts (Frederiksborggade 1b, 4. sal, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Stork Concepts',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 1b, 4. sal, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1457,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Hotel Cecil (Niels Hemmingsens Gade 11, 1153 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Cecil',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1153',
            'cust_city' => 'København K',
            'cust_address' => 'Niels Hemmingsens Gade 11, 1153 København K',
          ),
          'invoices' => 
          array (
            0 => 1369,
            1 => 1369,
            2 => 1361,
            3 => 1361,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-22',
            3 => '2018-02-22',
          ),
        ),
        'Nobis Hotel Copenhagen (Niels Brocks Gade 1, 1574 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nobis Hotel Copenhagen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1574',
            'cust_city' => 'København V',
            'cust_address' => 'Niels Brocks Gade 1, 1574 København V',
          ),
          'invoices' => 
          array (
            0 => 1107,
            1 => 1107,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 18, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 18, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 1443,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
          ),
        ),
        'Hotel Saxkjøbing (Torvet 9, 4990 Sakskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Saxkjøbing',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4990',
            'cust_city' => 'Sakskøbing',
            'cust_address' => 'Torvet 9, 4990 Sakskøbing',
          ),
          'invoices' => 
          array (
            0 => 758,
            1 => 758,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
            1 => '2017-07-08',
          ),
        ),
        'Restaurant Skade (Rosengaarden 12, 1174 Copenhagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Skade',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1174',
            'cust_city' => 'Copenhagen',
            'cust_address' => 'Rosengaarden 12, 1174 Copenhagen',
          ),
          'invoices' => 
          array (
            0 => 640,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-30',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmand (Sankt Annæ Plads 22, 1250 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmand',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'København K',
            'cust_address' => 'Sankt Annæ Plads 22, 1250 København K',
          ),
          'invoices' => 
          array (
            0 => 1429,
            1 => 1429,
            2 => 1429,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
            2 => '2018-03-16',
          ),
        ),
        'Dansk E-logistik, Att: Latron (Naverland 33, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk E-logistik, Att: Latron',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Naverland 33, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1415,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-12',
          ),
        ),
        'Spiritium Cocktails IVS (c/o Eye-Grain ApS, 2730 Herlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Spiritium Cocktails IVS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2730',
            'cust_city' => 'Herlev',
            'cust_address' => 'c/o Eye-Grain ApS, 2730 Herlev',
          ),
          'invoices' => 
          array (
            0 => 1383,
            1 => 1382,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
          ),
        ),
        'Mikkeller Meatpacking Aps (Flæsketorvet 51-55, 1711 KBH V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mikkeller Meatpacking Aps',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'KBH V',
            'cust_address' => 'Flæsketorvet 51-55, 1711 KBH V',
          ),
          'invoices' => 
          array (
            0 => 1344,
            1 => 1344,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
          ),
        ),
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1338,
            1 => 1185,
            2 => 1156,
            3 => 748,
            4 => 748,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-03',
            1 => '2017-12-13',
            2 => '2017-12-01',
            3 => '2017-07-14',
            4 => '2017-07-14',
          ),
        ),
        'Arla Unika Torvehallerne (Torvehallerne, hal 1 Stade 5F, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Arla Unika Torvehallerne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne, hal 1 Stade 5F, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1337,
            1 => 1337,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-05',
            1 => '2018-02-05',
          ),
        ),
        'Lidt-af-hvert I/S (Lindegårdsager 14, 2640 Hedehusene)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lidt-af-hvert I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2640',
            'cust_city' => 'Hedehusene',
            'cust_address' => 'Lindegårdsager 14, 2640 Hedehusene',
          ),
          'invoices' => 
          array (
            0 => 1083,
            1 => 1083,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
            1 => '2017-11-14',
          ),
        ),
        'Shoppartner A/S (Marienbergvej 132, 4760 Vordingborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Shoppartner A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4760',
            'cust_city' => 'Vordingborg',
            'cust_address' => 'Marienbergvej 132, 4760 Vordingborg',
          ),
          'invoices' => 
          array (
            0 => 1010,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Krusemynte (Nordre Strandvej 341A, 3100 Hornbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Krusemynte',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3100',
            'cust_city' => 'Hornbæk',
            'cust_address' => 'Nordre Strandvej 341A, 3100 Hornbæk',
          ),
          'invoices' => 
          array (
            0 => 721,
            1 => 721,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-06',
            1 => '2017-07-06',
          ),
        ),
        'Smagsløget (Storgade 15b, 4180 Sorø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagsløget',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4180',
            'cust_city' => 'Sorø',
            'cust_address' => 'Storgade 15b, 4180 Sorø',
          ),
          'invoices' => 
          array (
            0 => 675,
            1 => 675,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-14',
            1 => '2017-06-14',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1424,
            1 => 1424,
            2 => 1377,
            3 => 1007,
            4 => 1007,
            5 => 1007,
            6 => 1007,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
            2 => '2018-02-28',
            3 => '2017-10-31',
            4 => '2017-10-31',
            5 => '2017-10-31',
            6 => '2017-10-31',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1274,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-13',
          ),
        ),
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1266,
            1 => 1266,
            2 => 1266,
            3 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-31',
            1 => '2017-12-31',
            2 => '2017-12-31',
            3 => '2017-12-31',
          ),
        ),
        'Dansk E-logistik, att: Latron APS (Naverland 33, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk E-logistik, att: Latron APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Naverland 33, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1264,
            1 => 1189,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-04',
            1 => '2017-12-14',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1230,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1114,
            1 => 1114,
            2 => 1052,
            3 => 1052,
            4 => 865,
            5 => 865,
            6 => 865,
            7 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
            2 => '2017-11-09',
            3 => '2017-11-09',
            4 => '2017-09-23',
            5 => '2017-09-23',
            6 => '2017-09-23',
            7 => '2017-09-23',
          ),
        ),
        'skjold burne (Østergade 1, 1100 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'skjold burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1100',
            'cust_city' => 'København',
            'cust_address' => 'Østergade 1, 1100 København',
          ),
          'invoices' => 
          array (
            0 => 1011,
            1 => 859,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
            1 => '2017-09-19',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 903,
            2 => 903,
            3 => 903,
            4 => 702,
            5 => 702,
            6 => 702,
            7 => 702,
            8 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-09-30',
            2 => '2017-09-30',
            3 => '2017-09-30',
            4 => '2017-06-30',
            5 => '2017-06-30',
            6 => '2017-06-30',
            7 => '2017-06-30',
            8 => '2017-06-30',
          ),
        ),
        'Dansk e-logistik / att: Latron ApS (Naverland 33, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-logistik / att: Latron ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Naverland 33, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 874,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
          ),
        ),
        'Den Sidste Dråbe (Torvehallerne Hal 2, 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2, 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 777,
            1 => 754,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
            1 => '2017-07-20',
          ),
        ),
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 688,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-27',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 2000 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 2000 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 671,
            1 => 671,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
            1 => '2017-06-13',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1421,
            1 => 1147,
            2 => 1147,
            3 => 1099,
            4 => 882,
            5 => 841,
            6 => 800,
            7 => 693,
            8 => 669,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2017-12-04',
            2 => '2017-12-04',
            3 => '2017-11-23',
            4 => '2017-09-27',
            5 => '2017-08-31',
            6 => '2017-08-03',
            7 => '2017-06-28',
            8 => '2017-06-12',
          ),
        ),
        'Klosterkælderen (Store Gråbrødrestræde 23, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Klosterkælderen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Store Gråbrødrestræde 23, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1004,
            1 => 1004,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
            1 => '2017-10-17',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Rombo (Falkoner Allé 46, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rombo',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Allé 46, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1414,
            1 => 1414,
            2 => 1341,
            3 => 1220,
            4 => 1220,
            5 => 1220,
            6 => 1220,
            7 => 941,
            8 => 941,
            9 => 941,
            10 => 888,
            11 => 888,
            12 => 870,
            13 => 870,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-03-08',
            2 => '2018-02-01',
            3 => '2017-12-18',
            4 => '2017-12-18',
            5 => '2017-12-18',
            6 => '2017-12-18',
            7 => '2017-10-19',
            8 => '2017-10-19',
            9 => '2017-10-19',
            10 => '2017-09-28',
            11 => '2017-09-28',
            12 => '2017-09-12',
            13 => '2017-09-12',
          ),
        ),
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 924,
            1 => 924,
            2 => 924,
            3 => 924,
            4 => 690,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-13',
            1 => '2017-10-13',
            2 => '2017-10-13',
            3 => '2017-10-13',
            4 => '2017-06-26',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'MIKKELLER APS / SCANGLOBAL (HELGESHØJ ALLE 12, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MIKKELLER APS / SCANGLOBAL',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'HELGESHØJ ALLE 12, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1298,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1046,
            1 => 1005,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-10-31',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1017,
            1 => 780,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-01',
            1 => '2017-07-28',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
            1 => 858,
            2 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
            2 => '2017-09-19',
          ),
        ),
        'Drinky (Haslegårdsvej 20, 8210 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinky',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8210',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Haslegårdsvej 20, 8210 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 769,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Holte Vinlager, Odense (Dalumvej 59B, 5250 Odense SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Odense',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5250',
            'cust_city' => 'Odense SV',
            'cust_address' => 'Dalumvej 59B, 5250 Odense SV',
          ),
          'invoices' => 
          array (
            0 => 1233,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
          ),
        ),
        'Rahbek&Læssøe Aps (Rahbeksvej 9, 5230 Odense M)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5230',
            'cust_city' => 'Odense M',
            'cust_address' => 'Rahbeksvej 9, 5230 Odense M',
          ),
          'invoices' => 
          array (
            0 => 945,
            1 => 945,
            2 => 945,
            3 => 945,
            4 => 945,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-19',
            1 => '2017-10-19',
            2 => '2017-10-19',
            3 => '2017-10-19',
            4 => '2017-10-19',
          ),
        ),
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 649,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-22',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinspecialisten Middelfart ApS (Østergade 7, 5500 Middelfart)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Middelfart ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5500',
            'cust_city' => 'Middelfart',
            'cust_address' => 'Østergade 7, 5500 Middelfart',
          ),
          'invoices' => 
          array (
            0 => 1093,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1012,
            1 => 794,
            2 => 794,
            3 => 794,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
            1 => '2017-08-02',
            2 => '2017-08-02',
            3 => '2017-08-02',
          ),
        ),
      ),
    ),
  ),
  'Salg af varer/ydelser u/moms' => 
  array (
    'Fyn m.fl.' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Fjelsted Skov Kro (Store Landevej 92, 5592 Ejby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fjelsted Skov Kro',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5592',
            'cust_city' => 'Ejby',
            'cust_address' => 'Store Landevej 92, 5592 Ejby',
          ),
          'invoices' => 
          array (
            0 => 1467,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Højestene færgen (Jensens mole 5, 5700 Svendborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Højestene færgen',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5700',
            'cust_city' => 'Svendborg',
            'cust_address' => 'Jensens mole 5, 5700 Svendborg',
          ),
          'invoices' => 
          array (
            0 => 1458,
            1 => 1458,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Rahbek&Læssøe Aps (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1418,
            1 => 1418,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2018-03-14',
          ),
        ),
        'Holte Vinlager, Odense (Dalumvej 59B, 5250 Odense SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Odense',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5250',
            'cust_city' => 'Odense SV',
            'cust_address' => 'Dalumvej 59B, 5250 Odense SV',
          ),
          'invoices' => 
          array (
            0 => 1233,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
          ),
        ),
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 1118,
            1 => 1118,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-26',
            1 => '2017-11-26',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinens Verden, Slotsgade (Slotsgade 21 C-D, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinens Verden, Slotsgade',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Slotsgade 21 C-D, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1412,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-05',
          ),
        ),
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1179,
            1 => 1094,
            2 => 1094,
            3 => 1094,
            4 => 1094,
            5 => 830,
            6 => 830,
            7 => 830,
            8 => 830,
            9 => 794,
            10 => 794,
            11 => 794,
            12 => 794,
            13 => 794,
            14 => 794,
            15 => 794,
            16 => 794,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-11-17',
            2 => '2017-11-17',
            3 => '2017-11-17',
            4 => '2017-11-17',
            5 => '2017-08-23',
            6 => '2017-08-23',
            7 => '2017-08-23',
            8 => '2017-08-23',
            9 => '2017-08-02',
            10 => '2017-08-02',
            11 => '2017-08-02',
            12 => '2017-08-02',
            13 => '2017-08-02',
            14 => '2017-08-02',
            15 => '2017-08-02',
            16 => '2017-08-02',
          ),
        ),
        'Vinspecialisten Middelfart ApS (Østergade 7, 5500 Middelfart)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Middelfart ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5500',
            'cust_city' => 'Middelfart',
            'cust_address' => 'Østergade 7, 5500 Middelfart',
          ),
          'invoices' => 
          array (
            0 => 1093,
            1 => 1093,
            2 => 1093,
            3 => 1093,
            4 => 1093,
            5 => 1093,
            6 => 1093,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
            1 => '2017-11-17',
            2 => '2017-11-17',
            3 => '2017-11-17',
            4 => '2017-11-17',
            5 => '2017-11-17',
            6 => '2017-11-17',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmandsgaard (Torvet 5, 5970 Ærøskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmandsgaard',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5970',
            'cust_city' => 'Ærøskøbing',
            'cust_address' => 'Torvet 5, 5970 Ærøskøbing',
          ),
          'invoices' => 
          array (
            0 => 689,
            1 => 689,
            2 => 689,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-27',
            1 => '2017-06-27',
            2 => '2017-06-27',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1460,
            1 => 1460,
            2 => 1460,
            3 => 1460,
            4 => 1460,
            5 => 1305,
            6 => 1305,
            7 => 1252,
            8 => 1252,
            9 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2018-03-21',
            3 => '2018-03-21',
            4 => '2018-03-21',
            5 => '2018-01-09',
            6 => '2018-01-09',
            7 => '2017-12-21',
            8 => '2017-12-21',
            9 => '2017-12-21',
          ),
        ),
        'Den Franske Vinhandel (Skomagergade 3, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Franske Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Skomagergade 3, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1419,
            1 => 1419,
            2 => 1419,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-03-08',
            2 => '2018-03-08',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1416,
            1 => 1197,
            2 => 1197,
            3 => 1087,
            4 => 1018,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2017-12-13',
            2 => '2017-12-13',
            3 => '2017-11-20',
            4 => '2017-11-01',
          ),
        ),
        'Brdr D Østerbro (Østerbrogade 152, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Østerbro',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 152, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1402,
            1 => 1402,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-07',
            1 => '2018-03-07',
          ),
        ),
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 1397,
            1 => 904,
            2 => 904,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2017-10-11',
            2 => '2017-10-11',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1380,
            1 => 1380,
            2 => 1267,
            3 => 1267,
            4 => 1111,
            5 => 854,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-01-08',
            3 => '2018-01-08',
            4 => '2017-11-23',
            5 => '2017-09-08',
          ),
        ),
        'Vinspecialisten Lyngby (Klampenborgvej 232, 2800 Lyngby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lyngby',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2800',
            'cust_city' => 'Lyngby',
            'cust_address' => 'Klampenborgvej 232, 2800 Lyngby',
          ),
          'invoices' => 
          array (
            0 => 1374,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1340,
            1 => 1242,
            2 => 1091,
            3 => 1091,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-12-12',
            2 => '2017-11-17',
            3 => '2017-11-17',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1339,
            1 => 1339,
            2 => 1177,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
            2 => '2017-12-13',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1321,
            1 => 1321,
            2 => 1243,
            3 => 1243,
            4 => 1138,
            5 => 1138,
            6 => 1138,
            7 => 1138,
            8 => 1138,
            9 => 1138,
            10 => 1063,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-24',
            1 => '2018-01-24',
            2 => '2017-12-07',
            3 => '2017-12-07',
            4 => '2017-11-22',
            5 => '2017-11-22',
            6 => '2017-11-22',
            7 => '2017-11-22',
            8 => '2017-11-22',
            9 => '2017-11-22',
            10 => '2017-11-14',
          ),
        ),
        'Skjold Burne (Frederiksværksgade 95, 3400 hillerød)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3400',
            'cust_city' => 'hillerød',
            'cust_address' => 'Frederiksværksgade 95, 3400 hillerød',
          ),
          'invoices' => 
          array (
            0 => 1265,
            1 => 1265,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-02',
            1 => '2018-01-02',
          ),
        ),
        'Villa Patina (Algade 44, st, 4220 Korsør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Villa Patina',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4220',
            'cust_city' => 'Korsør',
            'cust_address' => 'Algade 44, st, 4220 Korsør',
          ),
          'invoices' => 
          array (
            0 => 1255,
            1 => 1255,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-28',
            1 => '2017-12-28',
          ),
        ),
        'Borgstrøm Vinhandel (Centerholmen 8A, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Borgstrøm Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Centerholmen 8A, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1237,
            1 => 1237,
            2 => 1237,
            3 => 1237,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-12-20',
            3 => '2017-12-20',
          ),
        ),
        'Skjold Burne Vanløse (jernbane alle 45, 2720 vanløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vanløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2720',
            'cust_city' => 'vanløse',
            'cust_address' => 'jernbane alle 45, 2720 vanløse',
          ),
          'invoices' => 
          array (
            0 => 1209,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Skælskør Vinhandel (Algade 6-8, 4230 Skælskør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skælskør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4230',
            'cust_city' => 'Skælskør',
            'cust_address' => 'Algade 6-8, 4230 Skælskør',
          ),
          'invoices' => 
          array (
            0 => 1176,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Helsingør Vinhandel (Bramstræde 2C, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Helsingør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Bramstræde 2C, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 1174,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Eriksen Vinhandel (Nygade 4, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Eriksen Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Nygade 4, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1173,
            1 => 1173,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-12-07',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1166,
            1 => 917,
            2 => 917,
            3 => 860,
            4 => 860,
            5 => 860,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-10-16',
            2 => '2017-10-16',
            3 => '2017-09-19',
            4 => '2017-09-19',
            5 => '2017-09-19',
          ),
        ),
        'VINSLOTTET I GREVE A/S (Håndværkerbyen 42, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VINSLOTTET I GREVE A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Håndværkerbyen 42, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1158,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
          ),
        ),
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1145,
            1 => 1145,
            2 => 1145,
            3 => 761,
            4 => 761,
            5 => 759,
            6 => 759,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
            1 => '2017-12-05',
            2 => '2017-12-05',
            3 => '2017-07-23',
            4 => '2017-07-23',
            5 => '2017-07-21',
            6 => '2017-07-21',
          ),
        ),
        'Prøvestenens Vinhandel ApS (Prøvestensvej 14, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Prøvestenens Vinhandel ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Prøvestensvej 14, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 1112,
            1 => 1112,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
          ),
        ),
        'Vinoteket ApS (Gentoftegade 54, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoteket ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gentoftegade 54, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1053,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
        '2010 Vin & Velsmag (C. E. Christiansens Vej 1, 4930 Maribo)' => 
        array (
          'info' => 
          array (
            'cust_name' => '2010 Vin & Velsmag',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4930',
            'cust_city' => 'Maribo',
            'cust_address' => 'C. E. Christiansens Vej 1, 4930 Maribo',
          ),
          'invoices' => 
          array (
            0 => 756,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-20',
          ),
        ),
        ' (Dyrehovedgårds Alle 18, 4220 Korsør)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4220',
            'cust_city' => 'Korsør',
            'cust_address' => 'Dyrehovedgårds Alle 18, 4220 Korsør',
          ),
          'invoices' => 
          array (
            0 => 701,
            1 => 701,
            2 => 701,
            3 => 701,
            4 => 701,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
            1 => '2017-06-30',
            2 => '2017-06-30',
            3 => '2017-06-30',
            4 => '2017-06-30',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmand (Sankt Annæ Plads 22, 1250 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmand',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'København K',
            'cust_address' => 'Sankt Annæ Plads 22, 1250 København K',
          ),
          'invoices' => 
          array (
            0 => 1429,
            1 => 1429,
            2 => 1429,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
            2 => '2018-03-16',
          ),
        ),
        'My Coffee Shop (Hundige Strandvej 212c, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'My Coffee Shop',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Hundige Strandvej 212c, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1386,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
          ),
        ),
        'Spiritium Cocktails IVS (c/o Eye-Grain ApS, 2730 Herlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Spiritium Cocktails IVS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2730',
            'cust_city' => 'Herlev',
            'cust_address' => 'c/o Eye-Grain ApS, 2730 Herlev',
          ),
          'invoices' => 
          array (
            0 => 1382,
            1 => 1382,
            2 => 1382,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-28',
          ),
        ),
        'Mikkeller Meatpacking Aps (Flæsketorvet 51-55, 1711 KBH V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mikkeller Meatpacking Aps',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'KBH V',
            'cust_address' => 'Flæsketorvet 51-55, 1711 KBH V',
          ),
          'invoices' => 
          array (
            0 => 1344,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1338,
            1 => 1185,
            2 => 1185,
            3 => 1156,
            4 => 1156,
            5 => 748,
            6 => 748,
            7 => 748,
            8 => 748,
            9 => 748,
            10 => 748,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-03',
            1 => '2017-12-13',
            2 => '2017-12-13',
            3 => '2017-12-01',
            4 => '2017-12-01',
            5 => '2017-07-14',
            6 => '2017-07-14',
            7 => '2017-07-14',
            8 => '2017-07-14',
            9 => '2017-07-14',
            10 => '2017-07-14',
          ),
        ),
        'GinGin I/S (Ternevej 30, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GinGin I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ternevej 30, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1320,
            1 => 1319,
            2 => 1143,
            3 => 1143,
            4 => 1037,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-27',
            1 => '2018-01-30',
            2 => '2017-12-02',
            3 => '2017-12-02',
            4 => '2017-11-04',
          ),
        ),
        'Lidt-af-hvert I/S (Lindegårdsager 14, 2640 Hedehusene)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lidt-af-hvert I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2640',
            'cust_city' => 'Hedehusene',
            'cust_address' => 'Lindegårdsager 14, 2640 Hedehusene',
          ),
          'invoices' => 
          array (
            0 => 1083,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
          ),
        ),
        'Smagsløget (Storgade 15b, 4180 Sorø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagsløget',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4180',
            'cust_city' => 'Sorø',
            'cust_address' => 'Storgade 15b, 4180 Sorø',
          ),
          'invoices' => 
          array (
            0 => 675,
            1 => 675,
            2 => 675,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-14',
            1 => '2017-06-14',
            2 => '2017-06-14',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'R60 (Mosedalsvej 17, 3740 Svaneke)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'R60',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3740',
            'cust_city' => 'Svaneke',
            'cust_address' => 'Mosedalsvej 17, 3740 Svaneke',
          ),
          'invoices' => 
          array (
            0 => 1428,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1421,
            1 => 1190,
            2 => 1147,
            3 => 1147,
            4 => 1147,
            5 => 1099,
            6 => 693,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
            1 => '2017-12-14',
            2 => '2017-12-04',
            3 => '2017-12-04',
            4 => '2017-12-04',
            5 => '2017-11-23',
            6 => '2017-06-28',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1381,
            1 => 1381,
            2 => 1381,
            3 => 1381,
            4 => 1381,
            5 => 1381,
            6 => 1381,
            7 => 1381,
            8 => 1381,
            9 => 1274,
            10 => 1274,
            11 => 1274,
            12 => 1201,
            13 => 1201,
            14 => 1124,
            15 => 1124,
            16 => 931,
            17 => 931,
            18 => 931,
            19 => 816,
            20 => 816,
            21 => 816,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-28',
            3 => '2018-02-28',
            4 => '2018-02-28',
            5 => '2018-02-28',
            6 => '2018-02-28',
            7 => '2018-02-28',
            8 => '2018-02-28',
            9 => '2018-01-13',
            10 => '2018-01-13',
            11 => '2018-01-13',
            12 => '2017-12-20',
            13 => '2017-12-20',
            14 => '2017-11-28',
            15 => '2017-11-28',
            16 => '2017-10-17',
            17 => '2017-10-17',
            18 => '2017-10-17',
            19 => '2017-08-20',
            20 => '2017-08-20',
            21 => '2017-08-20',
          ),
        ),
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1376,
            1 => 1376,
            2 => 1376,
            3 => 1301,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-28',
            3 => '2018-01-19',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1355,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
          ),
        ),
        'Holte Vinlager Roskilde (Ringstedgade 37, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager Roskilde',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ringstedgade 37, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1335,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1219,
            1 => 1122,
            2 => 1041,
            3 => 885,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-11-23',
            2 => '2017-11-07',
            3 => '2017-10-01',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 1820 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1820',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 1820 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 1216,
            1 => 1163,
            2 => 1163,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-07',
            2 => '2017-12-07',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 1102,
            1 => 1102,
            2 => 736,
            3 => 736,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
            2 => '2017-07-08',
            3 => '2017-07-08',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 865,
            1 => 865,
            2 => 865,
            3 => 865,
            4 => 865,
            5 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
            1 => '2017-09-23',
            2 => '2017-09-23',
            3 => '2017-09-23',
            4 => '2017-09-23',
            5 => '2017-09-23',
          ),
        ),
        'Stork concepts (Fredriksborg gade 1b 4 sal, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Stork concepts',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Fredriksborg gade 1b 4 sal, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 825,
            1 => 825,
            2 => 825,
            3 => 825,
            4 => 825,
            5 => 825,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-22',
            1 => '2017-08-22',
            2 => '2017-08-22',
            3 => '2017-08-22',
            4 => '2017-08-22',
            5 => '2017-08-22',
          ),
        ),
        'Den Sidste Dråbe (Torvehallerne Hal 2, 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2, 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 777,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
        'Den Sidste Dråbe (Hal 2, Frederiksborggade 21,, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Frederiksborggade 21,, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 705,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-01',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'PostNord A/S, Screeningecenter Øst. ATT Restaurant Tårnet, Folketinget Christiansborg (Oliefabriksvej 51, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PostNord A/S, Screeningecenter Øst. ATT Restaurant Tårnet, Folketinget Christiansborg',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Oliefabriksvej 51, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1152,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-06',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1439,
            1 => 1303,
            2 => 933,
            3 => 745,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2018-01-11',
            2 => '2017-10-24',
            3 => '2017-07-17',
          ),
        ),
        'Sigurd Muller Vinhandel A/S (Otto Mønstedsvej 2, 9200 Aalborg SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sigurd Muller Vinhandel A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg SV',
            'cust_address' => 'Otto Mønstedsvej 2, 9200 Aalborg SV',
          ),
          'invoices' => 
          array (
            0 => 1436,
            1 => 1096,
            2 => 813,
            3 => 813,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-11-22',
            2 => '2017-08-20',
            3 => '2017-08-20',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 1413,
            1 => 1413,
            2 => 1132,
            3 => 901,
            4 => 901,
            5 => 762,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
            1 => '2018-03-09',
            2 => '2017-11-29',
            3 => '2017-10-09',
            4 => '2017-10-09',
            5 => '2017-07-24',
          ),
        ),
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1400,
            1 => 1400,
            2 => 1400,
            3 => 1400,
            4 => 639,
            5 => 639,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-05',
            1 => '2018-03-05',
            2 => '2018-03-05',
            3 => '2018-03-05',
            4 => '2017-05-25',
            5 => '2017-05-25',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1399,
            1 => 1127,
            2 => 1092,
            3 => 1092,
            4 => 1042,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2017-11-29',
            2 => '2017-11-17',
            3 => '2017-11-17',
            4 => '2017-11-06',
          ),
        ),
        'Huset Appel (Feldsingvej 3, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Huset Appel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Feldsingvej 3, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 1398,
            1 => 1398,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
          ),
        ),
        'Vinspecialisten Varde (Ndr.Boulevard 90, 6800 Varde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Varde',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6800',
            'cust_city' => 'Varde',
            'cust_address' => 'Ndr.Boulevard 90, 6800 Varde',
          ),
          'invoices' => 
          array (
            0 => 1396,
            1 => 1396,
            2 => 733,
            3 => 733,
            4 => 733,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
            2 => '2017-07-10',
            3 => '2017-07-10',
            4 => '2017-07-10',
          ),
        ),
        'Vinoble Aarhus (Guldsmedgade 20, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Aarhus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Guldsmedgade 20, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1371,
            1 => 1371,
            2 => 1371,
            3 => 1371,
            4 => 1371,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-28',
            2 => '2018-02-28',
            3 => '2018-02-28',
            4 => '2018-02-28',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1364,
            1 => 1169,
            2 => 838,
            3 => 774,
            4 => 722,
            5 => 722,
            6 => 722,
            7 => 722,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2017-12-11',
            2 => '2017-08-25',
            3 => '2017-07-26',
            4 => '2017-07-05',
            5 => '2017-07-05',
            6 => '2017-07-05',
            7 => '2017-07-05',
          ),
        ),
        '1010 Vin (Fiskergangen 12b, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => '1010 Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Fiskergangen 12b, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1362,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-13',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1342,
            1 => 1300,
            2 => 1104,
            3 => 749,
            4 => 749,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-01-19',
            2 => '2017-11-23',
            3 => '2017-07-18',
            4 => '2017-07-18',
          ),
        ),
        'Vin&Vin (Torvet 8, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin&Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Torvet 8, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 1323,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Vinspecialisten Frederikshavn (Jernbanegade 2, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Frederikshavn',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Jernbanegade 2, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1307,
            1 => 981,
            2 => 981,
            3 => 981,
            4 => 981,
            5 => 740,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
            1 => '2017-10-27',
            2 => '2017-10-27',
            3 => '2017-10-27',
            4 => '2017-10-27',
            5 => '2017-07-05',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1296,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Vinshoppen (Hadsundvej 12, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinshoppen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Hadsundvej 12, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1225,
            1 => 938,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-17',
            1 => '2017-10-23',
          ),
        ),
        'Uhrskov Vinhandel (Bredgade 56, 7400 Herning)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Uhrskov Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7400',
            'cust_city' => 'Herning',
            'cust_address' => 'Bredgade 56, 7400 Herning',
          ),
          'invoices' => 
          array (
            0 => 1208,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1194,
            1 => 1194,
            2 => 1129,
            3 => 1128,
            4 => 1021,
            5 => 833,
            6 => 833,
            7 => 833,
            8 => 833,
            9 => 753,
            10 => 746,
            11 => 676,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-12-13',
            2 => '2017-11-29',
            3 => '2017-11-27',
            4 => '2017-11-02',
            5 => '2017-08-23',
            6 => '2017-08-23',
            7 => '2017-08-23',
            8 => '2017-08-23',
            9 => '2017-07-16',
            10 => '2017-07-16',
            11 => '2017-06-14',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1181,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Skjold Burne/Lysholdt (Fiskebrogade 8, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne/Lysholdt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Fiskebrogade 8, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1172,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1157,
            1 => 1157,
            2 => 1157,
            3 => 1157,
            4 => 771,
            5 => 771,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-12-07',
            2 => '2017-12-07',
            3 => '2017-12-07',
            4 => '2017-07-26',
            5 => '2017-07-26',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1149,
            1 => 1141,
            2 => 1022,
            3 => 895,
            4 => 895,
            5 => 663,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-06',
            1 => '2017-12-01',
            2 => '2017-11-02',
            3 => '2017-10-08',
            4 => '2017-10-08',
            5 => '2017-06-07',
          ),
        ),
        'Diagonalen Vin Og Blomster (Rådhusbakken 29, 7323 Give)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Diagonalen Vin Og Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7323',
            'cust_city' => 'Give',
            'cust_address' => 'Rådhusbakken 29, 7323 Give',
          ),
          'invoices' => 
          array (
            0 => 1120,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-24',
          ),
        ),
        'Lahvino Italiensk Vinimport (Farvervej 41, 8800 Viborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lahvino Italiensk Vinimport',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8800',
            'cust_city' => 'Viborg',
            'cust_address' => 'Farvervej 41, 8800 Viborg',
          ),
          'invoices' => 
          array (
            0 => 1078,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 1030,
            1 => 880,
            2 => 880,
            3 => 880,
            4 => 880,
            5 => 880,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
            1 => '2017-09-27',
            2 => '2017-09-27',
            3 => '2017-09-27',
            4 => '2017-09-27',
            5 => '2017-09-27',
          ),
        ),
        'Mundskænken ApS Broen Shopping (Mundskænken Broen Shopping, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mundskænken ApS Broen Shopping',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Mundskænken Broen Shopping, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1013,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'Hapa (Morsbøl Skolevej 1, 7200 Grindsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hapa',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7200',
            'cust_city' => 'Grindsted',
            'cust_address' => 'Morsbøl Skolevej 1, 7200 Grindsted',
          ),
          'invoices' => 
          array (
            0 => 944,
            1 => 944,
            2 => 944,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-26',
            2 => '2017-10-26',
          ),
        ),
        'Vinspecialisten Haderslev ApS (Storegade 30, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Haderslev ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Storegade 30, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 891,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-03',
          ),
        ),
        'Peter Vin / Øl & Vin ApS (Skrågade 33, 9400 Nørresundby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Peter Vin / Øl & Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9400',
            'cust_city' => 'Nørresundby',
            'cust_address' => 'Skrågade 33, 9400 Nørresundby',
          ),
          'invoices' => 
          array (
            0 => 660,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-05',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1438,
            1 => 1184,
            2 => 1117,
            3 => 1028,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2017-12-11',
            2 => '2017-11-27',
            3 => '2017-11-01',
          ),
        ),
        'Blach&Co (Algade 7, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Blach&Co',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Algade 7, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 1393,
            1 => 1393,
            2 => 1393,
            3 => 1393,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2018-03-01',
            2 => '2018-03-01',
            3 => '2018-03-01',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1378,
            1 => 1217,
            2 => 1144,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-20',
            2 => '2017-12-05',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1366,
            1 => 1366,
            2 => 1366,
            3 => 1250,
            4 => 1250,
            5 => 1250,
            6 => 1234,
            7 => 1154,
            8 => 1154,
            9 => 995,
            10 => 928,
            11 => 928,
            12 => 928,
            13 => 928,
            14 => 928,
            15 => 827,
            16 => 827,
            17 => 751,
            18 => 751,
            19 => 751,
            20 => 751,
            21 => 708,
            22 => 708,
            23 => 708,
            24 => 708,
            25 => 708,
            26 => 683,
            27 => 683,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
            2 => '2018-02-11',
            3 => '2017-12-22',
            4 => '2017-12-22',
            5 => '2017-12-22',
            6 => '2017-12-07',
            7 => '2017-12-04',
            8 => '2017-12-04',
            9 => '2017-10-29',
            10 => '2017-10-16',
            11 => '2017-10-16',
            12 => '2017-10-16',
            13 => '2017-10-16',
            14 => '2017-10-16',
            15 => '2017-08-20',
            16 => '2017-08-20',
            17 => '2017-07-18',
            18 => '2017-07-18',
            19 => '2017-07-18',
            20 => '2017-07-18',
            21 => '2017-07-05',
            22 => '2017-07-05',
            23 => '2017-07-05',
            24 => '2017-07-05',
            25 => '2017-07-05',
            26 => '2017-06-20',
            27 => '2017-06-20',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1210,
            1 => 902,
            2 => 902,
            3 => 755,
            4 => 755,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-10-11',
            2 => '2017-10-11',
            3 => '2017-07-19',
            4 => '2017-07-19',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1175,
            1 => 1170,
            2 => 1119,
            3 => 1024,
            4 => 916,
            5 => 898,
            6 => 819,
            7 => 819,
            8 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-12-11',
            2 => '2017-11-24',
            3 => '2017-11-03',
            4 => '2017-10-13',
            5 => '2017-10-10',
            6 => '2017-08-09',
            7 => '2017-08-09',
            8 => '2017-08-09',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1165,
            1 => 687,
            2 => 687,
            3 => 687,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-06-22',
            2 => '2017-06-22',
            3 => '2017-06-22',
          ),
        ),
        'Vinspecialisten Randers (Dytmærsken 3, 8900 Randers C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Randers',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8900',
            'cust_city' => 'Randers C',
            'cust_address' => 'Dytmærsken 3, 8900 Randers C',
          ),
          'invoices' => 
          array (
            0 => 1136,
            1 => 1136,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
            1 => '2017-11-28',
          ),
        ),
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1134,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-29',
          ),
        ),
        'Aabybro Mejeri (Brogårdsvej 148, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aabybro Mejeri',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Brogårdsvej 148, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1101,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-06',
          ),
        ),
        'Rene\'s Vin Og Grønttorv (Østergade 2 B, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rene\'s Vin Og Grønttorv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Østergade 2 B, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 732,
            1 => 732,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-13',
            1 => '2017-07-13',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hantwerk (Fiskerivej 2D, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hantwerk',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Fiskerivej 2D, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1410,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1391,
            1 => 1391,
            2 => 1295,
            3 => 1295,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-03',
            1 => '2018-03-03',
            2 => '2018-01-19',
            3 => '2018-01-19',
          ),
        ),
        'Aagaard Kro (Bramdrupvej 136, 6040 Egtved)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aagaard Kro',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6040',
            'cust_city' => 'Egtved',
            'cust_address' => 'Bramdrupvej 136, 6040 Egtved',
          ),
          'invoices' => 
          array (
            0 => 1182,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Hjerting Badehotel (Strandpromenaden 1, 6710 Esbjerg V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjerting Badehotel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6710',
            'cust_city' => 'Esbjerg V',
            'cust_address' => 'Strandpromenaden 1, 6710 Esbjerg V',
          ),
          'invoices' => 
          array (
            0 => 1006,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-24',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'Textur ApS (Vesterbro 65 st th, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Textur ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterbro 65 st th, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1343,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1199,
            1 => 1199,
            2 => 1199,
            3 => 1075,
            4 => 661,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-12-20',
            3 => '2017-11-15',
            4 => '2017-06-08',
          ),
        ),
        'Pakkecenter Nord (Aalborgvej 14, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pakkecenter Nord',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Aalborgvej 14, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 1183,
            1 => 1183,
            2 => 1183,
            3 => 1183,
            4 => 1183,
            5 => 1183,
            6 => 1183,
            7 => 1183,
            8 => 980,
            9 => 980,
            10 => 980,
            11 => 692,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-12-12',
            2 => '2017-12-12',
            3 => '2017-12-12',
            4 => '2017-12-12',
            5 => '2017-12-12',
            6 => '2017-12-12',
            7 => '2017-12-12',
            8 => '2017-10-26',
            9 => '2017-10-26',
            10 => '2017-10-26',
            11 => '2017-06-27',
          ),
        ),
        ' (Antoniusvej 2, 9000 9000)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => '9000',
            'cust_address' => 'Antoniusvej 2, 9000 9000',
          ),
          'invoices' => 
          array (
            0 => 978,
            1 => 978,
            2 => 691,
            3 => 691,
            4 => 691,
            5 => 691,
            6 => 691,
            7 => 691,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-26',
            2 => '2017-06-22',
            3 => '2017-06-22',
            4 => '2017-06-22',
            5 => '2017-06-22',
            6 => '2017-06-22',
            7 => '2017-06-22',
          ),
        ),
        'LT Automation ApS (Fåborgvej 15A, 9220 Aalborg Øst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'LT Automation ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9220',
            'cust_city' => 'Aalborg Øst',
            'cust_address' => 'Fåborgvej 15A, 9220 Aalborg Øst',
          ),
          'invoices' => 
          array (
            0 => 695,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-28',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1155,
            1 => 899,
            2 => 867,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-04',
            1 => '2017-10-09',
            2 => '2017-09-15',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Rombo (Falkoner Allé 46, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rombo',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Allé 46, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1414,
            1 => 1353,
            2 => 1341,
            3 => 1220,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2018-02-22',
            2 => '2018-02-01',
            3 => '2017-12-18',
          ),
        ),
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1351,
            1 => 1304,
            2 => 1304,
            3 => 1304,
            4 => 1304,
            5 => 1304,
            6 => 1304,
            7 => 1304,
            8 => 1304,
            9 => 1222,
            10 => 890,
            11 => 890,
            12 => 890,
            13 => 815,
            14 => 815,
            15 => 690,
            16 => 690,
            17 => 690,
            18 => 690,
            19 => 690,
            20 => 690,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-19',
            1 => '2018-01-10',
            2 => '2018-01-10',
            3 => '2018-01-10',
            4 => '2018-01-10',
            5 => '2018-01-10',
            6 => '2018-01-10',
            7 => '2018-01-10',
            8 => '2018-01-10',
            9 => '2017-12-18',
            10 => '2017-10-02',
            11 => '2017-10-02',
            12 => '2017-10-02',
            13 => '2017-08-15',
            14 => '2017-08-15',
            15 => '2017-06-26',
            16 => '2017-06-26',
            17 => '2017-06-26',
            18 => '2017-06-26',
            19 => '2017-06-26',
            20 => '2017-06-26',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1385,
            1 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
            1 => '2018-01-03',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1365,
            1 => 1365,
            2 => 1365,
            3 => 780,
            4 => 780,
            5 => 780,
            6 => 780,
            7 => 729,
            8 => 729,
            9 => 729,
            10 => 729,
            11 => 729,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
            2 => '2018-02-11',
            3 => '2017-07-28',
            4 => '2017-07-28',
            5 => '2017-07-28',
            6 => '2017-07-28',
            7 => '2017-07-11',
            8 => '2017-07-11',
            9 => '2017-07-11',
            10 => '2017-07-11',
            11 => '2017-07-11',
          ),
        ),
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1359,
            1 => 1257,
            2 => 1005,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
            1 => '2017-12-27',
            2 => '2017-10-31',
          ),
        ),
        'MIKKELLER APS / SCANGLOBAL (HELGESHØJ ALLE 12, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MIKKELLER APS / SCANGLOBAL',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'HELGESHØJ ALLE 12, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1298,
            1 => 1298,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
            1 => 858,
            2 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
            2 => '2017-09-19',
          ),
        ),
        'Alkoholfributik (Ringager 19, 2605 Brøndby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alkoholfributik',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2605',
            'cust_city' => 'Brøndby',
            'cust_address' => 'Ringager 19, 2605 Brøndby',
          ),
          'invoices' => 
          array (
            0 => 797,
            1 => 727,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-02',
            1 => '2017-07-13',
          ),
        ),
      ),
    ),
  ),
  'MJÖD Petersen' => 
  array (
    'Fyn m.fl.' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Fjelsted Skov Kro (Store Landevej 92, 5592 Ejby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fjelsted Skov Kro',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5592',
            'cust_city' => 'Ejby',
            'cust_address' => 'Store Landevej 92, 5592 Ejby',
          ),
          'invoices' => 
          array (
            0 => 1467,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1094,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Water Of Life (Søndergade 27, 8740 Brædstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Water Of Life',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8740',
            'cust_city' => 'Brædstrup',
            'cust_address' => 'Søndergade 27, 8740 Brædstrup',
          ),
          'invoices' => 
          array (
            0 => 1466,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Den Sidste Dråbe Spiritushandel Torvehallerne (Frederiksborggade 21, 1360 København, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel Torvehallerne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, 1360 København, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1448,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1346,
            1 => 1227,
            2 => 869,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-30',
            1 => '2017-12-12',
            2 => '2017-09-13',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1325,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1238,
            1 => 1171,
            2 => 1114,
            3 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-12',
            2 => '2017-11-23',
            3 => '2017-09-23',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 1820 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1820',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 1820 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 1163,
            1 => 1163,
            2 => 950,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-12-07',
            2 => '2017-10-26',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1061,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-13',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10 kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10 kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 853,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 736,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1099,
            1 => 930,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-10-19',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Bautahøj (Bautahøjvej 39, 3630 Jægerspris)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bautahøj',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3630',
            'cust_city' => 'Jægerspris',
            'cust_address' => 'Bautahøjvej 39, 3630 Jægerspris',
          ),
          'invoices' => 
          array (
            0 => 868,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-14',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 860,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
          ),
        ),
        'Bagsværd Vinhandel (Bagsværd Hovedgade 125, 2880 Bagsværd)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bagsværd Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2880',
            'cust_city' => 'Bagsværd',
            'cust_address' => 'Bagsværd Hovedgade 125, 2880 Bagsværd',
          ),
          'invoices' => 
          array (
            0 => 773,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
        'Skjold Burne (Adelgade 70, 4720 Præstø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4720',
            'cust_city' => 'Præstø',
            'cust_address' => 'Adelgade 70, 4720 Præstø',
          ),
          'invoices' => 
          array (
            0 => 719,
            1 => 719,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-05',
            1 => '2017-07-05',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1360,
            1 => 1360,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2018-02-22',
          ),
        ),
        'Smagførst (Jægergårdsgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Jægergårdsgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1211,
            1 => 862,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-09-23',
          ),
        ),
        'Smagførst (Otte Ruds Gade 106 st. 5, 8200 Aarhus N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus N',
            'cust_address' => 'Otte Ruds Gade 106 st. 5, 8200 Aarhus N',
          ),
          'invoices' => 
          array (
            0 => 896,
            1 => 887,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-09-30',
          ),
        ),
        'Vinspecialisten Haderslev ApS (Storegade 30, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Haderslev ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Storegade 30, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 891,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-03',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1306,
            1 => 1153,
            2 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
            1 => '2017-12-05',
            2 => '2017-12-05',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1165,
            1 => 687,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-06-22',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 902,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-11',
          ),
        ),
      ),
    ),
  ),
  'Nordisk Brænderi' => 
  array (
    'Fyn m.fl.' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Fjelsted Skov Kro (Store Landevej 92, 5592 Ejby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fjelsted Skov Kro',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5592',
            'cust_city' => 'Ejby',
            'cust_address' => 'Store Landevej 92, 5592 Ejby',
          ),
          'invoices' => 
          array (
            0 => 1467,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 1027,
            1 => 1027,
            2 => 1025,
            3 => 1025,
            4 => 649,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
            1 => '2017-11-03',
            2 => '2017-11-03',
            3 => '2017-11-03',
            4 => '2017-05-22',
          ),
        ),
        'Rahbek&Læssøe AoS (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe AoS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 785,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-02',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmandsgaard (Torvet 5, 5970 Ærøskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmandsgaard',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5970',
            'cust_city' => 'Ærøskøbing',
            'cust_address' => 'Torvet 5, 5970 Ærøskøbing',
          ),
          'invoices' => 
          array (
            0 => 952,
            1 => 807,
            2 => 689,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-08-10',
            2 => '2017-06-27',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Nørre Vosborg (Vembvej 35, 7570 Vemb)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nørre Vosborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7570',
            'cust_city' => 'Vemb',
            'cust_address' => 'Vembvej 35, 7570 Vemb',
          ),
          'invoices' => 
          array (
            0 => 1459,
            1 => 1459,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
          ),
        ),
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1180,
            1 => 934,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-10-25',
          ),
        ),
        'Mash Aarhus (Banegaardspladsen 12, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mash Aarhus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Banegaardspladsen 12, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1045,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Fiskehuset Thisted (Havnen 31, 7700 Thisted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fiskehuset Thisted',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7700',
            'cust_city' => 'Thisted',
            'cust_address' => 'Havnen 31, 7700 Thisted',
          ),
          'invoices' => 
          array (
            0 => 1456,
            1 => 1456,
            2 => 1456,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2018-03-28',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1438,
            1 => 1251,
            2 => 1184,
            3 => 1184,
            4 => 1117,
            5 => 1028,
            6 => 1028,
            7 => 699,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2017-12-21',
            2 => '2017-12-11',
            3 => '2017-12-11',
            4 => '2017-11-27',
            5 => '2017-11-01',
            6 => '2017-11-01',
            7 => '2017-06-30',
          ),
        ),
        'Arkitema Architects (Frederiksgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Arkitema Architects',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Frederiksgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1358,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
          ),
        ),
        'Herremagasinet (Jyllandsgade 22, 9520 Skørping)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Herremagasinet',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9520',
            'cust_city' => 'Skørping',
            'cust_address' => 'Jyllandsgade 22, 9520 Skørping',
          ),
          'invoices' => 
          array (
            0 => 1313,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1240,
            1 => 1240,
            2 => 630,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-12-19',
            2 => '2017-05-23',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1234,
            1 => 1084,
            2 => 872,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-11-15',
            2 => '2017-09-09',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1210,
            1 => 1210,
            2 => 1210,
            3 => 902,
            4 => 902,
            5 => 875,
            6 => 875,
            7 => 782,
            8 => 782,
            9 => 782,
            10 => 755,
            11 => 755,
            12 => 755,
            13 => 658,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-12-20',
            3 => '2017-10-11',
            4 => '2017-10-11',
            5 => '2017-09-25',
            6 => '2017-09-25',
            7 => '2017-07-31',
            8 => '2017-07-31',
            9 => '2017-07-31',
            10 => '2017-07-19',
            11 => '2017-07-19',
            12 => '2017-07-19',
            13 => '2017-06-08',
          ),
        ),
        'Hr Nielsens Specialiteter (jernbanegade 12 st. tv, 9460 Brovst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Nielsens Specialiteter',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9460',
            'cust_city' => 'Brovst',
            'cust_address' => 'jernbanegade 12 st. tv, 9460 Brovst',
          ),
          'invoices' => 
          array (
            0 => 1196,
            1 => 1195,
            2 => 923,
            3 => 923,
            4 => 923,
            5 => 923,
            6 => 923,
            7 => 799,
            8 => 799,
            9 => 799,
            10 => 799,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-18',
            1 => '2017-12-13',
            2 => '2017-10-13',
            3 => '2017-10-13',
            4 => '2017-10-13',
            5 => '2017-10-13',
            6 => '2017-10-13',
            7 => '2017-08-02',
            8 => '2017-08-02',
            9 => '2017-08-02',
            10 => '2017-08-02',
          ),
        ),
        'Knud Sko (Vestergade 23, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Knud Sko',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Vestergade 23, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1121,
            1 => 1121,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
          ),
        ),
        'Aabybro Mejeri (Brogårdsvej 148, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aabybro Mejeri',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Brogårdsvej 148, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1101,
            1 => 1101,
            2 => 1101,
            3 => 894,
            4 => 894,
            5 => 894,
            6 => 894,
            7 => 856,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
            2 => '2017-11-23',
            3 => '2017-10-05',
            4 => '2017-10-05',
            5 => '2017-10-05',
            6 => '2017-10-05',
            7 => '2017-09-08',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 954,
            1 => 898,
            2 => 878,
            3 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-02',
            1 => '2017-10-10',
            2 => '2017-09-25',
            3 => '2017-08-09',
          ),
        ),
        'Mokkahouse (Lystrupvej 62, 8240 Risskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mokkahouse',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8240',
            'cust_city' => 'Risskov',
            'cust_address' => 'Lystrupvej 62, 8240 Risskov',
          ),
          'invoices' => 
          array (
            0 => 932,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-18',
          ),
        ),
        'Rene\'s Vin Og Grønttorv (Østergade 2 B, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rene\'s Vin Og Grønttorv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Østergade 2 B, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 893,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-04',
          ),
        ),
        'Albert Rex (Nørregade 22, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Albert Rex',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nørregade 22, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 673,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1442,
            1 => 1056,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-11-10',
          ),
        ),
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1020,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-01',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Sigurd Muller Vinhandel A/S (Otto Mønstedsvej 2, 9200 Aalborg SV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sigurd Muller Vinhandel A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg SV',
            'cust_address' => 'Otto Mønstedsvej 2, 9200 Aalborg SV',
          ),
          'invoices' => 
          array (
            0 => 1436,
            1 => 1096,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-11-22',
          ),
        ),
        'Smagførst (Jægergårdsgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Jægergårdsgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1352,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
          ),
        ),
        'Vin&Vin (Torvet 8, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin&Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Torvet 8, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 1323,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Bisgaard Vinhandel (Jernbanegade 4, 9530 Støvring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bisgaard Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9530',
            'cust_city' => 'Støvring',
            'cust_address' => 'Jernbanegade 4, 9530 Støvring',
          ),
          'invoices' => 
          array (
            0 => 1311,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1303,
            1 => 933,
            2 => 745,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-11',
            1 => '2017-10-24',
            2 => '2017-07-17',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1300,
            1 => 1104,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-11-23',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1296,
            1 => 700,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-06-30',
          ),
        ),
        'Kolding Vinhandel (Låsbybanke 12, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kolding Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Låsbybanke 12, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 1289,
            1 => 1103,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-17',
            1 => '2017-11-23',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1254,
            1 => 1021,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-27',
            1 => '2017-11-02',
          ),
        ),
        'Uhrskov Vinhandel (Bredgade 56, 7400 Herning)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Uhrskov Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7400',
            'cust_city' => 'Herning',
            'cust_address' => 'Bredgade 56, 7400 Herning',
          ),
          'invoices' => 
          array (
            0 => 1106,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1100,
            1 => 1100,
            2 => 897,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-23',
            2 => '2017-10-09',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 1030,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
        'Mundskænken ApS Broen Shopping (Mundskænken Broen Shopping, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mundskænken ApS Broen Shopping',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Mundskænken Broen Shopping, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1013,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'Vinshoppen (Hadsundvej 12, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinshoppen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Hadsundvej 12, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 938,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-23',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 914,
            1 => 774,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-06',
            1 => '2017-07-26',
          ),
        ),
        'Vinspecialisten Silkeborg (Vestergade 23, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Silkeborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Vestergade 23, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 839,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 766,
            1 => 766,
            2 => 766,
            3 => 766,
            4 => 766,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
            1 => '2017-07-24',
            2 => '2017-07-24',
            3 => '2017-07-24',
            4 => '2017-07-24',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 762,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 726,
            1 => 724,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-12',
            1 => '2017-07-12',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1306,
            1 => 1306,
            2 => 1153,
            3 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
            1 => '2018-01-08',
            2 => '2017-12-05',
            3 => '2017-12-05',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'CANblau Aalborg (Ved stranden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'CANblau Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Ved stranden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 786,
            1 => 786,
            2 => 786,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-01',
            1 => '2017-08-01',
            2 => '2017-08-01',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1453,
            1 => 1453,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1440,
            1 => 1340,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-02-11',
          ),
        ),
        'Vinspecialisten Lyngby (Klampenborgvej 232, 2800 Lyngby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lyngby',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2800',
            'cust_city' => 'Lyngby',
            'cust_address' => 'Klampenborgvej 232, 2800 Lyngby',
          ),
          'invoices' => 
          array (
            0 => 1374,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1177,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1166,
            1 => 860,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-09-19',
          ),
        ),
        'GEORGE LORANGE & CO. A/S (Kirkestræde 4, 4700 Næstved)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GEORGE LORANGE & CO. A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4700',
            'cust_city' => 'Næstved',
            'cust_address' => 'Kirkestræde 4, 4700 Næstved',
          ),
          'invoices' => 
          array (
            0 => 1016,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        '1 Godt Sted (Buddingevej 310, 2860 Søborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => '1 Godt Sted',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2860',
            'cust_city' => 'Søborg',
            'cust_address' => 'Buddingevej 310, 2860 Søborg',
          ),
          'invoices' => 
          array (
            0 => 855,
            1 => 855,
            2 => 855,
            3 => 641,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
            1 => '2017-09-05',
            2 => '2017-09-05',
            3 => '2017-05-31',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 757,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-20',
          ),
        ),
        'Skjold Burne (Adelgade 70, 4720 Præstø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4720',
            'cust_city' => 'Præstø',
            'cust_address' => 'Adelgade 70, 4720 Præstø',
          ),
          'invoices' => 
          array (
            0 => 719,
            1 => 719,
            2 => 719,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-05',
            1 => '2017-07-05',
            2 => '2017-07-05',
          ),
        ),
        'Inco (Ingerslevsgade 42, 1711 København V.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V.',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V.',
          ),
          'invoices' => 
          array (
            0 => 656,
            1 => 656,
            2 => 656,
            3 => 656,
            4 => 656,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
            1 => '2017-06-07',
            2 => '2017-06-07',
            3 => '2017-06-07',
            4 => '2017-06-07',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1301,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1274,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-13',
          ),
        ),
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-31',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1171,
            1 => 1114,
            2 => 1114,
            3 => 1114,
            4 => 1052,
            5 => 1052,
            6 => 1052,
            7 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-11-23',
            2 => '2017-11-23',
            3 => '2017-11-23',
            4 => '2017-11-09',
            5 => '2017-11-09',
            6 => '2017-11-09',
            7 => '2017-09-23',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-06-30',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1271,
            1 => 1190,
            2 => 1147,
            3 => 1147,
            4 => 930,
            5 => 841,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-12',
            1 => '2017-12-14',
            2 => '2017-12-04',
            3 => '2017-12-04',
            4 => '2017-10-19',
            5 => '2017-08-31',
          ),
        ),
        'Klosterkælderen (Store Gråbrødrestræde 23, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Klosterkælderen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Store Gråbrødrestræde 23, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1004,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'The Bird & The Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & The Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 820,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1185,
            1 => 1185,
            2 => 1156,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-12-13',
            2 => '2017-12-01',
          ),
        ),
        'Smagsløget (Storgade 15b, 4180 Sorø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagsløget',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4180',
            'cust_city' => 'Sorø',
            'cust_address' => 'Storgade 15b, 4180 Sorø',
          ),
          'invoices' => 
          array (
            0 => 675,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-14',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Hotel Saxkjøbing (Torvet 9, 4990 Sakskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Saxkjøbing',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4990',
            'cust_city' => 'Sakskøbing',
            'cust_address' => 'Torvet 9, 4990 Sakskøbing',
          ),
          'invoices' => 
          array (
            0 => 758,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'MIKKELLER APS / SCANGLOBAL (HELGESHØJ ALLE 12, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MIKKELLER APS / SCANGLOBAL',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'HELGESHØJ ALLE 12, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1298,
            1 => 1298,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1017,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-01',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
            1 => 858,
            2 => 858,
            3 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
            2 => '2017-09-19',
            3 => '2017-09-19',
          ),
        ),
        'Drinky (Haslegårdsvej 20, 8210 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinky',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8210',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Haslegårdsvej 20, 8210 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 769,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 815,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-15',
          ),
        ),
      ),
    ),
  ),
  'Salg af fragt - momspligtig' => 
  array (
    'Fyn m.fl.' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Fjelsted Skov Kro (Store Landevej 92, 5592 Ejby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fjelsted Skov Kro',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5592',
            'cust_city' => 'Ejby',
            'cust_address' => 'Store Landevej 92, 5592 Ejby',
          ),
          'invoices' => 
          array (
            0 => 1467,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Højestene færgen (Jensens mole 5, 5700 Svendborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Højestene færgen',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5700',
            'cust_city' => 'Svendborg',
            'cust_address' => 'Jensens mole 5, 5700 Svendborg',
          ),
          'invoices' => 
          array (
            0 => 1458,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Rahbek&Læssøe Aps (Rahbeksvej 9, 5230 Odense M)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5230',
            'cust_city' => 'Odense M',
            'cust_address' => 'Rahbeksvej 9, 5230 Odense M',
          ),
          'invoices' => 
          array (
            0 => 1090,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
          ),
        ),
        'Boxit (Thorslundsvej 3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Boxit',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Thorslundsvej 3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 707,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-03',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmandsgaard (Torvet 5, 5970 Ærøskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmandsgaard',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5970',
            'cust_city' => 'Ærøskøbing',
            'cust_address' => 'Torvet 5, 5970 Ærøskøbing',
          ),
          'invoices' => 
          array (
            0 => 728,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-13',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1452,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Meny Hellerup (Strandvejen 64A, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Hellerup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Strandvejen 64A, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1336,
            1 => 768,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-07-25',
          ),
        ),
        'Skjold Burne Vanløse (jernbane alle 45, 2720 vanløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vanløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2720',
            'cust_city' => 'vanløse',
            'cust_address' => 'jernbane alle 45, 2720 vanløse',
          ),
          'invoices' => 
          array (
            0 => 1324,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-31',
          ),
        ),
        'Vinoteket ApS (Gentoftegade 54, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoteket ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gentoftegade 54, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1053,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
        'Zaizon (Falkoner Alle 33, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Zaizon',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Alle 33, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1047,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
        'VINSLOTTET I GREVE A/S (Håndværkerbyen 42, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VINSLOTTET I GREVE A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Håndværkerbyen 42, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 927,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 832,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-23',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'R60 (Mosedalsvej 17, 3740 Svaneke)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'R60',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3740',
            'cust_city' => 'Svaneke',
            'cust_address' => 'Mosedalsvej 17, 3740 Svaneke',
          ),
          'invoices' => 
          array (
            0 => 1428,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
        'The Bird & Kissmeyer (Vesterbrogade 3, 1603 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & Kissmeyer',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1603',
            'cust_city' => 'København K',
            'cust_address' => 'Vesterbrogade 3, 1603 København K',
          ),
          'invoices' => 
          array (
            0 => 1409,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1322,
            1 => 1271,
            2 => 1099,
            3 => 930,
            4 => 882,
            5 => 841,
            6 => 800,
            7 => 693,
            8 => 669,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-31',
            1 => '2018-01-12',
            2 => '2017-11-23',
            3 => '2017-10-19',
            4 => '2017-09-27',
            5 => '2017-08-31',
            6 => '2017-08-03',
            7 => '2017-06-28',
            8 => '2017-06-12',
          ),
        ),
        'The Bird & The Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & The Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 820,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        ' (Krogsbøllevej 13, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Krogsbøllevej 13, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1411,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1124,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
          ),
        ),
        'Brown Guy ApS / Balderdash (Valkendorfsgade 11, 1151 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brown Guy ApS / Balderdash',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1151',
            'cust_city' => 'København K',
            'cust_address' => 'Valkendorfsgade 11, 1151 København K',
          ),
          'invoices' => 
          array (
            0 => 1038,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-24',
          ),
        ),
        'Drinksonme (Geelsebaan 178, 2470 Retie)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinksonme',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2470',
            'cust_city' => 'Retie',
            'cust_address' => 'Geelsebaan 178, 2470 Retie',
          ),
          'invoices' => 
          array (
            0 => 1034,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-04',
          ),
        ),
        'Mondo Kaos (Birkegade 1, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mondo Kaos',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Birkegade 1, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1029,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
        'Stork concepts (Fredriksborg gade 1b 4 sal, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Stork concepts',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Fredriksborg gade 1b 4 sal, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 825,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-22',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 6 kld th, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 6 kld th, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 635,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'My 50\'s closet (Enghaven 11, 3630 Jægerspris)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'My 50\'s closet',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3630',
            'cust_city' => 'Jægerspris',
            'cust_address' => 'Enghaven 11, 3630 Jægerspris',
          ),
          'invoices' => 
          array (
            0 => 1406,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'Lidt-af-hvert I/S (Lindegårdsager 14, 2640 Hedehusene)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lidt-af-hvert I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2640',
            'cust_city' => 'Hedehusene',
            'cust_address' => 'Lindegårdsager 14, 2640 Hedehusene',
          ),
          'invoices' => 
          array (
            0 => 1083,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-14',
          ),
        ),
        'Att: Aliva Foods (Nrdr. Frihavnsgade 68, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Att: Aliva Foods',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Nrdr. Frihavnsgade 68, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1023,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Nobis Hotel Copenhagen (Niels Brocks Gade 1, 1574 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nobis Hotel Copenhagen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1574',
            'cust_city' => 'København V',
            'cust_address' => 'Niels Brocks Gade 1, 1574 København V',
          ),
          'invoices' => 
          array (
            0 => 1363,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
          ),
        ),
        'Vang & Barfred (Wilders Plads 11B, 1403 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vang & Barfred',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1403',
            'cust_city' => 'København K',
            'cust_address' => 'Wilders Plads 11B, 1403 København K',
          ),
          'invoices' => 
          array (
            0 => 1297,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 20, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 20, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 783,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-31',
          ),
        ),
        'Hotel Saxkjøbing (Torvet 9, 4990 Sakskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Saxkjøbing',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4990',
            'cust_city' => 'Sakskøbing',
            'cust_address' => 'Torvet 9, 4990 Sakskøbing',
          ),
          'invoices' => 
          array (
            0 => 758,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
        'Restaurant Skade (Rosengaarden 12, 1174 Copenhagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Skade',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1174',
            'cust_city' => 'Copenhagen',
            'cust_address' => 'Rosengaarden 12, 1174 Copenhagen',
          ),
          'invoices' => 
          array (
            0 => 640,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-30',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Villa Vest (Strandvejen 138, 9800 Lønstrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Villa Vest',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Lønstrup',
            'cust_address' => 'Strandvejen 138, 9800 Lønstrup',
          ),
          'invoices' => 
          array (
            0 => 1425,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Domestic Restaurant ApS (Mejlgade 35B, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Domestic Restaurant ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Mejlgade 35B, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1269,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-12',
          ),
        ),
        'Hjerting Badehotel (Strandpromenaden 1, 6710 Esbjerg V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjerting Badehotel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6710',
            'cust_city' => 'Esbjerg V',
            'cust_address' => 'Strandpromenaden 1, 6710 Esbjerg V',
          ),
          'invoices' => 
          array (
            0 => 1164,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-08',
          ),
        ),
        'Mash Aarhus (Banegaardspladsen 12, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mash Aarhus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Banegaardspladsen 12, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1045,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Arkitema Architects (Frederiksgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Arkitema Architects',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Frederiksgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1358,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
          ),
        ),
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1258,
            1 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-28',
            1 => '2017-11-06',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1033,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-04',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 844,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-04',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Skotlander ApS (Hjulmagervej 19, 9490 Pandrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9490',
            'cust_city' => 'Pandrup',
            'cust_address' => 'Hjulmagervej 19, 9490 Pandrup',
          ),
          'invoices' => 
          array (
            0 => 1348,
            1 => 1200,
            2 => 1035,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-20',
            2 => '2017-11-04',
          ),
        ),
        'Nordisk Brænderi (Hjortdalvej 227, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nordisk Brænderi',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 227, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 1293,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        ' (Thorsvej 6, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Thorsvej 6, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1202,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Pakkecenter Nord (Aalborgvej 14, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pakkecenter Nord',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Aalborgvej 14, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 1183,
            1 => 980,
            2 => 920,
            3 => 852,
            4 => 692,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-10-26',
            2 => '2017-10-15',
            3 => '2017-09-06',
            4 => '2017-06-27',
          ),
        ),
        'Skotlander ApS (Damvej 54, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Damvej 54, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1160,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
          ),
        ),
        'DRINX A/S (Fiskerihavnsgade 23, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'DRINX A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Fiskerihavnsgade 23, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 775,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-25',
          ),
        ),
        'LT Automation ApS (Fåborgvej 15A, 9220 Aalborg Øst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'LT Automation ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9220',
            'cust_city' => 'Aalborg Øst',
            'cust_address' => 'Fåborgvej 15A, 9220 Aalborg Øst',
          ),
          'invoices' => 
          array (
            0 => 695,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-28',
          ),
        ),
        ' (Antoniusvej 2, 9000 9000)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => '9000',
            'cust_address' => 'Antoniusvej 2, 9000 9000',
          ),
          'invoices' => 
          array (
            0 => 691,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-22',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'Textur ApS (Vesterbro 65 st th, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Textur ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterbro 65 st th, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1343,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hornsleth Bar Aalborg (Jomfru Ane Gade 11, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hornsleth Bar Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Jomfru Ane Gade 11, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1314,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
          ),
        ),
        'Hotel Skjern (Bredgade 58, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Skjern',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Bredgade 58, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 937,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
          ),
        ),
        'Mungo Park Kolding (Fredericiagade 1, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mungo Park Kolding',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Fredericiagade 1, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 936,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
          ),
        ),
        'Anholt Gin (Østervej 11, 8592 Anholt)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Anholt Gin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8592',
            'cust_city' => 'Anholt',
            'cust_address' => 'Østervej 11, 8592 Anholt',
          ),
          'invoices' => 
          array (
            0 => 698,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1036,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-04',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'missmia.dk (Nørre Søgade 7, 2. th, 1370 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'missmia.dk',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '1370',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Søgade 7, 2. th, 1370 København K',
          ),
          'invoices' => 
          array (
            0 => 1040,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-24',
          ),
        ),
      ),
    ),
  ),
  'Dråben import' => 
  array (
    'Fyn m.fl.' => 
    array (
      'Restaurant: Cafe' => 
      array (
        'Højestene færgen (Jensens mole 5, 5700 Svendborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Højestene færgen',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5700',
            'cust_city' => 'Svendborg',
            'cust_address' => 'Jensens mole 5, 5700 Svendborg',
          ),
          'invoices' => 
          array (
            0 => 1458,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1179,
            1 => 830,
            2 => 830,
            3 => 794,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-08-23',
            2 => '2017-08-23',
            3 => '2017-08-02',
          ),
        ),
        'Vinspecialisten Middelfart ApS (Østergade 7, 5500 Middelfart)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Middelfart ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5500',
            'cust_city' => 'Middelfart',
            'cust_address' => 'Østergade 7, 5500 Middelfart',
          ),
          'invoices' => 
          array (
            0 => 1093,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1453,
            1 => 1145,
            2 => 1074,
            3 => 1074,
            4 => 761,
            5 => 761,
            6 => 759,
            7 => 759,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2017-12-05',
            2 => '2017-11-15',
            3 => '2017-11-15',
            4 => '2017-07-23',
            5 => '2017-07-23',
            6 => '2017-07-21',
            7 => '2017-07-21',
          ),
        ),
        'Havannahuset (Algade 50, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Havannahuset',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Algade 50, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1327,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1302,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1267,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-08',
          ),
        ),
        'Inco Glostrup (Ejby Industrivej 11, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco Glostrup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Ejby Industrivej 11, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1262,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-04',
          ),
        ),
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
          ),
        ),
        'Backe Vin (Bjergegade 1, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Backe Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Bjergegade 1, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 951,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 942,
            1 => 881,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-19',
            1 => '2017-09-27',
          ),
        ),
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 904,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-11',
          ),
        ),
        'Brdr. D (Rolighedsvej 9, 1958 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr. D',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1958',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Rolighedsvej 9, 1958 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 793,
            1 => 793,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-03',
            1 => '2017-08-03',
          ),
        ),
        'Brdr D Østerbro (Østerbrogade 152, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Østerbro',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 152, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 792,
            1 => 792,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-03',
            1 => '2017-08-03',
          ),
        ),
        'Bagsværd Vinhandel (Bagsværd Hovedgade 125, 2880 Bagsværd)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bagsværd Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2880',
            'cust_city' => 'Bagsværd',
            'cust_address' => 'Bagsværd Hovedgade 125, 2880 Bagsværd',
          ),
          'invoices' => 
          array (
            0 => 773,
            1 => 773,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
            1 => '2017-07-26',
          ),
        ),
        '2010 Vin & Velsmag (C. E. Christiansens Vej 1, 4930 Maribo)' => 
        array (
          'info' => 
          array (
            'cust_name' => '2010 Vin & Velsmag',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4930',
            'cust_city' => 'Maribo',
            'cust_address' => 'C. E. Christiansens Vej 1, 4930 Maribo',
          ),
          'invoices' => 
          array (
            0 => 672,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
        'Inco (Ingerslevsgade 42, 1711 København V.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V.',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V.',
          ),
          'invoices' => 
          array (
            0 => 656,
            1 => 656,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
            1 => '2017-06-07',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Den Sidste Dråbe Spiritushandel Torvehallerne (Frederiksborggade 21, 1360 København, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel Torvehallerne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, 1360 København, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1448,
            1 => 1448,
            2 => 1448,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2018-03-28',
          ),
        ),
        'Den Sidste Dråbe (Linnésgade 14-18, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'Linnésgade 14-18, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 1433,
            1 => 1433,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 10 kld tv, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 10 kld tv, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 1426,
            1 => 1426,
            2 => 1426,
            3 => 818,
            4 => 818,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
            2 => '2018-03-16',
            3 => '2017-08-11',
            4 => '2017-08-11',
          ),
        ),
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1424,
            1 => 1290,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-01-19',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1381,
            1 => 1274,
            2 => 866,
            3 => 866,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-13',
            2 => '2017-09-23',
            3 => '2017-09-23',
          ),
        ),
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1376,
            1 => 1301,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-19',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1355,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1346,
            1 => 1291,
            2 => 1291,
            3 => 1239,
            4 => 1227,
            5 => 1122,
            6 => 1122,
            7 => 885,
            8 => 869,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-30',
            1 => '2018-01-18',
            2 => '2018-01-18',
            3 => '2017-12-20',
            4 => '2017-12-12',
            5 => '2017-11-23',
            6 => '2017-11-23',
            7 => '2017-10-01',
            8 => '2017-09-13',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1326,
            1 => 1325,
            2 => 1325,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-31',
            1 => '2018-01-19',
            2 => '2018-01-19',
          ),
        ),
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1266,
            1 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-31',
            1 => '2017-12-31',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1061,
            1 => 900,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-13',
            1 => '2017-10-10',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 702,
            2 => 702,
            3 => 702,
            4 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-06-30',
            2 => '2017-06-30',
            3 => '2017-06-30',
            4 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Torvehallerne Hal 2 Stade 5 D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2 Stade 5 D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 884,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-29',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 865,
            1 => 865,
            2 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
            1 => '2017-09-23',
            2 => '2017-09-23',
          ),
        ),
        'skjold burne (Østergade 1, 1100 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'skjold burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1100',
            'cust_city' => 'København',
            'cust_address' => 'Østergade 1, 1100 København',
          ),
          'invoices' => 
          array (
            0 => 859,
            1 => 859,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, st. tv., 2400 Kbh NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'Kbh NV',
            'cust_address' => 'Frederikssundsvej 33, st. tv., 2400 Kbh NV',
          ),
          'invoices' => 
          array (
            0 => 814,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-16',
          ),
        ),
        'Den Sidste Dråbe (Torvehallerne Hal 2, 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2, 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 777,
            1 => 754,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
            1 => '2017-07-20',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 6 kld th, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 6 kld th, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 706,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe (Hal 2, Frederiksborggade 21,, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Frederiksborggade 21,, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 705,
            1 => 648,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-01',
            1 => '2017-05-23',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 678,
            1 => 634,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
            1 => '2017-05-24',
          ),
        ),
        'Att: Gin, Rom & Cocktail Festivalen i Pressen (Rådhuspladsen 37, 1785 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Att: Gin, Rom & Cocktail Festivalen i Pressen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1785',
            'cust_city' => 'København V',
            'cust_address' => 'Rådhuspladsen 37, 1785 København V',
          ),
          'invoices' => 
          array (
            0 => 655,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, Hal 2 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, Hal 2 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 644,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1338,
            1 => 1338,
            2 => 1156,
            3 => 1156,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-03',
            1 => '2018-02-03',
            2 => '2017-12-01',
            3 => '2017-12-01',
          ),
        ),
        'GinGin I/S (Ternevej 30, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GinGin I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ternevej 30, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1319,
            1 => 1037,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-30',
            1 => '2017-11-04',
          ),
        ),
        'Smagning.com II (Bækkeskovvej 81, 2700 Brønshøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagning.com II',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2700',
            'cust_city' => 'Brønshøj',
            'cust_address' => 'Bækkeskovvej 81, 2700 Brønshøj',
          ),
          'invoices' => 
          array (
            0 => 1095,
            1 => 1095,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
            1 => '2017-11-20',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 882,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-27',
          ),
        ),
        'The Bird & The Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & The Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 820,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 18, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 18, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 808,
            1 => 808,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-10',
            1 => '2017-08-10',
          ),
        ),
        'Restaurant Format (Sankt Annæ Plads 20, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 20, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 783,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-31',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1441,
            1 => 1441,
            2 => 1258,
            3 => 1258,
            4 => 1110,
            5 => 1110,
            6 => 1043,
            7 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2017-12-28',
            3 => '2017-12-28',
            4 => '2017-11-23',
            5 => '2017-11-23',
            6 => '2017-11-06',
            7 => '2017-11-06',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1438,
            1 => 1117,
            2 => 1028,
            3 => 699,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
            1 => '2017-11-27',
            2 => '2017-11-01',
            3 => '2017-06-30',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 954,
            1 => 898,
            2 => 878,
            3 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-02',
            1 => '2017-10-10',
            2 => '2017-09-25',
            3 => '2017-08-09',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 863,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-21',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 687,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-22',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1435,
            1 => 867,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-20',
            1 => '2017-09-15',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1345,
            1 => 1345,
            2 => 1318,
            3 => 1318,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-02-12',
            2 => '2018-01-31',
            3 => '2018-01-31',
          ),
        ),
        'Skotlander ApS (Damvej 54, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Damvej 54, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 1299,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-15',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 823,
            1 => 703,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
            1 => '2017-06-30',
          ),
        ),
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 661,
            1 => 661,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-08',
            1 => '2017-06-08',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1295,
            1 => 1115,
            2 => 861,
            3 => 666,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-11-23',
            2 => '2017-09-19',
            3 => '2017-06-09',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1254,
            1 => 753,
            2 => 746,
            3 => 676,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-27',
            1 => '2017-07-16',
            2 => '2017-07-16',
            3 => '2017-06-14',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1149,
            1 => 895,
            2 => 741,
            3 => 670,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-06',
            1 => '2017-10-08',
            2 => '2017-07-05',
            3 => '2017-06-13',
          ),
        ),
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 795,
            1 => 795,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-03',
            1 => '2017-08-03',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 766,
            1 => 766,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
            1 => '2017-07-24',
          ),
        ),
        'Lahvino Italiensk Vinimport (Farvervej 41, 8800 Viborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Lahvino Italiensk Vinimport',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8800',
            'cust_city' => 'Viborg',
            'cust_address' => 'Farvervej 41, 8800 Viborg',
          ),
          'invoices' => 
          array (
            0 => 674,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'CANblau Aalborg (Ved stranden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'CANblau Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Ved stranden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 786,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-01',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1385,
            1 => 1385,
            2 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
            1 => '2018-02-20',
            2 => '2018-01-03',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Rombo (Falkoner Allé 46, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rombo',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Allé 46, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1341,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-01',
          ),
        ),
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1245,
            1 => 690,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-06-26',
          ),
        ),
      ),
    ),
  ),
  'Nordic Spirits egenimport' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1452,
            1 => 1380,
            2 => 1367,
            3 => 1267,
            4 => 1111,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-02-28',
            2 => '2018-02-22',
            3 => '2018-01-08',
            4 => '2017-11-23',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1379,
            1 => 1321,
            2 => 1138,
            3 => 1063,
            4 => 942,
            5 => 881,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-24',
            2 => '2017-11-22',
            3 => '2017-11-14',
            4 => '2017-10-19',
            5 => '2017-09-27',
          ),
        ),
        'Falkensten Vin (Jernbanegade 18 b, 3600 Frederikssund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Falkensten Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3600',
            'cust_city' => 'Frederikssund',
            'cust_address' => 'Jernbanegade 18 b, 3600 Frederikssund',
          ),
          'invoices' => 
          array (
            0 => 1305,
            1 => 1252,
            2 => 1252,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-09',
            1 => '2017-12-21',
            2 => '2017-12-21',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1302,
            1 => 1166,
            2 => 860,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-12-11',
            2 => '2017-09-19',
          ),
        ),
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1272,
            1 => 1235,
            2 => 1145,
            3 => 949,
            4 => 837,
            5 => 761,
            6 => 759,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-13',
            1 => '2017-12-20',
            2 => '2017-12-05',
            3 => '2017-10-16',
            4 => '2017-08-31',
            5 => '2017-07-23',
            6 => '2017-07-21',
          ),
        ),
        'Skjold Burne (Frederiksværksgade 95, 3400 hillerød)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3400',
            'cust_city' => 'hillerød',
            'cust_address' => 'Frederiksværksgade 95, 3400 hillerød',
          ),
          'invoices' => 
          array (
            0 => 1265,
            1 => 821,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-02',
            1 => '2017-08-20',
          ),
        ),
        'Inco Glostrup (Ejby Industrivej 11, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco Glostrup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Ejby Industrivej 11, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1262,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-04',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1197,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Vinoteket ApS (Gentoftegade 54, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoteket ApS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gentoftegade 54, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1053,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
        'VIN121 (Peter Bangs Vej 121, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VIN121',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Peter Bangs Vej 121, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1050,
            1 => 1050,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
            1 => '2017-11-08',
          ),
        ),
        'Brdr. D (Rolighedsvej 9, 1958 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr. D',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1958',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Rolighedsvej 9, 1958 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 986,
            1 => 940,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-10-26',
          ),
        ),
        'Backe Vin (Bjergegade 1, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Backe Vin',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Bjergegade 1, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 951,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Brdr D Østerbro (Østerbrogade 152, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Østerbro',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 152, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 947,
            1 => 947,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-26',
          ),
        ),
        'Skjold Burne Ringsted (Nørregade 45, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Ringsted',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nørregade 45, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 851,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-01',
          ),
        ),
        'Zaizon (Falkoner Alle 33, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Zaizon',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Alle 33, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 848,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'VINSLOTTET I GREVE A/S (Håndværkerbyen 42, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VINSLOTTET I GREVE A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Håndværkerbyen 42, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 834,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 832,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-23',
          ),
        ),
        'Den Franske Vinhandel (Skomagergade 3, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Franske Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Skomagergade 3, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 662,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
        'Inco (Ingerslevsgade 42, 1711 København V.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V.',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V.',
          ),
          'invoices' => 
          array (
            0 => 656,
            1 => 656,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
            1 => '2017-06-07',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1450,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-26',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel Torvehallerne (Frederiksborggade 21, 1360 København, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel Torvehallerne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, 1360 København, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1448,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1381,
            1 => 1274,
            2 => 1201,
            3 => 931,
            4 => 931,
            5 => 931,
            6 => 866,
            7 => 816,
            8 => 657,
            9 => 657,
            10 => 657,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-13',
            2 => '2017-12-20',
            3 => '2017-10-17',
            4 => '2017-10-17',
            5 => '2017-10-17',
            6 => '2017-09-23',
            7 => '2017-08-20',
            8 => '2017-06-07',
            9 => '2017-06-07',
            10 => '2017-06-07',
          ),
        ),
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1376,
            1 => 1301,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-19',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1355,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-15',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1325,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1291,
            1 => 1227,
            2 => 1219,
            3 => 1219,
            4 => 1123,
            5 => 929,
            6 => 869,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-18',
            1 => '2017-12-12',
            2 => '2017-12-19',
            3 => '2017-12-19',
            4 => '2017-11-28',
            5 => '2017-10-18',
            6 => '2017-09-13',
          ),
        ),
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1266,
            1 => 1266,
            2 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-31',
            1 => '2017-12-31',
            2 => '2017-12-31',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1230,
            1 => 900,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-10-10',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 903,
            2 => 903,
            3 => 702,
            4 => 702,
            5 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-09-30',
            2 => '2017-09-30',
            3 => '2017-06-30',
            4 => '2017-06-30',
            5 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Torvehallerne Hal 2 Stade 5 D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2 Stade 5 D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 884,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-29',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-23',
          ),
        ),
        'skjold burne (Østergade 1, 1100 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'skjold burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1100',
            'cust_city' => 'København',
            'cust_address' => 'Østergade 1, 1100 København',
          ),
          'invoices' => 
          array (
            0 => 859,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10 kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10 kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 853,
            1 => 836,
            2 => 836,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
            1 => '2017-08-24',
            2 => '2017-08-24',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, st. tv., 2400 Kbh NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'Kbh NV',
            'cust_address' => 'Frederikssundsvej 33, st. tv., 2400 Kbh NV',
          ),
          'invoices' => 
          array (
            0 => 814,
            1 => 798,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-16',
            1 => '2017-08-02',
          ),
        ),
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 736,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 6 kld th, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 6 kld th, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 706,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe (Hal 2, Frederiksborggade 21,, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Frederiksborggade 21,, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 705,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-01',
          ),
        ),
        'Att: Gin, Rom & Cocktail Festivalen i Pressen (Rådhuspladsen 37, 1785 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Att: Gin, Rom & Cocktail Festivalen i Pressen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1785',
            'cust_city' => 'København V',
            'cust_address' => 'Rådhuspladsen 37, 1785 København V',
          ),
          'invoices' => 
          array (
            0 => 655,
            1 => 655,
            2 => 655,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
            1 => '2017-06-07',
            2 => '2017-06-07',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, Hal 2 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, Hal 2 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 644,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 634,
            1 => 634,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
            1 => '2017-05-24',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'The Bird & Kissmeyer (Vesterbrogade 3, 1603 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & Kissmeyer',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1603',
            'cust_city' => 'København K',
            'cust_address' => 'Vesterbrogade 3, 1603 København K',
          ),
          'invoices' => 
          array (
            0 => 1409,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'Papa Bird (Kødboderne 7, 1717 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Papa Bird',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1717',
            'cust_city' => 'København V',
            'cust_address' => 'Kødboderne 7, 1717 København V',
          ),
          'invoices' => 
          array (
            0 => 1405,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'The Bird and the Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird and the Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 1404,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-07',
          ),
        ),
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 882,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-27',
          ),
        ),
        'The Bird & The Churchkey (Gammel Strand 44, 1202 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'The Bird & The Churchkey',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1202',
            'cust_city' => 'København K',
            'cust_address' => 'Gammel Strand 44, 1202 København K',
          ),
          'invoices' => 
          array (
            0 => 820,
            1 => 820,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
            1 => '2017-08-20',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1338,
            1 => 1185,
            2 => 1185,
            3 => 1156,
            4 => 1156,
            5 => 1156,
            6 => 1156,
            7 => 748,
            8 => 748,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-03',
            1 => '2017-12-13',
            2 => '2017-12-13',
            3 => '2017-12-01',
            4 => '2017-12-01',
            5 => '2017-12-01',
            6 => '2017-12-01',
            7 => '2017-07-14',
            8 => '2017-07-14',
          ),
        ),
        'GinGin I/S (Ternevej 30, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'GinGin I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Ternevej 30, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1320,
            1 => 1319,
            2 => 1143,
            3 => 1037,
            4 => 1037,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-27',
            1 => '2018-01-30',
            2 => '2017-12-02',
            3 => '2017-11-04',
            4 => '2017-11-04',
          ),
        ),
        'Smagning.com II (Bækkeskovvej 81, 2700 Brønshøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagning.com II',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2700',
            'cust_city' => 'Brønshøj',
            'cust_address' => 'Bækkeskovvej 81, 2700 Brønshøj',
          ),
          'invoices' => 
          array (
            0 => 1095,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
          ),
        ),
        'Att: Aliva Foods (Nrdr. Frihavnsgade 68, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Att: Aliva Foods',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Nrdr. Frihavnsgade 68, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1023,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
        'Byens Specialiteter & Gaver (Tinggade 12, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Byens Specialiteter & Gaver',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Tinggade 12, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 738,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-13',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 18, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 18, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 808,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-10',
          ),
        ),
        'Restaurant Format (Sankt Annæ Plads 20, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 20, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 783,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-31',
          ),
        ),
        'Restaurant Skade (Rosengaarden 12, 1174 Copenhagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Skade',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1174',
            'cust_city' => 'Copenhagen',
            'cust_address' => 'Rosengaarden 12, 1174 Copenhagen',
          ),
          'invoices' => 
          array (
            0 => 640,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-30',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Ukendt' => 
      array (
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1442,
            1 => 1442,
            2 => 1198,
            3 => 1056,
            4 => 913,
            5 => 703,
            6 => 703,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2017-12-20',
            3 => '2017-11-10',
            4 => '2017-10-12',
            5 => '2017-06-30',
            6 => '2017-06-30',
          ),
        ),
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1345,
            1 => 1318,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-12',
            1 => '2018-01-31',
          ),
        ),
        ' (Thorsvej 6, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Thorsvej 6, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1202,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Drinkx (Fiskerihavnsgade 23, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinkx',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Fiskerihavnsgade 23, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1055,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1020,
            1 => 723,
            2 => 661,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-01',
            1 => '2017-07-11',
            2 => '2017-06-08',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1441,
            1 => 1441,
            2 => 1258,
            3 => 1110,
            4 => 1043,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2017-12-28',
            3 => '2017-11-23',
            4 => '2017-11-06',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1240,
            1 => 863,
            2 => 630,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-09-21',
            2 => '2017-05-23',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1231,
            1 => 1119,
            2 => 954,
            3 => 954,
            4 => 954,
            5 => 878,
            6 => 829,
            7 => 819,
            8 => 819,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
            1 => '2017-11-24',
            2 => '2017-10-02',
            3 => '2017-10-02',
            4 => '2017-10-02',
            5 => '2017-09-25',
            6 => '2017-08-23',
            7 => '2017-08-09',
            8 => '2017-08-09',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1184,
            1 => 1028,
            2 => 918,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-11-01',
            2 => '2017-10-12',
          ),
        ),
        'Vinspecialisten Randers (Dytmærsken 3, 8900 Randers C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Randers',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8900',
            'cust_city' => 'Randers C',
            'cust_address' => 'Dytmærsken 3, 8900 Randers C',
          ),
          'invoices' => 
          array (
            0 => 1136,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinspecialisten Varde (Ndr.Boulevard 90, 6800 Varde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Varde',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6800',
            'cust_city' => 'Varde',
            'cust_address' => 'Ndr.Boulevard 90, 6800 Varde',
          ),
          'invoices' => 
          array (
            0 => 1396,
            1 => 733,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
            1 => '2017-07-10',
          ),
        ),
        'Smagførst (Jægergårdsgade 32, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Jægergårdsgade 32, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1352,
            1 => 1211,
            2 => 977,
            3 => 862,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2017-12-20',
            2 => '2017-10-27',
            3 => '2017-09-23',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1194,
            1 => 1021,
            2 => 833,
            3 => 791,
            4 => 753,
            5 => 746,
            6 => 676,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-11-02',
            2 => '2017-08-23',
            3 => '2017-08-02',
            4 => '2017-07-16',
            5 => '2017-07-16',
            6 => '2017-06-14',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1149,
            1 => 1149,
            2 => 1141,
            3 => 1141,
            4 => 895,
            5 => 741,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-06',
            1 => '2017-12-06',
            2 => '2017-12-01',
            3 => '2017-12-01',
            4 => '2017-10-08',
            5 => '2017-07-05',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1100,
            1 => 784,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-07-29',
          ),
        ),
        'Skjold Burne/Lysholdt (Fiskebrogade 8, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne/Lysholdt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Fiskebrogade 8, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1079,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 939,
            1 => 639,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
            1 => '2017-05-25',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 880,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-27',
          ),
        ),
        'Vinspecialisten Silkeborg (Vestergade 23, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Silkeborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Vestergade 23, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 839,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-31',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 828,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-22',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 766,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-24',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Comwell Århus (Værkmestergade 2, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Comwell Århus',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'Værkmestergade 2, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 1295,
            1 => 835,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-08-31',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1153,
            1 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
            1 => '2017-12-05',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hotel Skjern (Bredgade 58, 6900 Skjern)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Skjern',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6900',
            'cust_city' => 'Skjern',
            'cust_address' => 'Bredgade 58, 6900 Skjern',
          ),
          'invoices' => 
          array (
            0 => 937,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1385,
            1 => 1385,
            2 => 1385,
            3 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
            1 => '2018-02-20',
            2 => '2018-02-20',
            3 => '2018-01-03',
          ),
        ),
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1365,
            1 => 1214,
            2 => 1168,
            3 => 729,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-12-20',
            2 => '2017-12-11',
            3 => '2017-07-11',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
            1 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1222,
            1 => 1222,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-18',
            1 => '2017-12-18',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Flyblock.com ApS (Siriusvænget 11, 5210 Odense NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flyblock.com ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5210',
            'cust_city' => 'Odense NV',
            'cust_address' => 'Siriusvænget 11, 5210 Odense NV',
          ),
          'invoices' => 
          array (
            0 => 1118,
            1 => 1118,
            2 => 649,
            3 => 649,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-26',
            1 => '2017-11-26',
            2 => '2017-05-22',
            3 => '2017-05-22',
          ),
        ),
        'Boxit (Thorslundsvej 3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Boxit',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Thorslundsvej 3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 707,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-03',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 830,
            1 => 794,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-23',
            1 => '2017-08-02',
          ),
        ),
      ),
    ),
  ),
  'Fakturagebyrer' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1452,
            1 => 1270,
            2 => 1267,
            3 => 963,
            4 => 854,
            5 => 845,
            6 => 757,
            7 => 731,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-01-12',
            2 => '2018-01-08',
            3 => '2017-10-26',
            4 => '2017-09-08',
            5 => '2017-09-04',
            6 => '2017-07-20',
            7 => '2017-07-13',
          ),
        ),
        'Meny Hellerup (Strandvejen 64A, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Hellerup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Strandvejen 64A, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1407,
            1 => 768,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2017-07-25',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 1339,
            1 => 1177,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2017-12-13',
          ),
        ),
        'Helsingør Vinhandel (Bramstræde 2C, 3000 Helsingør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Helsingør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3000',
            'cust_city' => 'Helsingør',
            'cust_address' => 'Bramstræde 2C, 3000 Helsingør',
          ),
          'invoices' => 
          array (
            0 => 1174,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'VINSLOTTET I GREVE A/S (Håndværkerbyen 42, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'VINSLOTTET I GREVE A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Håndværkerbyen 42, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1158,
            1 => 927,
            2 => 834,
            3 => 643,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-10-17',
            2 => '2017-08-31',
            3 => '2017-06-06',
          ),
        ),
        '2010 Vin & Velsmag (C. E. Christiansens Vej 1, 4930 Maribo)' => 
        array (
          'info' => 
          array (
            'cust_name' => '2010 Vin & Velsmag',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4930',
            'cust_city' => 'Maribo',
            'cust_address' => 'C. E. Christiansens Vej 1, 4930 Maribo',
          ),
          'invoices' => 
          array (
            0 => 959,
            1 => 892,
            2 => 756,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
            1 => '2017-10-04',
            2 => '2017-07-20',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Hav Kildegårdsvej (Kildegårdsvej 15, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hav Kildegårdsvej',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Kildegårdsvej 15, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1427,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Mikkeller Meatpacking Aps (Flæsketorvet 51-55, 1711 KBH V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mikkeller Meatpacking Aps',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'KBH V',
            'cust_address' => 'Flæsketorvet 51-55, 1711 KBH V',
          ),
          'invoices' => 
          array (
            0 => 1344,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Shoppartner A/S (Marienbergvej 132, 4760 Vordingborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Shoppartner A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4760',
            'cust_city' => 'Vordingborg',
            'cust_address' => 'Marienbergvej 132, 4760 Vordingborg',
          ),
          'invoices' => 
          array (
            0 => 1010,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Byens Specialiteter & Gaver (Tinggade 12, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Byens Specialiteter & Gaver',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Tinggade 12, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 990,
            1 => 738,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
            1 => '2017-07-13',
          ),
        ),
        'Fish & Beer (Amagerbrogade 143, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fish & Beer',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Amagerbrogade 143, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 974,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1376,
            1 => 1301,
            2 => 688,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-19',
            2 => '2017-06-27',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 1820 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1820',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 1820 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 1216,
            1 => 1163,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-07',
          ),
        ),
        'Holte Vinlager, Hørsholm (Usserød kongevej 16, 2970 hørsholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Hørsholm',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2970',
            'cust_city' => 'hørsholm',
            'cust_address' => 'Usserød kongevej 16, 2970 hørsholm',
          ),
          'invoices' => 
          array (
            0 => 1068,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-15',
          ),
        ),
        'Den Sidste Dråbe (Torvehallerne Hal 2, 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2, 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 754,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-20',
          ),
        ),
        'Holte Vinlager, Frederiksberg Allé (Frederiksberg Allé 28, 2000 Frederiksberg C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Frederiksberg Allé',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg C',
            'cust_address' => 'Frederiksberg Allé 28, 2000 Frederiksberg C',
          ),
          'invoices' => 
          array (
            0 => 671,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 18, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 18, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 808,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-10',
          ),
        ),
        'Restaurant Format (Sankt Annæ Plads 20, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 20, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 783,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-31',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1441,
            1 => 1258,
            2 => 1110,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-12-28',
            2 => '2017-11-23',
          ),
        ),
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1408,
            1 => 1217,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
            1 => '2017-12-20',
          ),
        ),
        'Alexanders Blomster (østergade 1, 9560 Hadsund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alexanders Blomster',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9560',
            'cust_city' => 'Hadsund',
            'cust_address' => 'østergade 1, 9560 Hadsund',
          ),
          'invoices' => 
          array (
            0 => 1044,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-08',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1024,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1009,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Chokolenten (Bytorvet 17, 7730 Hanstholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokolenten',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7730',
            'cust_city' => 'Hanstholm',
            'cust_address' => 'Bytorvet 17, 7730 Hanstholm',
          ),
          'invoices' => 
          array (
            0 => 976,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Got2LoveIt (Gunderupvej 96, 9640 Farsø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Got2LoveIt',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9640',
            'cust_city' => 'Farsø',
            'cust_address' => 'Gunderupvej 96, 9640 Farsø',
          ),
          'invoices' => 
          array (
            0 => 632,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hantwerk (Fiskerivej 2D, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hantwerk',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Fiskerivej 2D, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1410,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
          ),
        ),
        'Løve\'s Bog- og Vincafé (nørregade 32, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Løve\'s Bog- og Vincafé',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'nørregade 32, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 730,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-13',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1318,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-31',
          ),
        ),
        'Skotlander ApS (Hjulmagervej 19, 9490 Pandrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skotlander ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9490',
            'cust_city' => 'Pandrup',
            'cust_address' => 'Hjulmagervej 19, 9490 Pandrup',
          ),
          'invoices' => 
          array (
            0 => 1113,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1296,
            1 => 991,
            2 => 763,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2017-10-27',
            2 => '2017-07-24',
          ),
        ),
        'Kolding Vinhandel (Låsbybanke 12, 6000 Kolding)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kolding Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6000',
            'cust_city' => 'Kolding',
            'cust_address' => 'Låsbybanke 12, 6000 Kolding',
          ),
          'invoices' => 
          array (
            0 => 1289,
            1 => 1103,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-17',
            1 => '2017-11-23',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1247,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-21',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1181,
            1 => 897,
            2 => 849,
            3 => 784,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-10-09',
            2 => '2017-09-04',
            3 => '2017-07-29',
          ),
        ),
        'Vinoble Trøjborg (Tordenskjoldsgade 3, 8200 Aarhus N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Trøjborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus N',
            'cust_address' => 'Tordenskjoldsgade 3, 8200 Aarhus N',
          ),
          'invoices' => 
          array (
            0 => 967,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Bisgaard Vinhandel (Jernbanegade 4, 9530 Støvring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bisgaard Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9530',
            'cust_city' => 'Støvring',
            'cust_address' => 'Jernbanegade 4, 9530 Støvring',
          ),
          'invoices' => 
          array (
            0 => 943,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 749,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-18',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 696,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
        'Vin&Vin (Torvet 8, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin&Vin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Torvet 8, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 633,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-24',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Brasserie Nieuwpoort (Marktplein 19, 8620 Nieuwpoort)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brasserie Nieuwpoort',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8620',
            'cust_city' => 'Nieuwpoort',
            'cust_address' => 'Marktplein 19, 8620 Nieuwpoort',
          ),
          'invoices' => 
          array (
            0 => 948,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'MIKKELLER APS / SCANGLOBAL (HELGESHØJ ALLE 12, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MIKKELLER APS / SCANGLOBAL',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'HELGESHØJ ALLE 12, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1298,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 1245,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-19',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Rahbek&Læssøe AoS (Læssøegade 22, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe AoS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Læssøegade 22, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 847,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-04',
          ),
        ),
      ),
    ),
  ),
  'Den Klodsede Bjørn Vodka' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Sprit & Co (Vodroffslund 7. st. th., 1914 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Sprit & Co',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1914',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Vodroffslund 7. st. th., 1914 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1450,
            1 => 1450,
            2 => 1390,
            3 => 1377,
            4 => 1377,
            5 => 1377,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-26',
            1 => '2018-03-26',
            2 => '2018-03-03',
            3 => '2018-02-28',
            4 => '2018-02-28',
            5 => '2018-02-28',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel Torvehallerne (Frederiksborggade 21, 1360 København, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel Torvehallerne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, 1360 København, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1448,
            1 => 1448,
            2 => 1448,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
            1 => '2018-03-28',
            2 => '2018-03-28',
          ),
        ),
        'Den Sidste Dråbe (Linnésgade 14-18, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'Linnésgade 14-18, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 1433,
            1 => 1433,
            2 => 1433,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2018-03-21',
            2 => '2018-03-21',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 10 kld tv, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 10 kld tv, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 1426,
            1 => 1426,
            2 => 818,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
            1 => '2018-03-16',
            2 => '2017-08-11',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18 (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel OBS åben for levering tir-lør kl 12-18',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1387,
            1 => 1387,
            2 => 1355,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-27',
            1 => '2018-02-27',
            2 => '2018-02-15',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1381,
            1 => 1274,
            2 => 1201,
            3 => 1124,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-13',
            2 => '2017-12-20',
            3 => '2017-11-28',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1346,
            1 => 1292,
            2 => 1253,
            3 => 1253,
            4 => 1239,
            5 => 1219,
            6 => 1219,
            7 => 1192,
            8 => 885,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-30',
            1 => '2018-01-19',
            2 => '2017-12-27',
            3 => '2017-12-27',
            4 => '2017-12-20',
            5 => '2017-12-19',
            6 => '2017-12-19',
            7 => '2017-12-14',
            8 => '2017-10-01',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Stade 5D, Frederiksborggade 21, 1360 København K, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 1325,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-31',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, 1360 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København',
            'cust_address' => 'Frederiksborggade 21, 1360 København',
          ),
          'invoices' => 
          array (
            0 => 1230,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1114,
            1 => 1052,
            2 => 1052,
            3 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-11-09',
            2 => '2017-11-09',
            3 => '2017-09-23',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 903,
            2 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-09-30',
            2 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Torvehallerne Hal 2 Stade 5 D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2 Stade 5 D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 884,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-29',
          ),
        ),
        'skjold burne (Østergade 1, 1100 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'skjold burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1100',
            'cust_city' => 'København',
            'cust_address' => 'Østergade 1, 1100 København',
          ),
          'invoices' => 
          array (
            0 => 859,
            1 => 859,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10 kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10 kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 853,
            1 => 836,
            2 => 836,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
            1 => '2017-08-24',
            2 => '2017-08-24',
          ),
        ),
        'Holte Vinlager, Gentofte (Smakkegårdsvej 137, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Gentofte',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Smakkegårdsvej 137, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 822,
            1 => 667,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-08-20',
            1 => '2017-06-08',
          ),
        ),
        'Den Sidste Dråbe (jægersborggade 6 kld th, 2200 København n)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København n',
            'cust_address' => 'jægersborggade 6 kld th, 2200 København n',
          ),
          'invoices' => 
          array (
            0 => 778,
            1 => 778,
            2 => 706,
            3 => 635,
            4 => 635,
            5 => 635,
            6 => 635,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
            1 => '2017-07-26',
            2 => '2017-06-30',
            3 => '2017-05-24',
            4 => '2017-05-24',
            5 => '2017-05-24',
            6 => '2017-05-24',
          ),
        ),
        'Den Sidste Dråbe (Torvehallerne Hal 2, 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2, 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 777,
            1 => 754,
            2 => 754,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
            1 => '2017-07-20',
            2 => '2017-07-20',
          ),
        ),
        'Den Sidste Dråbe (Hal 2, Frederiksborggade 21,, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Hal 2, Frederiksborggade 21,, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 705,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-01',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 678,
            1 => 659,
            2 => 634,
            3 => 634,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-13',
            1 => '2017-06-06',
            2 => '2017-05-24',
            3 => '2017-05-24',
          ),
        ),
        'Den Sidste Dråbe (Frederiksborggade 21, Hal 2 5D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21, Hal 2 5D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 644,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Taastrup Ny Vinhandel I/S (Taastrup Hovedgade 74, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Taastrup Ny Vinhandel I/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'Taastrup Hovedgade 74, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1440,
            1 => 1249,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
            1 => '2017-12-26',
          ),
        ),
        'Den Franske Vinhandel (Skomagergade 3, 4000 Roskilde)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Franske Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4000',
            'cust_city' => 'Roskilde',
            'cust_address' => 'Skomagergade 3, 4000 Roskilde',
          ),
          'invoices' => 
          array (
            0 => 1419,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-08',
          ),
        ),
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1416,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-14',
          ),
        ),
        'Brdr D Østerbro (Østerbrogade 152, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Østerbro',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerbrogade 152, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1402,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-07',
          ),
        ),
        'JP VIN APS (Hovedgaden 10, 4652 Hårlev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'JP VIN APS',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4652',
            'cust_city' => 'Hårlev',
            'cust_address' => 'Hovedgaden 10, 4652 Hårlev',
          ),
          'invoices' => 
          array (
            0 => 1397,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-01',
          ),
        ),
        'inco København Cash & Carry A/S (Ingerslevsgade 42, 1711 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco København Cash & Carry A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V',
          ),
          'invoices' => 
          array (
            0 => 1356,
            1 => 1356,
            2 => 1145,
            3 => 949,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
            1 => '2018-02-22',
            2 => '2017-12-05',
            3 => '2017-10-16',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1270,
            1 => 731,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-12',
            1 => '2017-07-13',
          ),
        ),
        'Inco Glostrup (Ejby Industrivej 11, 2600 Glostrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco Glostrup',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2600',
            'cust_city' => 'Glostrup',
            'cust_address' => 'Ejby Industrivej 11, 2600 Glostrup',
          ),
          'invoices' => 
          array (
            0 => 1262,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-04',
          ),
        ),
        'Skælskør Vinhandel (Algade 6-8, 4230 Skælskør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skælskør Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4230',
            'cust_city' => 'Skælskør',
            'cust_address' => 'Algade 6-8, 4230 Skælskør',
          ),
          'invoices' => 
          array (
            0 => 1176,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
          ),
        ),
        'Smagførst (Nørre Farimagsgade 61, 1364 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smagførst',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1364',
            'cust_city' => 'København K',
            'cust_address' => 'Nørre Farimagsgade 61, 1364 København K',
          ),
          'invoices' => 
          array (
            0 => 1166,
            1 => 1166,
            2 => 860,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-12-11',
            2 => '2017-09-19',
          ),
        ),
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1138,
            1 => 1063,
            2 => 942,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-22',
            1 => '2017-11-14',
            2 => '2017-10-19',
          ),
        ),
        'Zaizon (Falkoner Alle 33, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Zaizon',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Alle 33, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 1060,
            1 => 1047,
            2 => 848,
            3 => 848,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-13',
            1 => '2017-11-08',
            2 => '2017-08-31',
            3 => '2017-08-31',
          ),
        ),
        'Bagsværd Vinhandel (Bagsværd Hovedgade 125, 2880 Bagsværd)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bagsværd Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2880',
            'cust_city' => 'Bagsværd',
            'cust_address' => 'Bagsværd Hovedgade 125, 2880 Bagsværd',
          ),
          'invoices' => 
          array (
            0 => 773,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
        'Skjold Burne (Adelgade 70, 4720 Præstø)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4720',
            'cust_city' => 'Præstø',
            'cust_address' => 'Adelgade 70, 4720 Præstø',
          ),
          'invoices' => 
          array (
            0 => 719,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-05',
          ),
        ),
        'Inco (Ingerslevsgade 42, 1711 København V.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Inco',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'København V.',
            'cust_address' => 'Ingerslevsgade 42, 1711 København V.',
          ),
          'invoices' => 
          array (
            0 => 656,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-07',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmand (Sankt Annæ Plads 22, 1250 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmand',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'København K',
            'cust_address' => 'Sankt Annæ Plads 22, 1250 København K',
          ),
          'invoices' => 
          array (
            0 => 1429,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Hav Kildegårdsvej (Kildegårdsvej 15, 2900 Hellerup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hav Kildegårdsvej',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2900',
            'cust_city' => 'Hellerup',
            'cust_address' => 'Kildegårdsvej 15, 2900 Hellerup',
          ),
          'invoices' => 
          array (
            0 => 1427,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
        'Mikkeller Meatpacking Aps (Flæsketorvet 51-55, 1711 KBH V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mikkeller Meatpacking Aps',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1711',
            'cust_city' => 'KBH V',
            'cust_address' => 'Flæsketorvet 51-55, 1711 KBH V',
          ),
          'invoices' => 
          array (
            0 => 1344,
            1 => 1344,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
          ),
        ),
        'Flavour A/S (Skindergade 19. st., 1159 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Flavour A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K',
            'cust_address' => 'Skindergade 19. st., 1159 København K',
          ),
          'invoices' => 
          array (
            0 => 1156,
            1 => 748,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-01',
            1 => '2017-07-14',
          ),
        ),
        'Krusemynte (Nordre Strandvej 341A, 3100 Hornbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Krusemynte',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '3100',
            'cust_city' => 'Hornbæk',
            'cust_address' => 'Nordre Strandvej 341A, 3100 Hornbæk',
          ),
          'invoices' => 
          array (
            0 => 721,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-06',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 18, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 18, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 1372,
            1 => 808,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2017-08-10',
          ),
        ),
        'Restaurant Format (Sankt Annæ Plads 20, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 20, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 783,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-31',
          ),
        ),
        'Hotel Saxkjøbing (Torvet 9, 4990 Sakskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Saxkjøbing',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4990',
            'cust_city' => 'Sakskøbing',
            'cust_address' => 'Torvet 9, 4990 Sakskøbing',
          ),
          'invoices' => 
          array (
            0 => 758,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Hotel Cecil (Niels Hemmingsens Gade 11, 1153 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Cecil',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1153',
            'cust_city' => 'København K',
            'cust_address' => 'Niels Hemmingsens Gade 11, 1153 København K',
          ),
          'invoices' => 
          array (
            0 => 1369,
            1 => 1361,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-02-22',
          ),
        ),
        'Nobis Hotel Copenhagen (Niels Brocks Gade 1, 1574 København V)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Nobis Hotel Copenhagen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1574',
            'cust_city' => 'København V',
            'cust_address' => 'Niels Brocks Gade 1, 1574 København V',
          ),
          'invoices' => 
          array (
            0 => 1363,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-22',
          ),
        ),
        'Vang & Barfred (Wilders Plads 11B, 1403 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vang & Barfred',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1403',
            'cust_city' => 'København K',
            'cust_address' => 'Wilders Plads 11B, 1403 København K',
          ),
          'invoices' => 
          array (
            0 => 1297,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 1099,
            1 => 882,
            2 => 841,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
            1 => '2017-09-27',
            2 => '2017-08-31',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Alkoholfributik (Bakkedraget 1, 4793 Bogø By)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Alkoholfributik',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4793',
            'cust_city' => 'Bogø By',
            'cust_address' => 'Bakkedraget 1, 4793 Bogø By',
          ),
          'invoices' => 
          array (
            0 => 1447,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-28',
          ),
        ),
        'PureVodka (Nordrupvej 28, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'PureVodka',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nordrupvej 28, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1385,
            1 => 1268,
            2 => 1268,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
            1 => '2018-01-03',
            2 => '2018-01-03',
          ),
        ),
        'MIKKELLER APS / SCANGLOBAL (HELGESHØJ ALLE 12, 2630 Taastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MIKKELLER APS / SCANGLOBAL',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2630',
            'cust_city' => 'Taastrup',
            'cust_address' => 'HELGESHØJ ALLE 12, 2630 Taastrup',
          ),
          'invoices' => 
          array (
            0 => 1298,
            1 => 1298,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
            1 => '2018-01-19',
          ),
        ),
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
            1 => 858,
            2 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
            2 => '2017-09-19',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Rombo (Falkoner Allé 46, 2000 Frederiksberg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rombo',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '2000',
            'cust_city' => 'Frederiksberg',
            'cust_address' => 'Falkoner Allé 46, 2000 Frederiksberg',
          ),
          'invoices' => 
          array (
            0 => 941,
            1 => 941,
            2 => 870,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-19',
            1 => '2017-10-19',
            2 => '2017-09-12',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1441,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1184,
            1 => 1028,
            2 => 918,
            3 => 918,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-11-01',
            2 => '2017-10-12',
            3 => '2017-10-12',
          ),
        ),
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1170,
            1 => 878,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-09-25',
          ),
        ),
        'Livingzone (Enggaardsgade 27, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Livingzone',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Enggaardsgade 27, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 630,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-23',
          ),
        ),
      ),
      'Barer: Cocktailbar' => 
      array (
        'Pustervig ApS (Rosensgade 21, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Pustervig ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Rosensgade 21, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1435,
            1 => 1155,
            2 => 867,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-20',
            1 => '2017-12-04',
            2 => '2017-09-15',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Hantwerk (Fiskerivej 2D, 8000 Aarhus)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hantwerk',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus',
            'cust_address' => 'Fiskerivej 2D, 8000 Aarhus',
          ),
          'invoices' => 
          array (
            0 => 1410,
            1 => 1410,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-09',
            1 => '2018-03-09',
          ),
        ),
        'Løve\'s Bog- og Vincafé (nørregade 32, 8000 Århus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Løve\'s Bog- og Vincafé',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Århus C',
            'cust_address' => 'nørregade 32, 8000 Århus C',
          ),
          'invoices' => 
          array (
            0 => 730,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-13',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 1384,
            1 => 1294,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
            1 => '2018-01-19',
          ),
        ),
        'inco Aarhus A/S (Blomstervej 5, 8381 Tilst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'inco Aarhus A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8381',
            'cust_city' => 'Tilst',
            'cust_address' => 'Blomstervej 5, 8381 Tilst',
          ),
          'invoices' => 
          array (
            0 => 1315,
            1 => 1167,
            2 => 766,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-23',
            1 => '2017-12-11',
            2 => '2017-07-24',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1129,
            1 => 753,
            2 => 753,
            3 => 746,
            4 => 746,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-29',
            1 => '2017-07-16',
            2 => '2017-07-16',
            3 => '2017-07-16',
            4 => '2017-07-16',
          ),
        ),
        'Mundskænken ApS Broen Shopping (Mundskænken Broen Shopping, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mundskænken ApS Broen Shopping',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Mundskænken Broen Shopping, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1013,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-17',
          ),
        ),
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 933,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-24',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 897,
            1 => 849,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-09-04',
          ),
        ),
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 880,
            1 => 880,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-27',
            1 => '2017-09-27',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 700,
            1 => 700,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
            1 => '2017-06-30',
          ),
        ),
      ),
      'Restaurant: Michelinrestaurant' => 
      array (
        'Textur ApS (Vesterbro 65 st th, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Textur ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterbro 65 st th, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1343,
            1 => 1343,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
            1 => '2018-02-11',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1199,
            1 => 1020,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-11-01',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1056,
            1 => 913,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-10',
            1 => '2017-10-12',
          ),
        ),
        'Drinkx (Fiskerihavnsgade 23, 9900 Frederikshavn)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Drinkx',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9900',
            'cust_city' => 'Frederikshavn',
            'cust_address' => 'Fiskerihavnsgade 23, 9900 Frederikshavn',
          ),
          'invoices' => 
          array (
            0 => 1055,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-09',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1094,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Rahbek&Læssøe Aps (Rahbeksvej 9, 5230 Odense M)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Rahbek&Læssøe Aps',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5230',
            'cust_city' => 'Odense M',
            'cust_address' => 'Rahbeksvej 9, 5230 Odense M',
          ),
          'invoices' => 
          array (
            0 => 945,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-19',
          ),
        ),
      ),
    ),
  ),
  'Enghaven' => 
  array (
    'Jylland' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Butik Harald (Tordenskjoldsgade 63, 8200 Aarhus Nord)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Harald',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus Nord',
            'cust_address' => 'Tordenskjoldsgade 63, 8200 Aarhus Nord',
          ),
          'invoices' => 
          array (
            0 => 1441,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-21',
          ),
        ),
        'Hr Nielsens Specialiteter (jernbanegade 12 st. tv, 9460 Brovst)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Nielsens Specialiteter',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9460',
            'cust_city' => 'Brovst',
            'cust_address' => 'jernbanegade 12 st. tv, 9460 Brovst',
          ),
          'invoices' => 
          array (
            0 => 1195,
            1 => 1195,
            2 => 799,
            3 => 799,
            4 => 799,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-13',
            1 => '2017-12-13',
            2 => '2017-08-02',
            3 => '2017-08-02',
            4 => '2017-08-02',
          ),
        ),
        'Smageklubben ApS (Gjellerupvej 89A, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Smageklubben ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Gjellerupvej 89A, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 1184,
            1 => 1184,
            2 => 1117,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-11',
            1 => '2017-12-11',
            2 => '2017-11-27',
          ),
        ),
        'Hjortdal Købmand (Hjortdalvej 142, 9690 Fjerritslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hjortdal Købmand',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9690',
            'cust_city' => 'Fjerritslev',
            'cust_address' => 'Hjortdalvej 142, 9690 Fjerritslev',
          ),
          'invoices' => 
          array (
            0 => 875,
            1 => 875,
            2 => 875,
            3 => 817,
            4 => 817,
            5 => 817,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-25',
            1 => '2017-09-25',
            2 => '2017-09-25',
            3 => '2017-08-20',
            4 => '2017-08-20',
            5 => '2017-08-20',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1198,
            1 => 1198,
            2 => 1198,
            3 => 913,
            4 => 913,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-12-20',
            3 => '2017-10-12',
            4 => '2017-10-12',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Vinshoppen (Hadsundvej 12, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinshoppen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Hadsundvej 12, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 938,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-23',
          ),
        ),
        'Skjold Burne Vinhandel (Boulevarden 5, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vinhandel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Boulevarden 5, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 784,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-29',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Holte Vinlager, Virum (Virum _Torv 9, 2830 Virum)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager, Virum',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2830',
            'cust_city' => 'Virum',
            'cust_address' => 'Virum _Torv 9, 2830 Virum',
          ),
          'invoices' => 
          array (
            0 => 1301,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-19',
          ),
        ),
        'Dansk e-Logistik (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 1266,
            1 => 1266,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-31',
            1 => '2017-12-31',
          ),
        ),
        'Toke Krebs Eskildsen (Egilsgade 5, 2 sal TV, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toke Krebs Eskildsen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Egilsgade 5, 2 sal TV, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1203,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 1171,
            1 => 1114,
            2 => 1114,
            3 => 1114,
            4 => 1052,
            5 => 1052,
            6 => 1052,
            7 => 865,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-12',
            1 => '2017-11-23',
            2 => '2017-11-23',
            3 => '2017-11-23',
            4 => '2017-11-09',
            5 => '2017-11-09',
            6 => '2017-11-09',
            7 => '2017-09-23',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 903,
            1 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-30',
            1 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Torvehallerne Hal 2 Stade 5 D, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Torvehallerne Hal 2 Stade 5 D, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 884,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-29',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Skjold Burne Vanløse (jernbane alle 45, 2720 vanløse)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Vanløse',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2720',
            'cust_city' => 'vanløse',
            'cust_address' => 'jernbane alle 45, 2720 vanløse',
          ),
          'invoices' => 
          array (
            0 => 1209,
            1 => 1209,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
          ),
        ),
      ),
      'Restaurant: Restaurant' => 
      array (
        'Restaurant Format (Sankt Annæ Plads 20, 1250 KBH K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Format',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1250',
            'cust_city' => 'KBH K',
            'cust_address' => 'Sankt Annæ Plads 20, 1250 KBH K',
          ),
          'invoices' => 
          array (
            0 => 783,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-31',
          ),
        ),
        'Hotel Saxkjøbing (Torvet 9, 4990 Sakskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hotel Saxkjøbing',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4990',
            'cust_city' => 'Sakskøbing',
            'cust_address' => 'Torvet 9, 4990 Sakskøbing',
          ),
          'invoices' => 
          array (
            0 => 758,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-08',
          ),
        ),
        'Restaurant Skade (Rosengaarden 12, 1174 Copenhagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Restaurant Skade',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1174',
            'cust_city' => 'Copenhagen',
            'cust_address' => 'Rosengaarden 12, 1174 Copenhagen',
          ),
          'invoices' => 
          array (
            0 => 640,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-30',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'COOLSHOP (Bøgildsmindvej 3, 9300 NØRRESUNDBY)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'COOLSHOP',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '9300',
            'cust_city' => 'NØRRESUNDBY',
            'cust_address' => 'Bøgildsmindvej 3, 9300 NØRRESUNDBY',
          ),
          'invoices' => 
          array (
            0 => 858,
            1 => 858,
            2 => 858,
            3 => 858,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-19',
            1 => '2017-09-19',
            2 => '2017-09-19',
            3 => '2017-09-19',
          ),
        ),
      ),
    ),
  ),
  'Lokale Rødder Chips' => 
  array (
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Vin-Gaven (Vodskovvej 44, 9310 Vodskov)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin-Gaven',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9310',
            'cust_city' => 'Vodskov',
            'cust_address' => 'Vodskovvej 44, 9310 Vodskov',
          ),
          'invoices' => 
          array (
            0 => 1439,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-19',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1254,
            1 => 1254,
            2 => 1254,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-27',
            1 => '2017-12-27',
            2 => '2017-12-27',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1199,
            1 => 1199,
            2 => 1199,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-20',
            1 => '2017-12-20',
            2 => '2017-12-20',
          ),
        ),
      ),
      'Restaurant: Cafe' => 
      array (
        'Cafe Vesterå (Vesterå 4, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Cafe Vesterå',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vesterå 4, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1153,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Hr Skov ApS (Blåvandvej 37, 6857 Blåvand)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Hr Skov ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6857',
            'cust_city' => 'Blåvand',
            'cust_address' => 'Blåvandvej 37, 6857 Blåvand',
          ),
          'invoices' => 
          array (
            0 => 1144,
            1 => 1144,
            2 => 1144,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-05',
            1 => '2017-12-05',
            2 => '2017-12-05',
          ),
        ),
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1139,
            1 => 1139,
            2 => 1139,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-04',
            1 => '2017-12-04',
            2 => '2017-12-04',
          ),
        ),
        'Vinspecialisten Randers (Dytmærsken 3, 8900 Randers C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Randers',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8900',
            'cust_city' => 'Randers C',
            'cust_address' => 'Dytmærsken 3, 8900 Randers C',
          ),
          'invoices' => 
          array (
            0 => 1136,
            1 => 1136,
            2 => 1136,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
            1 => '2017-11-28',
            2 => '2017-11-28',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Vin de Table (Frederikssundsvej 33, 2400 København NV)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin de Table',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2400',
            'cust_city' => 'København NV',
            'cust_address' => 'Frederikssundsvej 33, 2400 København NV',
          ),
          'invoices' => 
          array (
            0 => 1243,
            1 => 1243,
            2 => 1243,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-07',
            1 => '2017-12-07',
            2 => '2017-12-07',
          ),
        ),
      ),
    ),
  ),
  'Æblerov' => 
  array (
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1430,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-03-16',
          ),
        ),
      ),
    ),
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Holte Vinlager (Kongevejen 333, 2840 Holte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Holte Vinlager',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2840',
            'cust_city' => 'Holte',
            'cust_address' => 'Kongevejen 333, 2840 Holte',
          ),
          'invoices' => 
          array (
            0 => 1102,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-23',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10 kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10 kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 853,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-09-05',
          ),
        ),
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 702,
            1 => 702,
            2 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
            1 => '2017-06-30',
            2 => '2017-06-30',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 659,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-06',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        ' (Dyrehovedgårds Alle 18, 4220 Korsør)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4220',
            'cust_city' => 'Korsør',
            'cust_address' => 'Dyrehovedgårds Alle 18, 4220 Korsør',
          ),
          'invoices' => 
          array (
            0 => 701,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 685,
            1 => 682,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-23',
            1 => '2017-06-22',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Den Gamle Købmandsgaard (Torvet 5, 5970 Ærøskøbing)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Gamle Købmandsgaard',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5970',
            'cust_city' => 'Ærøskøbing',
            'cust_address' => 'Torvet 5, 5970 Ærøskøbing',
          ),
          'invoices' => 
          array (
            0 => 952,
            1 => 689,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-09',
            1 => '2017-06-27',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Whisky.dk ApS (Sjølund Gade 12, 6093 Sjølund)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Whisky.dk ApS',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '6093',
            'cust_city' => 'Sjølund',
            'cust_address' => 'Sjølund Gade 12, 6093 Sjølund',
          ),
          'invoices' => 
          array (
            0 => 690,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-26',
          ),
        ),
      ),
    ),
  ),
  'Salg af varer/ydelser m/moms' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'My Coffee Shop (Hundige Strandvej 212c, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'My Coffee Shop',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Hundige Strandvej 212c, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1386,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-20',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 1380,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-28',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Herbie C/O Tapperiet (Nimbusvej 18, 2670 Greve)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Herbie C/O Tapperiet',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2670',
            'cust_city' => 'Greve',
            'cust_address' => 'Nimbusvej 18, 2670 Greve',
          ),
          'invoices' => 
          array (
            0 => 1334,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-02-11',
          ),
        ),
        'Mondo Kaos (Birkegade 1, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Mondo Kaos',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Birkegade 1, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1029,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
      ),
      'Barer: Bar' => 
      array (
        'Bandholm Hotel (Havnegade 37, 4941 Bandholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Bandholm Hotel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4941',
            'cust_city' => 'Bandholm',
            'cust_address' => 'Havnegade 37, 4941 Bandholm',
          ),
          'invoices' => 
          array (
            0 => 693,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-28',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Specialbutik' => 
      array (
        'Aakjær\'s (Strandbygade 46, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Aakjær\'s',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Strandbygade 46, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1133,
            1 => 1133,
            2 => 1133,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
            1 => '2017-11-28',
            2 => '2017-11-28',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Vinmonopolet ApS (Mads Clausensvej 5, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinmonopolet ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Mads Clausensvej 5, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 776,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-07-26',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1094,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-17',
          ),
        ),
      ),
    ),
  ),
  '[Ukendt producent]' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 10, kld tv, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 10, kld tv, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 1346,
          ),
          'invoices_dates' => 
          array (
            0 => '2018-01-30',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Brdr D Nygade (Nygade 33, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brdr D Nygade',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Nygade 33, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 639,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-05-25',
          ),
        ),
      ),
    ),
  ),
  'Nordic Spirits egne produkter (julekalender)' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        ' (Østerled 19, 2100 København Ø)' => 
        array (
          'info' => 
          array (
            'cust_name' => '',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2100',
            'cust_city' => 'København Ø',
            'cust_address' => 'Østerled 19, 2100 København Ø',
          ),
          'invoices' => 
          array (
            0 => 1142,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-12-04',
          ),
        ),
        'Gaudio Macao (Gyldenholm Allé 23, 2820 Gentofte)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gaudio Macao',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2820',
            'cust_city' => 'Gentofte',
            'cust_address' => 'Gyldenholm Allé 23, 2820 Gentofte',
          ),
          'invoices' => 
          array (
            0 => 1124,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-28',
          ),
        ),
        'skjold burne (Østergade 1, 1100 København)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'skjold burne',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1100',
            'cust_city' => 'København',
            'cust_address' => 'Østergade 1, 1100 København',
          ),
          'invoices' => 
          array (
            0 => 1011,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Jægersborggade 6, th, 2200 København N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2200',
            'cust_city' => 'København N',
            'cust_address' => 'Jægersborggade 6, th, 2200 København N',
          ),
          'invoices' => 
          array (
            0 => 965,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Den Sidste Dråbe Spiritushandel (Frederiksborggade 21 1360 KØBENHAVN K, 1360 København K)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Den Sidste Dråbe Spiritushandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1360',
            'cust_city' => 'København K',
            'cust_address' => 'Frederiksborggade 21 1360 KØBENHAVN K, 1360 København K',
          ),
          'invoices' => 
          array (
            0 => 964,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'IFKL (Magle Alle 3, 2770 Kastrup)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'IFKL',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2770',
            'cust_city' => 'Kastrup',
            'cust_address' => 'Magle Alle 3, 2770 Kastrup',
          ),
          'invoices' => 
          array (
            0 => 962,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
      'Forhandler: Vinbutik' => 
      array (
        'XOCOVINO (Skindergade 19, kl., 1159 København K.)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'XOCOVINO',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '1159',
            'cust_city' => 'København K.',
            'cust_address' => 'Skindergade 19, kl., 1159 København K.',
          ),
          'invoices' => 
          array (
            0 => 1087,
            1 => 1018,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-20',
            1 => '2017-11-01',
          ),
        ),
        'Skjold Burne Ringsted (Nørregade 45, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Skjold Burne Ringsted',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Nørregade 45, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 1008,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Vinoble Holbæk (Ahlgade 48D, 4300 Holbæk)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Holbæk',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4300',
            'cust_city' => 'Holbæk',
            'cust_address' => 'Ahlgade 48D, 4300 Holbæk',
          ),
          'invoices' => 
          array (
            0 => 975,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Brønshøj Vinhandel (Frederikssundsvej 152B, 2700 Brønshøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brønshøj Vinhandel',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2700',
            'cust_city' => 'Brønshøj',
            'cust_address' => 'Frederikssundsvej 152B, 2700 Brønshøj',
          ),
          'invoices' => 
          array (
            0 => 966,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Meny (Vermlandsgade 51, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Vermlandsgade 51, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 963,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        '2010 Vin & Velsmag (C. E. Christiansens Vej 1, 4930 Maribo)' => 
        array (
          'info' => 
          array (
            'cust_name' => '2010 Vin & Velsmag',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4930',
            'cust_city' => 'Maribo',
            'cust_address' => 'C. E. Christiansens Vej 1, 4930 Maribo',
          ),
          'invoices' => 
          array (
            0 => 959,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Toft Bryggen (Islands Brygge 25, 2300 Kbh S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Toft Bryggen',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'Kbh S',
            'cust_address' => 'Islands Brygge 25, 2300 Kbh S',
          ),
          'invoices' => 
          array (
            0 => 957,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Shoppartner A/S (Marienbergvej 132, 4760 Vordingborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Shoppartner A/S',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4760',
            'cust_city' => 'Vordingborg',
            'cust_address' => 'Marienbergvej 132, 4760 Vordingborg',
          ),
          'invoices' => 
          array (
            0 => 1010,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Byens Specialiteter & Gaver (Tinggade 12, 4100 Ringsted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Byens Specialiteter & Gaver',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4100',
            'cust_city' => 'Ringsted',
            'cust_address' => 'Tinggade 12, 4100 Ringsted',
          ),
          'invoices' => 
          array (
            0 => 990,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
          ),
        ),
        'Fish & Beer (Amagerbrogade 143, 2300 København S)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fish & Beer',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '2300',
            'cust_city' => 'København S',
            'cust_address' => 'Amagerbrogade 143, 2300 København S',
          ),
          'invoices' => 
          array (
            0 => 974,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
    ),
    'Jylland' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'MUMS (Gugvej 184, 9210 Aalborg SØ)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'MUMS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9210',
            'cust_city' => 'Aalborg SØ',
            'cust_address' => 'Gugvej 184, 9210 Aalborg SØ',
          ),
          'invoices' => 
          array (
            0 => 1030,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-03',
          ),
        ),
        'SLIK FOR VOKSNE (Kongensgade 100, 6700 Esbjerg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SLIK FOR VOKSNE',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6700',
            'cust_city' => 'Esbjerg',
            'cust_address' => 'Kongensgade 100, 6700 Esbjerg',
          ),
          'invoices' => 
          array (
            0 => 1022,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-02',
          ),
        ),
        'Vinspecialisten Skagen (Sct. Laurentiivej 43, 9990 Skagen)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Skagen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9990',
            'cust_city' => 'Skagen',
            'cust_address' => 'Sct. Laurentiivej 43, 9990 Skagen',
          ),
          'invoices' => 
          array (
            0 => 1021,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-02',
          ),
        ),
        'Vinspecialisten Haderslev ApS (Storegade 30, 6100 Haderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Haderslev ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6100',
            'cust_city' => 'Haderslev',
            'cust_address' => 'Storegade 30, 6100 Haderslev',
          ),
          'invoices' => 
          array (
            0 => 994,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
          ),
        ),
        'SPAR Klitmøller (Ørhagevej 71, 7700 Thisted)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'SPAR Klitmøller',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7700',
            'cust_city' => 'Thisted',
            'cust_address' => 'Ørhagevej 71, 7700 Thisted',
          ),
          'invoices' => 
          array (
            0 => 992,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
          ),
        ),
        'Meny Aalborg (Otto Mønstedsvej 1, 9200 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Meny Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9200',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Otto Mønstedsvej 1, 9200 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 991,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
          ),
        ),
        'Vinspecialisten Lemvig (Vestergade 11, 7620 Lemvig)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Lemvig',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7620',
            'cust_city' => 'Lemvig',
            'cust_address' => 'Vestergade 11, 7620 Lemvig',
          ),
          'invoices' => 
          array (
            0 => 987,
            1 => 985,
            2 => 984,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
            1 => '2017-07-26',
            2 => '2017-07-26',
          ),
        ),
        'Vin & Vin Aalborg (Vestre Alle 2, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vin & Vin Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vestre Alle 2, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 979,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
          ),
        ),
        'Huset Appel (Solbakken 23, 6823 Ansager)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Huset Appel',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '6823',
            'cust_city' => 'Ansager',
            'cust_address' => 'Solbakken 23, 6823 Ansager',
          ),
          'invoices' => 
          array (
            0 => 973,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-01',
          ),
        ),
        'Vinkyperen (Søren Frichs Vej 40B, 8230 Åbyhøj)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinkyperen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8230',
            'cust_city' => 'Åbyhøj',
            'cust_address' => 'Søren Frichs Vej 40B, 8230 Åbyhøj',
          ),
          'invoices' => 
          array (
            0 => 970,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-01',
          ),
        ),
        'Vinoble Trøjborg (Tordenskjoldsgade 3, 8200 Aarhus N)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinoble Trøjborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8200',
            'cust_city' => 'Aarhus N',
            'cust_address' => 'Tordenskjoldsgade 3, 8200 Aarhus N',
          ),
          'invoices' => 
          array (
            0 => 967,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Supervin (Skagensvej 201, 9800 Hjørring)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Supervin',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9800',
            'cust_city' => 'Hjørring',
            'cust_address' => 'Skagensvej 201, 9800 Hjørring',
          ),
          'invoices' => 
          array (
            0 => 961,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
          ),
        ),
        'Vinspecialisten Aalborg A/S (Vingårdsgade 13-15, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vinspecialisten Aalborg A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Vingårdsgade 13-15, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 960,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Vild Med Vin ApS (Nybovej 34, Port 5, 7500 Holstebro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Vild Med Vin ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7500',
            'cust_city' => 'Holstebro',
            'cust_address' => 'Nybovej 34, Port 5, 7500 Holstebro',
          ),
          'invoices' => 
          array (
            0 => 956,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-25',
          ),
        ),
      ),
      'Ukendt' => 
      array (
        'Salling Aalborg (Nytorv 8, 9000 Aalborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Salling Aalborg',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9000',
            'cust_city' => 'Aalborg',
            'cust_address' => 'Nytorv 8, 9000 Aalborg',
          ),
          'invoices' => 
          array (
            0 => 1020,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-01',
          ),
        ),
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 958,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
      'Forhandler: Specialbutik' => 
      array (
        'Butik Jes Pors (Vestergade 7, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Butik Jes Pors',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Vestergade 7, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 1015,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Chokoladehimlen ApS (Ballevej 21, 8300 Odder)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokoladehimlen ApS',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8300',
            'cust_city' => 'Odder',
            'cust_address' => 'Ballevej 21, 8300 Odder',
          ),
          'invoices' => 
          array (
            0 => 1009,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
        'Blach&Co (Algade 7, 9700 Brønderslev)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Blach&Co',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9700',
            'cust_city' => 'Brønderslev',
            'cust_address' => 'Algade 7, 9700 Brønderslev',
          ),
          'invoices' => 
          array (
            0 => 993,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-27',
          ),
        ),
        'Chokolenten (Bytorvet 17, 7730 Hanstholm)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Chokolenten',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '7730',
            'cust_city' => 'Hanstholm',
            'cust_address' => 'Bytorvet 17, 7730 Hanstholm',
          ),
          'invoices' => 
          array (
            0 => 976,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
        'Gjøl Liv (Fløjlsanden 5, 9440 Aabybro)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Gjøl Liv',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9440',
            'cust_city' => 'Aabybro',
            'cust_address' => 'Fløjlsanden 5, 9440 Aabybro',
          ),
          'invoices' => 
          array (
            0 => 972,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-01',
          ),
        ),
        'Fru Jakobsen (Østerbrogade 20, 9670 Løgstør)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Fru Jakobsen',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '9670',
            'cust_city' => 'Løgstør',
            'cust_address' => 'Østerbrogade 20, 9670 Løgstør',
          ),
          'invoices' => 
          array (
            0 => 971,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-01',
          ),
        ),
      ),
    ),
    'Onlinebutik' => 
    array (
      'Ukendt' => 
      array (
        'Kun Det Bedste (Priorsvej 21, 8600 Silkeborg)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Kun Det Bedste',
            'cust_landsdel' => 'Onlinebutik',
            'cust_postcode' => '8600',
            'cust_city' => 'Silkeborg',
            'cust_address' => 'Priorsvej 21, 8600 Silkeborg',
          ),
          'invoices' => 
          array (
            0 => 1017,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-11-01',
          ),
        ),
      ),
    ),
    'Fyn m.fl.' => 
    array (
      'Forhandler: Vinbutik' => 
      array (
        'Easy Wine Odense ApS (Skibhusvej 52C3, 5000 Odense C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Easy Wine Odense ApS',
            'cust_landsdel' => 'Fyn m.fl.',
            'cust_postcode' => '5000',
            'cust_city' => 'Odense C',
            'cust_address' => 'Skibhusvej 52C3, 5000 Odense C',
          ),
          'invoices' => 
          array (
            0 => 1012,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-31',
          ),
        ),
      ),
    ),
  ),
  'Salg af fragt - momsfrie' => 
  array (
    'Jylland' => 
    array (
      'Restaurant: Restaurant' => 
      array (
        'Brasserie Nieuwpoort (Marktplein 19, 8620 Nieuwpoort)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Brasserie Nieuwpoort',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8620',
            'cust_city' => 'Nieuwpoort',
            'cust_address' => 'Marktplein 19, 8620 Nieuwpoort',
          ),
          'invoices' => 
          array (
            0 => 948,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-10-26',
          ),
        ),
      ),
    ),
  ),
  'Import (Brug ikke mere)' => 
  array (
    'Jylland' => 
    array (
      'Ukendt' => 
      array (
        'F. Salling A/S (Søndergade 27, 8000 Aarhus C)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'F. Salling A/S',
            'cust_landsdel' => 'Jylland',
            'cust_postcode' => '8000',
            'cust_city' => 'Aarhus C',
            'cust_address' => 'Søndergade 27, 8000 Aarhus C',
          ),
          'invoices' => 
          array (
            0 => 703,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
          ),
        ),
      ),
    ),
  ),
  'Phantom Spirits' => 
  array (
    'Sjælland m.fl.' => 
    array (
      'Ukendt' => 
      array (
        'Dansk e-Logistik C/O Nordic Spirits (Herringvej 25, 4690 Dalby)' => 
        array (
          'info' => 
          array (
            'cust_name' => 'Dansk e-Logistik C/O Nordic Spirits',
            'cust_landsdel' => 'Sjælland m.fl.',
            'cust_postcode' => '4690',
            'cust_city' => 'Dalby',
            'cust_address' => 'Herringvej 25, 4690 Dalby',
          ),
          'invoices' => 
          array (
            0 => 702,
            1 => 702,
          ),
          'invoices_dates' => 
          array (
            0 => '2017-06-30',
            1 => '2017-06-30',
          ),
        ),
      ),
    ),
  ),
);

