<?php 
/*
ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);
error_reporting(E_ALL);
//set_error_handler("var_dump");
*/

if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Produktrapport</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<?php
//https://www.rfmeier.net/get-all-orders-for-a-product-in-woocommerce/
/**
* Get all orders given a Product ID.
*
* @global $wpdb
*
* @param integer $product_id The product ID.
*
* @return array An array of WC_Order objects.
*/
function get_orders_by_product($product_id) {

    global $wpdb;
    
    $raw = "
        SELECT
          `items`.`order_id`,
          MAX(CASE WHEN `itemmeta`.`meta_key` = '_product_id' THEN `itemmeta`.`meta_value` END) AS `product_id`
        FROM
          `{$wpdb->prefix}woocommerce_order_items` AS `items`
        INNER JOIN
          `{$wpdb->prefix}woocommerce_order_itemmeta` AS `itemmeta`
        ON
          `items`.`order_item_id` = `itemmeta`.`order_item_id`
        WHERE
          `items`.`order_item_type` IN('line_item')
        AND
          `itemmeta`.`meta_key` IN('_product_id')
        GROUP BY
          `items`.`order_item_id`
        HAVING
          `product_id` = %d";

    $sql = $wpdb->prepare( $raw, $product_id );
    
    return $wpdb->get_results($sql);
}

$PRODUCT_ID = (int)$_GET['pid'];
if ($PRODUCT_ID <= 0) {
  die('Error 1');
}

$product = wc_get_product($PRODUCT_ID);
echo "<h1>Gennemførte ordrer med &quot;" . $product->get_name() . "&quot;</h1>\n";

$order_ids = get_orders_by_product($PRODUCT_ID);
/*
$orders = array_map(function ($data) {
                      return wc_get_order( $data->order_id );
                    }, $order_ids);
    
//print_r($order_ids);
print_r($orders);
*/

//var_dump(wc_get_order(315));

$companies = array();
$deleted = array();

foreach ($order_ids as $oid) {
  $id = $oid->order_id;
  //echo "[" . $id . "]:\n";
  //var_dump($id);
  //continue;
  $o = wc_get_order($id);
  if ($o === FALSE) {
    $deleted[] = $id;
    continue;
  }
  $d = $o->get_data();
  
  if (strlen(trim($d['billing']['company'])) == 0) {
    $deleted[] = $id;
    continue;
  }
  
  //$key = $d['billing']['company'];
  $key = $d['billing']['company'] . ", " . $d['billing']['postcode'] . " " . $d['billing']['city'];
  //echo $d['id'] . ": " . $key . "<br />\n";
  $companies[$key]['stamdata'] = array('key' => $key,
                                       'company' => $d['billing']['company'], 
                                       'postcode' => $d['billing']['postcode'], 
                                       'city' => $d['billing']['city']);
  $companies[$key]['orders'][] = $d;  
}

//print_r($companies);

echo "<ul>\n";
foreach ($companies as $c => $ds) {
  echo "<li>" . $c . "</li>\n";
  
  /*
  echo "<ul>\n";
  foreach ($ds as $d) {
    echo "<li>" . $d['id'] . ": " . $d['billing']['company'] . "</li>\n";
  }
  echo "</ul>\n";
  */
}
echo "</ul>\n";


echo "<h4>Ordrer der er slettet eller med tomt navn</h4>\n";
echo "<ul>\n";
foreach ($deleted as $id) {
  echo "<li>" . $id . "</li>\n";
}
echo "</ul>\n";

echo "<h2>Rådata</h2>\n";

echo "<pre>\n";
foreach ($companies as $c) {
  $stamdata = $c['stamdata'];
  echo $stamdata['company'] . ";" . $stamdata['postcode'] . ";" . $stamdata['city'] . "\n";  
}
echo "</pre>\n";
?>

</body>
</html>

