<?php

class Inventory
{

    /**
     * @var string $ProductNumber
     * @access public
     */
    public $ProductNumber = null;

    /**
     * @var int $Quantity
     * @access public
     */
    public $Quantity = null;

    /**
     * @param int $Quantity
     * @access public
     */
    public function __construct($Quantity)
    {
      $this->Quantity = $Quantity;
    }

}
