<?php

class Get_StockTransactionsResponse
{

    /**
     * @var StockTransaction[] $Get_StockTransactionsResult
     * @access public
     */
    public $Get_StockTransactionsResult = null;

    /**
     * @param StockTransaction[] $Get_StockTransactionsResult
     * @access public
     */
    public function __construct($Get_StockTransactionsResult)
    {
      $this->Get_StockTransactionsResult = $Get_StockTransactionsResult;
    }

}
