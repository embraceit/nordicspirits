<?php

class CustomExpMsg
{

    /**
     * @var int $ErrorNumber
     * @access public
     */
    public $ErrorNumber = null;

    /**
     * @var string $ErrorMsg
     * @access public
     */
    public $ErrorMsg = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @param int $ErrorNumber
     * @access public
     */
    public function __construct($ErrorNumber)
    {
      $this->ErrorNumber = $ErrorNumber;
    }

}
