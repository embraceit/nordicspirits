<?php

class Get_OrderChanges
{

    /**
     * @var guid $AuthenticationID
     * @access public
     */
    public $AuthenticationID = null;

    /**
     * @param guid $AuthenticationID
     * @access public
     */
    public function __construct($AuthenticationID)
    {
      $this->AuthenticationID = $AuthenticationID;
    }

}
