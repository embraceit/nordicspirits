<?php

class OrderChange
{

    /**
     * @var string $InvoiceNumber
     * @access public
     */
    public $InvoiceNumber = null;

    /**
     * @var string $ProductNumber
     * @access public
     */
    public $ProductNumber = null;

    /**
     * @var int $QuantityDelivered
     * @access public
     */
    public $QuantityDelivered = null;

    /**
     * @var int $QuantityOrdered
     * @access public
     */
    public $QuantityOrdered = null;

    /**
     * @param int $QuantityDelivered
     * @param int $QuantityOrdered
     * @access public
     */
    public function __construct($QuantityDelivered, $QuantityOrdered)
    {
      $this->QuantityDelivered = $QuantityDelivered;
      $this->QuantityOrdered = $QuantityOrdered;
    }

}
