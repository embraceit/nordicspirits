<?php

class Product
{

    /**
     * @var string $EAN
     * @access public
     */
    public $EAN = null;

    /**
     * @var string $ProductID
     * @access public
     */
    public $ProductID = null;

    /**
     * @var string $ProductName
     * @access public
     */
    public $ProductName = null;

    /**
     * @var string $ProductVariantID
     * @access public
     */
    public $ProductVariantID = null;

    /**
     * @var string $ProductVariantName
     * @access public
     */
    public $ProductVariantName = null;

    /**
     * @var string $SupplierProductNumber
     * @access public
     */
    public $SupplierProductNumber = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}
