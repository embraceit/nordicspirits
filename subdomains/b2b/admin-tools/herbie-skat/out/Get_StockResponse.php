<?php

class Get_StockResponse
{

    /**
     * @var Stock[] $Get_StockResult
     * @access public
     */
    public $Get_StockResult = null;

    /**
     * @param Stock[] $Get_StockResult
     * @access public
     */
    public function __construct($Get_StockResult)
    {
      $this->Get_StockResult = $Get_StockResult;
    }

}
