<?php

class Stock
{

    /**
     * @var string $EAN
     * @access public
     */
    public $EAN = null;

    /**
     * @var int $InStock
     * @access public
     */
    public $InStock = null;

    /**
     * @param int $InStock
     * @access public
     */
    public function __construct($InStock)
    {
      $this->InStock = $InStock;
    }

}
