<?php

class Send_Products
{

    /**
     * @var guid $AuthenticationID
     * @access public
     */
    public $AuthenticationID = null;

    /**
     * @var Product[] $p
     * @access public
     */
    public $p = null;

    /**
     * @param guid $AuthenticationID
     * @param Product[] $p
     * @access public
     */
    public function __construct($AuthenticationID, $p)
    {
      $this->AuthenticationID = $AuthenticationID;
      $this->p = $p;
    }

}
