<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Herbie Skat</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body>

<h1>Herbie SKAT</h1>

<?php
  /*
  php wsdl2phpgenerator-2.5.5.phar -i http://test.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?singleWsdl -o out/
  https://github.com/wsdl2phpgenerator/wsdl2phpgenerator-cli
  
  php wsdl2phpgenerator-2.5.5.phar -i http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?singleWsdl -o out/
  https://github.com/wsdl2phpgenerator/wsdl2phpgenerator-cli
  */
  
  require 'out/InventoryService_V1_1.php';

  $authid_prod = 'A9C311DC-F6D2-4C54-95C0-F47E2C908D0F';
  $date_from = '2017-09-01';
  $stock_pars = new Get_StockTransactions($authid_prod, $date_from);
  
  $sas = array();
  
  try {    
    $client = new InventoryService_V1_1($options = array('url' => 'http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc', 'trace' => 1));
    $stock_trans = $client->Get_StockTransactions($stock_pars);
    //var_dump($stock_trans);
    //var_dump($stock_trans->Get_StockTransactionsResult);
    //var_dump($stock_trans);
    
    //print_r($stock_trans);
    
    foreach ($stock_trans->Get_StockTransactionsResult->StockTransaction as $o) {
      $sas[] = (array)$o;
    }

  } catch(Exception $fault){
    echo "Der skete en fejl.";
    die;
    /*
    var_dump($client->__getLastRequestHeaders());
    var_dump($client->__getLastRequest());
    var_dump($client->__getLastResponseHeaders());
    var_dump($client->__getLastResponse());
    echo $fault->getMessage();
    print_r($fault->detail);    
    */
  }
  

  
  $sas_herbie = array();
  foreach ($sas as $a) {
    if (strpos($a['EAN'], 'herbie') !== false) {
      
      $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s.u", $a['Date']);//
      $a['DatoDag'] = $a['Dato']->format('Y-m-d');
      $a['DatoKl'] = $a['Dato']->format('H:i');
      
      $sas_herbie[$a['EAN']][] = $a;
    }
  }
  
  //print_r($sas_herbie);

  function dato_sort($a, $b) {
      return $a['Dato'] > $b['Dato'];
  }
  
  //print_r($sas_herbie);
  //die;
  
  echo '<h2>Lagertransaktioner fra DEL hvor EAN indeholder &quot;herbie&quot; fra ' . $date_from . ' og frem</h2>';
 
  foreach ($sas_herbie as $p => $as) {
    echo '<h3>' . $p . '</h3>';

    usort($as, "dato_sort");
    
    $out = 0;
    $in = 0;
    
    echo "
      <table>
        <tr>
          <th>Dato</th>
          <th>Kl.</th>
          <th>Date (org)</th>
          <th>Ændring</th>
          <th>Lagerbeholdning</th>
        </tr>\n";  
          
    foreach ($as as $a) {
      $style = '';
      
      if ($a['ChangeInStock'] < 0) {
        $out += $a['ChangeInStock'];
        $style = ' style="background-color: rgba(0, 255, 0, .4);"';
      } else if ($a['ChangeInStock'] > 0) {
        $in += $a['ChangeInStock'];
        $style = ' style="background-color: rgba(0, 0, 255, .4);"';
      }

      echo "
          <tr" . $style . ">
            <td>" . $a['DatoDag'] . "</td>
            <td>" . $a['DatoKl'] . "</td>
            <td>" . $a['Date'] . "</td>
            <td>" . $a['ChangeInStock'] . "</td>
            <td>" . $a['InStock'] . "</td>
          </tr>\n";  
    }

    echo "
      </table>
      \n";
      
    echo "<p>Sum afgang (sum af negative ændringer, grønne - pga. salg) = " . $out . "</p>";
    echo "<p>Sum tilgang (sum af positive ændringer, lilla - pga. opfyldning) = " . $in . "</p>";
  }  
?>

</body>
</html>
