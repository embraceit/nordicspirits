<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Herbie Skat</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body>

<h1>Herbie</h1>

<?php
  /*
  php wsdl2phpgenerator-2.5.5.phar -i http://test.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?singleWsdl -o out/
  https://github.com/wsdl2phpgenerator/wsdl2phpgenerator-cli
  
  php wsdl2phpgenerator-2.5.5.phar -i http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?singleWsdl -o out/
  https://github.com/wsdl2phpgenerator/wsdl2phpgenerator-cli
  */
  
  require 'out/InventoryService_V1_1.php';

  $authid_prod = 'A9C311DC-F6D2-4C54-95C0-F47E2C908D0F';
  $date_from = '2018-01-01';
  $stock_pars = new Get_StockTransactions($authid_prod, $date_from);
  
  $sas = array();
  
  try {    
    $client = new InventoryService_V1_1($options = array('url' => 'http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc', 'trace' => 1));
    $stock_trans = $client->Get_StockTransactions($stock_pars);
    //var_dump($stock_trans);
    //var_dump($stock_trans->Get_StockTransactionsResult);
    //var_dump($stock_trans);
    
    //print_r($stock_trans);
    
    foreach ($stock_trans->Get_StockTransactionsResult->StockTransaction as $o) {
      $sas[] = (array)$o;
    }

  } catch(Exception $fault){
    echo "Der skete en fejl.";
    /*
    var_dump($client->__getLastRequestHeaders());
    var_dump($client->__getLastRequest());
    var_dump($client->__getLastResponseHeaders());
    var_dump($client->__getLastResponse());
    echo $fault->getMessage();
    print_r($fault->detail);
    */
    die;
  }
  
  //print_r($sas);
  
  $sas_herbie = array();
  foreach ($sas as $a) {
    //print_r($a);
    if (strpos($a['EAN'], 'herbie') !== false || substr($a['EAN'], 0, 3) == '120') {
      $mth = substr($a['Date'], 0, 7);
      
      if (strpos($a['Date'], '.') !== false) {        
        $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s.u", $a['Date']);
      } else {
        $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s", $a['Date']);
      }
      $a['DatoDag'] = $a['Dato']->format('Y-m-d');
      $a['DatoKl'] = $a['Dato']->format('H:i');
      
      $sas_herbie[$mth][$a['EAN']][] = $a;
    }
  }
  
  //print_r($sas_herbie);

  function dato_sort($a, $b) {
    return $a['Dato'] > $b['Dato'];
  }
  
  //print_r($sas_herbie);
  //die;
  
  krsort($sas_herbie);
  
  $reason_map = array(
    '0' => 'Binding changed',
    '1' => 'Physical destocking for transfer to new SKU',
    '2' => 'Saleable destocking for transfer to new SKU',
    '3' => 'Physical stock entry for transfer to new SKU',
    '4' => 'Saleable stock entry for transfer to new SKU',
    '5' => 'Stock entry because of error',
    '6' => 'Destocking because of error',
    '7' => 'Goods reception',
    '8' => 'Damaged in stock',
    '9' => 'Damaged return from customer',
    '10' => 'Damaged on arrival',
    '11' => 'Shipped/Dispatched',
    '12' => 'Customer returns',
    '13' => 'Stock entry on client request',
    '14' => 'Destocking on client request',
    '15' => 'Stock entry because of goods found',
    '16' => 'Destocking because of goods lost'
  );
  
  echo '<p>Lagertransaktioner fra DEL hvor EAN indeholder &quot;herbie&quot; eller starter med &quot;120&quot; fra ' . $date_from . ' og frem</p>';
  
  foreach ($sas_herbie as $mth => $mth_transactions) {
    echo '<h2 style="padding: 1em; background-color: #666666; color: #eeeeee;">' . $mth . '</h2>';

    foreach ($mth_transactions as $p => $as) {
      echo '<h3>' . $p . '</h3>';

      usort($as, "dato_sort");
      
      $sum_11 = 0;
      $out = 0;
      $in = 0;
      
      echo "
        <table>
          <tr>
            <th>Dato</th>
            <th>Kl.</th>
            <th>Date (org)</th>
            <th>Ændring</th>
            <th>Lagerbeholdning</th>
            <th>ÅrsagID</th>
            <th>Årsag</th>
          </tr>\n";  
            
      foreach ($as as $a) {
        $style = '';
        
        $reason = 'UNKNOWN';
        
        if (isset($reason_map[$a['StockTransactionDetailReasonID']])) {
          $reason = $reason_map[$a['StockTransactionDetailReasonID']];
        }
        
        // Shipped/Dispatched
        if ($a['StockTransactionDetailReasonID'] == '11') {
          $sum_11 += $a['ChangeInStock'];
          $style = ' style="background-color: rgba(0, 255, 0, .4);"';
        }
        
        if ($a['ChangeInStock'] < 0) {
          $out += $a['ChangeInStock'];
          //$style = ' style="background-color: rgba(0, 255, 0, .4);"';
        } else if ($a['ChangeInStock'] > 0) {
          $in += $a['ChangeInStock'];
          //$style = ' style="background-color: rgba(0, 0, 255, .4);"';
        }

        echo "
            <tr" . $style . ">
              <td>" . $a['DatoDag'] . "</td>
              <td>" . $a['DatoKl'] . "</td>
              <td>" . $a['Date'] . "</td>
              <td>" . $a['ChangeInStock'] . "</td>
              <td>" . $a['InStock'] . "</td>
              <td>" . $a['StockTransactionDetailReasonID'] . "</td>
              <td>" . $reason . "</td>
            </tr>\n";  
      }

      echo "
        </table>
        \n";
      
      echo "<p><strong>Sum ÅrsagID 11 (grønne) = " . $sum_11 . "</strong></p>";  
      echo "<p>Sum afgang (sum af negative ændringer) = " . $out . "</p>";
      echo "<p>Sum tilgang (sum af positive ændringer) = " . $in . "</p>";
    }  
  }
?>

</body>
</html>
