<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Herbie</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body>

<h1>Herbie</h1>

<?php
  /*
  php wsdl2phpgenerator-2.5.5.phar -i http://test.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?singleWsdl -o out/
  https://github.com/wsdl2phpgenerator/wsdl2phpgenerator-cli
  
  php wsdl2phpgenerator-2.5.5.phar -i http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc?singleWsdl -o out/
  https://github.com/wsdl2phpgenerator/wsdl2phpgenerator-cli
  */
  
  require 'out/InventoryService_V1_1.php';

  $authid_prod = 'A9C311DC-F6D2-4C54-95C0-F47E2C908D0F';
  $date_from = '2017-01-01';
  $date_to = DateTime::createFromFormat("Y-m-d H:i", "2017-12-31 23:59");
  $stock_pars = new Get_StockTransactions($authid_prod, $date_from);
  
  $sas = array();
  
  
  //$EANs = array('herbie-pos', 'herbie-gin-original', 'herbie-gin-colli6');

  
  try {    
    $client = new InventoryService_V1_1($options = array('url' => 'http://logistikdata.dansk-e-logistik.dk/V1/InventoryService/InventoryService_V1_1.svc', 'trace' => 1));
    $stock_trans = $client->Get_StockTransactions($stock_pars);
    //var_dump($stock_trans);
    //var_dump($stock_trans->Get_StockTransactionsResult);
    //var_dump($stock_trans);
    
    //print_r($stock_trans);
    
    foreach ($stock_trans->Get_StockTransactionsResult->StockTransaction as $o) {
      $sas[] = (array)$o;
    }

  } catch(Exception $fault){
    echo "Der skete en fejl.";
    /*
    var_dump($client->__getLastRequestHeaders());
    var_dump($client->__getLastRequest());
    var_dump($client->__getLastResponseHeaders());
    var_dump($client->__getLastResponse());
    echo $fault->getMessage();
    print_r($fault->detail);
    */
    die;
  }
  
  //print_r($sas);
  
  $sas_herbie = array();
  $NON_PHYS_CODES = array(5, 6, 13, 14);
  
  foreach ($sas as $a) {
    //print_r($a);
    
    
    //if (strpos($a['EAN'], 'herbie') === false) {
    if (strpos($a['EAN'], 'christmas') === false) {
    //if (!in_array($a['EAN'], $EANs)) {
      continue;
    } 

    /*
    if (in_array($a['StockTransactionDetailReasonID'], $NON_PHYS_CODES)) {
      continue;
    }
    */
        
    $mth = substr($a['Date'], 0, 7);
    
    if (strpos($a['Date'], '.') !== false) {        
      $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s.u", $a['Date']);
    } else {
      $a['Dato'] = DateTime::createFromFormat("Y-m-d\TH:i:s", $a['Date']);
    }
    
    if ($a['Dato'] > $date_to) {
      continue;
    }
    
    $a['DatoDag'] = $a['Dato']->format('Y-m-d');
    $a['DatoKl'] = $a['Dato']->format('H:i');
    
    $sas_herbie[$a['EAN']][] = $a;
  }
  
  //print_r($sas_herbie);

  function dato_sort($a, $b) {
    return $a['Dato'] > $b['Dato'];
  }
  
  //print_r($sas_herbie);
  //die;
  
  krsort($sas_herbie);
  
  echo '<h2>Lagertransaktioner fra DEL fra ' . $date_from . ' og frem til ' . $date_to->format('Y-m-d') . ' </h2>';
  
  $out_total = 0;
  $in_total = 0;
  
  foreach ($sas_herbie as $p => $as) {
    echo '<h3 style="padding: 0.5em; background-color: #999999; color: #eeeeee;">' . $p . '</h3>';

    $is_colli = false;
    $colli_factor = 1;
    
    if (strpos($p, 'colli6') !== false) {
      $is_colli = true;
      $colli_factor = 6;
    }
    
    if ($is_colli) {
      echo '<h4 style="color: red">Disse tal er ganget med kolli-faktoren ' . $colli_factor . '</h4>';
    }
    
    usort($as, "dato_sort");
    
    $out = 0;
    $in = 0;
    
    foreach ($as as $a) {
      $style = '';
      
      if ($is_colli) {
        $a['ChangeInStock'] = $colli_factor * $a['ChangeInStock'];
      }
      
      if ($a['ChangeInStock'] < 0) {
        $out += $a['ChangeInStock'];
        $style = ' style="background-color: rgba(0, 255, 0, .4);"';
      } else if ($a['ChangeInStock'] > 0) {
        $in += $a['ChangeInStock'];
        $style = ' style="background-color: rgba(0, 0, 255, .4);"';
      }
    }

    $out_total += $out;
    $in_total += $in; 

    echo "<p>Sum afgang (sum af negative ændringer) = " . $out . "</p>";
    echo "<p>Sum tilgang (sum af positive ændringer) = " . $in . "</p>";
    echo "<p>Anslået lagerbeholdning = " . ($in + $out) . "</p>";
  }
  
  echo '<h2 style="padding: 1em; background-color: #666666; color: #eeeeee;">Total</h2>';  
  echo "<p>Sum afgang (sum af negative ændringer) = " . $out_total . "</p>";
  echo "<p>Sum tilgang (sum af positive ændringer) = " . $in_total . "</p>";
  echo "<p>Anslået lagerbeholdning = " . ($in_total + $out_total) . "</p>";
    
?>

</body>
</html>
