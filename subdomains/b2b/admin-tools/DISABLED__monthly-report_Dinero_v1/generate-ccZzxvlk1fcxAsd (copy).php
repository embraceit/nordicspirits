<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

//require('include-dinero-fetch.php');

$month = preg_replace('/[^0-9-]/', '', $_GET['month']);

$DATE_FROM = $month . '-01';
$DATE_TO = (new DateTime($DATE_FROM))->format( 'Y-m-t' );

$OUT_main = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits</title>
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
';

////////////////////////////////////////////////////////////


$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

$FILENAME = 'report-' . $month . '-' . $dt->format('Y_m_d_His') . '.zip';

$OUT_main .= '<p>Baseret på Woo-data. Genereret ' . $dt->format('d-m-Y H:i:s') . '. Gemt i <code>' . $FILENAME . '</code></p>';

////////////////////////////////////////////////////////////

$orders = get_posts( array(
    'numberposts' => -1,
    'post_type'   => wc_get_order_types(),
    'post_status' => 'wc-completed',      
    'orderby'     => 'date',
    'order'       => 'DESC',
    'date_query'  => array(
        'column'  => 'post_date',
        'after'   => $DATE_FROM,
        'before'  => $DATE_TO
    )
));

print_r($orders);

$data = array();

foreach ($orders as $post) {
  $order_id = $post->ID;
  $order = new WC_Order($order_id);
  
  $customer_user_id = get_post_meta($order->post->ID, '_customer_user', true );
  $customer_user = get_user_by('id', $customer_user_id);
  $customer_user_roles = implode(';', $customer_user->roles);
  $customer_company = get_post_meta($order->post->ID, '_billing_company', true);
  $customer_city = get_post_meta($order->post->ID, '_billing_city', true);
  $customer_postcode = get_post_meta($order->post->ID, '_billing_postcode', true);
  $customer_address_1 = get_post_meta($order->post->ID, '_billing_address_1', true);
  $customer_cvr = get_post_meta($order->post->ID, '_billing_cvr', true);
  
  $items = $order->get_items();
  
  $items_simple = array();
  
  foreach ($items as $item) {
    $product_id = $item['product_id'];
    $product = new WC_Product($product_id);
    $product_name = $item['name'];
    $product_qty = $item['qty'];
    $variation_id = $item['variation_id'];
    
    if ($variation_id != 0) {
      $product = new WC_Product_Variation($variation_id);
    }
    
    //$qty_mult = 1;
    $is_colli = false;
    
    if (stripos($product_name, 'kolli') !== false) {
      //$qty_mult = 6;
      $is_colli = true;
    }
    
    //print_r($item);
    
    if ($product_qty > 0) {
      $items_simple[] = array(
        'product_id' => $product_id,
        'subtotal' => $item['subtotal'],
        'subtotal_tax' => $item['subtotal_tax'],
        'total' => $item['total'],
        'total_tax' => $item['total_tax'],
        'sku' => $product->get_sku(),
        'variation_id' => $variation_id,
        'name' => $product_name,
        'cats' => strip_tags($product->get_categories(';')),
        'producent' => $product->get_attribute('producent'),
        'is_colli' => $is_colli,
        'qty' => $product_qty);
    }
  }
  
  $data[$order_id] = array(
    'order_date' => substr($order->order_date, 0, 10),
    'shipping_method' => $shipping_method, 
    'order_id' => $order_id,

    'subtotal' => $order->subtotal,
    'subtotal_tax' => $order->subtotal_tax,
    'total' => $order->total,
    'total_tax' => $order->total_tax,
    'discount_total' => $order->discount_total,
    'discount_total_tax' => $order->discount_total_tax,

    'customer_user_id' => $customer_user_id,
    //'customer_user' => $customer_user,
    'customer_user_roles' => $customer_user_roles,
    'customer_company' => $customer_company,
    'customer_city' => $customer_city,
    'customer_postcode' => $customer_postcode,
    'customer_address_1' => $customer_address_1,
    'customer_cvr' => $customer_cvr,
    'items_simple' => $items_simple);

}

////////////////////////////////////////////////////////////
$OUT_main .= '
</body>
</html>
';
////////////////////////////////////////////////////////////

$zip = new ZipArchive();
$filename = "./reports/" . $FILENAME;

if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
    exit("cannot open <$filename>\n");
}

$zip->addFromString("index.html", $OUT_main);
/*$zip->addFromString("testfilephp.txt" . time(), "#1 This is a test string added as testfilephp.txt.\n");
$zip->addFromString("testfilephp2.txt" . time(), "#2 This is a test string added as testfilephp2.txt.\n");
$zip->addFile($thisdir . "/too.php","/testfromfile.php");
*/
echo "Genereret. Download går i gang om lidt.\n";
/*
echo "numfiles: " . $zip->numFiles . "\n";
echo "status:" . $zip->status . "\n";
*/
$zip->close();

