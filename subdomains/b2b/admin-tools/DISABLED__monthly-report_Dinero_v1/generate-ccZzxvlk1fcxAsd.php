<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

$month = preg_replace('/[^0-9-]/', '', $_GET['month']);

$month_input = $month . '-01';
$d_input = new DateTime($month_input);
$DATE_FROM = $d_input->format('Y-m-01');
$DATE_TO = $d_input->format('Y-m-t');

$OUT_main = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits</title>
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
';

////////////////////////////////////////////////////////////


$tz = 'Europe/Copenhagen';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz));
$dt->setTimestamp($timestamp);

$FILENAME = 'report-' . $month . '-' . $dt->format('Y_m_d_His') . '.zip';

$OUT_main .= '<p>Genereret ' . $dt->format('d-m-Y H:i:s') . '. Gemt i <code>' . $FILENAME . '</code></p>';


////////////////////////////////////////////////////////////

$zip = new ZipArchive();
$filename = "./reports/" . $FILENAME;

if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
    exit("cannot open <$filename>\n");
}


////////////////////////////////////////////////////////////

require('../include-dinero.php');
$client_id_secret = 'Nordic Spirits IVS' . ':' . 'TojI4fWZePZhMi6zg7PfaKHys9gmTv0YRetYdIQaK4';
$apikey = '9361e8bbcd144a208ed715d2415489c6';
$token = get_access_token($client_id_secret, $apikey);
$access_token = $token['access_token'];
$org_id = '117742';



// cache each hour
$sales_cache_filename = 'cache-dinero-invoices/sales-' . $org_id . '-' . $DATE_FROM . '-' . $DATE_TO . date("H") . '00.json';
$sales = NULL;  
if (file_exists($sales_cache_filename)) {
  //echo "CACHE!\n";
  $sales_json = file_get_contents($sales_cache_filename);
  $sales = json_decode($sales_json, true);
} else {
  //echo "API!\n";
  $sales = get_endpoint_all_pages('v1/' . $org_id . '/sales?startDate=' . $DATE_FROM . '&endDate=' . $DATE_TO . '&fields=Number,Guid,UpdatedAt,ContactName,Date,Description,Currency,TotalExclVat&statusFilter=Booked,Paid,OverPaid,Overdue&sort=VoucherNumber&deletedOnly=false', $access_token);
  $sales_json = json_encode($sales);
  file_put_contents($sales_cache_filename, $sales_json);
}

$data = array(
  'FetchOptions' => array('start_date' => $DATE_FROM, 'end_date' => $DATE_TO),
  'AccountNames' => array(),
  'ProductNames' => array(),
  'ProductGuids' => array(),
  'ContactNames' => array()
  );

foreach ($sales as $post) {
  $parsed = (int)preg_replace('/ordre[^0-9]*([0-9]+)/i', '${1}', $post['Description']);
  
  if ($parsed == 0) {
    continue;
  }
  
  $cache_filename = 'cache-dinero-invoices/invoices-' . $org_id . '-' . sprintf("%05d", $post['Number']) . '-' . $post['Guid'] . '-' . str_replace(':', '', $post['UpdatedAt']) . '.json';    
  $invoice = NULL;  
  if (file_exists($cache_filename)) {
    //echo "CACHE:\n<br />";
    $invoice_json = file_get_contents($cache_filename);
    $invoice = json_decode($invoice_json, true);
  } else {
    $endpoint_url = 'v1/' . $org_id . '/invoices/' . $post['Guid'];
    //echo "API: " . $endpoint_url . "\n<br />";
    $invoice = get_endpoint($endpoint_url, $access_token);
    
    // Only cache useful results
    if (isset($invoice['ProductLines'])) {
      $invoice_json = json_encode($invoice);
      file_put_contents($cache_filename, $invoice_json);
    }
  }
  
  if (isset($invoice['validationErrors'])) {
    // No need to include these as they are credit notes
    continue;
  }

  else if (!isset($invoice['ProductLines'])) {
    echo "\nInvoice ProductLines not set:\n";
    print_r($invoice);
    die;
  }

  $invoice_month = substr($invoice['Date'], 0, 7);

  for ($j = 0; $j < count($invoice['ProductLines']); ++$j) {  
    $line = $invoice['ProductLines'][$j];    
    
    if (strlen(trim($line['ProductGuid'])) == 0) {
      $line['ProductGuid'] = $line['Description'];
    }
    
    //$data['AccountTransactions'][$line['AccountNumber']]['AccountName'][$line['AccountName']] += 1;

    if (!isset($data['AccountNames'][$line['AccountNumber']])) {
      //$data['AccountNames'] = array($line['AccountNumber'] => array());
      $data['AccountNames'][$line['AccountNumber']] = array($line['AccountName'] => 0);
    }     
    if (!isset($data['ProductNames'][$line['ProductGuid']])) {
      //$data['ProductNames'] = array($line['ProductGuid'] => array());
      $data['ProductNames'][$line['ProductGuid']] = array($line['Description'] => 0);
    }     
    if (!isset($data['ProductGuids'][$line['Description']])) {
      //$data['ProductGuids'] = array($line['Description'] => array());
      $data['ProductGuids'][$line['Description']] = array($line['BaseAmountValue'] => array());
      $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']] = array($line['ProductGuid'] => 0);
    } else if (!isset($data['ProductGuids'][$line['Description']][$line['BaseAmountValue']])) {
      //$data['ProductGuids'][$line['Description']] = array($line['BaseAmountValue'] => array());
      $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']] = array($line['ProductGuid'] => 0);
    }    
    if (!isset($data['ContactNames'][$invoice['ContactGuid']])) {
      //$data['ContactNames'] = array($invoice['ContactGuid'] => array());
      $data['ContactNames'][$invoice['ContactGuid']] = array($invoice['ContactName'] => 0);
    }
    

    $data['AccountNames'][$line['AccountNumber']][$line['AccountName']] += 1;
    $data['ProductNames'][$line['ProductGuid']][$line['Description']] += 1;
    $data['ProductGuids'][$line['Description']][$line['BaseAmountValue']][$line['ProductGuid']] += 1;
    $data['ContactNames'][$invoice['ContactGuid']][$invoice['ContactName']] += 1;

    
    $sales_info = array(
      'Number' => $invoice['Number'],
      
      'ContactGuid' => $invoice['ContactGuid'],
      'ContactName' => $invoice['ContactName'],
      'OrderAddress' => $invoice['Address'],
      
      'ProductGuid' => $line['ProductGuid'],
      'ProductName' => $line['Description'],
      
      'Currency' => $invoice['Currency'],
      'ExternalReference' => $invoice['ExternalReference'],
      'Description' => $invoice['Description'],
      
      'InvoiceDate' => $invoice['Date'],
      'InvoicePaymentStatus' => $invoice['PaymentStatus'],

      'InvoiceTotalExclVat' => $invoice['TotalExclVat'],
      'InvoiceTotalVatableAmount' => $invoice['TotalVatableAmount'],
      'InvoiceTotalInclVat' => $invoice['TotalInclVat'],
      'InvoiceTotalTotalNonVatableAmount' => $invoice['TotalNonVatableAmount'],      
      'InvoiceTotalVat' => $invoice['TotalVat'],

      'BaseAmountValue' => $line['BaseAmountValue'],
      'BaseAmountValueInclVat' => $line['BaseAmountValueInclVat'],
      'Quantity' => $line['Quantity'],
      'TotalAmount' => $line['TotalAmount'],
      'TotalAmountInclVat' => $line['TotalAmountInclVat'],      
    );
    

    $data['AccountTransactions'][$line['AccountNumber']][$invoice_month]['ProductsSold']['ByProductThenContact'][$line['ProductGuid']]['Orders'][$invoice['ContactGuid']][] = $sales_info;
    $data['AccountTransactions'][$line['AccountNumber']][$invoice_month]['ProductsSold']['ByContactThenProduct'][$invoice['ContactGuid']]['Orders'][$line['ProductGuid']][] = $sales_info;
  }
}

//print_r($data);
//die;

// Only DKK!
foreach ($data['AccountTransactions'] as $account_number => $sales) {
  if ($account_number == 'A') {
    continue;
  }
  
  foreach ($sales as $month => $types) {
    foreach ($types['ProductsSold'] as $type_key => $type_items) {
      foreach ($type_items as $item_guid => $orders) {        
        foreach ($orders['Orders'] as $subkeyguid => $order_lines) {
          foreach ($order_lines as $order_line) {
            
            if ($order_line['Currency'] != "DKK") {
              print_r($order_line);
              die('Forventede kun DKK valuta');            
            }
          }
        }
      }
    }
  }
} 



// http://stackoverflow.com/a/2021729/7720448
function to_filename($str) {
  $str = preg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $str);
  $str = preg_replace("([\.]{2,})", '', $str);
  return $str;
}

function format_money($x) {
  if (strlen($x) == 0) {
    return '';
  }
  
  if ($x === 'NA') {
    return $x;
  }

  return number_format($x, $decimals = 2, $dec_point = ",", $thousands_sep = ".");
}

function format_number_csv($x) {
  if (strlen($x) == 0) {
    return '';
  }

  if ($x === 'NA') {
    return $x;
  }
  
  return number_format($x, $decimals = 2, $dec_point = ",", $thousands_sep = "");
}

function cmp_rawdata($a, $b) {
  $a_d = strtotime($a['date']);
  $b_d = strtotime($b['date']);
  return ($a_d < $b_d);
}



ksort($data['AccountTransactions'], SORT_REGULAR);

foreach ($data['AccountTransactions'] as $account_number => &$sales) { 
  krsort($sales, SORT_REGULAR);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
print_r($data['AccountNames']);
print_r($data['ProductNames']);
print_r($data['ProductGuids']);
print_r($data['ContactNames']);
die;
*/

foreach ($data['AccountTransactions'] as $account_number => $sales) {
  if ($account_number == 'A') {
    continue;
  }

  $account_name = array_keys($data['AccountNames'][$account_number])[0];
  
  $csv_prefix = $account_number . ' - ' . to_filename($account_name);
  $csv_details = '';
  
  $OUT_main .= "<h2>Konto " . $account_number . " (" . $account_name . ")</h2>\n";
  
  $rawdata = array();
  $summary = array();  
  
  foreach ($sales as $month => $types) {
    $rawdata[$month] = array();
    
    if (count($types) == 0 || !isset($types['ProductsSold']) || count($types['ProductsSold']) == 0) {
      continue;
    }
    
    foreach ($types['ProductsSold']['ByProductThenContact'] as $item_guid => $orders) {        
      foreach ($orders['Orders'] as $subkeyguid => $order_lines) {
        foreach ($order_lines as $order_line) {
        
          /*
          print_r($data['ProductNames']);
          var_dump($item_guid);
          var_dump($subkeyguid);
          die;
          */
          //print_r($data['ProductNames']);
          //die;
          //$sgn = ($order_line['SalesType'] == 'CreditNote') ? -1 : 1;
          $sgn = 1;
          $cust = array_keys($data['ProductNames'][$item_guid])[0];
          $prod = array_keys($data['ContactNames'][$subkeyguid])[0];
          
          if ($order_line['Currency'] != "DKK") {
              print_r($order_line);
              die('Forventede kun DKK valuta');
          }
          
          //print_r($order_line);
          
          $total_cost = 0;
          $quantity_cost = 0;
          $quantity_free = 0;          
          
          if (abs($order_line['TotalAmount']) < 0.01) { // Dinero is rounded to two decimals!
            $quantity_free = $sgn * $order_line['Quantity'];
          } else {
            $total_cost = $sgn * $order_line['TotalAmount'];
            $quantity_cost = $sgn * $order_line['Quantity'];          
          }
                    
          $rawdata[$month][] = array(
                         'date' => $order_line['InvoiceDate'], 
                         'prod' => $prod,
                         'cust' => $cust, 
                         'addr' => $order_line['OrderAddress'],
                         'quantity_cost' => $quantity_cost,
                         'quantity_free' => $quantity_free,
                         'total_cost' => $total_cost);
          
          if (!isset($summary[$prod])) {
            $summary[$prod] = array('quantity_cost' => 0, 
                                    'quantity_free' => 0, 
                                    'total_cost' => 0);
          }
          
          $summary[$prod]['quantity_cost'] += $quantity_cost;
          $summary[$prod]['quantity_free'] += $quantity_free;
          $summary[$prod]['total_cost'] += $total_cost;
        }
      }
    }    
  }


  $html_summary_head = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Oversigt</title>
 
  <style type="text/css">
    body {
      padding: 2em;
    }
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #666666;      
    }
    
    td, th {
      padding: 0.1em;
    }

		th {
		  text-align: left;
		}
		
		td {
		  text-align: right;
		  font-weight: normal;
		}		
  </style>
	</head>
<body>
  <h1>' . $DATE_FROM . ' -> ' . $DATE_TO . '</h1>';
   $html_summary_tail = '</body>
</html>';
  
  $html_summary = '
  <h2>' . $account_name . '</h2>
  <h3>Oversigt</h3>
  <table>
    <thead>
      <tr style="background-color: #cccccc">
        <th style="text-align: left">Produkt</th>
        <th>Antal betalte</th>
        <th>Totalpris for betalte excl. moms</th>
        <th>Gns. stykpris for betalte</th>
        <th>Antal til 0 DKK</th>
      </tr>
    </thead>
    <tbody>
  ';
  
  $csv_details = "Dato;År;Produkt;Kunde;Adresse;Postnr. GÆT;Antal;Totalpris excl. moms;Gns. stykpris;Antal til 0 DKK\n";
  
  $total_total_cost = 0;
  $total_quantity_cost = 0;
  $total_quantity_free = 0;
  
  ksort($summary, SORT_REGULAR);
  
  //print_r($summary);
  
  foreach ($summary as $prod => $s) {
    $total_total_cost += $s['total_cost'];
    $total_quantity_cost += $s['quantity_cost'];
    $total_quantity_free += $s['quantity_free'];

    $gnspris = ($s['quantity_cost'] == 0) ? 'NA' : (format_money($s['total_cost'] / $s['quantity_cost']) . ' DKK');

    
    $html_summary .= '
      <tr>
        <td style="text-align: left">' . $prod . '</td>
        <td>' . $s['quantity_cost'] . '</td>
        <td>' . format_money($s['total_cost']) . ' DKK</td>
        <td>' . $gnspris . '</td>
        <td>' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . '</td>
      </tr>
    ';      
  }
  
  $gnspris = ($total_quantity_cost == 0) ? 'NA' : (format_money($total_total_cost / $total_quantity_cost) . ' DKK');
  
  $html_summary .= '
    <tr style="background-color: #eeeeee;  font-weight: bold;">
      <td></td>
      <td>' . $total_quantity_cost . '</td>
      <td>' . format_money($total_total_cost) . ' DKK</td>
      <td>' . $gnspris . '</td>
      <td>' . $total_quantity_free . '</td>
    </tr>  
    </tbody>
  </table>
  ';
  //echo "<pre>" . $html_summary . "</pre>";
  
  $OUT_main .= $html_summary;
  
  $OUT_main .= '  
  <h3>Datagrundlag</h3>
  <table>
    <thead>
      <tr style="background-color: #cccccc">
        <th>Dato</th>
        <th style="text-align: left">Produkt</th>
        <th style="text-align: left">Kunde</th>
        <th style="text-align: left">Adresse</th>
        <th style="text-align: left">Postnr. GÆT</th>
        <th>Antal betalte</th>
        <th>Totalpris for betalte excl. moms</th>
        <th>Gns. stykpris for betalte</th>
        <th>Antal til 0 DKK</th>        
      </tr>
    </thead>
    <tbody>
  ';
  
  
  $total_total_cost = 0;
  $total_quantity_cost = 0;
  $total_quantity_free = 0;
  
  foreach ($rawdata as $month => $sales) {
    usort($sales, "cmp_rawdata");
    
    $OUT_main .= '
      <tr style="background-color: #eeeeee">
        <td style="font-weight: bold;">' . $month . '</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    ';
    
    foreach ($sales as $s) {
      $total_total_cost += $s['total_cost'];
      $total_quantity_cost += $s['quantity_cost'];
      $total_quantity_free += $s['quantity_free'];
      $addr = str_replace("\n", ', ', str_replace("\r", '', $s['addr']));
      $postnrguess = preg_replace('/^.*([0-9]{4}).*$/', '${1}', $addr);
      
      $gnspris = ($s['quantity_cost'] == 0) ? 'NA' : (format_money($s['total_cost'] / $s['quantity_cost']) . ' DKK');
      $gnspris_CSV = ($s['quantity_cost'] == 0) ? 'NA' : (format_number_csv($s['total_cost'] / $s['quantity_cost']));
      
      $OUT_main .= '
        <tr>
          <td>' . $s['date'] . '</td>
          <td style="text-align: left">' . $s['prod'] . '</td>
          <td style="text-align: left">' . $s['cust'] . '</td>
          <td style="text-align: left">' . $addr . '</td>
          <td style="text-align: left">' . $postnrguess . '</td>
          <td>' . $s['quantity_cost'] . '</td>
          <td>' . format_money($s['total_cost']) . ' DKK</td>
          <td>' . $gnspris . '</td>
          <td>' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . '</td>
        </tr>
      ';
      
      $csv_details .= $month . ';' . substr($month, 0, 4) . ';' . $s['prod'] . ';' . $s['cust'] . ';' . $addr . ';' . $postnrguess . ';' . $s['quantity_cost'] . ';' . format_number_csv($s['total_cost']) . ';' . $gnspris_CSV . ';' . ($s['quantity_free'] != 0 ? $s['quantity_free'] : '') . "\n";    
    }
  }
  
  $gnspris = ($total_quantity_cost == 0) ? 'NA' : (format_money($total_total_cost / $total_quantity_cost) . ' DKK');
  
  $OUT_main .= '
    <tr style="background-color: #eeeeee;  font-weight: bold;">
      <td style="font-weight: bold;">Total</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>' . $total_quantity_cost . '</td>
      <td>' . format_money($total_total_cost) . ' DKK</td>
      <td>' . $gnspris . '</td>
      <td>' . $total_quantity_free . '</td>
    </tr>
    </tbody>
  </table>
  ';
  
  //echo "<pre>" . $csv_details . "</pre>";
  
  //print_r($rawdata);
  
  $html_summary = $html_summary_head . $html_summary . $html_summary_tail;
  
  $html_summary = iconv("UTF-8", "ISO-8859-1", $html_summary);
  $csv_details = iconv("UTF-8", "ISO-8859-1", $csv_details);
  
  //file_put_contents('export/export - ' . $csv_prefix . ' - summary.html', $html_summary);
  //file_put_contents('export/export - ' . $csv_prefix . ' - datagrundlag.csv', $csv_details);

  $zip->addFromString($csv_prefix . ' - summary.html', $html_summary);
  $zip->addFromString($csv_prefix . ' - datagrundlag.csv', $csv_details);
  
  //print_r($sales);
  //die;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
$OUT_main .= '
</body>
</html>
';
////////////////////////////////////////////////////////////

$zip->addFromString("index.html", $OUT_main);
/*$zip->addFromString("testfilephp.txt" . time(), "#1 This is a test string added as testfilephp.txt.\n");
$zip->addFromString("testfilephp2.txt" . time(), "#2 This is a test string added as testfilephp2.txt.\n");
$zip->addFile($thisdir . "/too.php","/testfromfile.php");
*/
//echo "Genereret. Download går i gang om lidt.\n";
/*
echo "numfiles: " . $zip->numFiles . "\n";
echo "status:" . $zip->status . "\n";
*/
$zip->close();


//echo $OUT_main;

$quoted = sprintf('"%s"', addcslashes(basename($FILENAME), '"\\'));
$size   = filesize($filename);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $quoted); 
header('Content-Transfer-Encoding: binary');
header('Connection: Keep-Alive');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . $size);

readfile($filename);
