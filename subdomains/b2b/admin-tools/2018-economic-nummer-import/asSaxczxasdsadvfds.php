<?php 
if (!isset($_GET) || !isset($_GET['nowpload'])) {
  require('../../wp-load.php');
}

if (!is_user_logged_in() || !current_user_can('manage_options')) {
  // not admin
  auth_redirect();
  die; 
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>B2B Nordic Spirits &rsaquo; Data</title>

  <link rel='stylesheet' href='c3.min.css' type='text/css' media='all' />

  <script src="jquery-3.1.0.min.js" type="text/javascript"></script>
  <script src="d3.v3.min.js" type="text/javascript"></script>
  <script src="c3.min.js" type="text/javascript"></script>
  
  <style type="text/css">
    body {
      padding: 2em;
    }
    
		h1 a { 
		  background-image: none !important; 
		}
		
		table {
      border-collapse: collapse;
    }
    
    table, th, td {
      border: 1px solid #ccc;      
    }
    
    td, th {
      padding: 0.5em;
    }

		th {
		  text-align: left;
		  background-color: #cccccc;
		}
		
		td {
		  text-align: left;
		  font-weight: normal;
		}
  </style>
  <meta name='robots' content='noindex,nofollow' />
	</head>
<body class="login login-action-login wp-core-ui  locale-da-dk">
<?php
  function my_str_getcsv($x) {
    return str_getcsv($x, "\t");
  }
  
  $csv = array_map('my_str_getcsv', file('NSKunder.csv'));
  //print_r($csv);
  
  ///////////////////////////////////////////////////
    
  $economic_db = array();
  
  // $i = 1, not 0 as first (header) is skipped
  for ($i = 1; $i < count($csv); ++$i) {
    $row = $csv[$i];
    $economic_db[$row[0]] = $row;    
  }
  
  //print_r($economic_db);
  
  ///////////////////////////////////////////////////
  
  ///////////////////////////////////////////////////
  $users = get_users();  
  $woo_customers_by_wooid = array();
  $woo_customers_by_economicid = array();
  
  
  $n_min_lev_dist_eq0 = 0;
  $n_min_lev_dist_gt0 = 0;
  
  foreach ($users as $user) {
    $billing_company = get_user_meta($user->ID, 'billing_company', true);
    $billing_postcode = get_user_meta($user->ID, 'billing_postcode', true);
    $billing_city = get_user_meta($user->ID, 'billing_city', true);
    $billing_first_name = get_user_meta($user->ID, 'billing_first_name', true);
    $billing_last_name = get_user_meta($user->ID, 'billing_last_name', true);
    $billing_economic_kundenr = trim(get_user_meta($user->ID, 'billing_economic_kundenr', true));
    
    
    $shortname = $billing_company . ', ' . $billing_postcode . ' ' . $billing_city . ' (' . $billing_first_name . ' ' . $billing_last_name . ')';
    
    if (isset($woo_customers_by_wooid[$user->ID])) {
      die('Woo ID already set!');
    }
    
    ///////////////////
    $similarities = array();
    
    foreach ($economic_db as $row) {
      //print_r($row);      
      $similarities[$row[0]] = levenshtein($billing_company, $row[1]);
    }
    
    asort($similarities);
    
    $similarities_top3_keys = array_slice($similarities, 0, 3, $preserve_keys = true);
    //print_r($similarities_top3_keys);
    $similarities_top3 = array();
    $similarities_top3_str = array();
    
    foreach ($similarities_top3_keys as $key => $value) {
      $row = $economic_db[$key];      
      $similarities_top3[$key] = array('dist' => $value, 'row' => $row);
      $similarities_top3_str[] = $row[1] . " (e-conomic id = " . $row[0] . ")";
    }
    
    //die('MEDTAG LEVENSTEIN-AFSTAND, OG SORTER EFTER DEN BEDSTE AF DEM!');
    
    $similarities_top3_str = implode($similarities_top3_str, ", ");
    
    ///////////////////
    
    $min_lev_dist = array_values($similarities_top3_keys)[0];
    
    if ($min_lev_dist == 0) {
      $n_min_lev_dist_eq0 += 1;
    } else {
      $n_min_lev_dist_gt0 += 1;
    }
  
    $user_arr = array(
      'woo_id' => $user->ID,
      'shortname' => $shortname,
      'billing_company' => $billing_company,
      'billing_first_name' => $billing_first_name,
      'billing_last_name' => $billing_last_name,
      'billing_economic_kundenr' => $billing_economic_kundenr,
      'economic_export_top3_str' => $similarities_top3_str,
      'economic_export_top3' => $similarities_top3,
      'min_lev_dist' => $min_lev_dist
    );
    
    $woo_customers_by_wooid[$user->ID] = $user_arr;
    
    if (strlen($billing_economic_kundenr) > 0) {
      if (isset($woo_customers_by_economicid[$user->ID])) {
        die('Woo ID already set!');
      }
      
      $woo_customers_by_economicid[$billing_economic_kundenr] = $user_arr;
    }
    
    //print_r($user_arr);    
    //die('STOP!');
  }
  
  //print_r($woo_customers_by_economicid);
  
  //print_r($woo_customers_by_wooid);
  ///////////////////////////////////////////////////
  
  echo "
  <p>Sorteret efter minimum Levenstein-afstand.</p>
  <p>Levenstein-afstand på 0 er markeret tydeligt.</p>
  <p>Antal eksakte matches = " . $n_min_lev_dist_eq0 . ".</p>
  <p>Antal ineksakte matches = " . $n_min_lev_dist_gt0 . ".</p>
  
  
  <table>
    <tr>
      <th>Woo ID</th>
      <th>Name</th>
      <th>E-conomic ID registreret i Woo</th>
      <th>Min dist</th>
      <th>3 bedste E-conomic matches ud fra navn (Levenstein-afstand)</th>
    </tr>
    ";
    
  function cmp_min_lev_dist($a, $b) {
    if ($a['min_lev_dist'] == $b['min_lev_dist']) {
        return 0;
    }
    return ($a['min_lev_dist'] < $b['min_lev_dist']) ? -1 : 1;
  }
  
  usort($woo_customers_by_wooid, "cmp_min_lev_dist");

  foreach ($woo_customers_by_wooid as $user) {
    $extra_style_row = '';
    
    if ($user['min_lev_dist'] == 0) {
      $extra_style_row = ' style="background-color: #90EE90;"';
      
      $best_id = array_keys($user['economic_export_top3'])[0];
      //print_r($best_id);
      //update_user_meta($user['woo_id'], 'billing_economic_kundenr', $best_id);
    }
    
    echo "
    <tr" . $extra_style_row . ">
      <td>" . $user['woo_id'] . "</td>
      <td>" . $user['shortname'] . "</td>
      <td>" . $user['billing_economic_kundenr'] . "</td>
      <td>" . $user['min_lev_dist'] . "</td>
      <td><ul>
      ";
      //$user['economic_export_top3_str']
      
    foreach ($user['economic_export_top3'] as $row) {
      $extra_style_item = '';
      
      if ($row['dist'] == 0) {
        $extra_style_item = ' style="font-size: 1.5em; font-weight: bold; color: #006400;"';
      }
      
      echo "<li" . $extra_style_item . ">" . $row['row'][1] . " (e-conomic id = " . $row['row'][0] . ") med Levenstein-afstand = " . $row['dist'] . "</li>";
    }
    
    echo "
        </ul>
       </td>
    </tr>        
    ";
  }
  
  echo "
  </table>
  ";  
?>
</body>
</html>

