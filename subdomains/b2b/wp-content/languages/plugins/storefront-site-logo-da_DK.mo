��          �      �       H     I     �     �     �  �   �  
   �  
   �     �     �     �     �  	   �     	  *    �   J     �     �     �  �   �     �     �     �     �            	        (         	                                     
                   %sThanks for installing the Storefront Site Logo extension. To get started, visit the %sCustomizer%s.%s %sOpen the Customizer%s Branding Cheatin&#8217; huh? Choose branding style Lets you add a logo to your site by adding a Branding tab to the customizer where you can choose between "Title and Tagline" or "Logo image" for the Storefront theme. Logo Image Logo image Logo image and Tagline Storefront Site Logo Support Title and Tagline Wooassist http://wooassist.com/ PO-Revision-Date: 2017-12-17 21:08:17+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: da_DK
Project-Id-Version: Plugins - Storefront Site Logo - Development (trunk)
 %sTak fordi du installerede Storefront Site Logo udvidelsen. For at komme igang, gå til %sTilpasseren%s.%s %sÅbn Tilpasseren%s Branding Snyder&#8217; du? Vælg 'Branding' stil Lader dig tilføje et logo til dit websted ved at tilføje et 'Branding' faneblad til Tilpasseren, hvor du kan vælge mellem "Titel og Taglinje" eller "Logo billede" for Storefront tema. Logo billede Logo billede Loge billede og tagslinje Storefront website logo Support Titel og tagslinje Wooassist http://wooassist.com/ 