=== WooCommerce Orderstatuses ===
Contributors: YouBiz ApS
Tags: woocommerce, orderstatuses, youbiz
Requires at least: 3.9
Tested up to: 4.3.1
Stable tag: 1.1.0
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Adds custom order statuses to woocommerce

== Description ==
This module should only be used in coherence with the integration to Dansk E-Logistik from YouBiz ApS
It will not be very functional as a standalone solution, in fact it will not do any good at all.

= Requeriments = 


* WordPress 3.9 or later.
* WooCommerce 2.2 or later.

= Contribute =


== Installation ==

* Upload plugin files to your plugins folder, or install using WordPress built-in Add New Plugin installer;
* Activate the plugin;


== Frequently Asked Questions ==

= What is the plugin license? =

* This plugin is released under a GPL license.

= What is needed to use this plugin? =

* WordPress 3.9 or later.
* WooCommerce 2.2 or later.

= How this plugin works? =


== Screenshots ==


== Changelog ==

= 1.1.0 =

Generally cleaned up the code.
Added Icons to orderstatused,
Added Translations,
Removed unused statusttypes,
Added Complete button to order overview

Bugfixes:
Fixed bug that caused repporting to go nuts.

* Initial version.

== Upgrade Notice ==
You must completely remove any older versions before activating
= 1.1.0 =


* Initial version.
