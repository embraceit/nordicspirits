<?php
/**
 * Plugin Name: WooCommerce orderstatuses
 * Plugin URI: https://www.youbiz.dk/
 * Description: WooCommerce orderstatuses.
 * Version: 1.1.0
 * Author: Morten Krogh - YouBiz.dk
 * Author URI: http://youbiz.dk
 * Text Domain: wc-youbiz-orderstatus
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}



if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

  if ( ! class_exists( 'wc_youbiz_orderstatus' ) ) 
  {

    class wc_youbiz_orderstatus {


      /**
       * Plugin version.
       *
       * @var string
       */
      const VERSION = '1.1.0';

      /**
       * Instance of this class.
       *
       * @var object
       */
      protected static $instance = null;
      
                        private $del_pending="pending";
                        private $del_recieved="received";
                        private $del_ready="ready";
                        private $del_locked="locked";
                        private $del_sent="sent";
                        private $del_cancelled="cancelled";
                        private $del_on_hold="holding";
                        private $del_dropped="dropped";
                        private $del_force="forced";
      private $textdomain='wc-youbiz-orderstatus';
      private $st_append =" [e-Logistik]";  
      private function __construct() {
        
            // Load plugin text domain
        add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
        add_action( 'init',array($this, 'woo_e_log_register_shipment_order_status')); 

        add_filter( 'wc_order_statuses',array($this, 'woo_e_log_add_shipmentstatus_to_order_statuses' ));
        add_filter('woocommerce_reports_order_statuses',array($this,'woo_e_log_add_report_order_statuses'));
        add_action('woocommerce_api_edit_product',array($this,'woo_e_log_sync_stock'));
                    add_action( 'admin_enqueue_scripts',array($this, 'woo_e_log_load_admin_style' ));
                    add_filter('woocommerce_admin_order_actions',array($this, 'woo_e_log_add_action_button'), 10, 2);

     

        // called just before the woocommerce template functions are included
        //add_action( 'init', array( $this, 'include_template_functions' ), 20 );

        // called only after woocommerce has finished loading
        //add_action( 'woocommerce_init', array( $this, 'woocommerce_loaded' ) );

        // called after all plugins have loaded
        //add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );

        // indicates we are running the admin
        //if ( is_admin() ) {
        // ...
        //}

        // indicates we are being served over ssl
        //if ( is_ssl() ) {
        // ...
        // take care of anything else that needs to be done immediately upon plugin instantiation, here in the constructor
      //}
      }
      /**
       * Return an instance of this class.
       * @return object A single instance of this class.
       */
      public static function get_instance() {
        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
          self::$instance = new self;
        }

        return self::$instance;
      }
    
      public function load_plugin_textdomain()
      {
        $locale = apply_filters( 'plugin_locale', get_locale(), $this->textdomain );
        $file =trailingslashit( WP_LANG_DIR ) . $this->textdomain . '/'. $this->textdomain.'-' . $locale . '.mo';
        $file2=dirname( plugin_basename( __FILE__ ) ) . '/languages/'; 
        
        //load_textdomain( $this->textdomain, dirname(__FILE__) .  '/languages/'. $this->textdomain.'-' . $locale . '.mo' );
        load_textdomain( $this->textdomain, trailingslashit( WP_LANG_DIR ) . $this->textdomain . '/'. $this->textdomain.'-' . $locale . '.mo' );
        load_plugin_textdomain($this->textdomain, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

                    $this->del_pending=__("Pending",$this->textdomain) . $this->st_append;
                          $this->del_recieved=__("Received",$this->textdomain) . $this->st_append;
                          $this->del_ready=__("Processing",$this->textdomain) . $this->st_append;
                          $this->del_locked=__("Locked",$this->textdomain) . $this->st_append;
                          $this->del_sent=__("Shipped",$this->textdomain) . $this->st_append;
                          $this->del_cancelled=__("Cancelled",$this->textdomain) .  $this->st_append;
                          $this->del_on_hold=__("On hold",$this->textdomain) .$this->st_append;
                          $this->del_dropped=__("Dropshipped",$this->textdomain) . $this->st_append;
                          $this->del_force=__("Force shipment",$this->textdomain) .$this->st_append;  

      }

      public function woo_e_log_add_action_button ($actions, $order) {

        if ( $order->has_status( array( 'del-sent', 'del-dropped' ) ) ) {
                $action = array(
                    'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_mark_order_status&status=completed&order_id=' . $order->id ), 'woocommerce-mark-order-status' ),
                    'name'      => __( 'Complete', 'woocommerce' ),
                    'action'    => "complete"
                );
               array_push($actions, $action);

          }


        return $actions;
      }

      public function woo_e_log_load_admin_style()
      {
        //$plugin_dir_path = dirname(__FILE__);
        //echo $plugin_dir_path . '/assets/css/admin.css';
        wp_enqueue_style( 'wc_youbiz_orderstatuses_admin', '/wp-content/plugins/wc-youbiz-orderstatuses/assets/css/admin.css', array(), self::VERSION );

      }



      public function woo_e_log_sync_stock($id, $data=null)
      {
        global $woocommerce_wpml;
        if(isset($woocommerce_wpml))
        {
          $_pf = new WC_Product_Factory();
          $_product = $_pf->get_product($id);
          do_action( 'woocommerce_product_quick_edit_save', $_product );
          //file_put_contents("debug.txt",print_r($_product,true));               
        }
      }


      public function woo_e_log_register_shipment_order_status() {

        //addStatusToSystem("wc-awaiting-shipment","Awaiting shipment (DEL)" );
        $this->addStatusToSystem("wc-del-pending",$this->del_pending);//$this->del_pending ); 
        $this->addStatusToSystem("wc-del-recieved",$this->del_recieved);
        $this->addStatusToSystem("wc-del-ready",$this->del_ready );
        $this->addStatusToSystem("wc-del-locked",$this->del_locked );
        $this->addStatusToSystem("wc-del-sent",$this->del_sent );
        $this->addStatusToSystem("wc-del-cancelled",$this->del_cancelled );
        $this->addStatusToSystem("wc-del-on-hold",$this->del_on_hold );
        $this->addStatusToSystem("wc-del-dropped",$this->del_dropped );
        $this->addStatusToSystem("wc-del-force",$this->del_force );

      }


      private function addStatusToSystem($name, $label){
        register_post_status( $name, array(
              'label'                     => $label,
              'public'                    => false, /*maybe these statuses should be hidden for customers*/
              'exclude_from_search'       => false,
              'show_in_admin_all_list'    => true,
              'show_in_admin_status_list' => true,
              'label_count'               => _n_noop( $label .'<span class="count">(%s)</span>', $label .'<span class="count">(%s)</span>' )
              
              
              ) );

      }




      public function woo_e_log_add_shipmentstatus_to_order_statuses( $order_statuses ) {

        //$order_statuses["wc-awaiting-shipment"]="Awaiting shipment (DEL)";
        $order_statuses["wc-del-pending"]=$this->del_pending;
        $order_statuses["wc-del-recieved"]=$this->del_recieved;
        $order_statuses["wc-del-ready"]=$this->del_ready;
        $order_statuses["wc-del-locked"]=$this->del_locked;
        $order_statuses["wc-del-sent"]=$this->del_sent;
        $order_statuses["wc-del-cancelled"]=$this->del_cancelled;
        $order_statuses["wc-del-on-hold"]=$this->del_on_hold;
        $order_statuses["wc-del-dropped"]=$this->del_dropped;
        $order_statuses["wc-del-force"]=$this->del_force;
        return $order_statuses;

      }

      public function woo_e_log_add_report_order_statuses($order_statuses){
        //$order_statuses[]="awaiting-shipment";
        if(is_array($order_statuses) && count($order_statuses) == 1 && $order_statuses[0]=='refunded')
        {
          return $order_statuses;         
        }
        if(is_array($order_statuses))
        {

          $order_statuses[]="del-pending";
          $order_statuses[]="del-recieved";
          $order_statuses[]="del-ready";
          $order_statuses[]="del-locked";
          $order_statuses[]="del-sent";
          $order_statuses[]="del-dropped";
          $order_statuses[]="del-force";
          $order_statuses[]="del-on-hold";
        }
        return $order_statuses;
      }
      
      /**
       * Override any of the template functions from woocommerce/woocommerce-template.php
       * with our own template functions file
       */
      //public function include_template_functions() {
      //  include( 'woocommerce-template.php' );
      //}

      /**
       * Take care of anything that needs woocommerce to be loaded.
       * For instance, if you need access to the $woocommerce global
       */
      //public function woocommerce_loaded() {
      // ...
      //}

      /**
       * Take care of anything that needs all plugins to be loaded
       */
      //public function plugins_loaded() {
      // ...
      //}
    }

    // finally instantiate our plugin class and add it to the set of globals

    //$GLOBALS['wc_youbiz_orderstatus'] = new wc_youbiz_orderstatus();
    add_action( 'plugins_loaded', array( 'wc_youbiz_orderstatus', 'get_instance' ), 0 );
  }
}
?>