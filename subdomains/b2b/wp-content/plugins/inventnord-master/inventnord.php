<?php
/**
 * Plugin Name: Inventnord
 * Plugin URI:
 * Description: Inventnord logistik
 * Version: 1.0
 * Author: Ivan Ristic
 * Author URI: https://www.facebook.com/ivan.ristic1
 * License: All rights reserved copyright 2015 Ivan Ristic, details can be found in the license.txt file
 *
 * Created by PhpStorm.
 * User: ivanristic
 * Date: 7/22/14
 * Time: 3:00 AM
 */

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define( 'INVENTNORD_VERSION', '1.0.0' );
define( 'INVENTNORD__MINIMUM_WP_VERSION', '3.0' );
define( 'INVENTNORD__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'INVENTNORD__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

register_activation_hook( __FILE__, array( 'Inventnord', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'Inventnord', 'plugin_deactivation' ) );

require_once( INVENTNORD__PLUGIN_DIR . 'class.inventnord.php' );

add_action( 'init', array( 'Inventnord', 'init' ) );

/* Backend */
if ( is_admin() ) {

}