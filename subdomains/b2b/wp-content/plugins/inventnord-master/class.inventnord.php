<?php
/**
 * Created by PhpStorm.
 * User: ivanristic
 * Date: 7/22/14
 * Time: 3:17 AM
 */

class Inventnord {

    private static $initiated = false;

    // Static values rather then DEFINE
    private static $inventnord_api = "http://77.68.254.100/orders/getxmlorder.php";

    // Admin option field names
    private static $opt_in_username = 'in_username';
    private static $opt_in_password = 'in_password';
    private static $opt_in_reference = 'in_reference';
    private static $opt_in_ol_username = 'in_ol_username';
    private static $opt_in_ol_password = 'in_ol_password';

    public static function init() {
        if ( ! self::$initiated ) {
            self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() {
        self::$initiated = true;

        add_action( 'woocommerce_order_status_processing', array( 'Inventnord', 'prepare_xml' ));

        // Admin options
        add_action( 'admin_menu', array( 'Inventnord', 'inventnord_menu' ) );

        //add_filter( 'pre_comment_approved', array( 'Inventnord', 'last_comment_status' ), 10, 2 );
    }

    public static function prepare_xml ($order_id) {
        $order = new WC_Order($order_id);

        $user_id = $order->user_id;
        $firstname = $order->shipping_first_name;
        $lastname = $order->shipping_last_name;
        $shipping_address = $order->shipping_address_1;
        $postcode = $order->shipping_postcode;
        $city = $order->shipping_city;
        $company = $order->shipping_company;
        $country = $order->shipping_country;

        if (empty($country)) $country = "DK";

        $delivery_info = $order->order_comments;

        $phone = $order->billing_phone;
        $email = $order->billing_email;

        $date =  date('d-m-Y');

        /* Create Array of elements */
        $xmlOrderDynamicValues = array(
            'PackageProviderCode' => '7', // Post Danmark, or GLS, nummeric codes 1-9
            'KundeID' => 20,
            'OrderID' => $order_id,
            'OLSusername' => get_option( self::$opt_in_ol_username ),
            'OLSpassword' => get_option( self::$opt_in_ol_password ),
            'ReceiverName' => "$firstname $lastname",
            'ReceiverAddress' => $shipping_address,
            'ReceiverAddress2' => "",
            'ReceiverZip' => $postcode,
            'ReceiverCity' => $city,
            'ReceiverCountry' => $country,
            'ReceiverCompany' => $company,
            'DeliveryInfo' => $delivery_info,
            'ReceiverPhone' => $phone,
            'ReceiverEMail' => $email,
            'Reference' => $company, //get_option( self::$opt_in_reference ),
            'EffectiveDate' => $date,
        );

        /* Get XML document */
        $xml = self::createOrderDocument($xmlOrderDynamicValues, $order);

        /* Save document */
        $xml->save(INVENTNORD__PLUGIN_DIR . "/XML/ns".$order_id.".xml");
        $xml = $xml->saveXML();

      

        $response = self::sendRequest($xml);
    }

    /**
     * @param $xml
     * @return bool|mixed
     *
     * Continue no matter what happens, we are hooked in to payment completed.
     * Add logging afterwards.
     */
    private static function sendRequest($xml) {
        try {
            /* HTTP AUTH */
            $username = get_option(self::$opt_in_username);
            $password = get_option(self::$opt_in_password);

            /* Curl the XML at inventnord */
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, self::$inventnord_api);
            curl_setopt($ch, CURLOPT_POST, 1);
            $headers = array(
                "Content-type: text/xml",
                "Content-length: " . strlen($xml)
            );

            // send xml request to a server
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $xml);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HEADER, true);
            $data = curl_exec($ch);

            /* Unlink file and close curl */
            curl_close($ch);

              file_put_contents(INVENTNORD__PLUGIN_DIR.'/TESTBLA.txt', $data);

            return $data;
        } catch (Exception $e) {
            // Continue since we are in the payment hook
            return false;
        }
    }

    private static function createOrderDocument($xmlOrderDynamicValues, $order) {

        // Create document
        $xml = new DOMDocument('1.0', 'ISO-8859-1');

        // Create primary container
        $xmlOrder = $xml->createElement('Order');

        // Set header values
        $xmlOrder->setAttribute('xmlns', 'http://lager.bbn.ad/');
        $xmlOrder->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');

        // ParcelShop
        $xmlParcelShopID = $xml->createElement('ParcelShopID');
        $xmlParcelShopID->setAttribute('xsi:nil', 'true');
        $xmlOrder->appendChild($xmlParcelShopID);

        // Loop key value pairs of level 1 body
        foreach ($xmlOrderDynamicValues as $name => $value) {
            $element = $xml->createElement($name, $value);
            $xmlOrder->appendChild($element);
        }

        // Get order lines
        $xmlOrder->appendChild(self::getOrderLinesAsXml($xml, $order));

        // Append body to xml document
        $xml->appendChild($xmlOrder);

        return $xml;

    }

    private static function getOrderLinesAsXml($xml, $order) {

        $xmlOrderLines = $xml->createElement('OrderLines');

        $items = $order->get_items();
        foreach ( $items as $item ) {
            $product = new WC_Product_Variable($item['product_id']);
            if ( $product->has_child() ) {
                $product = new WC_Product_Variation($item['variation_id']);
            }
            $xmlArticleNo = $xml->createElement('ArticleNo', $product->get_sku());
            $xmlAmount = $xml->createElement('Amount', $item['qty']);
            $xmlOrderLine = $xml->createElement('OrderLine');

            $xmlOrderLine->appendChild($xmlArticleNo);
            $xmlOrderLine->appendChild($xmlAmount);

            $xmlOrderLines->appendChild($xmlOrderLine);
        }

        return $xmlOrderLines;
    }

    /**
    * admin menu
    **/
    public static function inventnord_menu() {
        add_menu_page('Inventnord Settings', 'Inventnord Settings', 'manage_options', 'my-top-level-handle', array( 'Inventnord', 'inventnord_settings' ), '', 40);
    }

    public static function inventnord_settings() {
        //must check that the user has the required capability
        if (!current_user_can('manage_options'))
        {
            wp_die( __('You do not have sufficient permissions to access this page.') );
        }

        $hidden_field_name = 'mt_submit_hidden';

        // Read in existing option value from database
        $opt_val_username = get_option( self::$opt_in_username );
        $opt_val_password = get_option( self::$opt_in_password );
        $opt_val_ol_username = get_option( self::$opt_in_ol_username );
        $opt_val_ol_password = get_option( self::$opt_in_ol_password );
        $opt_val_reference = get_option( self::$opt_in_reference );

        // See if the user has posted us some information
        // If they did, this hidden field will be set to 'Y'
        if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
            // Read their posted value
            $opt_val_username = $_POST[ self::$opt_in_username ];
            $opt_val_password = $_POST[ self::$opt_in_password ];
            $opt_val_ol_username = $_POST[ self::$opt_in_ol_username ];
            $opt_val_ol_password = $_POST[ self::$opt_in_ol_password ];
            $opt_val_reference = $_POST[ self::$opt_in_reference ];

            // Save the posted value in the database
            update_option( self::$opt_in_username, $opt_val_username );
            // Save the posted value in the database
            update_option( self::$opt_in_password, $opt_val_password );
            // Save the posted value in the database
            update_option( self::$opt_in_ol_username, $opt_val_ol_username );
            // Save the posted value in the database
            update_option( self::$opt_in_ol_password, $opt_val_ol_password );
            // Save the posted value in the database
            update_option( self::$opt_in_reference, $opt_val_reference );

            // Put an settings updated message on the screen

            ?>
            <div class="updated"><p><strong><?php _e('settings saved.', 'inventnord' ); ?></strong></p></div>
        <?php

        }

        // Now display the settings editing screen
        echo '<div class="wrap">';

        // header
        echo "<h2>" . __( 'Inventnord Settings', 'inventnord' ) . "</h2>";
        // settings form
        ?>
        <p>These values are given out by <a href="http://www.inventnord.dk" target="_blank">Inventnord.dk</a> </p>

        <form name="inventnord_form" method="post" action="">
            <input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

            <p>
                <span style="min-width: 80px; display: inline-block;"><?php _e("User:", 'inventnord' ); ?></span>
                <input type="text" name="<?php echo self::$opt_in_username; ?>" value="<?php echo $opt_val_username; ?>" size="40">
            </p>

            <p>
                <span style="min-width: 80px; display: inline-block;"><?php _e("Password:", 'inventnord' ); ?></span>
                <input type="text" name="<?php echo self::$opt_in_password; ?>" value="<?php echo $opt_val_password; ?>" size="40">
            </p>

            <p>
                <span style="min-width: 80px; display: inline-block;"><?php _e("Reference:", 'inventnord' ); ?></span>
                <input type="text" name="<?php echo self::$opt_in_reference; ?>" value="<?php echo $opt_val_reference; ?>" size="40">
            </p><hr />

            <p>
                <span style="min-width: 80px; display: inline-block;"><?php _e("OLUsername:", 'inventnord' ); ?></span>
                <input type="text" name="<?php echo self::$opt_in_ol_username; ?>" value="<?php echo $opt_val_ol_username; ?>" size="40">
            </p>

            <p>
                <span style="min-width: 80px; display: inline-block;"><?php _e("OLPassword:", 'inventnord' ); ?></span>
                <input type="text" name="<?php echo self::$opt_in_ol_password; ?>" value="<?php echo $opt_val_ol_password; ?>" size="40">
            </p><hr>

            <p class="submit">
                <input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
            </p>

        </form>
        </div>

    <?php
    }

    private static function bail_on_activation( $message, $deactivate = true ) {
        ?>
        <!doctype html>
        <html>
        <head>
            <meta charset="<?php bloginfo( 'charset' ); ?>">
            <style>
                * {
                    text-align: center;
                    margin: 0;
                    padding: 0;
                    font-family: "Lucida Grande",Verdana,Arial,"Bitstream Vera Sans",sans-serif;
                }
                p {
                    margin-top: 1em;
                    font-size: 18px;
                }
            </style>
        <body>
        <p><?php echo esc_html( $message ); ?></p>
        </body>
        </html>
        <?php
        if ( $deactivate ) {
            $plugins = get_option( 'active_plugins' );
            $inventnord = plugin_basename( INVENTNORD__PLUGIN_DIR . 'inventnord.php' );
            $update  = false;
            foreach ( $plugins as $i => $plugin ) {
                if ( $plugin === $inventnord ) {
                    $plugins[$i] = false;
                    $update = true;
                }
            }

            if ( $update ) {
                update_option( 'active_plugins', array_filter( $plugins ) );
            }
        }
        exit;
    }

    public static function woocommerce_activated() {
        if ( ! function_exists( 'is_woocommerce_activated' ) ) {
            function is_woocommerce_activated() {
                if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
            }
        }
        return true;
    }

    /**}
     * Attached to activate_{ plugin_basename( __FILES__ ) } by register_activation_hook()
     * @static
     */
    public static function plugin_activation() {
        if ( version_compare( $GLOBALS['wp_version'], INVENTNORD__MINIMUM_WP_VERSION, '<' ) ) {
            load_plugin_textdomain( 'akismet' );

            $message = '<strong>'.sprintf(esc_html__( 'Inventnord %s requires WordPress %s or higher.' , 'inventnord'), INVENTNORD_VERSION, INVENTNORD__MINIMUM_WP_VERSION ).'</strong> '.sprintf(__('Please <a href="%1$s">upgrade WordPress</a> to a current version.</a>.', 'inventnord'), 'https://codex.wordpress.org/Upgrading_WordPress');

            inventnord::bail_on_activation( $message );
        }

        if (!self::woocommerce_activated()) {
            $message = '<strong>'.sprintf(esc_html__( ' Please install WooCommerce for this plugin to work. </strong>'));
            inventnord::bail_on_activation( $message );
        }
    }

    /**
     * Removes all connection options
     * @static
     */
    public static function plugin_deactivation( ) {
        //tidy up
    }

} 