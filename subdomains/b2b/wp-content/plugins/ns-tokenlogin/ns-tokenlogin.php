<?php
/*
Plugin Name: NS Token Login
Plugin URI: http://www.tada.dk
Description: Direct login via token.
Version: 0.0.1
Author: Mikkel Meyer Andersen
Author URI: http://www.tada.dk
License: GPL2				
*/

// Based on wp-qr-code-login.

$TOKENLOGIN_DISALLOW_ADMINS = TRUE;

//mikl: http://b2b.nordicspirits.dk/wp-login.php?token=274ac5579680f0e3eba49b447c973c9305b1599586f81314c03621a51297f6d1
//mikl: http://b2b.nordicspirits.dk/wp-login.php?token=274ac5579680f0e3eba49b447c973c9305b1599586f81314c03621a51297f6d1


function tokenlogin_init() {
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
}
add_action('init', 'tokenlogin_init', 1);


function tokenlogin_destructor() {
  session_destroy();
}
add_action('wp_logout', 'tokenlogin_destructor');
add_action('wp_login', 'tokenlogin_destructor');


function get_user_roles($uid) {
  $user_info = get_userdata($uid);

  return $user_info->roles;
}

function can_user_do_token_login($uid) {
  global $TOKENLOGIN_DISALLOW_ADMINS;
  
  if ($TOKENLOGIN_DISALLOW_ADMINS === FALSE) {
    return true;
  }
  
  $roles = get_user_roles($uid);
  
  foreach ($roles as $role) {
    if ($role == 'administrator') {
      return false;
    }
  }
  /*
  if (count($roles) == 1 && $roles[0] == 'customer') {
    return true;
  }
  */
  
  return true;
}

function get_user_token_ignore_role($uid) {
  $user = get_user_by('id', $uid);
  
  $token = hash('sha256', wp_salt('secure_auth') . $user->user_email);
  
  if (strlen($token) == 64) {
    return $token;
  }
  
  return NULL;
}

function get_user_token($uid) {
  if (!can_user_do_token_login($uid)) {
    return NULL;
  }
  
  $token = get_user_token_ignore_role($uid);
  return $token;
}


/**
 * finds user by token
 *
 */
function try_token_login() {
  //echo 'mikl1234';
  global $_GET;
 
  //print_r($_GET);
  if (!isset($_GET) || !isset($_GET['token']) || strlen($_GET['token']) != 64)  {
    return;
  }
  $token = preg_replace( "/[^0-9a-z]/", "", $_GET['token'] );
  
	global $wpdb;
	
	$query = "SELECT ID, user_email FROM " . $wpdb->base_prefix . "users";
	$users = $wpdb->get_results($query, ARRAY_A);
	
	$token_uid = NULL;
	
	//print_r($users);
	
	foreach ($users as $user_info) {
	  $user_token = get_user_token($user_info['ID']);
	  
	  if ($user_token !== NULL && $user_token == $token) {
	    $token_uid = $user_info['ID'];
	    break;
	  }
	}
	
	//echo $token_uid;
	//die;

	if (NULL !== $token_uid) {
	  // http://wordpress.stackexchange.com/a/128445
	  if (!can_user_do_token_login($token_uid)) {
	    return false;
	  }
	  
	  $user = get_user_by('id', $token_uid);
	  
	  if ( !is_wp_error( $user ) ) {	  
      wp_clear_auth_cookie();
      
      $_SESSION['tokenlogin_loggedintoken'] = 'yes';
      do_action('wp_login', $user->user_login, $user);
      
      wp_set_current_user($token_uid);
      wp_set_auth_cookie($token_uid);

      // http://betterwp.net/wordpress-tips/safely-redirect-urls-in-wordpress/
      $location = empty($_GET['redirect_to']) ? home_url() : $_GET['redirect_to'];
      wp_safe_redirect( $location );
      exit();
    }

	  return true;
	} else {
		return false;
	}		
}

//add_action('login_message', 'try_token_login');

//https://codex.wordpress.org/Function_Reference/wp_signon
//add_action( 'after_setup_theme', 'try_token_login' );

//add_action( 'login_head', 'try_token_login' );
add_filter( 'authenticate', 'try_token_login', 10, 3 );

/********************************************************/

// http://onetarek.com/wordpress/how-to-add-extra-meta-field-for-user-profile-in-wordpress-plugin/
function token_user_profile_field( $user )
{
  $token = get_user_token($user->ID);
  
  if ($token === NULL) {
    $token = 'Ingen token (kun kunder med den ene rolle <i>customer</i> kan logge ind med token).';
  }
   
  echo '
   <h3>Login med token</h3>
   
   <table class="form-table">
   <tr>
   <th>Token</th>
   <td>
   ' . $token . '
   </td>
   </tr>
   </table>
   ';
}
 
add_action( 'show_user_profile', 'token_user_profile_field' );
add_action( 'edit_user_profile', 'token_user_profile_field' );


function add_user_token_column($columns) {
  $columns['user_tokenlogin_token'] = 'Token';
  return $columns;
}
add_filter('manage_users_columns', 'add_user_token_column');


function show_user_token_column_content($value, $column_name, $user_id) {
  //$user = get_userdata($user_id);
	
	if ('user_tokenlogin_token' == $column_name) {
		$token = get_user_token($user_id);
		$token = '<a href="http://b2b.nordicspirits.dk/wp-login.php?token=' . $token . '" target="_blank">' . $token . '</a>';
		return $token;
  }
  
  return $value;
}
add_action('manage_users_custom_column', 'show_user_token_column_content', 10, 3);


function tokenlogin_mailchimp_token($data, $user) {
  $token = get_user_token($user->ID);
  
  if ($token !== NULL) {
    $data['LOGINTOKEN'] = $token;
  }
  
  /*
  $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/tokenlogin-debug.log', "a") or die("Unable to open file!");
  fwrite($myfile, print_r($user, true) . "\n\n" . print_r($data, true) . "\n\n==============================\n\n");
  fclose($myfile);
  */
  
  return $data;
}
add_filter('mailchimp_sync_user_data', 'tokenlogin_mailchimp_token', 10, 2);
//add_filter( 'mc4wp_debug_log_level', function() { return 'debug'; } );

