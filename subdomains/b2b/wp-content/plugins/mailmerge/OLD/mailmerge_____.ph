<?php
/* 
Plugin Name: WordPress Mail Merge
Plugin URI: http://www.tada.dk
Description: Send mail to selected users. Based on mass-email-to-users.
Version: 0.0.1
Author: Mikkel Meyer Andersen
Author URI: http://www.tada.dk
License: GPL2
*/  
 
//error_reporting(0);
set_time_limit(5000);

add_action('admin_menu',    'mailmerge_plugin_menu');  

$OUTPUT_FILENAME = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/mailmerge-mails.txt';
$WWW_FILENAME = 'http://b2b.nordicspirits.dk/wp-content/uploads/mailmerge-mails.txt';
 
function mailmerge_plugin_menu() {
  $hook_suffix_mailmerge_p=add_menu_page(__('Mail Merge'), __("Mail Merge"), 'administrator', 'MailMerge','mailmerge_func');
  add_action( 'load-' . $hook_suffix_mailmerge_p , 'mailmerge_plugin_admin_init' );
}


function mailmerge_plugin_admin_init(){  	 
}

function mailmerge_send_mail($user_id) {
  global $OUTPUT_FILENAME;
  
  $user = get_user_by('id', $user_id);
  $company = trim(get_user_meta($user->ID, 'billing_company', true));
  $phone = get_user_meta($user->ID, 'billing_phone', true);
  $token = get_user_token($user_id); // tokenlogin

  $body = 'Kære ' . $company . ',

Vi har forsøgt at gøre livet lettere for dig og har derfor lavet en online bestiling af Skotlander Rum. Sammen med Nordisk Brænderi, som laver en dejlig gin, har vi lanceret Nordic Spirits, der er en paraply af nordiske spiritusdråber.

Fremover kan du logge ind på 
http://b2b.nordicspirits.dk
Brugernavn/e-mail-adresse: ' . $user->user_email . '
Adgangskode: ' . $phone . '

Du kan også benytte dette direkte link, der automatisk logger dig ind:
http://b2b.nordicspirits.dk/wp-login.php?token=' . $token . '

Du finder opdaterede forhandlerpriser og vi leverer dag til dag og ganske fragtfrit ved bestillinger for over kr. 2.000.

Jeg tillader mig at kontakte jer i løbet af et par uger og har du spørgsmål til brug af Nordic Spirits, vores produkter eller vil du bare have en hyggesludder, så er det bare at ringe til undertegnede på 26151514. 

Hav en dejlig dag :-).';
  
  $to = $user->user_email;
  
  if (strlen($company) > 0) {
    $to = $company . ' <' . $user->user_email . '>';
  }
  
  $myfile = fopen($OUTPUT_FILENAME, "a") or die("Unable to open file!");
  fwrite($myfile, $to . "\n\n" . $body . "\n\n------------------------------------------\n\n");
  fclose($myfile);
  
  return true;
}


function mailmerge_send_mail_OLD($user_id) {
  $user = get_user_by('id', $user_id);
  $company = trim(get_user_meta($user->ID, 'billing_company', true));
  $phone = get_user_meta($user->ID, 'billing_phone', true);
  $token = get_user_token($user_id); // tokenlogin

  $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Nordic Spirits</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
    <p>
    Kære ' . $company . ',
    </p>

    <p>
    Vi har forsøgt at gøre livet lettere for dig og har derfor lavet en online bestiling af Skotlander Rum. Sammen med Nordisk Brænderi, som laver en dejlig gin, har vi lanceret Nordic Spirits, der er en paraply af nordiske spiritusdråber.
    </p>

    <p>
    Fremover kan du logge ind på 
    <a href="http://b2b.nordicspirits.dk">http://b2b.nordicspirits.dk</a><br />
    Brugernavn/e-mail-adresse: ' . $user->user_email . '<br />
    Adgangskode: ' . $phone . '
    </p>

    <p>
    Du kan også benytte dette direkte link, der automatisk logger dig ind:<br />
    <a href="http://b2b.nordicspirits.dk/wp-login.php?token=' . $token . '">http://b2b.nordicspirits.dk/wp-login.php?token=' . $token . '</a>.
    </p>

    <p>
    Du finder opdaterede forhandlerpriser og vi leverer dag til dag og ganske fragtfrit ved bestillinger for over kr. 2.000.
    </p>

    <p>
    Jeg tillader mig at kontakte jer i løbet af et par uger og har du spørgsmål til brug af Nordic Spirits, vores produkter eller vil du bare have en hyggesludder, så er det bare at ringe til undertegnede på 26151514. 
    </p>

    <p>
    Hav en dejlig dag :-).
    </p>

    <p>
    --------------------<br />
    Gode hilsner / Best regards<br />
    <br />
    Anders Skotlander<br />
    SKOTLANDER RUM - Handcrafted Danish Rum<br />
    <br />
    <img src="cid:skotlander-logo" style="width: 60px" />
    <br />
    Phone (+45) 26151514<br />
    Skype: skotlander<br />
    <br />
    <a href="http://www.skotlander.com">www.skotlander.com</a>
    </p>  
</body>
</html>
  ';
  
  //<img src="http://b2b.nordicspirits.dk/wp-content/themes/storefront-child/images/logo.png" style="width: 60px" />
  
  $logo_path = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/storefront-child/images/logo.png';
  
  $file = $logo_path;
  $uid = 'skotlander-logo';
  $attachment_name = 'logo.png';

  global $phpmailer;
  add_action('phpmailer_init', function(&$phpmailer) use($file, $uid, $attachment_name) {
    $phpmailer->SMTPKeepAlive = true;
    $phpmailer->AddEmbeddedImage($file, $uid, $attachment_name);
  });
  
  $headers= "MIME-Version: 1.0\n" .
        "From: B2B Nordic Spirits <post@nordicspirits.dk>\n" .
        "Content-Type: text/html; charset=\"UTF-8\"\n";

  $to = $user->user_email;
  
  if (strlen($company) > 0) {
    $to = $company . ' <' . $user->user_email . '>';
  }
  
  //now just call wp_mail()
  //wp_mail($user->user_email, 'Hi John', $body, $headers);
  //return wp_mail('mikl@mikl.dk', 'Bestil Skotlander Rum online', $body, $headers);
  return wp_mail($to, 'Bestil Skotlander Rum online', $body, $headers);
}

function mailmerge_user_list_for_role($role_name, $role_label) {
  global $wpdb;
  
  //$query = "SELECT ID, user_email FROM $wpdb->users"; 
  //$results = $wpdb->get_results($query,'ARRAY_A');
  $results = get_users(array('role' => $role_name));
  $totalRecordForQuery = sizeof($results);
        
  if ($totalRecordForQuery == 0){
    //echo 'Ingen fundet.';
    return;
  }
  
  $msg_success = get_option('mailmerge_succ');
  update_option('mailmerge_succ', '');
 
  $msg_error = get_option('mailmerge_err');
  update_option('mailmerge_err', '');
  
  if ($msg_success != ""){ 
    echo '<div id="msg_success" class="notice"><p>' . $msg_success . '</p></div>';
    $msg_success = "";
  }
  
  if ($msg_error != ""){
    echo '<div id="msg_error" class="error"><p>' . $msg_error . '</p></div>';
    $msg_error = "";
  }
  
  echo '<h2>' . $role_label . '</h2>';
  
  echo '
      <table class="widefat fixed" cellspacing="0" style="width:97% !important" >
        <thead>
        <tr>
          <th scope="col" id="name" class="manage-column column-name" style="">
            <label><input onclick="chkAll_' . $role_name . '(this)" type="checkbox" id=\'chkall_header_' . $role_name . '\'>&nbsp;Vælg alle</label>
           </th>
          <th scope="col" id="name" class="manage-column column-name" style="">Brugernavn</th>
          <th scope="col">Firma</th>
          <th scope="col">CVR</th>
        </tr>
        </thead>

        <tfoot>
        <tr>
          <th scope="col" id="name" class="manage-column column-name" style="">
            <label><input onclick="chkAll_' . $role_name . '(this)" type="checkbox" id=\'chkall_footer_' . $role_name . '\'>&nbsp;Vælg alle</label>
          </th>
          <th scope="col" id="name" class="manage-column column-name" style="">Brugernavn</th> 
          <th scope="col">Firma</th>
          <th scope="col">CVR</th>
        </tr>
        </tfoot>

        <tbody id="the-list" class="list:cat">
  ';
  
  foreach($results as $user) {
    $company = get_user_meta($user->ID, 'billing_company', true);
    $cvr = get_user_meta($user->ID, 'billing_cvr', true);
    
    echo '
      <tr class="iedit alternate">
        <td class="name column-name" style="border:1px solid #DBDBDB;padding-left:13px;">
          <label><input type="checkBox" name="ckboxs[\'' . $role_name . '\'][]" value="' . $user->ID . '" />&nbsp;' . $user->user_email . '</label>
        </td>
        <td style="border:1px solid #DBDBDB;">' . $user->user_login . '</td>
        <td style="border:1px solid #DBDBDB;">' . $company . '</td>
        <td style="border:1px solid #DBDBDB;">' . $cvr . '</td>
      </tr>';
   }
   
   echo '
       </tbody>       
      </table>
    
  <script type="text/javascript">
    function chkAll_' . $role_name . '(id) {
      document.getElementById(\'chkall_header_' . $role_name . '\').checked = id.checked;
      document.getElementById(\'chkall_footer_' . $role_name . '\').checked = id.checked;
      
      var objs = document.getElementsByName("ckboxs[\'' . $role_name . '\'][]");
      
      for (var i = 0; i < objs.length; i++) {
        objs[i].checked = id.checked;
      }
    } 
  </script>
  ';
}

function mailmerge_user_list() {
  echo '<h1>Send oprettelsesinformations-e-mail til brugere (af TADA)</h1>';
  
  echo '
  <div class="error"><p>UNDER UDVIKLING!</p></div>  
  <div style="width: 100%;">  
    <form method="POST" action="" id="sendemail" name="sendemail">
       ' . wp_nonce_field('action_mailmerge_nonce','mailmerge_nonce') . '
      <input type="hidden" value="mailmerge_sendmail" name="action" id="action">
  ';


  $roles = get_editable_roles();
  
  foreach ($roles as $key => $value) {
    mailmerge_user_list_for_role($key, $value['name']);
  }
  
  echo '
      <p>      
        <input type="submit" value="Send oprettelsesinformations-e-mail" class="button-primary">  
      </p>
    </form>
  </div>
  <div class="error"><p>UNDER UDVIKLING!</p></div>  
  ';
}

function mailmerge_send_mails_to_selected_user() {
  global $OUTPUT_FILENAME;
  global $WWW_FILENAME;
  
  $retrieved_nonce = '';

  if (isset($_POST['mailmerge_nonce']) && $_POST['mailmerge_nonce'] != '') {
    $retrieved_nonce = $_POST['mailmerge_nonce'];
  }
  
  if (!wp_verify_nonce($retrieved_nonce, 'action_mailmerge_nonce')) {
    wp_die('Security check fail'); 
  }
  
  $user_ids = array();
  
  if (!isset($_POST) || !isset($_POST['ckboxs']) || !is_array($_POST['ckboxs']) || count($_POST['ckboxs']) == 0) {
    return;
  }
  
  foreach ($_POST['ckboxs'] as $arr) {
    if (!is_array($arr) || count($arr) == 0) {
      continue;
    }
    
    foreach ($arr as $user_id) {
      $user_ids[] = $user_id;
    }
  }

  $myfile = fopen($OUTPUT_FILENAME, "w") or die("Unable to open file!");
  fwrite($myfile, '');
  fclose($myfile);
  
  // In the case that users could end up having several roles (some plugins may support that)
  $user_ids = array_unique($user_ids);
  
  $n_sent_ok = 0;
  
  foreach ($user_ids as $user_id) {
    $res = mailmerge_send_mail($user_id);
      
    if ($res) {
      $n_sent_ok++;
    } 
  }

  if ($n_sent_ok == count($user_ids)){     
    update_option('mailmerge_succ', 'Alle ' . count($user_ids) . ' e-mails gemt succesfuldt i <a href="' . $WWW_FILENAME . '">' . $WWW_FILENAME . '</a>.');   
  } else{
    update_option('mailmerge_err', 'Fejl i afsendelsen af ' . (count($user_ids) - $n_sent_ok) . ' ud af ' . count($user_ids) . ' e-mails.');
  } 

  echo "<script>location.href='admin.php?page=MailMerge';</script>"; 
  exit;
}

function mailmerge_func() {
  $action = $_POST['action']; 
 
  switch($action) {
    case 'mailmerge_sendmail':
      mailmerge_send_mails_to_selected_user();
      break;
         
    default: 
      mailmerge_user_list();
      break;  
  }
} 

