TADA TEST <testopret@mikl.dk>


    Kære TADA TEST,
    
    Vi har forsøgt at gøre livet lettere for dig og har derfor lavet en online bestiling af Skotlander Rum. Sammen med Nordisk Brænderi, som laver en dejlig gin, har vi lanceret Nordic Spirits, der er en paraply af nordiske spiritusdråber.
    
    Fremover kan du logge ind på 
    <a href="http://b2b.nordicspirits.dk">http://b2b.nordicspirits.dk</a><br />
    Brugernavn/e-mail-adresse: testopret@mikl.dk<br />
    Adgangskode: 40622987
    
    Du kan også benytte dette direkte link, der automatisk logger dig ind:<br />
    http://b2b.nordicspirits.dk/wp-login.php?token=9104a4099cbcc5f5483eccef389c1ed4c960c6f89cf997342529bf11681dddef

    Du finder opdaterede forhandlerpriser og vi leverer dag til dag og ganske fragtfrit ved bestillinger for over kr. 2.000.

    Jeg tillader mig at kontakte jer i løbet af et par uger og har du spørgsmål til brug af Nordic Spirits, vores produkter eller vil du bare have en hyggesludder, så er det bare at ringe til undertegnede på 26151514. 

    Hav en dejlig dag :-).
  

------------------------------------------

