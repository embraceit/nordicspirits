<?php

add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);
function custom_variation_price( $price, $product ) {
  $available_variations = $product->get_available_variations();
  
  //echo '<!--';
  //print_r($product);
  //print_r($available_variations);
  //print_r($product->get_variation_prices( $include_taxes ));
  //echo '-->';
  
  $lowest = $product->min_variation_price;
  $highest = $product->max_variation_price;
  
  foreach ($available_variations as $var) {
    $descr = $var['attributes']['attribute_enkeltkolli'];
    $price = NULL;
    
    if (strpos(strtolower($descr), 'kolli') !== FALSE) {
      $kolli_pcs = (int)preg_replace('/^kolli.*([0-9]+).*betal for.*$/i', '${1}', $descr, $limit = 1);
      $price = $var['display_regular_price'] / $kolli_pcs;
    } else if (strpos(strtolower($descr), 'enkeltvis') !== FALSE) {
      $price = $var['display_regular_price'];
    }
    
    if (is_null($price)) {
      continue;
    }
    
    if ($price < $lowest) {
      $lowest = $price;
    }
    
    if ($price > $highest) {
      $highest = $price;
    }    
  }
  
  return '<span class="from">Stk. pris fra ' . wc_price($lowest) . ' </span>';

  /*
  $price = '';

  if ( !$product->min_variation_price || $product->min_variation_price !== $product->max_variation_price ) {
    $price .= '<span class="from">' . _x('Fra', 'min_price', 'woocommerce') . ' </span>';
  }

  $price .= woocommerce_price($product->get_price());

  if ( $product->max_variation_price && $product->max_variation_price !== $product->min_variation_price ) {
      $price .= '<span class="to"> ' . _x('-', 'max_price', 'woocommerce') . ' </span>';

      $price .= woocommerce_price($product->max_variation_price);
  }

  return $price;
  */
}

/*
function filter_woocommerce_get_variation_price($price, $product) { 
    return 66; 
}; 
//add_filter( 'woocommerce_get_variation_price', 'filter_woocommerce_get_variation_price', 10, 2 );
*/

function filter_woocommerce_get_variation_price($price, $product) { 
  /*  
  echo '<!--';
  print_r($product);
  echo '-->';
  */
  return $price; 
}; 
//add_filter( 'woocommerce_get_variation_price', 'filter_woocommerce_get_variation_price', 10, 2 );


/*
add_filter( 'woocommerce_variation_option_name', 'display_price_in_variation_option_name' );

function display_price_in_variation_option_name( $term ) {
global $wpdb, $product;

$result = $wpdb->get_col( "SELECT slug FROM {$wpdb->prefix}terms WHERE name = '$term'" );

$term_slug = ( !empty( $result ) ) ? $result[0] : $term;

$query = "SELECT postmeta.post_id AS product_id
FROM {$wpdb->prefix}postmeta AS postmeta
LEFT JOIN {$wpdb->prefix}posts AS products ON ( products.ID = postmeta.post_id )
WHERE postmeta.meta_key LIKE 'attribute_%'
AND postmeta.meta_value = '$term_slug'
AND products.post_parent = $product->id";

$variation_id = $wpdb->get_col( $query );

$parent = wp_get_post_parent_id( $variation_id[0] );

if ( $parent > 0 ) {
$_product = new WC_Product_Variation( $variation_id[0] );
return $term . ' (' . woocommerce_price( $_product->get_price() ) . ')';
}
return $term;

}
*/

/*
 //Add prices to variations 
 add_filter( 'woocommerce_variation_option_name', 'display_price_in_variation_option_name' ); 
 function display_price_in_variation_option_name( $term ) { global $wpdb, $product; $result = $wpdb->get_col( "SELECT slug FROM {$wpdb->prefix}terms WHERE name = '$term'" ); $term_slug = ( !empty( $result ) ) ? $result[0] : $term; $query = "SELECT postmeta.post_id AS product_id FROM {$wpdb->prefix}postmeta AS postmeta LEFT JOIN {$wpdb->prefix}posts AS products ON ( products.ID = postmeta.post_id ) WHERE postmeta.meta_key LIKE 'attribute_%' AND postmeta.meta_value = '$term_slug' AND products.post_parent = $product->id"; $variation_id = $wpdb->get_col( $query ); $parent = wp_get_post_parent_id( $variation_id[0] ); if ( $parent > 0 ) { $_product = new WC_Product_Variation( $variation_id[0] ); 
 //this is where you can actually customize how the price is displayed 
 return $term . ' (' . woocommerce_price( $_product->get_price() ) . ')'; } 
 return $term; } 
 
*/





