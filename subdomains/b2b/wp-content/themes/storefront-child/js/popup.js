/*
jQuery(document).ready(function($) {
    function isScrolledTo(elem) {
        var docViewTop = $(window).scrollTop(); //num of pixels hidden above current screen
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top; //num of pixels above the elem
        var elemBottom = elemTop + $(elem).height();
        return ((elemTop <= docViewBottom)); //if the bottom of the current viewing area is lower than the top of the trigger
    }
    var trigger = $('#site-navigation');    //set the trigger
    var reached = false;

    $(window).scroll(function() {
        if(isScrolledTo(trigger)) {
            //slide CTA onto screen
            if(!reached){
                $('#slider').animate({
                    bottom: 0
                }, "fast");
                reached = true;
            }
        }
        
        if (!isScrolledTo(trigger)) {
            //slide CTA off of screen
            if(reached){
                $('#slider').animate({
                    bottom: -200
                }, "fast");
                reached = false;
            }
        }
    });
});
*/

jQuery(document).ready(function($) {
  $('#slider').animate({ bottom: 0 }, "fast");
  $('#slider-close').click(function() {
    $('#slider').animate({ bottom: -375 }, "fast");
  });
});

