<?php
function customer_is_key_account_with_special_price() {
  $user = wp_get_current_user();
  
  // mikl
  if ($user->ID == 1) {
    //return true;
  }
  
  // tada
  if ($user->ID == 494) {
    return false;
  }
  
  // Frederik
  if ($user->ID == 35) {
    return false;
  }
  
  $user_roles = (array)$user->roles;
  
  if (in_array('key_account', $user_roles)) {
    return true;
  }
  
  return false;
}

function customer_exempt_from_invoice_fee() {
  $user = wp_get_current_user();
  
  $user_roles = (array)$user->roles;
  
  if (in_array('fritag_fakturagebyr', $user_roles)) {
    return true;
  }
  
  return false;
}



/*********************************************************/

function show_message_free_shipping_buy_more($total) {
  //echo '<!-- mikl ' . print_r(WC()->cart, true) . ' -->';  
  if ($total < 2000) {
    $total_to_free = 2000 - $total;
    
    
    //echo '<div class="woocommerce-info"><strong>Hvis du handler for yderligere ' . wc_price($total_to_free) . '  (efter fratrækkelse af rabatter) kan du få gratis fragt</strong>.<br />(Fragtprisen er ' . wc_price(150) . ', men der er gratis fragt på ordrer over ' . wc_price(2000) . ' efter fratrækkelse af rabatter.)</div>';
    $content_new = '<h4 style="color: white">GRATIS FRAGT?</h4><strong>Hvis du handler for yderligere ' . wc_price($total_to_free) . '  (efter fratrækkelse af rabatter) kan du få gratis fragt</strong>.<br />(Fragtprisen er ' . wc_price(150) . ', men der er gratis fragt på ordrer over ' . wc_price(2000) . ' efter fratrækkelse af rabatter.)';
    
    ////////////////////////////////////////////////////////////////////////////    
    // https://blog.hartleybrody.com/adding-subtle-pop-ups-to-your-website/
    //echo '<div id="slider">' . $content_new . '</div>';      
    echo '<div id="slider"><span id="slider-close"><img src="' . get_stylesheet_directory_uri() . '/images/close.png' . '" /></span> ' . $content_new . '</div>';
    wp_register_script('nordicspirits-popup', get_stylesheet_directory_uri() . '/js/popup.js', array('jquery'));
    wp_enqueue_script('nordicspirits-popup');
    ////////////////////////////////////////////////////////////////////////////
  }
}

function nordic_spirits_cart_message_free_shipping() {
  if ($GLOBALS['post']->post_content == '[woocommerce_cart]') {
    //$total = WC()->cart->total/1.25;
    $total = WC()->cart->cart_contents_total;
    show_message_free_shipping_buy_more($total);
  }
}
add_action( 'woocommerce_before_cart_table', 'nordic_spirits_cart_message_free_shipping', 10, 0 );


function nordic_spirits_checkout_message_free_shipping() {
//  print_r($GLOBALS['post']);
  if ( ! is_ajax() ) {
    if ($GLOBALS['post']->post_content == '[woocommerce_checkout]') {
      //$total = WC()->cart->total/1.25;
      $total = WC()->cart->cart_contents_total;
      show_message_free_shipping_buy_more($total);
    }
  }
}
add_action( 'woocommerce_review_order_before_cart_contents', 'nordic_spirits_checkout_message_free_shipping', 10, 0 );




function filter_woocommerce_coupon_message( $msg, $msg_code, $instance ) { 
  return '';
}         
add_filter( 'woocommerce_coupon_message', 'filter_woocommerce_coupon_message', 10, 3 );

function filter_woocommerce_coupon_error( $err, $err_code, $instance ) { 
  return '';
}
add_filter( 'woocommerce_coupon_error', 'filter_woocommerce_coupon_error', 10, 3 );  


function nordic_spirits_gratis_fragt_coupon() {
  $coupon_code = 'gratis-fragt';
  //$total = WC()->cart->total/1.25;
  $total = WC()->cart->cart_contents_total;
  
  //echo '<!-- mikl ' . $total . ' -->';  
  
  if ($total < 2000) {
    WC()->cart->remove_coupon($coupon_code);
    return;
  }

  if (WC()->cart->has_discount($coupon_code)) {
    return;
  }

  WC()->cart->add_discount($coupon_code);
}
//add_action( 'woocommerce_cart_updated', 'nordic_spirits_gratis_fragt_coupon', 11, 0 );




function nordic_spirits_gratis_flaske_5000() {
  $coupon_code = 'gratis-flaske';
  
  if (customer_is_key_account_with_special_price()) {
    WC()->cart->remove_coupon($coupon_code);
    return;
  }
  
  //$total = WC()->cart->total/1.25;
  $total = WC()->cart->cart_contents_total;
  $coupon = new WC_Coupon($coupon_code);
  
  //echo '<!-- mikl ' . $total . ' -->';  
  
  //return;
  
  //if ($total < 5000) {
  if ($total < 2000) {
    WC()->cart->remove_coupon($coupon_code);
    return;
  } else {
    if (WC()->cart->has_discount($coupon_code)) {
      // no-op
    } else {
      // add discount if discounted product is in cart
      $do_discount = false;
      
      foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {            
        $product = $cart_item['data'];
        if (in_array($product->id, $coupon->product_ids)) {
          $do_discount = true;
          break;
        }
      }
      
      if ($do_discount) {
        WC()->cart->add_discount($coupon_code);
      }

      return;
    }
  }    
}
// 5000KAMPAGNE: udkommenteret midlertidigt 2017-08-08
//add_action( 'woocommerce_cart_updated', 'nordic_spirits_gratis_flaske_5000', 12, 0 );



function nordic_spirits_fritag_fakturagebyr() {
  if (customer_exempt_from_invoice_fee()) {
    return;
  }
  
  //if (WC()->session->get('chosen_payment_method') == 'cod') {
  $payment_method = WC()->session->get('chosen_payment_method');
  if ($payment_method == 'bacs') {
    WC()->cart->add_fee('Fakturagebyr', 100, TRUE, '*');
  }
  
  if ($payment_method == 'cod') {
    //WC()->cart->add_fee('Gebyr for Leverandørservice', 100, TRUE, '*');
  }
  
  return;
}
add_action( 'woocommerce_cart_calculate_fees', 'nordic_spirits_fritag_fakturagebyr');






function show_message_free_gratis_flaske_buy_more($total) {
  if (customer_is_key_account_with_special_price()) {    
    return;
  }  
  
  $coupon_code = 'gratis-flaske';
  /*
  
  */
  // Martin, mail 2017-10-09 @ 9:30
  if ($total >= 2000 && !WC()->cart->has_discount($coupon_code)) {
  //if ($total >= 2000 && $total < 5000) {
    $coupon = new WC_Coupon('gratis-flaske');
        
    //$total_to_free = 5000 - $total;
    
    ///////////////////////////////
    
    $content = '<div class="woocommerce-info"><h4 style="color: white">KAMPAGNE</h4><strong>Køb for minimum 2.000 DKK (efter fratrækkelse af rabatter) og få en gratis flaske af et af nedenstående produkter</strong>:';
    
    $content_new = '<h4 style="color: white">KAMPAGNE</h4><strong>Køb for minimum 2.000 DKK (efter fratrækkelse af rabatter) og få en gratis flaske af et af nedenstående produkter</strong>:';
    
    /*
    echo '<!-- mikl ';
    echo '-->';
    */
  
    $ids = $coupon->product_ids;
    
    $content .= '<ul>';
    //$content_new .= '<ul>';
    $content_new .= '<table><tr>';
    foreach ($ids as $id) {
      $product = new WC_Product($id);
      //$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');
      $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'thumbnail');
      $imghtml = '';
      
      if (count($image) > 0 && !is_null($image[0]) && strlen($image[0]) > 0) {
        $imghtml = '<img src="' . $image[0] . '" data-id="' . $id . '" />';
      }      
      
      
      $content .= '<li><a href="' . $product->post->guid . '">' . $imghtml . ' ' . $product->post->post_title . '</a><br /><br /><a href="' . do_shortcode('[add_to_cart_url id="' . $id . '"]') . '">Tryk for at tilføje 1 flaske til kurven</a></li>';
      //$content_new .= '<li><a href="' . $product->post->guid . '">' . $product->post->post_title . '</a> - <a href="' . do_shortcode('[add_to_cart_url id="' . $id . '"]') . '">tryk for at tilføje 1 flaske til kurven</a></li>';
      
      //<button type="submit" name="add-to-cart" value="2146" class="single_add_to_cart_button button alt">Tilføj til kurv</button>
      
      //$content_new .= '<td style="background-color: #cccccc;"><a href="' . $product->post->guid . '">' . $imghtml . ' ' . $product->post->post_title . '</a><br /><br /><a href="' . do_shortcode('[add_to_cart_url id="' . $id . '"]') . '">Tryk for at tilføje 1 flaske til kurven</a></td>' . "\n";
      $content_new .= '<td style="background-color: #cccccc;"><a href="' . $product->post->guid . '">' . $imghtml . ' ' . $product->post->post_title . '</a><br /><br /><button type="submit" name="add-to-cart" value="' . $id . '" class="single_add_to_cart_button button alt">Tilføj til kurv</button></td>' . "\n";
      //$content_new .= '<td style="background-color: #cccccc;"><a href="' . $product->post->guid . '">' . $product->post->post_title . '</a> - <a href="' . do_shortcode('[add_to_cart_url id="' . $id . '"]') . '">tryk for at tilføje 1 flaske til kurven</a></td>' . "\n";
    }
    $content .= '</ul>';    
    $content .= '</div>';
    
    //$content_new .= '</ul>';
    $content_new .= '</tr></table>';
    
    //echo $content;
    
    ////////////////////////////////////////////////////////////////////////////    
    // https://blog.hartleybrody.com/adding-subtle-pop-ups-to-your-website/
    echo '<div id="slider"><span id="slider-close"><img src="' . get_stylesheet_directory_uri() . '/images/close.png' . '" /></span> ' . $content_new . '</div>';
    wp_register_script('nordicspirits-popup', get_stylesheet_directory_uri() . '/js/popup.js', array('jquery'));
    wp_enqueue_script('nordicspirits-popup');
    ////////////////////////////////////////////////////////////////////////////
  }
}

function nordic_spirits_cart_message_gratis_flaske() {
  if ($GLOBALS['post']->post_content == '[woocommerce_cart]') {
    //$total = WC()->cart->total/1.25;
    $total = WC()->cart->cart_contents_total;
    show_message_free_gratis_flaske_buy_more($total);
  }
}
//5000KAMPAGNE: indkøbskurv, deaktiveret 2017-08-08 midlertidigt
add_action( 'woocommerce_before_cart_table', 'nordic_spirits_cart_message_gratis_flaske', 10, 0 );

function nordic_spirits_checkout_message_gratis_flaske() {
  if ( ! is_ajax() ) {
    if ($GLOBALS['post']->post_content == '[woocommerce_checkout]') {
      //$total = WC()->cart->total/1.25;
      $total = WC()->cart->cart_contents_total;
      show_message_free_gratis_flaske_buy_more($total);
    }
  }
}
//checkout , keep it commented out!!!
//add_action( 'woocommerce_review_order_before_cart_contents', 'nordic_spirits_checkout_message_gratis_flaske', 10, 0 );





















// https://www.themelocation.com/limit-purchase-quantity-of-products-of-a-category-in-woocommerce/
function check_total() {
  // Only run in the Cart or Checkout pages
  if(!is_cart() && !is_checkout() ) {
    return;
  }

  global $woocommerce, $product;

  $total_quantity = 0;
  $display_notice = 1;
  $i = 0;
  //loop through all cart products
  foreach ( $woocommerce->cart->cart_contents as $product ) {

    // See if any product is from the samples category or not
    if (has_term('samples', 'product_cat', $product['product_id'])) {
      $total_quantity += $product['quantity'];
    }
  }
  
  if ($total_quantity <= 1) {
    $display_notice = 0;
  } 
  
  $products_error = array();

  foreach ( $woocommerce->cart->cart_contents as $product ) {
    if (!has_term('samples', 'product_cat', $product['product_id'])) {
      continue;
    }
    
    if ($display_notice != 1) {
      continue;
    }
    
    $product_title = wc_get_product($product['product_id'])->get_title();
    $products_error[] = $product_title;
  }
  
  if (count($products_error) > 0) {
    wc_add_notice(sprintf("Der er kun mulighed for 1 gratis sample per ordre. Venligst ret antallet så der kun er 1 stk. af en af de valgte [%s] i kurven.", implode(', ', $products_error)), 'error');
  }
}
add_action( 'woocommerce_check_cart_items', 'check_total' );


