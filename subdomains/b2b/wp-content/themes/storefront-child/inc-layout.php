<?php


function nordicspirits_remove_storefront_header_search() {
  remove_action( 'storefront_sidebar', 'storefront_get_sidebar',			10 );
  //remove_action( 'storefront_header', 'storefront_product_search', 	40 );
  //remove_action( 'storefront_header', 'storefront_header_cart', 60 );
  
  remove_action( 'storefront_content_top', 'woocommerce_breadcrumb', 	10 );
}

add_action( 'init', 'nordicspirits_remove_storefront_header_search' );

function nordicspirits_scripts_styles() {
  /*
  wp_register_script('flexslider', './wp-content/themes/' . get_stylesheet() . '/js/jquery.flexslider-min.js', '2014-12-30', true);
  wp_enqueue_script('flexslider');
  
  wp_enqueue_style( 'flexslider', './wp-content/themes/' . get_stylesheet() . '/css/flexslider.css');
  */
  
  /*
  wp_register_script('imgcarousel', './wp-content/themes/' . get_stylesheet() . '/js/jquery.jcarousel.min.js', '2014-12-30', true);
  wp_enqueue_script('imgcarousel');
  
  wp_enqueue_style( 'imgcarouselcss', './wp-content/themes/' . get_stylesheet() . '/css/jcarousel.basic.css');
  */

}
add_action('wp_enqueue_scripts', 'nordicspirits_scripts_styles');


function nordicspirits_custom_javascript() {
  wp_dequeue_script('storefront-sticky-payment');
}
add_filter('wp_enqueue_scripts','nordicspirits_custom_javascript', 21); // storefront adds at priority 20


//https://wordpress.org/support/topic/how-to-remove-remove-designed-by-woothemes-from-footer
function custom_remove_footer_credit(){
    return false; //return true to show it.
}
add_filter('storefront_credit_link','custom_remove_footer_credit',10);

function nordicspirits_carussel() {
  //<div style="background: linear-gradient(to bottom, #f4f4f4 0%, #f4f4f4 50%, #fcfcfc 50%, #fcfcfc 100%);">
/*
  echo '
    <div style="background-color: #fcfcfc; padding-top: 1em;">
      <a href="/produkt-kategori/gin/">
        <img src="/wp-content/themes/' . get_stylesheet() . '/slider/test 3.jpg" class="aligncenter" />
      </a>
    </div>
  ';
*/

/*
    <div id="container" style="background-color: #fcfcfc; padding-top: 1em; text-align: center; ">
    
    
    </div>
    
    https://www.woothemes.com/flexslider/
*/



  $imgs = array('<a href="/produkt/skotlander-rum-5/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel1.jpg" /></a>',
                '<a href="/produkt/den-klodsede-bjoern-vodka/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel2.jpg" /></a>',
//                '<a href="/produkt-kategori/pre-bottled/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel3.jpg" /></a>',
                '<a href="/produkt-kategori/gin/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel4.jpg" /></a>');
                
  $img = $imgs[array_rand($imgs)];


  $uid = get_current_user_id();
  $welcome = '';
  
  if ($uid !== FALSE && $uid > 0) {    
    $company = get_user_meta($uid, 'billing_company', true);

    $welcome = '
    <span class="hide-small" style="float: left;">
      <span style="color: #558e54; font-size: 2em;">Velkommen, ' . $company . '</span>.
    </span>
    ';
  }
  

  foreach ($imgs as $i => $img) {
    echo '<span class="carousel-srcs" style="display: none;">' . $img . '</span>';    
  }
  $show_i = array_rand($imgs);
  
  echo '
        <div class="hide-small" id="carousel" style="margin-top: 1em; padding-bottom: 1em;">
        ' . $imgs[$show_i] . '
        </div>
      ';
      
  
  echo $welcome;
  
/*
   echo '
          <div class="hide-small" style="margin-top: 1em; padding-bottom: 1em;">
            ' . $img . '
          </div>
        ';
*/
/*
  echo '
        <div class="jcarousel-wrapper">
          <div class="jcarousel">
            <ul>
              <li>
    	    	    <a href="/produkt/skotlander-rum-5/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel1.jpg" /></a>
    	    		</li>
    	    		<li>
    	    	    <a href="/produkt/den-klodsede-bjoern-vodka/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel2.jpg" /></a>
    	    		</li>
    	    		<li>
    	    	    <a href="/produkt-kategori/pre-bottled/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel3.jpg" /></a>
    	    		</li>
    	    		<li>
    	    	    <a href="/produkt-kategori/gin/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel4.jpg" /></a>
    	    		</li>
            </ul>
          </div>          
        </div> 
  ';

   */
/*
  echo '
        <div class="flexslider">
          <ul class="slides">
            <li>
  	    	    <a href="/produkt/skotlander-rum-5/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel1.jpg" /></a>
  	    		</li>
  	    		<li>
  	    	    <a href="/produkt/den-klodsede-bjoern-vodka/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel2.jpg" /></a>
  	    		</li>
  	    		<li>
  	    	    <a href="/produkt-kategori/pre-bottled/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel3.jpg" /></a>
  	    		</li>
  	    		<li>
  	    	    <a href="/produkt-kategori/gin/"><img src="/wp-content/themes/' . get_stylesheet() . '/slider/karussel4.jpg" /></a>
  	    		</li>
          </ul>
        </div>
  
  ';
   */
   
   
  
  
  storefront_product_search(); 

}
//add_action( 'storefront_before_content', 'storefront_header_cart', 6 );


function nordicspirits_script_footer(){ 
  echo '
    <script type="text/javascript">
      (function($) {
        setInterval(function () {
          var n = $(".carousel-srcs").children().size();
          //var show_i = 1;
          var show_i = Math.floor(Math.random() * n);

          var oldimg = $("#carousel").children().get(0);
          var newimg = $(".carousel-srcs").children().get(show_i);
          
          while (show_i >= n || $(newimg).attr("href") == $(oldimg).attr("href")) {
            show_i = Math.floor(Math.random() * n);
            newimg = $(".carousel-srcs").children().get(show_i);
          }

          /*
          console.log(n);
          console.log(show_i);
          console.log(newimg);
          console.log($(newimg).attr("href"));
          console.log(oldimg);
          console.log($(oldimg).attr("href"));
          */
          
          $("#carousel").fadeOut("slow", function(){
              $(this).html($(newimg).clone());
              $("#carousel").fadeIn("slow");
          });
        }, 5000);
      })(jQuery);
    </script>  
  ';
} 
//2016-10-28
//add_action( 'storefront_header', 'nordicspirits_carussel', 51);
//add_action('wp_footer', 'nordicspirits_script_footer', 20);


/*
function nordicspirits_script_footer(){ 
  echo "
    <script type=\"text/javascript\">
      (function($) {
          $(function() {
              $('.jcarousel').jcarousel();

              $('.jcarousel-control-prev')
                  .on('jcarouselcontrol:active', function() {
                      $(this).removeClass('inactive');
                  })
                  .on('jcarouselcontrol:inactive', function() {
                      $(this).addClass('inactive');
                  })
                  .jcarouselControl({
                      target: '-=1'
                  });

              $('.jcarousel-control-next')
                  .on('jcarouselcontrol:active', function() {
                      $(this).removeClass('inactive');
                  })
                  .on('jcarouselcontrol:inactive', function() {
                      $(this).addClass('inactive');
                  })
                  .jcarouselControl({
                      target: '+=1'
                  });

              $('.jcarousel-pagination')
                  .on('jcarouselpagination:active', 'a', function() {
                      $(this).addClass('active');
                  })
                  .on('jcarouselpagination:inactive', 'a', function() {
                      $(this).removeClass('active');
                  })
                  .jcarouselPagination();
          });
      })(jQuery);
    </script>  
  ";
} 
*/
/*
function nordicspirits_script_footer(){ 
  echo "
    <script type=\"text/javascript\">
      (function($) {
          $(function() {
              $('.flexslider').flexslider({
                animation: \"slide\"
              });
          });
      })(jQuery);
    </script>  
  ";
} 
*/
//add_action('wp_footer', 'nordicspirits_script_footer', 20);



function nordicspirits_add_user_company() {
  $uid = get_current_user_id();
  
  if ($uid === FALSE || $uid < 0) { 
    return;
  }

  $company = get_user_meta($uid, 'billing_company', true);;
  
  echo '
  <span style="float: left;">
    <span style="color: #558e54; font-size: 2em;">Velkommen, ' . $company . '</span>.
  </span>
  ';

  /*
    echo '
  <div style="position:relative; clear: both; margin-bottom: -2em">
    <h3 style="color: #558e54">Velkommen, ' . $company . '.</h3>
  </div>
  ';
  */
    /*
  echo '
  <div class="site-branding">
    <h1 class="site-title">
      <a href="http://b2b.nordicspirits.dk/" rel="home">B2B Nordic Spirits</a>
    </h1>
	  <p class="site-description">Online distribution til specialhandel</p>
	</div>
  ';
  */
  /*
  echo '
  <div style="position: relative; clear: both; margin-bottom: -3em">
    <h3 style="color: #558e54">Velkommen, ' . $company . '.</h3>
    <h4><a href="./my-account/">Jeres konto</a></h4>
  </div>
  ';
  */ 
}
add_action('storefront_header', 'nordicspirits_add_user_company', 21); // @hooked storefront_site_branding - 20

/*
* @hooked storefront_site_branding - 20
* @hooked storefront_secondary_navigation - 30
* @hooked storefront_product_search - 40
* @hooked storefront_primary_navigation - 50
* @hooked storefront_header_cart - 60
*/
			
