<?php

// Remove ePay's (and all others') logo
function filter_woocommerce_gateway_icon($icon) { 
  /*
  echo '<!-- mikl test -->';
  echo '<!-- ';
  echo $icon;
  echo '-->';
  */
  return '';
}
         
// add the filter 
add_filter( 'woocommerce_gateway_icon', 'filter_woocommerce_gateway_icon', 10, 1); 


function output_total_excluding_tax() {
  ?>
  <tr class="total_ex_tax">
    <th>Total (excl. moms)</th>
    <td><?php echo WC()->cart->get_total_ex_tax(); ?></td>
  </tr>
  <?php
}
add_action( 'woocommerce_cart_totals_before_order_total', 'output_total_excluding_tax' );
add_action( 'woocommerce_review_order_before_order_total', 'output_total_excluding_tax' );


function translate_text($translated) { 
  if ($translated == 'Total') {
    //return 'Total (incl. moms)';
  } 
  
  //$translated = str_ireplace('Total', 'Total (excl. moms)', $translated); 
  return $translated; 
}

add_filter('gettext', 'translate_text'); 
add_filter('ngettext', 'translate_text');


//////////////////
// change COD payment method order status from processing to on-hold
add_action('woocommerce_thankyou_cod', 'action_woocommerce_thankyou_cod', 10, 1);

function action_woocommerce_thankyou_cod($order_id) {
	$order = wc_get_order($order_id);
	$order->update_status('on-hold');
}


