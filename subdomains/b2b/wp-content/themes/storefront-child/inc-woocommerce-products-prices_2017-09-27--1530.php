<?php
function get_ketaccountprice($product) {
  $pricing_rules = get_post_meta($product->id, '_pricing_rules', TRUE);
  
  $ketaccountprice = NULL;
  $min = NULL;
  $max = NULL;
  
  if (!is_null($pricing_rules) && count($pricing_rules) > 0) {
    foreach ($pricing_rules as $val) {
      if (is_null($val['conditions'])) {
        continue;
      }
      
      $key_account_rule = FALSE;
      
      foreach ($val['conditions'] as $con) {
        if (is_null($con['type']) || $con['type'] != 'apply_to') {
          continue;
        }
        
        if (is_null($con['args']) || 
            is_null($con['args']['applies_to']) || $con['args']['applies_to'] != 'roles' ||
            is_null($con['args']['roles']) || is_null($con['args']['roles'][0]) || $con['args']['roles'][0] != 'key_account') {
          continue;
        }
        
        $key_account_rule = TRUE;
        //break;
      }
      
      if (!$key_account_rule) {
        continue;
      }
      
      if (is_null($val['rules']) || 
          is_null($val['rules'][1]) || $val['rules'][1]['type'] != 'fixed_price') {
        continue;
      }
      
      $ketaccountprice = $val['rules'][1]['amount'];
      
      if (is_null($min) || $min > $ketaccountprice) {
        $min = $ketaccountprice;
      }
      
      if (is_null($max) || $max < $ketaccountprice) {
        $max = $ketaccountprice;
      }
    }
  }  
  
  /*
  if ($ketaccountprice == $product->get_price()) {
    return NULL;
  }
  
  return $ketaccountprice;
  */
  return array($min, $max);
}

function get_kolliprice($product) {
  $pricing_rules = get_post_meta($product->id, '_pricing_rules', TRUE);  
  $kolliprice = NULL;

  
  if (!($product->is_type('simple'))) {
    return NULL;
  }
  
  //print_r($pricing_rules);
  /*
  if (!is_array($pricing_rules)) {
    print_r($pricing_rules);
    die;  
  }
  */
  
  if (is_null($pricing_rules) || !is_array($pricing_rules) || count($pricing_rules) == 0) {
    return NULL;
  }
  
  foreach ($pricing_rules as $val) {
    if (is_null($val['conditions'])) {
      continue;
    }
    
    $everyone_rule = FALSE;
    
    foreach ($val['conditions'] as $con) {
      if (is_null($con['type']) || $con['type'] != 'apply_to') {
        continue;
      }
      
      if (is_null($con['args']) || 
          is_null($con['args']['applies_to']) || $con['args']['applies_to'] != 'everyone') {
        continue;
      }
      
      $everyone_rule = TRUE;
      break;
    }
    
    if (!$everyone_rule) {
      continue;
    }
    
    
    if (is_null($val['blockrules']) || 
        is_null($val['blockrules'][1]) || 
          $val['blockrules'][1]['type'] != 'percent_adjustment' || 
          $val['blockrules'][1]['amount'] != '100' || 
          $val['blockrules'][1]['repeating'] != 'yes') {            
      continue;
    }
    
    $from = $val['blockrules'][1]['from'];
    $adjust = $val['blockrules'][1]['adjust'];
    
    $req_pcs = $from + $adjust;
    $free_pcs = $adjust;

    $kolliprice = array('req_pcs' => $req_pcs, 'free_pcs' => $free_pcs);
  }
  
  return $kolliprice;
}

function custom_price_html($price, $product){
  /* First, check key account and state normal price */
  $ketaccountprice = NULL;
  
  //echo "\n<!-- mikl 20170927 -->\n";
  
  if (customer_is_key_account_with_special_price()) {
    $ketaccountprice = get_ketaccountprice($product);
  }
  
  // 2017-09-27 FIXME
  /*
  pris for valgt variant på produktside virker ikke...
  */
  if (FALSE) {
  //if (!is_null($ketaccountprice)) {
    $price_extra = '<del>' . $price . '</del>';
    
    //$price =  $price_extra . '<br />' . wc_price($ketaccountprice) . ' (key account)';
    
    if (count($ketaccountprice) == 1 || $ketaccountprice[0] == $ketaccountprice[1]) {
      $price =  $price_extra . '<br />' . wc_price($ketaccountprice[0]) . ' (key account)';    
    } else {
      $price =  $price_extra . '<br />' . wc_price($ketaccountprice[0]) . ' - ' . wc_price($ketaccountprice[1]) . ' (key account)';    
    }
    //return $price;
    
    return apply_filters('get_price', $price);
  }
  
  ////////////////
  
  /* First, check key account and state normal price */
  $kolliprice = get_kolliprice($product);
  
  if (!is_null($kolliprice)) {
    $req_pcs = $kolliprice['req_pcs'];
    $free_pcs = $kolliprice['free_pcs'];
    $new_price = ceil(((($req_pcs - $free_pcs)*$product->price)/$req_pcs));
    
    $price_extra = ' (v. ' . $req_pcs . ' stk.: ' . apply_filters('get_price', $new_price) . ',00 DKK)';
  
    //echo '<!-- ' . $price . '<br />' . $price_extra . '-->';
    $price = $price . '<br />' . $price_extra;
    return apply_filters('get_price', $price);
  }
  
  return apply_filters('get_price', $price);
}

add_filter( 'woocommerce_get_price_html', 'custom_price_html', 100, 2 );



/*
add_filter( 'woocommerce_variable_price_html', 'wc_wc20_variation_price_format', 10, 2 );
function wc_wc20_variation_price_format( $price, $product ) {
	$min_price = $product->get_variation_price( 'min', true );
	$max_price = $product->get_variation_price( 'max', true );
	if ($min_price != $max_price){
		$price = 'From $'.$product->get_price();
		return $price;
	}
}
*/

//add_filter( 'woocommerce_variable_price_html', 'wc_wc20_variation_price_format', 10, 2 );
function wc_wc20_variation_price_format($price, $product) {
  //return '';
  
  if ($product->is_type('simple')) {
    return $price;
  }
  
  /*
  $variations1 = $product->get_available_variations();
  foreach ($variations1 as $value) {
    $single_variation = new WC_Product_Variation($value);
    echo "<li>" . $single_variation->regular_price . "</li>\n";
  }
  echo "\n<!-- mikl\n";
  print_r($product);
  echo "-->\n";
  */
  
	$min_price = $product->get_variation_price('min', true);
	$max_price = $product->get_variation_price('max', true);
	
	if ($min_price != $max_price){
		$price = wc_price($min_price) . ' - '. wc_price($max_price);
		return $price;
	}
	
	return $price;
}


