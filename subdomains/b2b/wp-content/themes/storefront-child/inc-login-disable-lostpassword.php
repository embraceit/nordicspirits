<?php

function nordicspirits_allow_password_reset() { return false; }
add_filter('allow_password_reset', 'nordicspirits_allow_password_reset');
add_filter('login_errors', create_function('$a', "return 'Der kunne ikke logges ind med de angivne oplysninger. Kontakt os venligst.';"));

