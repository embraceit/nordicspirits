<?php

/*
 https://wordpress.org/support/topic/display-custom-billing-fields-in-backend-user-profile
 https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 http://rahuljalavadiya.blogspot.com/2015/05/how-to-add-custom-checkout-fields-in.html
 https://support.woothemes.com/hc/en-us/articles/203182373-How-to-add-custom-fields-in-user-registration-on-the-My-Account-page
 */
function nordicspirits_woocommerce_billing_fields_admin($profile_fields) {
  // Remove shipping:
  //unset($profile_fields['shipping']);
  
	// Add new fields
	$profile_fields['billing']['fields']['billing_cvr'] = array( 'label' => 'CVR', 'description' => '' );
	//$profile_fields['billing']['fields']['billing_butik'] = array( 'label' => 'Butik', 'description' => '' );
	
	$profile_fields['billing']['fields']['billing_invoiceemail'] = array( 'label' => 'Faktura e-mailadresse', 'description' => 'Faktura sendes til e-mailadressen' );
	return $profile_fields;
}
add_filter('woocommerce_customer_meta_fields', 'nordicspirits_woocommerce_billing_fields_admin');


/**
 * Display field value on the order edit page
 * https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
function nordicspirits_checkout_field_display_admin_order_meta($order){
  echo '<p><strong>CVR:</strong> ' . get_post_meta($order->id, '_billing_cvr', true) . '</p>';
  echo '<p><strong>Faktura e-mailadresse:</strong> ' . get_post_meta($order->id, '_billing_invoiceemail', true) . '</p>';
}
add_action( 'woocommerce_admin_order_data_after_billing_address', 'nordicspirits_checkout_field_display_admin_order_meta', 10, 1 );


function nordicspirits_woocommerce_billing_fields_checkout( $fields ) {    
  $fields['billing_cvr'] = array(
      'label'         => __( 'CVR', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true
  );
  
  $user = wp_get_current_user();
  
  
  $billing_invoiceemail_default = '???';
   
  // If user is set, use that
  if ($user) {
    $user_billing_invoiceemail = get_user_meta( $user->ID, 'billing_invoiceemail', true );
    
    if (strlen(trim($user_billing_invoiceemail)) > 0) {
      $billing_invoiceemail_default = $user_billing_invoiceemail;
    } else {
      $billing_invoiceemail_default = get_user_meta( $user->ID, 'billing_email', true );
    }     
  }
  
  //print_r($fields);
  
  $fields['billing_invoiceemail'] = array(
      'label'         => __( 'Faktura e-mailadresse', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true,
      'default'       => $billing_invoiceemail_default
  );
  
  /*
  $fields['billing_butik'] = array(
      'label'         => __( 'Butik', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true
  );
  */
  
  /*
  Firmanavn/butik: 
  Adresse:
  Cvr:
  (slet butik)
  Kontaktperson:
  Email: + Tlf
  
    [billing_first_name] => Array
    [billing_last_name] => Array
    [billing_company] => Array
    [billing_email] => Array
    [billing_phone] => Array
    [billing_country] => Array
    [billing_address_1] => Array
    [billing_address_2] => Array
    [billing_city] => Array
    [billing_postcode] => Array
    
    [billing_state] => Array
)
  */
  // print_r($fields);
  $fields_new = array(
    'billing_company' => $fields['billing_company'],
    'billing_address_1' => $fields['billing_address_1'],
    'billing_address_2' => $fields['billing_address_2'],
    'billing_postcode' => $fields['billing_postcode'],
    'billing_city' => $fields['billing_city'],
    'billing_country' => $fields['billing_country'],
    'billing_cvr' => $fields['billing_cvr'],
    'billing_first_name' => $fields['billing_first_name'],
    'billing_last_name' => $fields['billing_last_name'],
    'billing_email' => $fields['billing_email'],
    'billing_invoiceemail' => $fields['billing_invoiceemail'],
    'billing_phone' => $fields['billing_phone']
  );
  
  return $fields_new;
}

add_filter('woocommerce_billing_fields', 'nordicspirits_woocommerce_billing_fields_checkout');


// http://stackoverflow.com/questions/12958193/show-custom-field-on-order-in-woocommerce
function nordicspirits_order_complete_details_after_customer_details($order){
  echo '
		<tr>
			<th>CVR:</th>
			<td>' . get_post_meta($order->id, '_billing_cvr', true) . '</td>
		</tr>  
		<tr>
			<th>Faktura e-mailadresse:</th>
			<td>' . get_post_meta($order->id, '_billing_invoiceemail', true) . '</td>
		</tr>  
  ';
}

add_action( 'woocommerce_order_details_after_customer_details', 'nordicspirits_order_complete_details_after_customer_details', 10, 1 );

// https://wordpress.org/support/topic/how-to-remove-email-and-telephone-from-order-confirmation-email
function nordicspirits_email_customer_details($fields, $sent_to_admin, $order ) {
  $fields["billing_cvr"] = array('label' => 'CVR', 'value' => get_post_meta($order->id, '_billing_cvr', true));
  $fields["billing_invoiceemail"] = array('label' => 'Faktura e-mailadresse', 'value' => get_post_meta($order->id, '_billing_invoiceemail', true));
  return $fields;
}
add_filter('woocommerce_email_customer_details_fields', 'nordicspirits_email_customer_details', 40, 3);

