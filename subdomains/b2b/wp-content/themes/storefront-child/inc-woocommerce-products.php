<?php
// Simple products
function nordicspirits_woocommerce_quantity_input_args($args, $product) {
  //print_r($args);
  
  if (!isset($args['input_name']) || $args['input_name'] != 'quantity') {
    return $args;
  }
  
  $default_count = $product->get_attribute('std_count');
  
  if ($default_count === NULL || $default_count == '') {
    return $args;
  }
  
  $default_count = explode(', ', $default_count);
  if (count($default_count) == 0) {
    return $args;
  }

  $default_count = max($default_count);
  if ($default_count <= 0) {
    return $args;
  }
  
  //print_r($default_count);
   
  $args['input_value'] 	= $default_count;	// Starting value
  //$args['max_value'] 		= 80; 	// Maximum value
  //$args['min_value'] 		= 1;   	// Minimum value
  //$args['step'] 		    = 1;    // Quantity steps
  return $args;
}
add_filter( 'woocommerce_quantity_input_args', 'nordicspirits_woocommerce_quantity_input_args', 10, 2 );

/****************/


function nordicspirits_woocommerce_product_description() {
  global $product;
  //wc_get_template( 'single-product/tabs/description.php' );
  the_content();
}
add_action( 'woocommerce_single_product_summary', 'nordicspirits_woocommerce_product_description', 35); // woocommerce_template_single_add_to_cart - 30
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');

function nordicspirits_woocommerce_remove_product_page_skus($enabled) {
  //return false;

  if ( !is_admin() && is_product() ) {
    return false;
  }

  return $enabled;
}
add_filter( 'wc_product_sku_enabled', 'nordicspirits_woocommerce_remove_product_page_skus' );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta');


