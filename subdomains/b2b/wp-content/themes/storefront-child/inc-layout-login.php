<?php

//https://wordpress.org/support/topic/how-do-i-change-wordpress-logo-on-login-page
// custom admin login logo
function custom_login_logo() {
  /*
	echo '<style type="text/css">
		h1 a { background-image: url(./wp-content/themes/' . get_stylesheet() . '/images/logo.png) !important; }
	</style>';
	*/
	echo '<style type="text/css">
		h1 a { background-image: none !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');

//https://wordpress.org/support/topic/remove-registration-link-from-wp-login-page
//    .message.register{display:none}
function hide_login_nav() {
  echo '<style type="text/css">
    #nav{display:none}
    #backtoblog{display:none}
  </style>';
}
add_action( 'login_head', 'hide_login_nav' );

function login_logo_url_title() {
    return 'B2B Nordic Spirits';
}
add_filter('login_headertitle', 'login_logo_url_title' );

/*
function loginfooter() { 
  echo '
    <div style="text-align: center; padding-top: 50px;">
      <table style="margin-left: auto; margin-right: auto;" border="0">
        <tr>
          <td style="padding: 10px; text-align: right; width: 150px">
            <img style="width: 100%" src="./wp-content/themes/' . get_stylesheet() . '/images/herbie-gin-logo-trans.png" />
          </td>
          <td style="padding: 10px; text-align: center; width: 100px">
            <img style="width: 100%" src="./wp-content/themes/' . get_stylesheet() . '/images/logo.png" />
          </td>
          <td style="padding: 10px; text-align: left; width: 150px">
            <img style="width: 100%" src="./wp-content/themes/' . get_stylesheet() . '/images/nordisk.png" />
          </td>
          <td style="padding: 10px; text-align: left; width: 150px">
            <img style="width: 100%" src="./wp-content/themes/' . get_stylesheet() . '/images/clumsebear-logo.png" />
          </td>
        </tr>
      </table>
    </div>
    
    <div style="text-align: center; padding-top: 25px;">
      Ønsker du at forhandle vores produkter, så kontakt os i dag:
      <br />
      Telefon 2615 1514 / post@nordicspirits.dk
    </div>
  ';
}
add_action('login_footer','loginfooter');
*/


function loginfooter() { 
  echo '
    <div style="text-align: center; padding-top: 50px;">
    ';

  $recent = new WP_Query("page_id=518"); 
  while ($recent->have_posts()) {
    $recent->the_post();
    the_content(); 
  }
  
  echo '  </div>';
}
add_action('login_footer','loginfooter');

