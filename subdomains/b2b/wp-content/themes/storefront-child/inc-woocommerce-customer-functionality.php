<?php

/*
 https://wordpress.org/support/topic/display-custom-billing-fields-in-backend-user-profile
 https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 http://rahuljalavadiya.blogspot.com/2015/05/how-to-add-custom-checkout-fields-in.html
 https://support.woothemes.com/hc/en-us/articles/203182373-How-to-add-custom-fields-in-user-registration-on-the-My-Account-page
 */
function nordicspirits_woocommerce_billing_fields_admin($profile_fields) {
  // Remove shipping:
  //unset($profile_fields['shipping']);
  
	// Add new fields
	$profile_fields['billing']['fields']['billing_cvr'] = array( 'label' => 'CVR', 'description' => '' );
	//$profile_fields['billing']['fields']['billing_butik'] = array( 'label' => 'Butik', 'description' => '' );
	
	$profile_fields['billing']['fields']['billing_invoiceemail'] = array( 'label' => 'Faktura e-mailadresse', 'description' => 'Faktura sendes til e-mailadressen' );
	
	$profile_fields['billing']['fields']['billing_ls_regnr'] = array( 'label' => 'Bank reg. nr. (til Leverandørservice)', 'description' => 'Bank reg. nr. (til Leverandørservice)' );
	$profile_fields['billing']['fields']['billing_ls_kontonr'] = array( 'label' => 'Bank kontonr. (til Leverandørservice)', 'description' => 'Bank kontonr. (til Leverandørservice)' );
	
	$profile_fields['billing']['fields']['billing_economic_kundenr'] = array( 'label' => 'E-conomic kundenr.', 'description' => 'E-conomic kundenr.' );
	
	return $profile_fields;
}
add_filter('woocommerce_customer_meta_fields', 'nordicspirits_woocommerce_billing_fields_admin');


/**
 * Display field value on the order edit page
 * https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
/*
function nordicspirits_checkout_field_display_admin_order_meta($order){
  echo '<p><strong>CVR:</strong> ' . get_post_meta($order->id, '_billing_cvr', true) . '</p>';
  echo '<p><strong>Faktura e-mailadresse:</strong> ' . get_post_meta($order->id, '_billing_invoiceemail', true) . '</p>';
  echo '<p><strong>Bank reg. nr. (til Leverandørservice):</strong> ' . get_post_meta($order->id, '_billing_ls_regnr', true) . '</p>';
  echo '<p><strong>Bank kontonr. (til Leverandørservice):</strong> ' . get_post_meta($order->id, '_billing_ls_kontonr', true) . '</p>';
}
add_action( 'woocommerce_admin_order_data_after_billing_address', 'nordicspirits_checkout_field_display_admin_order_meta', 10, 1 );
*/

// Setting custom fields Keys/Labels pairs in admin edit order pages and allow edit this fields correctly.
//https://stackoverflow.com/questions/48410344/display-editable-custom-fields-values-in-woocommerce-edit-order-page
function add_woocommerce_admin_billing_fields($billing_fields) {
  $billing_fields['cvr'] = array( 'label' => 'CVR', 'required' => 'required' );
  $billing_fields['invoiceemail'] = array( 'label' => 'Faktura e-mailadresse' );
  $billing_fields['ls_regnr'] = array( 'label' => 'Bank reg. nr. (til Leverandørservice)' );
  $billing_fields['ls_kontonr'] = array( 'label' => 'Bank kontonr. (til Leverandørservice)' );
  return $billing_fields;
}
add_filter('woocommerce_admin_billing_fields', 'add_woocommerce_admin_billing_fields');

//https://stackoverflow.com/questions/48410344/display-editable-custom-fields-values-in-woocommerce-edit-order-page
/*
function add_woocommerce_found_customer_details($customer_data, $user_id, $type_to_load) {
  if ($type_to_load == 'billing') {
    $customer_data['billing_cvr'] = get_user_meta($user_id, 'billing_cvr', true);
  }
  return $customer_data;
}
add_filter( 'woocommerce_found_customer_details', 'add_woocommerce_found_customer_details', 10, 3);
*/

function nordicspirits_ajax_get_customer_details($data, $customer, $user_id ){
  $data['billing']['cvr'] = get_user_meta($user_id, 'billing_cvr', true);
  $data['billing']['invoiceemail'] = get_user_meta($user_id, 'billing_invoiceemail', true);
  $data['billing']['ls_regnr'] = get_user_meta($user_id, 'billing_ls_regnr', true);
  $data['billing']['ls_kontonr'] = get_user_meta($user_id, 'billing_ls_kontonr', true);
  return $data;
}
add_filter('woocommerce_ajax_get_customer_details', 'nordicspirits_ajax_get_customer_details', 10, 3);

function nordicspirits_woocommerce_billing_fields_checkout( $fields ) {  
  //print_r($fields);
  //die;
  
  $fields['billing_cvr'] = array(
      'label'         => __( 'CVR', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true
  );
  
  $user = wp_get_current_user();
  
  
  $billing_invoiceemail_default = '???';
  $billing_ls_regnr_default = '???';
  $billing_ls_kontonr_default = '???';
   
  // If user is set, use that
  if ($user) {
    $user_billing_invoiceemail = get_user_meta( $user->ID, 'billing_invoiceemail', true );
    
    if (strlen(trim($user_billing_invoiceemail)) > 0) {
      $billing_invoiceemail_default = $user_billing_invoiceemail;
    } else {
      $billing_invoiceemail_default = get_user_meta( $user->ID, 'billing_email', true );
    }
    
    /////////////
    $billing_ls_regnr_default = get_user_meta( $user->ID, 'billing_ls_regnr', true );
    $billing_ls_kontonr_default = get_user_meta( $user->ID, 'billing_ls_kontonr', true );
  }
  
  //print_r($fields);
  
  $fields['billing_invoiceemail'] = array(
      'label'         => __( 'Faktura e-mailadresse', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true,
      'default'       => $billing_invoiceemail_default
  );
  
  /*
  $fields['billing_ls_regnr'] = array(
      'label'         => __( 'Bank reg. nr. (til Leverandørservice)', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true,
      'default'       => $billing_ls_regnr_default
  );
  
  $fields['billing_ls_kontonr'] = array(
      'label'         => __( 'Bank kontonr. (til Leverandørservice)', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true,
      'default'       => $billing_ls_kontonr_default
  );
  */
  
  /*
  $fields['billing_butik'] = array(
      'label'         => __( 'Butik', 'woocommerce' ),
      'required'      => true,
      'class'         => array( 'form-row-wide' ),
      'clear'         => true
  );
  */
  
  /*
  Firmanavn/butik: 
  Adresse:
  Cvr:
  (slet butik)
  Kontaktperson:
  Email: + Tlf
  
    [billing_first_name] => Array
    [billing_last_name] => Array
    [billing_company] => Array
    [billing_email] => Array
    [billing_phone] => Array
    [billing_country] => Array
    [billing_address_1] => Array
    [billing_address_2] => Array
    [billing_city] => Array
    [billing_postcode] => Array
    
    [billing_state] => Array
)
  */
  // print_r($fields);
  $fields_new = array(
    'billing_company' => $fields['billing_company'],
    'billing_address_1' => $fields['billing_address_1'],
    'billing_address_2' => $fields['billing_address_2'],
    'billing_postcode' => $fields['billing_postcode'],
    'billing_city' => $fields['billing_city'],
    'billing_country' => $fields['billing_country'],
    'billing_cvr' => $fields['billing_cvr'],
    'billing_first_name' => $fields['billing_first_name'],
    'billing_last_name' => $fields['billing_last_name'],
    'billing_email' => $fields['billing_email'],
    'billing_invoiceemail' => $fields['billing_invoiceemail'],
    //'billing_ls_regnr' => $fields['billing_ls_regnr'],
    //'billing_ls_kontonr' => $fields['billing_ls_kontonr'],
    'billing_phone' => $fields['billing_phone']
  );
  
  return $fields_new;
}

add_filter('woocommerce_billing_fields', 'nordicspirits_woocommerce_billing_fields_checkout');


// http://stackoverflow.com/questions/12958193/show-custom-field-on-order-in-woocommerce
function nordicspirits_order_complete_details_after_customer_details($order){
  echo '
		<tr>
			<th>CVR:</th>
			<td>' . get_post_meta($order->id, '_billing_cvr', true) . '</td>
		</tr>  
		<tr>
			<th>Faktura e-mailadresse:</th>
			<td>' . get_post_meta($order->id, '_billing_invoiceemail', true) . '</td>
		</tr> 
  ';
  
  /*

		<tr>
			<th>Bank reg. nr. (til Leverandørservice):</th>
			<td>' . get_post_meta($order->id, '_billing_ls_regnr', true) . '</td>
		</tr>    
  */
}

add_action( 'woocommerce_order_details_after_customer_details', 'nordicspirits_order_complete_details_after_customer_details', 10, 1 );

// https://wordpress.org/support/topic/how-to-remove-email-and-telephone-from-order-confirmation-email
function nordicspirits_email_customer_details($fields, $sent_to_admin, $order ) {
  $fields["billing_cvr"] = array('label' => 'CVR', 'value' => get_post_meta($order->id, '_billing_cvr', true));
  $fields["billing_invoiceemail"] = array('label' => 'Faktura e-mailadresse', 'value' => get_post_meta($order->id, '_billing_invoiceemail', true));
  //$fields["billing_ls_regnr"] = array('label' => 'Bank reg. nr. (til Leverandørservice)', 'value' => get_post_meta($order->id, '_billing_ls_regnr', true));
  return $fields;
}
add_filter('woocommerce_email_customer_details_fields', 'nordicspirits_email_customer_details', 40, 3);

