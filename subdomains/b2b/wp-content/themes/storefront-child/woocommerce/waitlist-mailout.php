<?php
/**
 * Waitlist Mailout email
 *
 * @author		Neil Pie
 * @package		WooCommerce_Waitlist/Templates/Emails/Plain
 * @version		1.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

do_action('woocommerce_email_header', $email_heading);
?>

<p>Hej,</p>

<p><?php
echo sprintf( __( '%1$s er nu tilbage på lager hos %2$s.', 'woocommerce-waitlist' ), $product_title, get_bloginfo( 'name' ) ) . " ";
echo __('Du modtager denne e-mail, da denne e-mail-adresse stod til at blive notificeret når dette produkt atter var på lager.', 'woocommerce-waitlist' );
?></p>
 <?php
	do_action('woocommerce_email_footer'); 
?>
