<?php

// https://www.tychesoftwares.com/how-to-add-prefix-or-suffix-to-woocommerce-order-number/

add_filter( 'woocommerce_order_number', 'change_woocommerce_order_number' );

function change_woocommerce_order_number( $order_id ) {
  if ($order_id <= 2118) {
    return $order_id;
  }
  
  return 'NS' . $order_id;
}

