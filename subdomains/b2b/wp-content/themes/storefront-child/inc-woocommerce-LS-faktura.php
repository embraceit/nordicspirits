<?php


//https://businessbloomer.com/disable-payment-gateway-specific-user-role-woocommerce/

/*
function nordicspirits_LS_enable( $available_gateways ) {
  global $woocommerce;
  if ( isset( $available_gateways['cod'] ) && !current_user_can('leverandorservice') ) {
    unset( $available_gateways['cod'] );
  } 
  return $available_gateways;
}
add_filter( 'woocommerce_available_payment_gateways', 'nordicspirits_LS_enable' );
*/


function nordicspirits_fakturabetaling_enable($available_gateways) {
  global $woocommerce;
  if ( isset( $available_gateways['bacs'] ) && !current_user_can('faktura_ok') ) {
    unset( $available_gateways['bacs'] );
  } 
  return $available_gateways;
}
//add_filter( 'woocommerce_available_payment_gateways', 'nordicspirits_fakturabetaling_enable' );


function nordicspirits_LS_description($available_gateways) {
  global $woocommerce;
  if (isset($available_gateways['cod'])) {
    $user = wp_get_current_user();
    
    $billing_ls_regnr = get_user_meta( $user->ID, 'billing_ls_regnr', true );
    $billing_ls_kontonr = get_user_meta( $user->ID, 'billing_ls_kontonr', true );
    
    if (strlen($billing_ls_regnr) != 4 | strlen($billing_ls_kontonr) <= 2) {
      if (strpos($available_gateways['cod']->description, 'generelle regler') === false) {
        $available_gateways['cod']->description = $available_gateways['cod']->description . "
        <br />
        <br />      
        <strong>Vi kan se, at vi endnu ikke har registreret dine oplysninger for at kunne bruge denne betalingsmåde. Det er helt okay. Når du har lagt ordren, skal du venligst fremsende mail til os (<a href=\"mailto:post@nordicspirits.dk\">post@nordicspirits.dk</a>) med dit bank reg. nr. samt kontonr. samt en bekræftelse af at du har læst de <a href=\"http://www.nets.eu/dk-da/Produkter/automatiske-betalinger/Documents/leverandoerservice/LS_Regler_DB_debitor_regler_DK.pdf\">generelle regler</a>. Fx <code>\"Til LeverandørService er mit reg. nr. XXXX og mit kontonr. er YYYYYYY. Jeg har læst og accepterer de generelle regler beskrevet her: http://www.nets.eu/dk-da/Produkter/automatiske-betalinger/Documents/leverandoerservice/LS_Regler_DB_debitor_regler_DK.pdf.\"</code></strong>";      
      }
    }

    /*
    echo "<!---\n";
    echo "reg: " . $billing_ls_regnr . "\n";
    echo "konto: " . $billing_ls_kontonr . "\n";
    //print_r($available_gateways['cod']['description'])
    print_r($available_gateways['cod']->description);
    echo "--->\n";
    */
  } 
  return $available_gateways;
}
//add_filter( 'woocommerce_available_payment_gateways', 'nordicspirits_LS_description' );


