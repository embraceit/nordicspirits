<?php
//https://docs.woocommerce.com/document/hide-other-shipping-methods-when-free-shipping-is-available/
//https://businessbloomer.com/woocommerce-hide-shipping-options-free-shipping-available/

function nordicspirits_unset_shipping_when_free_is_available_in_zone( $rates, $package ) {
  $all_free_rates = array();

  foreach ( $rates as $rate_id => $rate ) {
    if ( 'free_shipping' === $rate->method_id ) {
      $all_free_rates[ $rate_id ] = $rate;
      break;
    }
  }

  if ( empty( $all_free_rates )) {
    return $rates;
  } else {
    return $all_free_rates;
  } 
}
add_filter( 'woocommerce_package_rates', 'nordicspirits_unset_shipping_when_free_is_available_in_zone', 10, 2 );
 
