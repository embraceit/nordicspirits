<?php
function nordic_spirits_tonicmixer($cart) { 
  $user = wp_get_current_user();
  
  $product_ids = array(
    
    // Beers:
    3797,
    3796,
    3795,
    3794,
    /* Old:
    3600,  // Young Lust - Belgian blond (Icon series)
    3599,  // Into The Black - Black IPA (Icon series)
    3598,  // Stockholm Syndrome DIPA (Icon series)
    3597,  // Kissmeyer Icons: Strange Fruit Tripel
    */
    
    2249,  // Oh Deer Tonic (24 stk)
    2339,  // Topaz æblemost (6 stk)
    3246,  // Oh Deer Øko Tonic – Rabarber (24 stk)
    3388,  // Brick 50cl
    1179   // New Black 5cl
  );
  
  
  // excl tax
  $total = WC()->cart->cart_contents_total;
  //$total = WC()->cart->get_total_ex_tax();
  
  // excl tax
  if ($total >= 2000 && $total < 4000) {
    $contains_tonicmixer = false;
    
    foreach(WC()->cart->get_cart() as $cart_item_key => $values) {
		  $_product = $values['data'];
	
		  if (in_array($_product->id, $product_ids)) {
			  $contains_tonicmixer = true;
			  break;			  
		  }
	  }
	  
	  //if ($contains_tonicmixer && $user->ID == 1) {
	  if ($contains_tonicmixer) {
      WC()->cart->add_fee('Fragtgebyr på flasker med lav stykpris ved ordrer under 4.000 kr.', 150, TRUE);    
    } 
    
	  /*
    // mikl
    if ($user->ID == 1) {
      WC()->cart->add_fee('Tonic-gebyr', 150, TRUE);    
    } 
    */   
  }
}; 
         
// add the action 
add_action('woocommerce_cart_calculate_fees', 'nordic_spirits_tonicmixer'); 


