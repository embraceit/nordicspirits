<?php

function nordicspirits_last_login($user_login, $user){
  $DK_tz = new DateTimeZone('Europe/Copenhagen');
  $dt = new DateTime('now', $DK_tz);

  update_user_meta($user->ID, '_nordicspirits_last_login', $dt->getTimestamp());
  
  $logins = 1;
  $prev_logins = get_user_meta($user->ID, '_nordicspirits_logins_total', true);  
  if ($prev_logins != '') {
    $logins += (int)$prev_logins;
  }  
  update_user_meta($user->ID, '_nordicspirits_logins_total', $logins);
  
  if (isset($_SESSION) && isset($_SESSION['tokenlogin_loggedintoken']) && $_SESSION['tokenlogin_loggedintoken'] == 'yes') {
    $prev_tokenlogins = get_user_meta($user->ID, '_nordicspirits_tokenlogins_total', true);  

    $tokenlogins = 1;
    
    if ($prev_tokenlogins != '') {
      $tokenlogins += (int)$prev_tokenlogins;
    }  
    update_user_meta($user->ID, '_nordicspirits_tokenlogins_total', $tokenlogins);
  }  
}

add_action('wp_login', 'nordicspirits_last_login', 10, 2 );


function nordicspirits_logininfo_column($columns) {
  $columns['nordicspirits_logininfo_lastlogin'] = 'Seneste login';
  $columns['nordicspirits_logininfo_loginstotal'] = 'Logins';
  $columns['nordicspirits_logininfo_tokenloginstotal'] = 'Tokenlogins';
  return $columns;
}
add_filter('manage_users_columns', 'nordicspirits_logininfo_column');


function nordicspirits_logininfo_column_show_content($value, $column_name, $user_id) {
	if ($column_name == 'nordicspirits_logininfo_lastlogin') {
		$lastlogin = get_user_meta($user_id, '_nordicspirits_last_login', true);
		if ($lastlogin != '') {
		  $DK_tz = new DateTimeZone('Europe/Copenhagen');
      $dt = new DateTime('now', $DK_tz);
      $dt->setTimestamp($lastlogin);
      return $dt->format(get_option('date_format') . ' H:i:s');
		  //return date(get_option('date_format') . ' H:i:s', $lastlogin);
		}		
		return '[NA]';
  }

	else if ($column_name == 'nordicspirits_logininfo_loginstotal') {
		$loginstotal = get_user_meta($user_id, '_nordicspirits_logins_total', true);
		if ($loginstotal != '') {
      return $loginstotal;
		}		
		return 0;
  }

	else if ($column_name == 'nordicspirits_logininfo_tokenloginstotal') {
		$tokenloginstotal = get_user_meta($user_id, '_nordicspirits_tokenlogins_total', true);
		if ($tokenloginstotal != '') {
      return $tokenloginstotal;
		}		
		return 0;
  }
    
  return $value;
}
add_action('manage_users_custom_column', 'nordicspirits_logininfo_column_show_content', 11, 3);



//make the new column sortable
function user_sortable_columns( $columns ) {
    $columns['nordicspirits_logininfo_lastlogin'] = 'nordicspirits_logininfo_lastlogin';
    return $columns;
}
add_filter( 'manage_users_sortable_columns', 'user_sortable_columns' );
 
function logininfo_lastlogin_user_query($userquery){ 
    if ('nordicspirits_logininfo_lastlogin' == $userquery->query_vars['orderby']) {
        global $wpdb;
        $userquery->query_from .= " LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) ";//note use of alias
        $userquery->query_where .= " AND alias.meta_key = '_nordicspirits_last_login' ";//which meta are we sorting with?
        $userquery->query_orderby = " ORDER BY alias.meta_value ".($userquery->query_vars["order"] == "ASC" ? "asc " : "desc ");//set sort order
    }
}
add_action('pre_user_query', 'logininfo_lastlogin_user_query');

