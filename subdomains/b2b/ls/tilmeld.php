<?php 
require('../wp-load.php');

if (!is_user_logged_in()) {
  // not admin
  auth_redirect();
  die; 
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="da-DK">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Nordic Spirits - B2B - Tilmeld til betalingsservice</title>	
	<link rel='stylesheet' id='buttons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/buttons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='dashicons-css'  href='http://b2b.nordicspirits.dk/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
  <link rel='stylesheet' id='login-css'  href='http://b2b.nordicspirits.dk/wp-admin/css/login.min.css?ver=4.4.2' type='text/css' media='all' />
  <script type='text/javascript' src='http://b2b.nordicspirits.dk/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
  <style type="text/css">
    body {
      padding: 10em;
    }
  </style>
	</head>
<body class="wp-core-ui locale-da-dk">
<?php 
$recent = new WP_Query("page_id=2849"); 
while ($recent->have_posts()) {
  $recent->the_post();
  the_content(); 
}

//if (current_user_can('leverandorservice')) {
if (false) {
  echo "
    <p style=\"font-weight: bold; color: red;\">Du er allerede tilmeldt Leverandørservice. Venligst kontakt os for at få ændret oplysninger som fx kontonummer.</p>
  ";  
} else {
  $form_posted = FALSE;
  
  print_r($_POST);
  //die;
  
  if (isset($_POST) && isset($_POST['kundeTxt']) && isset($_POST['regTxt']) && isset($_POST['kontoTxt'])) {
    //$form_posted = TRUE;
  }
  
  if ($form_posted) {

    print_r($_POST);
    $url = 'http://itnapps.eu/LS_tilmeld.aspx?cvr=37767379&amp;krnr=49387';
    //http://itnapps.eu/LS_tilmeld.aspx?cvr=37767379&krnr=49387&regTxt=1234
    
    //kundeTxt
    //regTxt
    //kontoTxt
    //cvrTxt
    //emailTxt
    //kreditNrTxt = 49387
    //acceptChk
    
    $_POST['kundeTxt'] = preg_replace('/[^0-9]/', '', $_POST['kundeTxt']);
    $_POST['regTxt'] = preg_replace('/[^0-9]/', '', $_POST['regTxt']);
    $_POST['kontoTxt'] = preg_replace('/[^0-9]/', '', $_POST['kontoTxt']);
    $_POST['cvrTxt'] = preg_replace('/[^0-9]/', '', $_POST['cvrTxt']);
    $_POST['emailTxt'] = preg_replace('/[^0-9A-Za-z@._-]/', '', $_POST['emailTxt']);
    $_POST['kreditNrTxt'] = '49387';
    //$_POST['acceptChk'] = '';

    print_r($_POST);
    //die;
  } else {
    $current_user = wp_get_current_user();
    //<form name="form1" method="post" action="http://itnapps.eu/LS_tilmeld.aspx?cvr=37767379&amp;krnr=49387" onsubmit="javascript:return WebForm_OnSubmit();" id="form1">
?>
<form name="form1" method="post" action="./tilmeld.php" id="form1">
    <table class="style1">
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <span id="Label2">Kunde nr. (webshop)</span>
            </td>
            <td>
                <input name="kundeTxt" type="text" id="kundeTxt" readonly="readonly" value="<?php echo $current_user->ID; ?>" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <span id="Label3">Reg. nr.</span>
            </td>
            <td>
                <input name="regTxt" type="text" id="regTxt" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <span id="Label4">Kontonr.</span>
            </td>
            <td>
                <input name="kontoTxt" type="text" id="kontoTxt" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <span id="Label5">CVR-nr.</span>
            </td>
            <td>
                <input name="cvrTxt" type="text" maxlength="8" id="cvrTxt" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <span id="Label8">E-mail.</span>
            </td>
            <td>
                <input name="emailTxt" type="text" id="emailTxt" readonly="readonly" value="<?php echo $current_user->user_email; ?>" />
            &nbsp;</td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;
                <input id="acceptChk" type="checkbox" name="acceptChk" /><label for="acceptChk">Jeg har læst og accepteret "generelle regler".</label>
            </td>
            <td><a id="HyperLink1" href="http://www.nets.eu/dk-da/Produkter/automatiske-betalinger/Documents/leverandoerservice/LS_Regler_DB_debitor_regler_DK.pdf" target="_blank">Generelle regler</a></td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td>
                <input type="submit" name="sendBt" value="Send tilmelding" id="sendBt" style="padding: 0.5em;" disabled="disabled" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td colspan="2">
                
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                &nbsp;</td>
            <td colspan="2">
                <a id="HyperLink1" href="http://www.nets.eu/dk-da/Produkter/automatiske-betalinger/Documents/leverandoerservice/LS_Regler_DB_debitor_regler_DK.pdf" target="_blank">Generelle regler</a>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</form>
<?php
  } // form posted
} // already has role
?>

<script type='text/javascript'>
  jQuery(document).ready(function ($) {
    $('#acceptChk').change(function () {
        $('#sendBt').prop("disabled", !this.checked);
    });
  });
</script>

<p>
Nordic Spirits IVS<br />
CVR: DK37767379 / Tlf. +45 2310 9505<br />
<a href="mailto:post@nordicspirits.dk">post@nordicspirits.dk</a>
</p>
<?php
 // Footer
 echo '
    <div style="text-align: left;">
    ';

  $recent = new WP_Query("page_id=518"); 
  while ($recent->have_posts()) {
    $recent->the_post();
    the_content(); 
  }
  
  echo '  </div>    
  ';
?>
</body>
</html>

